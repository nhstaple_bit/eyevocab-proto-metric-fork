"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StateSet = exports.StateGet = void 0;
var mithril_1 = __importDefault(require("mithril"));
var stream_1 = __importDefault(require("mithril/stream"));
var mergerino_1 = __importDefault(require("mergerino"));
var client_state_1 = require("./client-state");
// for convenience in the actions
var Set = stream_1.default();
/**
 * The StateGet function is exported as default
 */
exports.StateGet = stream_1.default.scan(mergerino_1.default, client_state_1.ClientState, Set);
exports.StateSet = function (state) {
    Set(state);
    mithril_1.default.redraw();
};

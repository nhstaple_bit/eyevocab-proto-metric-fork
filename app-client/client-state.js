"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientState = void 0;
var model_1 = require("./model");
exports.ClientState = {
    component: "Dash",
    display: "Dash",
    dashDisplay: "Create",
    Deck: {
        collectionOne: new model_1.CollectionClient({ id: '_new' })
    },
    Dash: {
        languageOne: {
            id: ''
        },
        collectionList: [],
        collectionOne: new model_1.CollectionClient({ id: '_new' }),
        setCreateOptions: [
            {
                id: "manual",
                name: "Manual Set",
                description: "Uses the sets in the order created. Automatically chooses the next not-yet-practiced set. Units are still randomized per set."
            },
            {
                id: "random",
                name: "Random Set",
                description: "Creates a set at random from the entire collection. Will not repeat words."
            },
            {
                id: "smart_incrementing",
                name: "Smart Set Even Distribution",
                description: "Automatically generates sets of the most challenging words in a language."
            },
            {
                id: "smart_random",
                name: "Smart Set Random Distribution",
                description: "Automatically generates sets of words at random that are evenly challenging."
            }
        ],
        sessionNextOptions: [
            {
                id: "incremental",
                name: "Next Set",
                description: "Advances the set in the order of the collection item list by automatically choosing the next not-yet-practiced set. Units are still randomized per set."
            },
            {
                id: "smart_collection",
                name: "Smart Collection Set",
                description: "Will attempt to choose a set of language items (words) that are most challenging to the user based on evaluation of the user's past collection sessions. Only adds words that have already been practiced"
            },
            {
                id: "smart_language_descending",
                name: "Smart Set Hardest",
                description: "Will generate a set of most challenging language items (words) from a collection based on evaluation of a entire language's metrics. Evaluates per user for least practiced language items."
            },
            { id: "smart_language_ascending",
                name: "Smart Set Easiest",
                description: "Will generate a set of least challenging language items (words) from a collection based on evaluation of a entire language's metrics. Evaluates per user for least practiced language items."
            }
        ],
        sessionIntervalOptions: [
            {
                id: "after_first",
                name: "After First",
                description: "Reminder is sent at an interval relative to the timestamp of the first practice session. This makes for a more uniform collection reminder schedule."
            },
            {
                id: "after_last",
                name: "After Last (recommended)",
                description: "Reminder is sent at an interval relative to the timestamp of the last practice session. This makes for a more forgiving reminder schedule."
            }
        ],
        sessionModeOptions: [
            {
                id: "test",
                name: "Test",
                description: "Test mode is hard."
            },
            {
                id: "practice",
                name: "Practice",
                description: "Practice Mode is very fogiving with hints and etc."
            },
            {
                id: "review",
                name: "Review",
                description: "A simple look-over session."
            }
        ],
        sessionSetOptions: [
            {
                id: "smart",
                name: "Smart Set",
                description: "Creates a set at random from the entire collection. Will not repeat words."
            },
            {
                id: "random",
                name: "Random Set",
                description: "Creates a set at random from the entire collection. Will not repeat words."
            },
            {
                id: "manual",
                name: "Manual Set",
                description: "Uses the sets in the order created. Automatically chooses the next not-yet-practiced set. Units are still randomized per set."
            }
        ],
    }
};

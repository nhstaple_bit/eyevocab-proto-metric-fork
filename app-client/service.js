"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Service = void 0;
// manages requests, data, and state
var state_1 = require("./state");
// this is probably better off refactored into small modular functions
var Service = /** @class */ (function () {
    function Service() {
    }
    // posts collection state changes to the server
    Service.CollectionPost = function () { };
    // validates changes to a collection settings
    Service.CollectionValid = function () { };
    // validates changes to a collection session settings
    Service.CollectionSessionValid = function () { };
    // add a reminder
    Service.CollectionSessionStateAdd = function () {
        state_1.StateGet().Dash.collectionOne.setRemindList.unshift({});
    };
    Service.CollectionSessionStateSet = function (i, sessionState) {
    };
    Service.CollectionSessionStateRemove = function (i) {
    };
    Service.CollectionCreate = function () {
        var _a = state_1.StateGet().Dash, languageOne = _a.languageOne, collectionOne = _a.collectionOne, collectionList = _a.collectionList;
        if (collectionOne.id === '_new') {
            collectionOne.id = '_' + Math.floor((Math.random() * 10000));
            collectionOne.idLanguage = languageOne.id;
        }
        collectionList.unshift(collectionOne);
        state_1.StateSet({ Dash: { collectionOne: collectionOne, collectionList: collectionList } });
        return collectionOne;
    };
    return Service;
}());
exports.Service = Service;

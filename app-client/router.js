"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Router = exports.Routes = void 0;
var mithril_1 = __importDefault(require("mithril"));
var keyboard_1 = require("./keyboard");
var state_1 = require("./state");
// an empty component for handling route changes
var RouterComponent = {
    oncreate: function () { return exports.Router(mithril_1.default.route.param()); },
    // don't think we need to check the route each view render
    //view:()=>Router(m.route.param())
    view: function () { }
};
// mithril handles these route paths
exports.Routes = {
    "/": RouterComponent,
    "/:lang": RouterComponent,
    "/:lang/:idCollection": RouterComponent,
    "/:lang/:idCollection/:idSession": RouterComponent
};
// initiate the Lang var with a default value 
// which is whatever is the first language in our list
var Lang, IdCollection, IdSession;
/**
 * Manages route changes.
 */
exports.Router = function (_a) {
    var lang = _a.lang, idCollection = _a.idCollection, idSession = _a.idSession;
    // if we are changing languages
    if (Lang !== lang) {
        // reset the route var
        Lang = lang;
        if (!lang) {
            state_1.StateSet({ display: 'Dash' });
            return;
        }
        else 
        // if the lang code is NOT in the list of Languages
        if (!keyboard_1.LangList.includes(lang)) {
            // Send them back to start
            window.location.href = "/app/#!/";
            return;
        }
        // change the window location so the url is right.
        window.location.href = "/app/#!/" + lang;
        // get list of collections from the api
        // set app state
        state_1.StateSet({ display: 'Language', Dash: {
                languageOne: {
                    id: lang
                }
            } });
    }
    // if we are changing collections
    if (idCollection === '_new') {
        state_1.StateSet({ display: 'CollectionCreate' });
    }
    else if (idCollection !== IdCollection) {
        IdCollection = idCollection;
        console.log(idCollection);
        var collectionOne = state_1.StateGet().Dash.collectionList.find(function (a) { return a.id === idCollection; });
        if (!collectionOne) {
            // we gotta go get it from server
        }
        state_1.StateSet({ display: 'Collection', Dash: { collectionOne: collectionOne } });
        // get one collection from api
    }
};
/**
 * When the form input changes, handle input, event and route the change.
 */
exports.Router.fromInput = function (e) {
    exports.Router(e.target.value);
    e.target.blur();
};
/**
 * When the window location hash changes, handle the url and route the change.
 */
exports.Router.fromUrl = function (url) {
    console.log(url);
    var path = url.split('#!/').pop().split('/');
    exports.Router({
        lang: path[0],
        idCollection: path[1],
        idSession: path[2]
    });
};

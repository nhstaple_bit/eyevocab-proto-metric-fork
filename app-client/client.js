"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mithril_1 = __importDefault(require("mithril"));
var app_1 = require("./app");
var router_1 = require("./router");
var state_1 = require("./state");
// when the url hash chagnes we fire the router's fromUrl static method
window.onhashchange = function (e) { return router_1.Router.fromUrl(e.newURL); };
// set our routes
mithril_1.default.route(document.getElementById("m-route"), "/", router_1.Routes);
// mount our app
mithril_1.default.mount(document.getElementById("m-app"), {
    view: function () { return mithril_1.default(app_1.App, state_1.StateGet()); }
});

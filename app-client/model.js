"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CollectionClient = void 0;
var CollectionClient = /** @class */ (function () {
    function CollectionClient(collection) {
        this.setRemindList = [];
        Object.assign(this, collection);
    }
    return CollectionClient;
}());
exports.CollectionClient = CollectionClient;

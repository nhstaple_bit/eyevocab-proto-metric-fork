import m from "mithril"
export const Deck = {
    view:function({attrs:state}){
        return (<div class="component-deck">
            <h2>Deck!</h2>
            <ul>
                <li>Nav/Position</li>
                <li>Card</li>
                <ul>
                    <li>image</li>
                    <li>audio</li>
                    <li>text</li>
                </ul>
                <li>Meta Card</li>
                <ul>
                    <li>image name and description</li>
                    <li>image credit/source</li>
                    <li>etc meta-data</li>
                </ul>
                <li>input/prompt</li>
                <li>keyboard</li>
                <li>control menu</li>
            </ul>
        </div>)
    }
}
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Deck = void 0;
var mithril_1 = __importDefault(require("mithril"));
exports.Deck = {
    view: function (_a) {
        var state = _a.attrs;
        return (mithril_1.default("div", { class: "component-deck" },
            mithril_1.default("h2", null, "Deck!"),
            mithril_1.default("ul", null,
                mithril_1.default("li", null, "Nav/Position"),
                mithril_1.default("li", null, "Card"),
                mithril_1.default("ul", null,
                    mithril_1.default("li", null, "image"),
                    mithril_1.default("li", null, "audio"),
                    mithril_1.default("li", null, "text")),
                mithril_1.default("li", null, "Meta Card"),
                mithril_1.default("ul", null,
                    mithril_1.default("li", null, "image name and description"),
                    mithril_1.default("li", null, "image credit/source"),
                    mithril_1.default("li", null, "etc meta-data")),
                mithril_1.default("li", null, "input/prompt"),
                mithril_1.default("li", null, "keyboard"),
                mithril_1.default("li", null, "control menu"))));
    }
};

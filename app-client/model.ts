import {
    Collection,
    SessionSetOptions,
    SetCreateOptions,
    SessionIntervalOptions,
    SessionNextOptions,
    SessionModeOptions
} from "../model"

export type ComponentType = "Deck" | "Dash"

export type DeckDisplayType = "Card" | "Meta"

export type DashDisplayType = "Dash" | "Language" | "Collection" | "CollectionCreate" | "CollectionEdit" | 'CollectionEditSession'

export type ComponentDashDisplayType = "Create" | "Edit" | "Item" | "Invite" | "Access"

export interface StateModel {
    component:ComponentType,
    display:DeckDisplayType | DashDisplayType,
    dashDisplay:ComponentDashDisplayType,
    Deck:Deck
    Dash:Partial<Dash>
}

export class CollectionClient implements Partial<Collection> {
    id
    name
    description
    idLanguage
    setRemindList = []
    constructor(collection:Partial<Collection>){
        Object.assign(this,collection)
    }
}

export interface Dash {
    // the active collection in the dash
    collectionOne:CollectionClient
    languageOne:{
        id:string
    }
    collectionList:CollectionClient[]
    setCreateOptions:ValueOf<{
        [key in SetCreateOptions]:{
            id:key
            name:string
            description:string
        }
    }>[]
    sessionSetOptions:ValueOf<{
        [key in SessionSetOptions]:{
            id:key
            name:string
            description:string
        }
    }>[]
    sessionIntervalOptions:ValueOf<{
        [key in SessionIntervalOptions]:{
            id:key
            name:string
            description:string
        }
    }>[]
    
    sessionModeOptions:ValueOf<{
        [key in SessionModeOptions]:{
            id:key
            name:string
            description:string
        }
    }>[]
    sessionNextOptions:ValueOf<{
        [key in SessionNextOptions]:{
            id:key
            name:string
            description:string
        }
    }>[]
}

type ValueOf<T> = T[keyof T];

export interface Deck {
    collectionOne:CollectionClient
}

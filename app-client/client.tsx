import m from "mithril"
import {App} from "./app"
import {Router,Routes} from "./router"
import {StateGet} from "./state"

// when the url hash chagnes we fire the router's fromUrl static method
window.onhashchange = e=>Router.fromUrl(e.newURL);

// set our routes
m.route(document.getElementById("m-route"), "/", Routes)

// mount our app
m.mount(document.getElementById("m-app"),{
    view: () => m(App, StateGet())
  })

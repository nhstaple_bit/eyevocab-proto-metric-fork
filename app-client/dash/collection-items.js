"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CollectionEditItems = void 0;
var mithril_1 = __importDefault(require("mithril"));
exports.CollectionEditItems = {
    view: function () {
        return (mithril_1.default("div", { class: "collection-items" },
            mithril_1.default("h2", null, "Edit Items")));
    }
};

import m from "mithril"
import { CollectionEditDisplay,CollectionSessionDisplay } from "../action"

export const Collection = {
    view:function({attrs:state}){
        return (<div class="dash-collection">
            <h2>Collection Name {state.collectionOne.name}</h2>
            <ul>
                <li>Name  &amp; Description</li>
                <li><a href="" onclick={CollectionEditDisplay}>Name  &amp; Description</a></li>
                <li><a href="" onclick={CollectionSessionDisplay}>Spacing  &amp; Notifications</a></li>
                <li>Language Item edit List</li>
                <li>Invite &amp; Access</li>
            </ul>

        </div>)
    }
} 
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CollectionEdit = void 0;
var mithril_1 = __importDefault(require("mithril"));
exports.CollectionEdit = {
    view: function (_a) {
        var state = _a.attrs;
        return (mithril_1.default("div", { class: "collection-edit" },
            mithril_1.default("h2", null,
                "Edit Collection:",
                state.collectionOne.name),
            mithril_1.default("ul", null,
                mithril_1.default("li", null, "Name  & Description"),
                mithril_1.default("li", null, "Spacing  & Notifications (display, but no edit"),
                mithril_1.default("li", null, "Language Item edit List"),
                mithril_1.default("li", null, "Invite & Access"))));
    }
};

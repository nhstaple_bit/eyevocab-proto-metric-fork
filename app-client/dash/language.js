"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Language = void 0;
var mithril_1 = __importDefault(require("mithril"));
exports.Language = {
    view: function (_a) {
        var state = _a.attrs;
        console.log(state.collectionList);
        return (mithril_1.default("div", { class: "dash-language" },
            mithril_1.default("ul", null,
                mithril_1.default("li", null,
                    mithril_1.default("a", { href: "/app/#!/" + state.languageOne.id + "/_new" },
                        "Create ",
                        state.languageOne.id,
                        " Collection"))),
            "Collection List",
            mithril_1.default("ul", null, state.collectionList.map(function (one) {
                return mithril_1.default("li", null,
                    mithril_1.default("a", { href: "/app/#!/" + state.languageOne.id + "/" + one.id },
                        "Edit ",
                        one.name));
            }))));
    }
};

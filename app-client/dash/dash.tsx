import m from "mithril"
import {LangList} from "../keyboard"
export const Dash = {

    view:function({attrs:state}){
        
        return (<div class="component-dash">
            <h2>Dash!</h2>
            <ul>
                <li>Languages List</li>
                <ul>
                    {
                        LangList.map(lang=>
                            (<li><a href={`/app/#!/${lang}`}>{lang}</a></li>)
                        )
                    } 
                </ul>
                <li>Collections</li>
                <ul>
                    <li>Create New Collection -- Collection Create</li>
                    <li>Search Collections</li>
                    <li>Collection List Result</li>
                    <ul>
                        <li>Launch Practice/Test Session -- Deck</li>
                        <li>Edit -- Collection Edit</li>
                    </ul>
                </ul>
                <li>Collection Create</li>
                <ul>
                    <li>Name</li>
                    <li>Description</li>
                </ul>
                <li>Collection Edit</li>
                <ul>
                    <li>Name</li>
                    <li>Description</li>
                    <li>Session Spacing</li>
                    <li>Word list add/delete/order</li>
                </ul>
                <li>Collection Share</li>
                <ul>
                    <li>Invite: Invite existing users or by email/phone</li>
                    <li>Access: Edit access controls</li>
                </ul>
            </ul>

        </div>)
    }
}
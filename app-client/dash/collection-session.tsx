import m from "mithril"
import {Dash } from "../model"
import { CollectionSessionPost, CollectionSessionStateAdd, CollectionSessionStateRemove } from "../action"

export const CollectionEditSession = {
    view:function({attrs:state}){
        const {
            collectionOne,
            sessionSetOptions,
            sessionModeOptions,
            sessionNextOptions,
            sessionIntervalOptions
        } = state as Dash
        
        return (<form onsubmit={CollectionSessionPost} class="collection-spacing">
            <h2>Spacing &amp; Notifications</h2>
            <fieldset>

                <legend>Sessions and Reminders: Determine how sessions and reminders behave.</legend>

                {

                    collectionOne.setRemindList.map((one, i) => (

                        <div>
                            <h2>Reminder #{i + 1} <button onclick={CollectionSessionStateRemove(i)}>Remove Reminder</button></h2>
                            <div class="form-item">
                                <label>Session Type</label>
                                <select
                                    value={one.sessionType}
                                    oninput={e => one.sessionType = e.target.value} >
                                    {
                                        sessionSetOptions.map(a => (
                                            <option value={a.id}>{a.name}</option>
                                        ))
                                    }

                                </select>
                                <ol>
                                    <strong>How do sessions behave by default? (Different than set behavior above: a session is a user engaging with a set.)</strong>
                                    {
                                        sessionSetOptions.map(a => (
                                            <li>
                                                <strong>{a.name}</strong> <span>{a.description}</span>
                                            </li>
                                        ))
                                    }
                                </ol>
                            </div>

                            <div class="form-item">
                                <label>Reminder Mode</label>
                                <select
                                    value={one.type}
                                    oninput={e => one.type = e.target.value} >
                                    {
                                        sessionModeOptions.map((a, j) => (
                                            <option value={a.id}>
                                                {j + 1} {' '} {a.name}
                                            </option>
                                        ))
                                    }
                                </select>
                                <ol>
                                    <strong>Remind Mode allows for test or practice reminder.</strong>
                                    {
                                        sessionModeOptions.map(a => (
                                            <li>
                                                <strong>{a.name}</strong> <span>{a.description}</span>
                                            </li>
                                        ))
                                    }
                                </ol>
                            </div>
                            <div class="form-item">
                                <label>Reminder Type</label>
                                <select
                                    value={one.type}
                                    oninput={e =>  one.type = e.target.value} >
                                    {
                                        sessionNextOptions.map((a, j) => (
                                            <option value={a.id}>
                                                {j + 1} {' '} {a.name}
                                            </option>
                                        ))
                                    }
                                </select>
                                <ol>
                                    <strong>Remind Type allows for reminders to behave differently per collection.</strong>
                                    {
                                        sessionNextOptions.map(a => (
                                            <li>
                                                <strong>{a.name}</strong> <span>{a.description}</span>
                                            </li>
                                        ))
                                    }
                                </ol>
                            </div>
                            <div class="form-item">
                                <label>Practice Delay</label>
                                <input
                                    value={one.intervalHour}
                                    oninput={e => one.intervalHour = e.target.value}
                                />
                                <ul>
                                    <strong>Remind to practice this many hour(s) after {state.collectionOne.setRemindList[i].type === 'after_last' ? 'last' : 'first'} session.</strong>
                                    {
                                        sessionIntervalOptions.map(one => (
                                            <li>
                                                <strong>{one.name}</strong> <span>{one.description}</span>
                                            </li>
                                        ))
                                    }
                                </ul>
                            </div>
                            <div class="form-item">
                                <label>Test Delay</label>
                                <input
                                    value={one.testIntervalHour}
                                    oninput={e => one.testIntervalHour = e.target.value }
                                />
                                <ul>
                                    <li>Remind to test this many hour(s) after {state.collectionOne.setRemindList[i].type === 'after_last' ? 'last' : 'first'} session.</li>
                                </ul>
                            </div>
                            <div class="form-item">
                                <label>Repeat Count</label>
                                <input
                                    value={one.repeatCount}
                                    oninput={e =>one.repeatCount =  e.target.value }
                                />
                                <ul>
                                    <li>How many practices before the reminders stop. Zero for infinite.</li>
                                </ul>
                            </div>
                            <div class="form-item">
                                <label>% Terminal Threshold</label>
                                <input
                                    value={one.terminalPercent}
                                    oninput={e => one.terminalPercent =  e.target.value }
                                />
                                <ul>
                                    <li>
                                        What score (percent) for all words in a set above which reminders cease.</li>
                                </ul>
                            </div>
                            <hr />
                        </div>
                    ))
                }



                <div class="form-item">

                    <button onclick={CollectionSessionStateAdd}>Add Reminder</button>

                    <ul>
                        <li>
                            <strong>Add Reminder</strong> <span>Add another reminder.</span>
                        </li>
                    </ul>
                </div>
                <hr />
                <div class="form-item">

                    <input type="submit" value="Save Set Options" />

                    <ul>
                        <li>
                            <strong>Set Reminders</strong> <span>Only saves set reminders.</span>
                        </li>
                    </ul>
                </div>
            </fieldset>
        </form>)
    }
}

import m from "mithril"
export const CollectionEdit = {
    view:function({attrs:state}){
        return (<div class="collection-edit">
            <h2>Edit Collection:{state.collectionOne.name}</h2>
            
            <ul>
                <li>Name  &amp; Description</li>
                <li>Spacing  &amp; Notifications (display, but no edit</li>
                <li>Language Item edit List</li>
                <li>Invite &amp; Access</li>
            </ul>

        </div>)
    }
}
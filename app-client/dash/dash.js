"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Dash = void 0;
var mithril_1 = __importDefault(require("mithril"));
var keyboard_1 = require("../keyboard");
exports.Dash = {
    view: function (_a) {
        var state = _a.attrs;
        return (mithril_1.default("div", { class: "component-dash" },
            mithril_1.default("h2", null, "Dash!"),
            mithril_1.default("ul", null,
                mithril_1.default("li", null, "Languages List"),
                mithril_1.default("ul", null, keyboard_1.LangList.map(function (lang) {
                    return (mithril_1.default("li", null,
                        mithril_1.default("a", { href: "/app/#!/" + lang }, lang)));
                })),
                mithril_1.default("li", null, "Collections"),
                mithril_1.default("ul", null,
                    mithril_1.default("li", null, "Create New Collection -- Collection Create"),
                    mithril_1.default("li", null, "Search Collections"),
                    mithril_1.default("li", null, "Collection List Result"),
                    mithril_1.default("ul", null,
                        mithril_1.default("li", null, "Launch Practice/Test Session -- Deck"),
                        mithril_1.default("li", null, "Edit -- Collection Edit"))),
                mithril_1.default("li", null, "Collection Create"),
                mithril_1.default("ul", null,
                    mithril_1.default("li", null, "Name"),
                    mithril_1.default("li", null, "Description")),
                mithril_1.default("li", null, "Collection Edit"),
                mithril_1.default("ul", null,
                    mithril_1.default("li", null, "Name"),
                    mithril_1.default("li", null, "Description"),
                    mithril_1.default("li", null, "Session Spacing"),
                    mithril_1.default("li", null, "Word list add/delete/order")),
                mithril_1.default("li", null, "Collection Share"),
                mithril_1.default("ul", null,
                    mithril_1.default("li", null, "Invite: Invite existing users or by email/phone"),
                    mithril_1.default("li", null, "Access: Edit access controls")))));
    }
};

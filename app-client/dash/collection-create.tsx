import m from "mithril"
import {CollectionCreateAction} from "../action"
export const CollectionCreate = {
    view:function({attrs:state}){
        return (<form  
            
            onsubmit={CollectionCreateAction}
            class="collection-create">
            <h2>Create {state.languageOne.id} Collection</h2>
            <ul>
                <li>Name  <input onkeyup={e=>state.collectionOne.name = e.target.value} value={state.collectionOne.name} /></li>
                <li>Description  <input onkeyup={e=>state.collectionOne.description = e.target.value} value={state.collectionOne.description} /></li>
                <li><button>Create</button></li>
                <li>Spacing  &amp; Notifications (create once, cannot edit?)</li>
            </ul>

        </form>)
    }
}
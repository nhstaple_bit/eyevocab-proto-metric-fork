"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CollectionCreate = void 0;
var mithril_1 = __importDefault(require("mithril"));
var action_1 = require("../action");
exports.CollectionCreate = {
    view: function (_a) {
        var state = _a.attrs;
        return (mithril_1.default("form", { onsubmit: action_1.CollectionCreateAction, class: "collection-create" },
            mithril_1.default("h2", null,
                "Create ",
                state.languageOne.id,
                " Collection"),
            mithril_1.default("ul", null,
                mithril_1.default("li", null,
                    "Name  ",
                    mithril_1.default("input", { onkeyup: function (e) { return state.collectionOne.name = e.target.value; }, value: state.collectionOne.name })),
                mithril_1.default("li", null,
                    "Description  ",
                    mithril_1.default("input", { onkeyup: function (e) { return state.collectionOne.description = e.target.value; }, value: state.collectionOne.description })),
                mithril_1.default("li", null,
                    mithril_1.default("button", null, "Create")),
                mithril_1.default("li", null, "Spacing  & Notifications (create once, cannot edit?)"))));
    }
};

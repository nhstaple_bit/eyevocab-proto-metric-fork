import m from "mithril"

export const Language = {
    view:function({attrs:state}){
        console.log(state.collectionList);
        
        return (<div class="dash-language">
            <ul>
                <li><a href={`/app/#!/${state.languageOne.id}/_new`}>Create {state.languageOne.id} Collection</a></li>
            </ul>
            Collection List
            <ul>
                {
                    state.collectionList.map(one=>{
                        return <li><a href={`/app/#!/${state.languageOne.id}/${one.id}`}>Edit {one.name}</a></li>
                    })
                }
            </ul>
            
        </div>)
    }
} 
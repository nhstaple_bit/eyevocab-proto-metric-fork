"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Collection = void 0;
var mithril_1 = __importDefault(require("mithril"));
var action_1 = require("../action");
exports.Collection = {
    view: function (_a) {
        var state = _a.attrs;
        return (mithril_1.default("div", { class: "dash-collection" },
            mithril_1.default("h2", null,
                "Collection Name ",
                state.collectionOne.name),
            mithril_1.default("ul", null,
                mithril_1.default("li", null, "Name  & Description"),
                mithril_1.default("li", null,
                    mithril_1.default("a", { href: "", onclick: action_1.CollectionEditDisplay }, "Name  & Description")),
                mithril_1.default("li", null,
                    mithril_1.default("a", { href: "", onclick: action_1.CollectionSessionDisplay }, "Spacing  & Notifications")),
                mithril_1.default("li", null, "Language Item edit List"),
                mithril_1.default("li", null, "Invite & Access"))));
    }
};

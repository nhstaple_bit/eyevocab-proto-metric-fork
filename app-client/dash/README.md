# The Dash #

Dash functionality includes:
- language select
- collection launch (launch Deck in practice/test mode)
- collection create
- collection edit/invite/access

## Session: Notifications & Spacing ##

"Session" refers to the time in which a user interacts with a specific set of language items in a collection in the Deck. When creating a collection, a set of rules define session parameter - different ways that the Deck behaves per collection. One category of these parameters governs how EyeVocab notifies users to interact with sets, and especially when it reminds users. We call this feature "notification and spacing", and it is defined in a sub-component of the collection edit component.

In this folder is `collection-session.tsx` with html form elements that allow a user to define when users of a collection are notified with reminders to practice or test sets in that collection. Definition settings work like this:

- each collection contains an array of reminder settings (`setRemindList`)
- each item in that list is an object with properties - will update soon with model and data
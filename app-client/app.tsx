import m from "mithril"
import * as Dash from "./dash"
import * as Deck from "./deck"

export const App = {
    view:function({attrs:state}){
        
        return (<div>
            <h1>EyeVocab App</h1>
            {
                state.component === "Dash" && m(Dash[state.display],state.Dash)
            }
            {
                state.component === "Deck" && m(Deck[state.display],state.Deck)
            }
           
        </div>)
    }
};

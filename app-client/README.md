# Client Folder #

The main JS app that gets bundle into the public folder.

“Deck” Redesign & Refactor

## Task: “Deck” Redesign & Refactor from Alpha ##

Refactor based on new data models, Typescript, and better UI/X specification.

## Task: "Dash" Redesign & Refactor from Alpha ##

Same as above, the Dash (dashboard) is used for navigating languages and collections, and for exiting, sharing, and reviewing performance and etc - basically anything that isn't the Deck.

## Task: Invite & Share Component Refactor ##

The interface for Invite & Share is part of the "Dash: Edit Collection" component. Invitations and Sharing are controlled per existing Collection. Features must include:

* Share by existing contacts or external email or phone number.
* Set permissions at the time of share.
* Set permissions per individual at the time of share.
* Groups create at share time or add to existing.

## Task: Spacing & Notifications Component Stub ##

The interface for Spacing & Notifications is part of the "Dash: Edit Collection" component. Spacing & Notifications specification is well documented. 

**Note 2020.10.17**: Some existing client-side content for this exists outside the Alpha - it will be added to this folder shortly, and if it isn't ask Christian for it.


## Task: “Collection” & “Set” Create & Edit Interface ##

Rebuild component for editing collections and building sets around Typescript and based on the Alpha and new specifications.
import m from "mithril"
import {LangList} from "./keyboard"
import {StateSet,StateGet} from "./state"


// an empty component for handling route changes
const RouterComponent = {
    oncreate:()=>Router(m.route.param()),
    // don't think we need to check the route each view render
    //view:()=>Router(m.route.param())
    view:()=>{}
}

// mithril handles these route paths
export const Routes = {
    "/": RouterComponent,
    "/:lang": RouterComponent,
    "/:lang/:idCollection": RouterComponent,
    "/:lang/:idCollection/:idSession": RouterComponent
}

// initiate the Lang var with a default value 
// which is whatever is the first language in our list
let Lang,
    IdCollection,
    IdSession
/**
 * Manages route changes.
 */
export const Router= ({lang,idCollection,idSession})=>{
    
    // if we are changing languages
    if (Lang!==lang){
        
        // reset the route var
        Lang = lang
        if(!lang){
            StateSet({display:'Dash'})
            return
        } else
        // if the lang code is NOT in the list of Languages
        if(!LangList.includes(lang)){
            // Send them back to start
            window.location.href = `/app/#!/`
            return
        }
        
        
        // change the window location so the url is right.
        window.location.href = `/app/#!/${lang}`

        // get list of collections from the api

        // set app state
        StateSet({display:'Language',Dash:{
            languageOne:{
                id:lang
            }
        }})

    }
    
    // if we are changing collections
    if(idCollection ==='_new'){
        StateSet({display:'CollectionCreate'})
    } else if(idCollection !==IdCollection) {
        IdCollection = idCollection
        console.log(idCollection);
        let collectionOne = StateGet().Dash.collectionList.find(a=>a.id===idCollection)
        
        if(!collectionOne){
            // we gotta go get it from server
        }
        StateSet({display:'Collection',Dash:{collectionOne}})
        // get one collection from api
        
    }
}

/**
 * When the form input changes, handle input, event and route the change.
 */
Router.fromInput=function(e){
    Router(e.target.value) 
    e.target.blur()
}

/**
 * When the window location hash changes, handle the url and route the change.
 */
Router.fromUrl=(url)=>{
    console.log(url);
    const path = url.split('#!/').pop().split('/')
    
    Router({
        lang:path[0],
        idCollection:path[1],
        idSession:path[2]
    }) 
}


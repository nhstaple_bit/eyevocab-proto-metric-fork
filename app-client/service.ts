// manages requests, data, and state
import {StateSet,StateGet} from "./state"

// this is probably better off refactored into small modular functions
export class Service {
    // posts collection state changes to the server
    static CollectionPost(){}

    // validates changes to a collection settings
    static CollectionValid(){}

    // validates changes to a collection session settings
    static CollectionSessionValid(){}

    // add a reminder
    static CollectionSessionStateAdd(){
        StateGet().Dash.collectionOne.setRemindList.unshift({})
    }
    static CollectionSessionStateSet(i,sessionState){

    }
    static CollectionSessionStateRemove(i){

    }

    static CollectionCreate(){
        const {languageOne,collectionOne,collectionList} = StateGet().Dash

        if(collectionOne.id==='_new'){
            collectionOne.id='_'+Math.floor((Math.random()*10000))
            collectionOne.idLanguage=languageOne.id
        }
        collectionList.unshift(collectionOne)
        StateSet({Dash:{collectionOne,collectionList}})
        
        return collectionOne
    }
}
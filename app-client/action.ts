// provides actions to components
import {Service} from "./service"
import {StateSet} from "./state"

export const CollectionEditDisplay = (e)=>{
    e.preventDefault()
    StateSet({display:'CollectionEdit'})
}

export const CollectionSessionDisplay = (e)=>{
    e.preventDefault()
    StateSet({display:'CollectionEditSession'})
}

export const CollectionCreateAction = (e)=>{
    e.preventDefault()
    const collection = Service.CollectionCreate()

    window.location.href = `/app/#!/${collection.idLanguage}/${collection.id}`
}

export const CollectionPost = async (e)=>{
    e.preventDefault()
    Service.CollectionValid()
    Service.CollectionPost()
}


export const CollectionSessionPost = async (e)=>{
    e.preventDefault()
    Service.CollectionSessionValid()
    Service.CollectionPost()
}


// export const CollectionSessionStateSet = (e)=>{
//     e.preventDefault()
//     Service.CollectionSessionStateAdd()
// }

export const CollectionSessionStateAdd =  (e)=>{
    e.preventDefault()
    Service.CollectionSessionStateAdd()
}


export const CollectionSessionStateRemove = (i)=>async (e)=>{
    e.preventDefault()
    Service.CollectionSessionStateRemove(i)
}
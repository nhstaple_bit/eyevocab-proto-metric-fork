"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
var mithril_1 = __importDefault(require("mithril"));
var Dash = __importStar(require("./dash"));
var Deck = __importStar(require("./deck"));
exports.App = {
    view: function (_a) {
        var state = _a.attrs;
        return (mithril_1.default("div", null,
            mithril_1.default("h1", null, "EyeVocab App"),
            state.component === "Dash" && mithril_1.default(Dash[state.display], state.Dash),
            state.component === "Deck" && mithril_1.default(Deck[state.display], state.Deck)));
    }
};

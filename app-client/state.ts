import m from "mithril"
import stream from "mithril/stream"
import merge from "mergerino"
import {ClientState} from "./client-state"
import {StateModel} from "./model"
// for convenience in the actions
const Set = stream()
/**
 * The StateGet function is exported as default
 */
export const StateGet = stream.scan(merge,ClientState, Set)

export const StateSet = (state:Partial<StateModel>)=>{

  Set(state)
  m.redraw()
}

import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
//import { uglify } from "rollup-plugin-uglify";

export default [{
    input: 'keyboard/index.js',
    
    output: {
        file: 'public/lib/keyboard.js',
        // globals: {
        //   'mithril': 'm'
        // },
        sourcemap:true,
        format: 'iife'
      },
    plugins: [
      commonjs(),
      resolve(
        {
          jsnext: true,
          main: true
        }
      ),
      //uglify()
      
    ],
    onwarn: function(warning) {
      // Skip certain warnings
  
      // should intercept ... but doesn't in some rollup versions
      if ( warning.code === 'THIS_IS_UNDEFINED' ) { return; }
  
      // console.warn everything else
      console.warn( warning.message );
  }
  },
  {
    input: 'app-client/client.js',
    
    output: {
        file: 'public/lib/script.js',
        // globals: {
        //   'mithril': 'm'
        // },
        sourcemap:true,
        format: 'iife'
      },
    plugins: [
      commonjs(),
      resolve(
        {
          jsnext: true,
          main: true
        }
      ),
      //uglify()
      
    ],
    onwarn: function(warning) {
      // Skip certain warnings
  
      // should intercept ... but doesn't in some rollup versions
      if ( warning.code === 'THIS_IS_UNDEFINED' ) { return; }
  
      // console.warn everything else
      console.warn( warning.message );
  }
  }]


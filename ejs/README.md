# EJS Folder #

Contains ejs template files for compiling into HTML. 

Please edit these files rather than the ones in the `/public' folder.

## Task: Expand HTML presentation and content ##

The current html is very plain and boring. We want a simple yet effective look and feel for the site: Google "brutalist web design". We do not want anything too bloated or fancy! Ask Christian for some basic content to get started.

Robert Blake is overseeing content creation.

# API-metric
## Metric specification
#### Authors
##### Christian Coulon, Nick Stapleton
#### Description
Students are assessed during a `practice`, `test`, or `review` session on a `deck` of cards from a `set` of `items` from a  `collection` for a specific `language`.

In short: a `metric` is a record of a user's interactions with the EyeVocab assessment system and includes the following datafields:

## Metric API

| Field | Description |
| :---  | :---        |
| `id`    | a unique ID for **this** `metric` |
| `idSet` | a unique ID for the `set` **this** `metric` was generated from |
| `idCollection` | a unique ID for the `collection` the `set` was taken from for **this** `metric` |
| `idLanguage` | a unique ID for the `language` the `collection` was taken from for **this** `metric` | 
| `mode` | the type of assessment session: `practice`, `test`, or `review` | 
| `idEntity` | a unique ID for the user that generated this `metric` |
| `msStart` | a UNIX timestamp for the moment the user begins the assessment session |
| `msEnd` | the amount of ms elapsed since `msStart` |
| `itemList` | an array of IDs corresponding to `items` used in the `set` |
| `eventList` | an array of `MetricEvents` generated during the user's assessment session (see `MetricEvent` below) |

## MetricEvent API
`MetricEvent`s are a record of the user's interactions with a `set` in an assessment session. An instance of a `metric` can have any number of `MetricEvents` stored in it's `Metric.eventList` array. 

| Field | Description |
| :---  | :---        |
| `msNow` | the elapsed time between the start of the session and the moment this event occured; computed by `Date.now() - Metric.msStart` |
| `eventType` | a *string identifier describing the type of event that was generated |
| `data` | a JSON object that contains the data for the event described by the `Metric.eventType` identifier (see `MetricEvent.data` below) |

*optimization pass: switch string identifiers to an enum identifiers

## MetricEvent.data API

`metric_event_key`: a key is pressed. A user presses a key in an effort to type a correct

* `status`: ok | fail -- was it an error or a successful press?

`metric_event_setting_hint`

* `status`: on | off -- user turns hinting on or off

`metric_event_item_next`: a user completes an item

* `status`: ok | skip -- did the user complete the item successfully or did they skip
* `idItem`: the id of the item
* `len`:  number -- how many characters did they complete? (maybe redundant)

`metric_event_control`: did a user pause, restart, or stop a session? 

* `status`: pause | restart | stop
* `idItem`: the id of the item paused

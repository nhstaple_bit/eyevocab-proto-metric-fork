"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("./config");
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var app_api_1 = require("./app-api");
var app_api_2 = require("./app-api");
var app = express_1.default();
app.use(body_parser_1.default.json());
app.use(express_1.default.static('public'));
app.use('/', app_api_1.ResponseApi);
app.use('/', app_api_1.AuthApi);
app.use('/api/collection', app_api_2.CollectionApi);
app.use(function (req) {
    throw new app_api_1.Fail(404, req.path + ": Route missing!");
});
app.use(function (err, req, res) { return app_api_1.FailApi(err, res); });
var port = process.env.SERVER_PORT || 9000;
app.listen(port, function () { return console.log("Visit http://localhost:" + port); });

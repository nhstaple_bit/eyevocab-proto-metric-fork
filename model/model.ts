
export interface Response {
    status:number,
    success:boolean,
    data:object,
    auth:object
}
export interface RequestParams {
    pathParams?:[],
    params?:object,
    body?:object,
}


export interface Entity {
    id:string,
    idEntity_type:string,
    stampCreate:string,
    idEntity_create:string,
    name:string,
    description:string
}


export interface Identity extends Entity {
    idEntity_type:'identity_person'|'identity_group'|'identity_anonymous',
}


export interface IdentityPerson extends Identity {
    idEntity_type:'identity_person'
}

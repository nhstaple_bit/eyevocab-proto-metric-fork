"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigModel = exports.ConfigMap = void 0;
exports.ConfigMap = {
    NODE_ENV: '',
    SERVER_PORT: 0,
    AWS_REGION: '',
    AWS_SECRET_NAME: '',
    RDB_DB: '',
    JWT_SALT: '',
    JWT_EXPIRES_IN: '',
    TWILIO_ACCOUNT_SID: '',
    TWILIO_AUTH_TOKEN: '',
    SENDGRID_API_KEY: '',
    GOOGLE_CLIENT_ID: '',
    GOOGLE_CLIENT_SECRET: '',
    FACEBOOK_APP_ID: '',
    FACEBOOK_APP_SECRET: '',
};
var ConfigModel = /** @class */ (function () {
    function ConfigModel(env) {
        if (!env) {
            throw new Error('Missing .env file');
        }
        if (env.NODE_ENV) {
            throw new Error('NODE_ENV should not be set in the .env file, please pass it as an execution variable.');
        }
        Object.keys(exports.ConfigMap).forEach(function (k) {
            if (!process.env[k]) {
                throw new Error('Missing Environment Variable: ' + k);
            }
        });
    }
    return ConfigModel;
}());
exports.ConfigModel = ConfigModel;

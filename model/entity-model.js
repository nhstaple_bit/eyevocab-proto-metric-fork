"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityClientModel = exports.EntityCreateModel = exports.EntityModel = void 0;
var EntityModel = /** @class */ (function () {
    function EntityModel(entity) {
        this.id = (entity === null || entity === void 0 ? void 0 : entity.id) || '_new';
        this.name = (entity === null || entity === void 0 ? void 0 : entity.name) || '';
        this.description = (entity === null || entity === void 0 ? void 0 : entity.description) || '';
        this.idEntity_type = (entity === null || entity === void 0 ? void 0 : entity.idEntity_type) || 'type_entity';
        this.idAccount = (entity === null || entity === void 0 ? void 0 : entity.idAccount) || 'account_system_core';
        this.stampCreate = (entity === null || entity === void 0 ? void 0 : entity.stampCreate) || new Date;
        this.isPublic = (entity === null || entity === void 0 ? void 0 : entity.isPublic) || false;
        this.myAccessFrom = (entity === null || entity === void 0 ? void 0 : entity.myAccessFrom) || [];
        this.myAccessTo = (entity === null || entity === void 0 ? void 0 : entity.myAccessTo) || [];
    }
    return EntityModel;
}());
exports.EntityModel = EntityModel;
var EntityCreateModel = /** @class */ (function (_super) {
    __extends(EntityCreateModel, _super);
    function EntityCreateModel(entity) {
        var _this = _super.call(this, entity) || this;
        delete _this.id;
        _this.stampCreate = new Date();
        return _this;
    }
    return EntityCreateModel;
}(EntityModel));
exports.EntityCreateModel = EntityCreateModel;
var EntityClientModel = /** @class */ (function () {
    function EntityClientModel(entity) {
        this.id = (entity === null || entity === void 0 ? void 0 : entity.id) || '_new';
        this.name = (entity === null || entity === void 0 ? void 0 : entity.name) || '';
        this.description = (entity === null || entity === void 0 ? void 0 : entity.description) || '';
        this.idEntity_type = (entity === null || entity === void 0 ? void 0 : entity.idEntity_type) || 'type_entity';
        this.stampCreate = (entity === null || entity === void 0 ? void 0 : entity.stampCreate) || new Date;
        this.isPublic = (entity === null || entity === void 0 ? void 0 : entity.isPublic) || false;
        this.myAccessFrom = (entity === null || entity === void 0 ? void 0 : entity.myAccessFrom) || [];
        this.myAccessTo = (entity === null || entity === void 0 ? void 0 : entity.myAccessTo) || [];
    }
    return EntityClientModel;
}());
exports.EntityClientModel = EntityClientModel;

import {EntityModel,EntityMeta} from './entity-model'
import {AccessEntityType,CompositionAccessEntityType,InstrumentAccessEntityType,IdentityAccessEntityType} from './entity-identity-model'


// basic entity access is a record or three entities, one of which is an access type entity
interface EntityAccess {
    idEntity_to:string
    idEntity_from:string
    idEntity_is:AccessEntityType
}


export interface AccessEntityMeta  extends EntityMeta {
    idEntity_type:AccessEntityType
}

// a type for constructing an entity access record from partial data
export interface EntityAccessNew extends EntityAccess {
    id?:string
    
    idAccount:string
    idEntity_create:string
  
    depth?:number

    accessList?:string[]
    
    stampCreate?:Date
    stampDelete?:Date|null
    
}

export interface IdentityEntityAccessNew extends EntityAccessNew {    
    idEntity_is:IdentityAccessEntityType
}

export interface InstrumentEntityAccessNew extends EntityAccessNew {    
    idEntity_is:InstrumentAccessEntityType
}


export interface CompositionEntityAccessNew extends EntityAccessNew {    
    idEntity_is:CompositionAccessEntityType
}



export interface EntityAccessResult extends EntityAccessModel {
    
    entityTo?:EntityModel
    entityFrom?:EntityModel
    entityIs?:EntityModel

}



// here we use access record filter to return a list of records that match
// note that to,from,is values can be array of ids so that we can get any records that match
export interface EntityAccessFilter {
    idEntity_to:string | string[]
    idEntity_from:string | string[]
    idEntity_is:string | string[]
}


// here we use access record filter to return a list of records that match but can only return one record
// note that to,from,is values can be strings and so we can only get one match, because we only want one
// access record per unique to,from,is
export interface EntityAccessOneFilter extends EntityAccessFilter {
    idEntity_to:string
    idEntity_from:string
    idEntity_is:string
}


// create access record
export class EntityAccessModel implements EntityAccessNew {

    id:string
    idAccount:string
    idEntity_create:string
    
    idEntity_is:AccessEntityType
    idEntity_to:string
    idEntity_from:string
    
    depth:number

    accessList:string[]
    
    stampCreate:Date
    stampDelete?:Date

   constructor(entityAccess:EntityAccessNew){
       this.id = entityAccess.id || '_new'
       this.idEntity_is  = entityAccess.idEntity_is || 'type_access'
       this.idEntity_to = entityAccess.idEntity_to || 'meta_type'
       this.idEntity_to = entityAccess.idEntity_to || 'meta_type'
       this.accessList = entityAccess.accessList || []
       this.idEntity_create = entityAccess.idEntity_create || 'identity_anonymous'
       this.stampCreate = entityAccess.stampCreate || new Date()
       this.stampDelete = entityAccess.stampDelete || null
    }
} 

// create identity access record
export class IdentityEntityAccessModel extends EntityAccessModel {
    constructor(entityAccess:IdentityEntityAccessNew){
        super(entityAccess)
     }
}

// create composition access record
export class CompositionEntityAccessModel extends EntityAccessModel {
    constructor(entityAccess:CompositionEntityAccessNew){
        super(entityAccess)
     }
}


// create music instrument record
export class InstrumentEntityAccessModel extends EntityAccessModel {
    constructor(entityAccess:InstrumentEntityAccessNew){
        super(entityAccess)
     }
}

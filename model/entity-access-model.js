"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.InstrumentEntityAccessModel = exports.CompositionEntityAccessModel = exports.IdentityEntityAccessModel = exports.EntityAccessModel = void 0;
// create access record
var EntityAccessModel = /** @class */ (function () {
    function EntityAccessModel(entityAccess) {
        this.id = entityAccess.id || '_new';
        this.idEntity_is = entityAccess.idEntity_is || 'type_access';
        this.idEntity_to = entityAccess.idEntity_to || 'meta_type';
        this.idEntity_to = entityAccess.idEntity_to || 'meta_type';
        this.accessList = entityAccess.accessList || [];
        this.idEntity_create = entityAccess.idEntity_create || 'identity_anonymous';
        this.stampCreate = entityAccess.stampCreate || new Date();
        this.stampDelete = entityAccess.stampDelete || null;
    }
    return EntityAccessModel;
}());
exports.EntityAccessModel = EntityAccessModel;
// create identity access record
var IdentityEntityAccessModel = /** @class */ (function (_super) {
    __extends(IdentityEntityAccessModel, _super);
    function IdentityEntityAccessModel(entityAccess) {
        return _super.call(this, entityAccess) || this;
    }
    return IdentityEntityAccessModel;
}(EntityAccessModel));
exports.IdentityEntityAccessModel = IdentityEntityAccessModel;
// create composition access record
var CompositionEntityAccessModel = /** @class */ (function (_super) {
    __extends(CompositionEntityAccessModel, _super);
    function CompositionEntityAccessModel(entityAccess) {
        return _super.call(this, entityAccess) || this;
    }
    return CompositionEntityAccessModel;
}(EntityAccessModel));
exports.CompositionEntityAccessModel = CompositionEntityAccessModel;
// create music instrument record
var InstrumentEntityAccessModel = /** @class */ (function (_super) {
    __extends(InstrumentEntityAccessModel, _super);
    function InstrumentEntityAccessModel(entityAccess) {
        return _super.call(this, entityAccess) || this;
    }
    return InstrumentEntityAccessModel;
}(EntityAccessModel));
exports.InstrumentEntityAccessModel = InstrumentEntityAccessModel;

# Model Folder #

Contains Models for server and client.

Models allow for data to be structured both generally and specifically for use in the rest of the application.

## Task: Metrics, Spacing & Reminders ##

Our Metrics, Spacing, and Reminders functionality is in spec. It needs to be translated into "stubs", or rough code outlines in this model folder. 

**Note 2020.10.17**: Much of the code in this folder is currently in a very rough state and will be updated very soon to reflect a better picture of our Beta.
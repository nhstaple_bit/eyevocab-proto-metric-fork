interface Entity {
    id:string    
    name:string
    description:string
    idEntity_type:string
    idEntity_create:string
    stampCreate:Date
    stampDelete:Date
    isPublic:boolean,
    myAccessFrom:string[]
    myAccessTo:string[]
    idAccount:string
}

export interface EntityMeta extends Partial<Entity>{
    id:string
    name:string
    description:string
    idEntity_type:string
}

export class EntityModel implements Partial<Entity> {
    
    id?:string
    name:string
    description:string
    idEntity_create:string
    idEntity_type:string
    idAccount?:string
    stampCreate?:Date
    stampDelete?:Date
    isPublic?:boolean
    myAccessFrom?:string[]
    myAccessTo?:string[]

   constructor(entity?:Partial<EntityModel>){
       this.id = entity?.id || '_new'
       this.name = entity?.name || ''
       this.description  = entity?.description || ''
       this.idEntity_type = entity?.idEntity_type || 'type_entity'
       this.idAccount = entity?.idAccount || 'account_system_core'
       this.stampCreate = entity?.stampCreate ||  new Date
       this.isPublic = entity?.isPublic || false
       this.myAccessFrom = entity?.myAccessFrom || []
       this.myAccessTo = entity?.myAccessTo || []
    }

}

export class EntityCreateModel extends EntityModel {
    constructor(entity:Partial<EntityModel>){
        super(entity)
        delete this.id
        this.stampCreate = new Date()
    }
}

export class EntityClientModel {
    
    id:string
    name:string
    description:string
    idEntity_type:string
    stampCreate?:Date
    stampDelete?:Date
    isPublic:boolean
    myAccessFrom?:string[]
    myAccessTo?:string[]

   constructor(entity:EntityModel){
       this.id = entity?.id || '_new'
       this.name = entity?.name || ''
       this.description  = entity?.description || ''
       this.idEntity_type = entity?.idEntity_type || 'type_entity'
       this.stampCreate = entity?.stampCreate ||  new Date
       this.isPublic = entity?.isPublic || false
       this.myAccessFrom = entity?.myAccessFrom || []
       this.myAccessTo = entity?.myAccessTo || []
    }

}


// entities by type

export type MetaType = "meta_type"

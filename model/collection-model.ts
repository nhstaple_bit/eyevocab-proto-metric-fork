import { Entity } from "./model"

export interface Collection extends Entity {
    idEntity_type:'language_collection',
    idLanguage:string
    setList:Item[],
    setRemindList:any[]
    accessList:string[],
}

export interface Item extends Entity {
    idEntity_type:'language_item'
    
}

// defines how sets are generated when a collection is created 
export type SetCreateOptions = 'manual' | 'smart_incrementing' | 'smart_random' | 'random'


// defines how sets are generated when a session is created 
export type SessionSetOptions = 'manual' | 'random' | 'smart'

// allows different kinds of session presets, 
// "incremental" selects the first unpracticed (or practiced n times where n is the least often practiced set)
// "smart_user" selects most challenging set of language items from the collection based on user metrics
// "smart_hardest" selects the most challenging set of language items based on collected metrics across all users per language
// "smart_easiest" selects the least challenging set of language items based on collected metrics across all users per language
export type SessionNextOptions = 'incremental' | 'smart_collection' | 'smart_language_ascending' | 'smart_language_descending'

// for notifications & spacing, defined the mode that the reminder suggests
export type SessionModeOptions = 'test' | 'practice' | 'review'

// for notifications & spacing, defines session interval as relative to the first or last session per user per collection
export type SessionIntervalOptions = 'after_last' | 'after_first'


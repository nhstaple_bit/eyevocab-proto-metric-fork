"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountSettingModel = exports.AccountModel = exports.AccountUpdateModel = exports.AccountCreateModel = exports.AccountClientModel = void 0;
var entity_identity_model_1 = require("./entity-identity-model");
var Account = /** @class */ (function () {
    function Account(account) {
        this.id = account.id || '_new';
        this.idEntity = account.idEntity || 'identity_anonymous';
    }
    return Account;
}());
var AccountClientModel = /** @class */ (function (_super) {
    __extends(AccountClientModel, _super);
    function AccountClientModel(account) {
        var _this = _super.call(this, account) || this;
        _this.identity = new entity_identity_model_1.IdentityClientModel(account.identity);
        _this.email = !!(account === null || account === void 0 ? void 0 : account.email);
        _this.phone = !!(account === null || account === void 0 ? void 0 : account.phone);
        _this.setting = new AccountSettingModel(account.setting);
        return _this;
    }
    return AccountClientModel;
}(Account));
exports.AccountClientModel = AccountClientModel;
var AccountCreateModel = /** @class */ (function (_super) {
    __extends(AccountCreateModel, _super);
    function AccountCreateModel(account) {
        var _this = _super.call(this, account) || this;
        delete _this.id;
        if (account.email) {
            _this.email = account.email;
        }
        if (account.phone) {
            _this.phone = account.phone;
        }
        _this.tokenAuth = account.tokenAuth;
        _this.tokenConfirm = account.tokenConfirm;
        return _this;
    }
    return AccountCreateModel;
}(Account));
exports.AccountCreateModel = AccountCreateModel;
var AccountUpdateModel = /** @class */ (function (_super) {
    __extends(AccountUpdateModel, _super);
    function AccountUpdateModel(account) {
        var _this = _super.call(this, account) || this;
        Object.assign(_this, account);
        delete _this.identity;
        return _this;
    }
    return AccountUpdateModel;
}(Account));
exports.AccountUpdateModel = AccountUpdateModel;
var AccountModel = /** @class */ (function (_super) {
    __extends(AccountModel, _super);
    function AccountModel(account) {
        var _this = _super.call(this, account) || this;
        Object.assign(_this, account);
        _this.setting = new AccountSettingModel(_this.setting);
        _this.identity = new entity_identity_model_1.IdentityModel(_this.identity);
        return _this;
    }
    return AccountModel;
}(Account));
exports.AccountModel = AccountModel;
var AccountSettingModel = /** @class */ (function () {
    function AccountSettingModel(setting) {
        if (setting === void 0) { setting = {}; }
    }
    return AccountSettingModel;
}());
exports.AccountSettingModel = AccountSettingModel;

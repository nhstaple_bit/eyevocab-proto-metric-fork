
import {IdentityMeta,IdentityModel,IdentityClientModel} from "./entity-identity-model"



/// cdc: this needs a refactor for a simpler account model

export interface AccountMeta extends Account {
    id:string
    email:string
    phone:string
    identity:IdentityMeta
}

class Account  {
    id?:string
    email?:string|boolean
    phone?:string|boolean
    stampCreate?: Date
    stampDelete?: Date
    idEntity?: string
    
    // used to confirm an account for the first time
    tokenConfirm?: string

    // when the confirm token was created, in case we want an expiry
    stampTokenConfirm?: Date

    // cached with jwt token, used to recover account auth cache
    tokenAuth?: string

    // the passwordless uuid token is used for email or text based auth signin
    tokenPasswordless?: string | number
    // when the confirm token was created, in case we want an expiry
    stampTokenPasswordless?: Date
    // the list of previous passwordless hash tokens
    tokenPasswordlessList?: string[]
        
    // the password hash token
    tokenPassword?: string
    // encryption method/salt version
    tokenPasswordEncryptType?:string
    // when the password token was created, in case we want an expiry
    stampTokenPassword?: Date
    // the list of previous password hash tokens
    tokenPasswordList?: string[]

    multiFactor?:'factor2'
    // a list of sent email ids - each corresponds to an email sent and connects that email to an email template so we know
    // who has been sent what
    sentList?: string[]
    setting?:AccountSettingModel
    identity?:IdentityModel | IdentityClientModel | IdentityMeta

    constructor (account:Account){
        this.id = account.id || '_new'
        this.idEntity = account.idEntity  || 'identity_anonymous' 
    }
}


export class AccountClientModel extends Account {
    email:boolean
    phone:boolean
    setting?:AccountSettingModel
    identity?:IdentityClientModel
    constructor(account:Account){
        super(account)
        this.identity = new IdentityClientModel(account.identity as IdentityModel)
        this.email = !!account?.email
        this.phone = !!account?.phone
        this.setting = new AccountSettingModel(account.setting)
    }
}

export class AccountCreateModel extends Account  {
    idEntity:string
    email:string
    phone:string
    tokenAuth:string
    tokenConfirm:string
    constructor(account:AccountCreateModel){
        super(account)
        delete this.id
        if(account.email){
            this.email = account.email
        }
        if(account.phone){
            this.phone = account.phone
        }
        this.tokenAuth = account.tokenAuth
        this.tokenConfirm = account.tokenConfirm
    }
}


export class AccountUpdateModel extends Account {
    id:string
    constructor(account:AccountUpdateModel){
        super(account)
        Object.assign(this,account)
        delete this.identity
    }
}

export class AccountModel extends Account {
    id:string
    email?:string
    phone?:string
    identity?:IdentityModel
    constructor (account:AccountModel){
        super(account)
        Object.assign(this,account)
        this.setting = new AccountSettingModel(this.setting)
        this.identity = new IdentityModel(this.identity)
    }
}

export class AccountSettingModel {
    constructor(setting:Partial<AccountSettingModel>={}){
        
    }

}




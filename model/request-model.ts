import {AccountModel} from "./account-model"
import {IdentityModel} from "./entity-identity-model"
export interface AccessRequest {
    account?:AccountModel
    identity?:IdentityModel
    params?:{
        [key: string]: string|number
    }
    query?:{
        [key: string]: string|number
    }
    body?:{
        [key: string]: string|number
    }
}
"use strict";
// "Metric" is a record of a uer's session with a single collection. 
// It should keep track of ALL the things. These include:
// if the session is a practice or test
// which language items are interacted with
// how long they are interacted with
// which keys are pressed correctly
// which keys are pressed incorrectly
// how long between each key press
// if hints are on or off (when they are turned on or off)
// if the virtual keyboard is in use and what state it is in (when these things change)
// and etc ... spec tbd
Object.defineProperty(exports, "__esModule", { value: true });

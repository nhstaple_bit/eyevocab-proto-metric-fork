export class ResponseModel  {

    status?:string|number
    notify?:string
    log?:Error|Fail|string[]|Error[]|Fail[]

    constructor (response:Response){
        Object.assign(this,response)
    }
}


export class Fail extends Error implements ResponseModel {
    status:number
    notify:string
    constructor(status:number,notify:string){
        super(notify)
        this.name = this.constructor.name;
        this.status = status || 500
        this.notify = notify || ''
    }
}

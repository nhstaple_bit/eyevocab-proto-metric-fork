import {EntityModel,EntityMeta,EntityClientModel,EntityCreateModel} from "./entity-model"
import { AccessEntityMeta } from "./entity-access-model"

export type TypeIdentity = "type_identity"
type IdentityPersonType = 'identity_person'
export type IdentityType = IdentityPersonType|'identity_group'|'identity_anonymous'

// identity entity


export interface IdentityMeta  extends EntityMeta {
    id:string
    name:string
    description:string
    idEntity_type:IdentityType
}

export interface IdentityAccessEntityMeta extends AccessEntityMeta {
    idEntity_type:IdentityAccessEntityType
}


export interface CompositionAccessEntityMeta extends AccessEntityMeta {
    idEntity_type:CompositionAccessEntityType
}


export interface InstrumentAccessEntityMeta extends AccessEntityMeta {
    idEntity_type:InstrumentAccessEntityType
}



export class IdentityModel extends EntityModel {
    idEntity_type:IdentityType
    constructor(identity:IdentityModel){
        super(identity)
    }
}

export class IdentityClientModel extends EntityClientModel {
    idEntity_type:IdentityType
    constructor(identity:IdentityModel){
        super(identity)
    }
}

export class IdentityCreateModel extends EntityCreateModel {
    idEntity_type:IdentityType
    constructor(identity:Partial<IdentityModel>){
        super(identity)
    }
}


export interface IdentityPerson  extends EntityModel {
    idEntity_type:IdentityPersonType
}



// Music entities
export type TypeMusic = "type_music"

export type CompositionMusicType = "music_composition"
export type InstrumentMusicType = "music_instrument"

export type MusicType = CompositionMusicType | InstrumentMusicType

export interface MusicEntityMeta extends Omit<EntityMeta, "idEntity_type"> {}

export class CompositionEntityModel extends EntityModel {
    idEntity_type:'music_composition'
    constructor(entity:Omit<CompositionEntityModel, "idEntity_type">){
        super({...entity,idEntity_type:'music_composition'})
    }
}

export class InstrumentEntityModel extends EntityModel {
    idEntity_type:'music_instrument'
    constructor(identity:Omit<InstrumentEntityModel, "idEntity_type">){
        super({...identity,idEntity_type:'music_instrument'})
    }
}

// AccessEntities



export type TypeAccess = 'type_access'


export type AccessEntityType = TypeAccess|IdentityAccessEntityType|MusicAccessEntityType

export class AccessEntityModel extends EntityModel {
    idEntity_type:AccessEntityType
    constructor(accessEntity:AccessEntityModel){
        super(accessEntity)
    }
}



export type MusicAccessEntityType = InstrumentAccessEntityType|CompositionAccessEntityType


export type InstrumentAccessEntityType = 'access_instrument'


export class InstrumentAccessEntityModel extends AccessEntityModel {
    idEntity_type:InstrumentAccessEntityType
    constructor(accessEntity:InstrumentAccessEntityModel){
        super(accessEntity)
    }
}




export type CompositionAccessEntityType = 'access_composition'

export class CompositionAccessEntityModel extends AccessEntityModel {
    idEntity_type:CompositionAccessEntityType
    constructor(accessEntity:CompositionAccessEntityModel){
        super(accessEntity)
    }
}


export type IdentityAccessEntityType = 'access_identity'

export class IdentityAccessEntityModel extends AccessEntityModel {
    idEntity_type:IdentityAccessEntityType
    constructor(accessEntity:IdentityAccessEntityModel){
        super(accessEntity)
    }
}


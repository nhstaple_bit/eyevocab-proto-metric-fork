"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IdentityAccessEntityModel = exports.CompositionAccessEntityModel = exports.InstrumentAccessEntityModel = exports.AccessEntityModel = exports.InstrumentEntityModel = exports.CompositionEntityModel = exports.IdentityCreateModel = exports.IdentityClientModel = exports.IdentityModel = void 0;
var entity_model_1 = require("./entity-model");
var IdentityModel = /** @class */ (function (_super) {
    __extends(IdentityModel, _super);
    function IdentityModel(identity) {
        return _super.call(this, identity) || this;
    }
    return IdentityModel;
}(entity_model_1.EntityModel));
exports.IdentityModel = IdentityModel;
var IdentityClientModel = /** @class */ (function (_super) {
    __extends(IdentityClientModel, _super);
    function IdentityClientModel(identity) {
        return _super.call(this, identity) || this;
    }
    return IdentityClientModel;
}(entity_model_1.EntityClientModel));
exports.IdentityClientModel = IdentityClientModel;
var IdentityCreateModel = /** @class */ (function (_super) {
    __extends(IdentityCreateModel, _super);
    function IdentityCreateModel(identity) {
        return _super.call(this, identity) || this;
    }
    return IdentityCreateModel;
}(entity_model_1.EntityCreateModel));
exports.IdentityCreateModel = IdentityCreateModel;
var CompositionEntityModel = /** @class */ (function (_super) {
    __extends(CompositionEntityModel, _super);
    function CompositionEntityModel(entity) {
        return _super.call(this, __assign(__assign({}, entity), { idEntity_type: 'music_composition' })) || this;
    }
    return CompositionEntityModel;
}(entity_model_1.EntityModel));
exports.CompositionEntityModel = CompositionEntityModel;
var InstrumentEntityModel = /** @class */ (function (_super) {
    __extends(InstrumentEntityModel, _super);
    function InstrumentEntityModel(identity) {
        return _super.call(this, __assign(__assign({}, identity), { idEntity_type: 'music_instrument' })) || this;
    }
    return InstrumentEntityModel;
}(entity_model_1.EntityModel));
exports.InstrumentEntityModel = InstrumentEntityModel;
var AccessEntityModel = /** @class */ (function (_super) {
    __extends(AccessEntityModel, _super);
    function AccessEntityModel(accessEntity) {
        return _super.call(this, accessEntity) || this;
    }
    return AccessEntityModel;
}(entity_model_1.EntityModel));
exports.AccessEntityModel = AccessEntityModel;
var InstrumentAccessEntityModel = /** @class */ (function (_super) {
    __extends(InstrumentAccessEntityModel, _super);
    function InstrumentAccessEntityModel(accessEntity) {
        return _super.call(this, accessEntity) || this;
    }
    return InstrumentAccessEntityModel;
}(AccessEntityModel));
exports.InstrumentAccessEntityModel = InstrumentAccessEntityModel;
var CompositionAccessEntityModel = /** @class */ (function (_super) {
    __extends(CompositionAccessEntityModel, _super);
    function CompositionAccessEntityModel(accessEntity) {
        return _super.call(this, accessEntity) || this;
    }
    return CompositionAccessEntityModel;
}(AccessEntityModel));
exports.CompositionAccessEntityModel = CompositionAccessEntityModel;
var IdentityAccessEntityModel = /** @class */ (function (_super) {
    __extends(IdentityAccessEntityModel, _super);
    function IdentityAccessEntityModel(accessEntity) {
        return _super.call(this, accessEntity) || this;
    }
    return IdentityAccessEntityModel;
}(AccessEntityModel));
exports.IdentityAccessEntityModel = IdentityAccessEntityModel;

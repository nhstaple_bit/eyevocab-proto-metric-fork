"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CollectionApi = void 0;
var express_1 = require("express");
exports.CollectionApi = express_1.Router();
exports.CollectionApi.get('/:id', function (_a, res) {
    var id = _a.params.id;
    res.json({ id: id });
});
exports.CollectionApi.post('/session-reminder-set/:id', function (_a, res) {
    var id = _a.params.id, body = _a.body;
    console.log("at this point we have our session reminder params in the body");
    console.log(body);
    // next we stuff them in the database  
    res.json({ id: id });
});

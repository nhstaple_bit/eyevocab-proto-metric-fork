import { ResponseModel } from "../model"
export const FailApi = (fail,res)=>{
    const status = typeof fail.status ==='number' && fail.status || 500
    const notify =  fail.message || fail.notify
    res.status(status).json({status,notify})
    console.log(fail.message)
    console.log(fail.stack)
    
}

export class Fail extends Error implements ResponseModel {
    status:number
    notify:string
    constructor(status:number,notify:string){
        super(notify)
        this.name = this.constructor.name;
        this.status = status || 500
        this.notify = notify || ''
    }
}

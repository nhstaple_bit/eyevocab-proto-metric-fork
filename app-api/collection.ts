import {Router} from "express"

export const CollectionApi = Router()

CollectionApi.get('/:id',({params:{id}},res)=>{
    res.json({id})
})

CollectionApi.post('/session-reminder-set/:id',({params:{id},body},res)=>{
    console.log("at this point we have our session reminder params in the body");
    console.log(body);
    // next we stuff them in the database  
    res.json({id})
})
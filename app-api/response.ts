import {FailApi} from "./fail"

export const ResponseApi = async (req,res,next)=>{
    

    res.jsonAuth = (auth)=>{
        res.status(200).json({auth})
    }

    res.jsonFail = (fail)=>{
        FailApi(fail,res)
    }

    res.jsonState = (state)=>{
        const {access} = req
        res.status(200).json({access,state})
    }

    res.jsonData = (data)=>{
        const {access} = req
        res.status(200).json({access,data})
    }

    next()
    

}

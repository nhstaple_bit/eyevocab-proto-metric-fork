# Admin Client Folder #

Builds the Admin JS app bundle.

## Task: Refactor from Alpha ##

Look at the Alpha version and rebuild around Typescript and new specification.

## Task: Metrics Sessions Specification & Implementation ##

Starting with models, build a working service for interacting with the database and other server-side services to provide authenticated/access controlled metrics data i/o to the API endpoints.


## Task: “Collection” & “Set” Create & Edit API ##

Rebuild API for editing collections and building sets around Typescript and based on the Alpha and new specifications.


## Task: "Spacing & Notifications" ##

- Rebuild API for editing collections spacing and notification settings
    - Has been stubbed in `./collection.ts', no database yet. No data yet! Coming soon.
- Build the chron job algo for figuring out how and when to notify people and send notification.
    - Has been stubbed in `./session.ts', no database yet. No data yet! Coming soon.
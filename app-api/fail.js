"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Fail = exports.FailApi = void 0;
exports.FailApi = function (fail, res) {
    var status = typeof fail.status === 'number' && fail.status || 500;
    var notify = fail.message || fail.notify;
    res.status(status).json({ status: status, notify: notify });
    console.log(fail.message);
    console.log(fail.stack);
};
var Fail = /** @class */ (function (_super) {
    __extends(Fail, _super);
    function Fail(status, notify) {
        var _this = _super.call(this, notify) || this;
        _this.name = _this.constructor.name;
        _this.status = status || 500;
        _this.notify = notify || '';
        return _this;
    }
    return Fail;
}(Error));
exports.Fail = Fail;

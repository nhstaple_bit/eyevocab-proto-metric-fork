"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IdentityList = void 0;
exports.IdentityList = [
    {
        id: "mock_identity_1",
        name: "Johnny D",
        description: "I'm a Freshman",
        idEntity_type: "identity_person",
        idEntity_create: "mock_identity_system",
        stampCreate: new Date()
    },
    {
        id: "mock_identity_2",
        name: "Suzy Q",
        description: "I'm a Senior",
        idEntity_type: "identity_person",
        idEntity_create: "mock_identity_system",
        stampCreate: new Date()
    }
];

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.collectionList = void 0;
exports.collectionList = [
    {
        id: "mock_collection_1",
        name: "Spanish 101: Summer 2020",
        idLanguage: 'language_spanish',
        description: "A set of Spanish words to know by the end of summer.",
        idEntity_create: 'identity_person_1',
        idEntity_type: 'language_collection',
        setList: [],
        stampCreate: new Date().toString(),
        accessList: [],
        setRemindList: []
    },
    {
        id: "mock_collection_2",
        name: "Punjab 101: Summer 2020",
        idLanguage: 'language_punjabi',
        description: "A set of Punjab words to know by the end of summer.",
        idEntity_create: 'identity_person_1',
        idEntity_type: 'language_collection',
        setList: [],
        stampCreate: new Date().toString(),
        accessList: [],
        setRemindList: []
    }
];

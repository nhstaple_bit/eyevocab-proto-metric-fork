"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccessList = void 0;
exports.AccessList = [
    {
        id: "mock_access_1",
        idEntity_from: 'mock_identity_2',
        idEntity_to: 'mock_collection_1',
        idEntity_isList: ['access_collection_read', 'access_collection_admin'],
        stampCreate: new Date().toString(),
        idAccount: 'account_system'
    },
    {
        id: "mock_access_2",
        idEntity_from: 'mock_identity_1',
        idEntity_to: 'mock_collection_1',
        idEntity_isList: ['access_collection_read',],
        stampCreate: new Date().toString(),
        idAccount: 'account_system'
    },
    {
        id: "mock_access_3",
        idEntity_from: 'mock_identity_1',
        idEntity_to: 'mock_collection_2',
        idEntity_isList: ['access_collection_read', 'access_collection_admin'],
        stampCreate: new Date().toString(),
        idAccount: 'account_system'
    }
];

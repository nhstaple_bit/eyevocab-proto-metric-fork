import {Metric} from "../model"

export const MetricList:Metric[] = [
    {
        id:"mock_metric_1",
        stampCreate:new Date().toString(),
        idEntity_collection:'mock_collection_1',
        idEntity_language:'language_spanish',
        eventList:[
            {
                id:'mock_event_1',
                msStart:Date.now()
            }
        ]
        

    },
    {
        id:"mock_metric_2",
        stampCreate:new Date().toString(),
        idEntity_collection:'mock_collection_2',
        idEntity_language:'language_spanish',
        eventList:[
            {
                id:'mock_event_1',
                msStart:Date.now()
            }
        ]
        

    },
]
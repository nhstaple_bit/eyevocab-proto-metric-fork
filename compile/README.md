# Compile Folder #

Code for autocompiling HTML and README files. Also code for pushing per root folder README files to GDrive, Box, Confluence.

## Task: Buid README docs compile and push code ##

Logic is pretty simple:

Compile all code comments and append to a single README per root project folder ie all the code comments in the files in `/model` should end up appended to `/model/README.md`

Then build API wrappers for GDrive, Box, and Confluence which authenticate and then create or replace documents in predefined folders in each of these services.

**Note 2020.10.17**: Some of this code is written. Ask Christian for his private pranch.
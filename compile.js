const recursive = require("recursive-readdir");
const {readFileSync,writeFileSync,mkdirSync} = require("fs");
const ejs = require('ejs')
const { join } = require('path');
const { kill } = require("process");
var showdown  = require('showdown'),
    converter = new showdown.Converter()

    converter.setOption('noHeaderId', true);

const ejsTemplate= readFileSync(join(__dirname,"ejs/index.ejs"),'utf-8')

recursive(join(__dirname,"md"), function (err, files) {
  // `files` is an array of file paths
  files.forEach(filePath=>{
    const htm = converter.makeHtml(readFileSync(filePath,'utf-8'));
    const html = ejs.render(ejsTemplate, {htm})
    
    const htmlPath = filePath.replace('/md/','/public/').replace('.md','.html');
    const parentDir = htmlPath.split('/')
    parentDir.pop()
    mkdirSync(parentDir.join('/'), { recursive: true })
    writeFileSync(htmlPath,html)
    
  })
});
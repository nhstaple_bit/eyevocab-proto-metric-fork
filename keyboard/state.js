// import InputControl from "./input";
// import InputControl from "../input"
// import Keyboard from "simple-keyboard";
// import LayoutMapping, {LayoutMappingDestroy,SuperKeysMap,SubKeysMap,LanguageLayoutMap, SourceLayoutMod} from "../simplekeyboard/simplekeyboard_layout_mapping"
// import LayoutsEyevocab from "../simplekeyboard/layouts-eyevocab/layouts-eyevocab"
// let keyboardLayoutState = 'default',
//     inputState = {
//       letterSuper:'',
//       letterSuccess:'',
//       letterFail:'',
//       inputValue:''
//     },
//     KeyboardControl = false,
//     SuperKeysList=[],
//     SubKeysList=[],
//     onInput,
//     onKeyDown,
//     onKeyUp,
//     onInputLetterChange
// export default function(){
//   InputControl.init()
//   InputControl.onKeyDown((e)=>{
//     if(e.isPressedAlt){
//       if(AltKeyMapOn['_'+e.keyCode]){
//         AltKeyMapOn['_'+e.keyCode](e)
//       }
//     }
//     if(KeyMapOn['_'+e.keyCode]){
//       KeyMapOn['_'+e.keyCode](e)
//     }
//     if(onKeyDown) {
//       onKeyDown(e)
//     }
//   })
//   InputControl.onKeyUp((e)=>{
//     if(KeyMapOff['_'+e.keyCode]){
//       KeyMapOff['_'+e.keyCode](e)
//     }
//     if(onKeyUp)onKeyUp(e)
//   })
//   return {
//     destroy:()=>{
//       onInput = null
//       InputControl.destroy()
//       //keyboardDestroy()
//       onKeyDown = null
//       onKeyUp = null
//     },
//     superKeysListGet,
//     subKeysListGet,
//     inputLetterReset,
//     onInputLetterChangeInit:(a)=>{
//       onInputLetterChange = a
//     },
//     keyboardReset,
//     keyboardCreate,
//     keyboardDestroy,
//     keyboardLayoutSet,
//     keyboardLayoutByLetterGet,
//     keyboardLayoutStateGet,
//     keyboardButtonElementGet,
//     hintLetterStateGet,
//     inputDomInit
//   }
// }
// function superKeysListGet(){
//   return SuperKeysList
// }
// function subKeysListGet(){
//   return SubKeysList
// }
// const KeyMapOff={}
// function ActionOff(keyCode){
//   return (a)=>{
//     KeyMapOff['_'+keyCode]=a
//   }
// }
// const KeyMapOn={}
// function ActionOn(keyCode){
//   return (a)=>{
//     KeyMapOn['_'+keyCode]=a
//   }
// }
// function inputLetterReset(){
//   inputState.letter = ''
//   inputState.superState = []
//   inputState.subState = []
// }
// function inputLetterSet(letter){
//   inputState.superStateList = SuperKeysList.filter(one=>{
//     return one[0]===letter
//   })
//   inputState.subStateList = SubKeysList.filter(one=>{
//     return one[0]===letter
//   })
//   //console.log(inputState.superState,'input super state');
//   inputState.letter = letter
//   if(onInputLetterChange){
//     onInputLetterChange(inputState)
//   }
// }
// let inputDom
// function inputDomInit(dom){
//   inputDom = dom
//   let pressLetter = ''
//   inputDom.onkeypress=function(e){
//     //e.preventDefault()
//     //inputDom.value = ''
//     pressLetter = inputDom.value//e.key
//   }
//   inputDom.oninput = function(e){
//     //e && e.preventDefault()
//     const v = inputDom.value
//     inputDom.value=''
//     if(KeyboardControl
//       // && keyboardLetterSet===true
//     ){
//       //inputLetterSet(keyboardLetterSet)
//       //keyboardLetterSet = false
//       return
//     } else {
//       inputLetterSet(v)
//     }
//     // if(TargetLetterList.indexOf(v)>=0){
//     //
//     // }
//     // else if(KeyboardControl){
//     //   return
//     // } else {
//     //   inputLetterSet(v)
//     // }
//   }
// }
// let TargetLetterList=[]
// let TargetLetterSuperList=[]
// let TargetLayoutMap={}
// //let keyboardLetterSet = false
// function keyboardCreate(idLanguage){
//   if(KeyboardControl)return
//   const targetCode = LanguageLayoutMap[idLanguage]
//   TargetLayoutMap = LayoutsEyevocab[targetCode] || LayoutsEyevocab['english']
//   TargetLetterList = TargetLayoutMap.default.concat(TargetLayoutMap.shift)
//   SuperKeysList = SuperKeysMap[targetCode] || []
//   SubKeysList = SubKeysMap[targetCode] || []
//   TargetLetterList = TargetLayoutMap.default.concat(TargetLayoutMap.shift).concat(SuperKeysList.map(one=>one[2]))
//   KeyboardControl = new Keyboard({
//     onChange: (input) => {
//       if(InputControl.isPressedAlt()){
//         return
//       }
//       //keyboardLetterSet = input
//       inputLetterSet(input)
//       KeyboardControl.setInput('')
//     },
//     onKeyPress:function(e){
//       if(true){
//         KeyboardControl.setInput('')
//         return
//       }
//     },//button => onKeyPress(button),
//     //onKeyReleased,
//     //onKeyUp: onKeyUp,
//     disableCaretPositioning: true,
//     sourceLayout:'english',
//     layout:TargetLayoutMap,
//     modules: [LayoutMapping]
//   })
//   //keyboardDestroy()
// }
// function keyboardLayoutStateGet(){
//   return keyboardLayoutState
// }
// function keyboardLayoutSet(layout){
//   if(keyboardLayoutState === layout)return
//   keyboardLayoutState = layout
//   KeyboardControl.setOptions({
//     layoutName: layout || (KeyboardControl.options.layoutName === "default" ? "shift" : "default")
//   })
// }
// function keyboardLayoutByLetterGet(letter){
//   const a = TargetLayoutMap.default.flat().join().split(' ')
//   const b = TargetLayoutMap.shift.flat().join().split(' ')
//   return (a.indexOf(letter)>=0 && 'default')
//   || (b.indexOf(letter)>=0 && 'shift')
//   || 'none'
// }
// function keyboardShiftButtonElementGet(letter){
//   keyboardLayoutSet('shift')
//   let f = KeyboardControl.getButtonElement(letter)
//   return (f instanceof Array && f || ( f && [f] ) ) || []
// }
// function hintLetterStateGet(hintLetter,inputLetter){
//   hintLetter = ((!hintLetter.trim()) && '{space}') || hintLetter.trim()
//   let hintLayout = keyboardLayoutByLetterGet(hintLetter)
//   if(hintLayout==='none'){
//     const superOne = SuperKeysList.find(one=>(
//       one[one.length-1] === hintLetter
//     ))
//     const subOne = SubKeysList.find(one=>(
//       one[one.length-1] === hintLetter
//     ))
//     if(superOne){
//       hintLetter = inputLetter !== superOne[0] ? superOne[0] : superOne[1]
//     } else if (subOne){
//       hintLetter = inputLetter !== subOne[0] ? subOne[0] : subOne[1]
//     }
//   }
//   hintLayout = keyboardLayoutByLetterGet(hintLetter)
//   let hintKeys = []
//   let hintShiftKeys = []
//   if(KeyboardControl && hintLayout!=='none'){
//     keyboardLayoutSet(hintLayout)
//     if(hintLayout==='shift'){
//       hintKeys = keyboardShiftButtonElementGet(hintLetter)
//       hintShiftKeys = keyboardShiftButtonElementGet('{shift}')
//     } else {
//       hintKeys = keyboardButtonElementGet(hintLetter)
//     }
//   }
//   return {
//     letter:hintLetter,
//     keys:hintKeys,
//     keysShift:hintShiftKeys
//   }
// }
// function keyboardButtonElementGet(letter){
//   let f = KeyboardControl.getButtonElement(letter)
//   if(!f) f = keyboardShiftButtonElementGet(letter)
//   if(!f.length) keyboardLayoutSet('default')
//   return f && (f instanceof Array && f || ( f && [f] ) ) || []
// }
// function keyboardDestroy(){
//   if(KeyboardControl){
//     KeyboardControl.destroy()
//     KeyboardControl = false
//     LayoutMappingDestroy()
//     inputDom.focus()
//   }
// }

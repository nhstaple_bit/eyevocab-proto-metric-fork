// handler module for physical keyboard events




export default function({
        onKeyDown,
        onKeyUp,
        onShift,
        onAlt,
        onEnter
    }){
    

    let AltState='down',
    ShiftState='down',
    isAltPressed=false,
    isShiftPressed=false,
    isInit=false

    init()

    return {
        init,
        destroy,
        isAltPressed:()=>isAltPressed,
        isShiftPressed:()=>isShiftPressed
    }

function init(){
    if(isInit)return
    isInit = true
    window.addEventListener('keyup', eventOnKeyUp, false);
    window.addEventListener('keydown', eventOnKeyDown, false);
}
function destroy(){
    isInit = false
    window.removeEventListener('keydown',eventOnKeyDown, false)
    window.removeEventListener('keyup',eventOnKeyUp, false)
}

function eventOnKeyUp(e){
    if(onKeyUp){
        onKeyUp(keycodeMap(e,'up'))
    }
}

function eventOnKeyDown(e){
    if(onKeyDown){
        onKeyDown(keycodeMap(e,'down'))
    }
}

function keycodeMap(e,upOrDown){
    let IsAlt,IsShift
    switch(e.keyCode) {
    case 16:
        isShiftPressed = (IsShift = ShiftState = upOrDown)==='down'
        onShift && onShift(upOrDown)
    break;
    case 18:
        isAltPressed = (IsAlt = AltState = upOrDown)==='down'
        onAlt && onAlt(upOrDown)
    break;
    case 13:
        onEnter && onEnter(upOrDown)
    break;

    }
    return Object.assign(
    e,
    {
        IsShift,
        ShiftState,
        IsAlt,
        AltState
    }
    )
}

  
}

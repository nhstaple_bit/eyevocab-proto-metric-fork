"use strict";
// handler module for physical keyboard events
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(_a) {
    var onKeyDown = _a.onKeyDown, onKeyUp = _a.onKeyUp, onShift = _a.onShift, onAlt = _a.onAlt, onEnter = _a.onEnter;
    var AltState = 'down', ShiftState = 'down', isAltPressed = false, isShiftPressed = false, isInit = false;
    init();
    return {
        init: init,
        destroy: destroy,
        isAltPressed: function () { return isAltPressed; },
        isShiftPressed: function () { return isShiftPressed; }
    };
    function init() {
        if (isInit)
            return;
        isInit = true;
        window.addEventListener('keyup', eventOnKeyUp, false);
        window.addEventListener('keydown', eventOnKeyDown, false);
    }
    function destroy() {
        isInit = false;
        window.removeEventListener('keydown', eventOnKeyDown, false);
        window.removeEventListener('keyup', eventOnKeyUp, false);
    }
    function eventOnKeyUp(e) {
        if (onKeyUp) {
            onKeyUp(keycodeMap(e, 'up'));
        }
    }
    function eventOnKeyDown(e) {
        if (onKeyDown) {
            onKeyDown(keycodeMap(e, 'down'));
        }
    }
    function keycodeMap(e, upOrDown) {
        var IsAlt, IsShift;
        switch (e.keyCode) {
            case 16:
                isShiftPressed = (IsShift = ShiftState = upOrDown) === 'down';
                onShift && onShift(upOrDown);
                break;
            case 18:
                isAltPressed = (IsAlt = AltState = upOrDown) === 'down';
                onAlt && onAlt(upOrDown);
                break;
            case 13:
                onEnter && onEnter(upOrDown);
                break;
        }
        return Object.assign(e, {
            IsShift: IsShift,
            ShiftState: ShiftState,
            IsAlt: IsAlt,
            AltState: AltState
        });
    }
}
exports.default = default_1;

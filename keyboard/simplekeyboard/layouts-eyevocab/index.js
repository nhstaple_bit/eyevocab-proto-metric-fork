export * from './arabic'
export * from './burmese'
export * from './chinese'
export * from './czech'
export * from './english'
export * from './french'
export * from './georgian'
export * from './german'
export * from './hindi'
export * from './punjabi'
export * from './italian'
export * from './japanese'
export * from './korean'
export * from './russian'
export * from './sindhi'
export * from './spanish'
export * from './thai'
export * from './turkish'

// import arabic from './arabic'
// import burmese from './burmese'
// import chinese from './chinese'
// import czech from './czech'
// import english from './english'
// import french from './french'
// import georgian from './georgian'
// import german from './german'
// import hindi from './hindi'
// import punjabi from './punjabi'
// import italian from './italian'
// import japanese from './japanese'
// import korean from './korean'
// import russian from './russian'
// import sindhi from './sindhi'
// import spanish from './spanish'
// import thai from './thai'
// import turkish from './turkish'
// import urdu from './urdu'

// export default {
//   arabic,
//   burmese,
//   chinese,
//   czech,
//   english,
//   french,
//   georgian,
//   german,
//   hindi,
//   punjabi,
//   italian,
//   japanese,
//   korean,
//   russian,
//   sindhi,
//   spanish,
//   thai,
//   turkish,
//   urdu
// }

//for some keys in Punjabi we have to have 3 options, I'm not sure how to write that into the keyboard. 
//these are all Punjabi symbols on the keyboard we need, missing are numbers and some diacritics such as : or ;

export const punjabi = {
  default: [
    "\u0a67 \u0a68 \u0a69 \u0a6a \u0a6b \u0a6c \u0a6d \u0a6e \u0a6f \u0a66 \u002d \u003d {bksp}",
    "{tab} \u0a73 \u0a05 \u0a47 \u0a30 \u0a24 \u0a2f \u0a41 \u0a3f \u0a4b \u0a2a \u0a71 \u0a70 \u005c",
    "{lock} \u0a3e \u0a38 \u0a26 \u0a4d \u0a17 \u0a39 \u0a1c \u0a15 \u0a32 \u003b \u0027 {enter}",
    "{shift} \u0a19 \u0a5c \u0a1a \u0a35 \u0a2c \u0a28 \u0a2e \u002c \u0964 \u002f {shift}",
    ".com @ {space}"
  ],
  shift: [
    "\u0a75 \u0021 \u0040 \u0023 \u0024 \u0025 \u005e \u0026 \u002a \u0028 \u0029 \u005f \u002b {bksp}",
    "{tab} \u0a72 \u0a06 \u0a48 \u0a25 \u0a42 \u0a40 \u0a4c \u0a2b \u005b \u005d \u007c" ,
    "{lock} \u0a3e \u0a36 \u0a27 \u0a3c \u0a18 \u0a03 \u0a1d \u0a16 \u0a33 \u003a \u0022 {enter}",
     "{shift}  \u0a5c\u0a4d\u0a39 \u0a1b \u0a74 \u0a2d \u0a23 \u0a02 \u002e \u0965 \u003f {shift}",
    ".com @ {space}"
//  ],
//  option: [
//    "\u0060 \u0031 \u0032 \u0033 \u0034 \u0035 \u0036 \u0037 \u0038 \u0039 \u0030 \u002d \u003d {bksp}",
//    "{tab} \u0a0f \u0a1f \u0a09 \u0a07 \u0a13 \u0a5e \u005b \u005d \u005c" ,
//    "{lock} \u0a05 \u0a21 \u0a5a \u0a5b \u0a59 \u003b \u0027 {enter}",
//     "{shift} \u0a1e \u0a01 \u003c \u003e {shift}",
//    ".com @ {space}"
  ]
};


//export const punjabi = {
//  default: [
//    "` \u090D \u0945 \u094D\u0930 \u0930\u094D \u091C\u094D\u091E \u0924\u094D\u0930 \u0915\u094D\u0937 \u0936\u094D\u0930 \u096F \u0966 - \u0943 {bksp}",
//    "{tab} \u094C \u0948 \u093E \u0940 \u0942 \u092C \u0939 \u0917 \u0926 \u091C \u0921 \u093C \u0949 \\",
//    "{lock} \u094B \u0947 \u094D \u093F \u0941 \u092A \u0930 \u0915 \u0924 \u091A \u091F {enter}",
//    "{shift} \u0902 \u092E \u0928 \u0935 \u0932 \u0938 , . \u092F {shift}",
//    ".com @ {space}"
//  ],
//  shift: [
//    "~ \u0967 \u0968 \u0969 \u096A \u096B \u096C \u096D \u096E \u096F \u0966 \u0903 \u090B {bksp}",
//    "{tab} \u0914 \u0910 \u0906 \u0908 \u090A \u092D \u0919 \u0918 \u0927 \u091D \u0922 \u091E \u0911",
//    "{lock} \u0913 \u090F \u0905 \u0907 \u0909 \u092B \u0931 \u0916 \u0925 \u091B \u0920 {enter}",
//    '{shift} "" \u0901 \u0923 \u0928 \u0935 \u0933 \u0936 \u0937 \u0964 \u095F {shift}',
//    ".com @ {space}"
//  ]
// };


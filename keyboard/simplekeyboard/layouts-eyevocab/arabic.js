export const arabic = {
  default: [
    "\u0630 \u0661 \u0662 \u0663 \u0664 \u0665 \u0666 \u0667 \u0668 \u0669 \u0660 \u002d \u003d {bksp}",
    "{tab}  \u0636 \u0635 \u062b \u0642 \u0641 \u063a \u0639 \u0647 \u062e \u062d \u062c \u062f  \\",
    "{lock}  \u0634 \u0633 \u064a \u0628 \u0644 \u0627 \u062a \u0646 \u0645 \u0643 \u0637  {enter}",
    "{shift} \u0626 \u0621 \u0624 \u0631 \u0644\u0627 \u0649 \u0629 \u0648 \u0632 \u0638  {shift}",
    ".com @ {space}"
  ],
  shift: [
    "\u0021 \u0040 \u0023 \u0024 \u0025 \u005e \u0026 \u002a \u0029 \u0028 \u005f \u002b {bksp}",
    "{tab}  \u064e \u064b \u064f \u064c \ufef9 \u0625 \u2019 \u00f7 \u00d7 \u061b \u003e \u003c \u007c",
    '{lock}  \u0650 \u064d \u005d \u005b \u0644\u0623 \u0623 \u0640 \u060c \u002f \u003a \u0022  {enter}',
    "{shift}  \u007e \u0652 \u007b \u007d \u0644\u0622 \u0622 \u2018 \u002c \u002e \u061f  {shift}",
    ".com @ {space}"
  ]
};

//export const arabic = {
//  default: [
//    "\u0630 ١ ٢ ٣ ٤ ٥ ٦ ٧ ٨ ٩ ٠ - = {bksp}",
//    "{tab} ض ص ث ق ف غ ع ه خ ح ج د \\",
//    "{lock} ش س ي ب ل ا ت ن م ك ط {enter}",
//    "{shift} ئ ء ؤ ر لا ى ة و ز ظ {shift}",
//    ".com @ {space}"
//  ],
//  shift: [
//    "ّ ! @ # $ % ^ & * ) ( _ + {bksp}",
//    "{tab} َ ً ُ ٌ ﻹ إ ’ ÷ × ؛ > < |",
//   '{lock} ِ ٍ ] [ لأ أ ـ ، / : " {enter}',
//    "{shift} ~ ْ { } لآ آ ‘ , . ؟ {shift}",
//    ".com @ {space}"
//  ]
//};


// Robert's Shift keys
//
// \u0634 \u0633 \u064A \u0628 \u0644 \u0627 \u062A \u0646 \u0645 \u0643 \u061B
// \u0638 \u0637 \u0630 \u062F \u0632 \u0631 \u0648 \u060C \u002E

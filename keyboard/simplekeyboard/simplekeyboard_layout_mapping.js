"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubKeysMap = exports.SuperKeysMap = exports.SourceLayoutMod = exports.LanguageLayoutMap = exports.LayoutMappingDestroy = void 0;
var simple_keyboard_layouts_1 = __importDefault(require("simple-keyboard-layouts"));
//import { IsPressedAlt,IsPressedShift } from '../client_input'
var initModule;
var LayoutMapping = /** @class */ (function () {
    function LayoutMapping() {
        this.layouts = new simple_keyboard_layouts_1.default();
    }
    LayoutMapping.prototype.init = function (keyboard) {
        var _this = this;
        /**
         * Registering module
         */
        keyboard.registerModule("layoutMapping", function (module) {
            var _a = keyboard.options, sourceLayout = _a.sourceLayout, layout = _a.layout;
            var sourceLayoutObj = typeof sourceLayout === "object"
                ? sourceLayout
                : _this.layouts.get(sourceLayout);
            var layoutObj = typeof layout === "object"
                ? layout
                : _this.layouts.get(layout);
            /**
             * Highlight button
             */
            module.highlightButton = function (event) {
                var physicalKeyboardKeyName = module.sourceLayoutKeyMaps(keyboard.physicalKeyboard.getSimpleKeyboardLayoutKey(event));
                var sourceLayoutIndexes = module.getLayoutKeyIndex(physicalKeyboardKeyName, sourceLayoutObj);
                if (sourceLayoutIndexes) {
                    var rIndex = sourceLayoutIndexes.rIndex, bIndex = sourceLayoutIndexes.bIndex;
                    var layoutKeyName = module.findLayoutKeyByIndex(rIndex, bIndex, layoutObj);
                    var buttonElement = module.getButtonInLayout(layoutKeyName);
                    if (!buttonElement) {
                        console.log("Could not find button in layout", layoutKeyName);
                        return false;
                    }
                    if (Array.isArray(buttonElement)) {
                        buttonElement.forEach(function (item) {
                            item.style.background = "#9ab4d0";
                            item.style.color = "white";
                        });
                        /**
                         * Trigger press
                         */
                        buttonElement[0].onpointerdown();
                        buttonElement[0].onpointerup();
                    }
                    else {
                        buttonElement.style.background = "#9ab4d0";
                        buttonElement.style.color = "white";
                        /**
                         * Trigger press
                         */
                        buttonElement.onpointerdown();
                        buttonElement.onpointerup();
                    }
                }
                else {
                    console.error("Key", physicalKeyboardKeyName, "not found in source layout");
                }
            };
            /**
             * Unhighlight button
             */
            module.unhighlightButton = function (event) {
                var physicalKeyboardKeyName = module.sourceLayoutKeyMaps(keyboard.physicalKeyboard.getSimpleKeyboardLayoutKey(event));
                var sourceLayoutIndexes = module.getLayoutKeyIndex(physicalKeyboardKeyName, sourceLayoutObj);
                if (sourceLayoutIndexes) {
                    var rIndex = sourceLayoutIndexes.rIndex, bIndex = sourceLayoutIndexes.bIndex;
                    var layoutKeyName = module.findLayoutKeyByIndex(rIndex, bIndex, layoutObj);
                    var buttonElement = module.getButtonInLayout(layoutKeyName);
                    if (!buttonElement) {
                        console.log("Could not find button in layout", layoutKeyName);
                        return false;
                    }
                    if (Array.isArray(buttonElement)) {
                        buttonElement.forEach(function (item) {
                            item.removeAttribute("style");
                        });
                    }
                    else {
                        buttonElement.removeAttribute("style");
                    }
                }
            };
            /**
             * Get button in layout
             */
            module.getButtonInLayout = function (layoutKeyName) {
                var buttonElement = keyboard.getButtonElement(layoutKeyName) ||
                    keyboard.getButtonElement("{" + layoutKeyName + "}");
                return buttonElement;
            };
            /**
             * Get layout key's index
             */
            module.getLayoutKeyIndex = function (layoutKey, layout) {
                try {
                    var layoutName = keyboard.options.layoutName;
                    layout[layoutName].forEach(function (row, rIndex) {
                        var rowButtons = row.split(" ");
                        rowButtons.forEach(function (button, bIndex) {
                            if (button === layoutKey) {
                                throw {
                                    rIndex: rIndex,
                                    bIndex: bIndex
                                };
                            }
                        });
                    });
                    return false;
                }
                catch (res) {
                    return res;
                }
            };
            /**
             * Find layout key by index
             */
            module.findLayoutKeyByIndex = function (rIndex, bIndex, layout) {
                var layoutName = keyboard.options.layoutName;
                var row = layout[layoutName][rIndex];
                if (row) {
                    var rowButtons = row.split(" ");
                    return rowButtons[bIndex];
                }
            };
            /**
             * Define key listeners
             */
            module.initListeners = function () {
                /**
                 * Handle keyboard press
                 */
                HighlightKeyDownFn = HighlightKeyDown(module);
                document.addEventListener("keydown", HighlightKeyDownFn);
                HighlightKeyUpFn = HighlightKeyUp(module);
                document.addEventListener("keyup", HighlightKeyUpFn);
            };
            /**
             * Custom layout overrides
             */
            module.sourceLayoutKeyMaps = function (keyName) {
                var retval;
                switch (keyName) {
                    case "backspace":
                        retval = "{bksp}";
                        break;
                    case "shiftleft":
                        retval = "{shift}";
                        break;
                    case "shiftright":
                        retval = "{shift}";
                        break;
                    case "space":
                        retval = "{space}";
                        break;
                    case "enter":
                        retval = "{enter}";
                        break;
                    default:
                        retval = keyName;
                        break;
                }
                return retval;
            };
            /**
             * Start module
             */
            module.start = function () {
                module.initListeners();
                keyboard.setOptions({
                    layout: layoutObj
                });
            };
            if (initModule)
                return;
            module.start();
        });
    };
    return LayoutMapping;
}());
function LayoutMappingDestroy() {
    HighlightKeyDownFn && document.removeEventListener("keydown", HighlightKeyDownFn);
    HighlightKeyUpFn && document.removeEventListener("keyup", HighlightKeyUpFn);
}
exports.LayoutMappingDestroy = LayoutMappingDestroy;
var HighlightKeyDownFn;
var HighlightKeyUpFn;
var HighlightKeyDown = function (that) { return function (e) {
    that.highlightButton(e);
}; };
var HighlightKeyUp = function (that) { return function (e) {
    that.unhighlightButton(e);
}; };
exports.default = LayoutMapping;
exports.LanguageLayoutMap = {
    'language_arabic_2d_edition': 'arabic',
    'language_latin_boyd_pharr': 'english',
    'language_latin': 'english',
    'language_latin_jrjs': 'english',
    'language_arabic': 'arabic',
    'language_latin_lnm_2': 'english',
    'language_arabic_through_ak_i_3d_edition': 'arabic',
    'language_greek': 'greek',
    'language_arabic_copy': 'arabic',
    'language_latin_ecce': 'english',
    'language_arabic_typing_module': 'arabic',
    'language_russian_from_latin': 'russian',
    'language_arabictext': 'arabic',
    'language_latin_tarq_fall??': 'english',
    'language_german': 'german',
    'language_latin_henle_1': 'english',
    'language_sounds_feb_12': 'english',
    'language_spanish_archive_march_13_2014': 'spanish',
    'language_spanish_current_1-2-3': 'spanish',
    'language_latin_unzipped': 'english',
    'language_latin_mueller_caesar': 'english',
    'language_spanish_nexos_original_mainly': 'spanish',
    'language_spanish_xxx': 'spanish',
    'language_latin_wheelock': 'english',
    'language_latin_tarquinius_rising': 'english',
    'language_spanish': 'spanish',
    'language_punjabi': 'punjabi'
};
exports.SourceLayoutMod = {
    default: [
        "` 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
        "{tab} q w e r t y u i o p [ ] \\",
        "{lock} a s d f g h j k l ; ' {enter}",
        "{shift} z x c v b n m , . / {shift}",
        ".com @ {space}"
    ],
    shift: [
        "` 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
        "{tab} q w e r t y u i o p [ ] \\",
        "{lock} a s d f g h j k l ; ' {enter}",
        "{shift} z x c v b n m , . / {shift}",
        ".com @ {space}"
    ]
};
// this is the set of keys per language that must
// be pressed in combination
exports.SuperKeysMap = {
    spanish: [
        // first is the supering key,
        // second is the letter key
        // third is the target letter
        //['`','n','ñ'],
        ["¨", 'u', 'ü'],
        ["´", 'e', 'é'],
        ["´", 'i', 'í'],
        ["´", 'o', 'ó'],
        ["´", 'u', 'ú'],
        ["´", 'a', 'á']
    ],
    arabic: []
};
// SubKeys are letters that can be modified
// by pushing other keys
exports.SubKeysMap = {
    punjabi: [
        // the pattern goes that letter in first position
        // can be turned into any letter in last position
        // by pressing any keys in between in any order
        // "a" can be modified by "b" to produce "e"
        // ['a','b','e'],
        // "a" can be modified by "c" to produce "f"
        // ['a','c','f'],
        // "a" can be modified by "c" of "b" to produce "g"
        // note that the modified key
        // ['a','c','b','g'],
        // however if the target is "z"
        // then you can git "a" then "b" then "g"
        // to produce "z"
        // by adding this:
        ['b', 'g', 'z'],
        // this works in languages like Punjabi
        // where  key can be modified more than once
        // by pressing the letter, then the modifiers
        // so if there are two ways to produce "z"
        // as in the order of the modifier doesn't matter
        // then to reach a single modifier
        // you can do this:
        ['a', 'g', 'd'],
        // and then reach the same double modifier "z"
        // like this:
        ['g', 'b', 'z']
        // and the result is that you can
        // produce "z" in two ways:
        // a + b + g = z
        // a + g + f = z
        // and as well each of the combinations
        // can be used to describe a single modified letter "a"
        // a + b = c
        // a + g = d
        // to handle multi modifier keys
        // in which the letter is pressed followed by
    ]
};

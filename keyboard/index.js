"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mithril_1 = __importDefault(require("mithril"));
var keyboard_1 = require("./keyboard");
var router_1 = require("./router");
var App = {
    oncreate: function () {
        router_1.Router(mithril_1.default.route.param('lang'));
    },
    view: function () {
        return (mithril_1.default("div", null,
            mithril_1.default("h1", null, "EyeVocab #5"),
            mithril_1.default("select", { value: mithril_1.default.route.param('lang'), onchange: router_1.Router.fromInput }, keyboard_1.LangList.map(function (lang) { return (mithril_1.default("option", { value: lang }, lang)); })),
            mithril_1.default("br", null),
            mithril_1.default("div", { id: "keyboard", class: "simple-keyboard" }),
            mithril_1.default("br", null),
            mithril_1.default("table", null,
                mithril_1.default("tr", null,
                    mithril_1.default("td", null, "input letter: "),
                    mithril_1.default("td", null,
                        mithril_1.default("input", { value: keyboard_1.InputLetterGet() }))),
                mithril_1.default("tr", null,
                    mithril_1.default("td", null, "unicode: "),
                    mithril_1.default("td", null,
                        mithril_1.default("input", { value: keyboard_1.InputUnicodeGet() }))),
                mithril_1.default("tr", null,
                    mithril_1.default("td", null, "input string: "),
                    mithril_1.default("td", null,
                        mithril_1.default("input", { value: keyboard_1.InputStringGet() }))))));
    }
};
window.onhashchange = function (e) { return router_1.Router.fromUrl(e.newURL); };
mithril_1.default.route(document.getElementById("m-app"), "/english", {
    // auth
    "/:lang": App
});

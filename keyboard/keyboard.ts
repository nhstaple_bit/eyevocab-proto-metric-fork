import m from "mithril"
import Input from "./input";
import Keyboard from "simple-keyboard";
import LayoutMapping, {LayoutMappingDestroy} from "./simplekeyboard/simplekeyboard_layout_mapping"
import * as LayoutsEyevocab from "./simplekeyboard/layouts-eyevocab"

// list our language codes
export const LangList = Object.keys(LayoutsEyevocab)

// initialize our general input listener for super keys
const InputControl = Input({
  onKeyDown,
  onKeyUp,
  onShift,
  onEnter
} as any)

// reference our Keyboard instance
let KeyboardControl:any

/**
 * Function initializes keyboard per language
 */
export const KeyboardInit = (lang='english')=>{
  
  InputReset()

  if(KeyboardControl){
    KeyboardControl.destroy()
    LayoutMappingDestroy()
  }

  // creating a keyboard instance returns a controller for existing keyboard instance
  return KeyboardControl = new Keyboard({
    onChange,
    disableCaretPositioning: true,
    sourceLayout:'english',
    layout:LayoutsEyevocab[lang],
    modules: [LayoutMapping]
  })
}


function onShift(upOrDown){
  KeyboardControl.setOptions({
    layoutName: upOrDown ==='down'?"shift":"default"
  })
}

function onEnter(upOrDown){
  console.log(upOrDown,'enter');
  
  upOrDown === 'down' && InputReset()
  
}

function onKeyDown (){
  
}

function onKeyUp (){
}

function onChange(input){
  KeyboardControl.setInput('')
  // if we are using Alt then we don't record any change
  if(InputControl.isAltPressed()){  
    return
  }
  InputString = InputString + (InputLetter = input)
  
  m.redraw()
}

// the Input
let InputString = ''
let InputLetter = ''
export const InputStringGet=()=>InputString
export const InputLetterGet=()=>InputLetter
export const InputUnicodeGet =()=> InputLetter.length && ('\\u'+InputLetter.charCodeAt(0).toString(16).padStart(4,'0')) || ''
const InputReset = ()=>{
  InputString = ''
  InputLetter = ''
  m.redraw()
}
import {KeyboardInit,LangList} from "./keyboard";

// initiate the Lang var with a default value 
// which is whatever is the first language in our list
let Lang=LangList[0];

/**
 * Manages route changes.
 */
export const Router= (lang)=>{
    // if we are changing languages
    if (Lang!==lang){
        // reset the route var
        Lang = lang

        // if the lang code is NOT in the list of Languages
        if(LangList.indexOf(lang)<0){
            // Send them back to start
            window.location.href = `/keyboard/#!/${LangList[0]}`
            return
        }
  
        // change the window location so the url is right.
        window.location.href = `/keyboard/#!/${lang}`

        // create a new keyboard with the routed language
        KeyboardInit(lang)
    
    }
}

/**
 * When the form input changes, handle input, event and route the change.
 */
Router.fromInput=function(e){
    Router(e.target.value) 
    e.target.blur()
}

/**
 * When the window location hash changes, handle the url and route the change.
 */
Router.fromUrl=(url)=>{
    Router(url && url.split('/').pop() ) 
}
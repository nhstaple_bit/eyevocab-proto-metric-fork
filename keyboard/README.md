# Keyboard Folder #

EyeVocab uses [Simple Keybaord](https://www.npmjs.com/package/simple-keyboard) with a plugin for cross language input and multi-keys and our own keyboard layout designs.

Original keyboard layouts are here (for reference and starting over): `/keyboard/simplekeyboard/layouts-default`

EyeVocab keyboard layouts are here: `/keyboard/simplekeyboard/layouts-default`

The plugin that gives us cross language interaction with the Simple Keyboard is here: `/keyboard/simplekeyboard/simplekeyboard_layout_mapping.ts` 

The rest of the files in this folder are to implement a demo of the keyboard for development purposes. You can see it by following the instructions in the README.md in the root of the repo.

## Task: Look into the plugin for Typescript refactor ##

The plugin (`/keyboard/simplekeyboard/simplekeyboard_layout_mapping.ts` ) was built a while ago and probably needs some Typescript attention.

## Task: Refactor the input for multi-keys ##

Multi-keys including super-keys (modifier plus letter) and sub-keys (letter followed by one or more modifiers) was complete in the Alpha so code exists. However the implementation needs Typescript and a clean-up.

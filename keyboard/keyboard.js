"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InputUnicodeGet = exports.InputLetterGet = exports.InputStringGet = exports.KeyboardInit = exports.LangList = void 0;
var mithril_1 = __importDefault(require("mithril"));
var input_1 = __importDefault(require("./input"));
var simple_keyboard_1 = __importDefault(require("simple-keyboard"));
var simplekeyboard_layout_mapping_1 = __importStar(require("./simplekeyboard/simplekeyboard_layout_mapping"));
var LayoutsEyevocab = __importStar(require("./simplekeyboard/layouts-eyevocab"));
// list our language codes
exports.LangList = Object.keys(LayoutsEyevocab);
// initialize our general input listener for super keys
var InputControl = input_1.default({
    onKeyDown: onKeyDown,
    onKeyUp: onKeyUp,
    onShift: onShift,
    onEnter: onEnter
});
// reference our Keyboard instance
var KeyboardControl;
/**
 * Function initializes keyboard per language
 */
exports.KeyboardInit = function (lang) {
    if (lang === void 0) { lang = 'english'; }
    InputReset();
    if (KeyboardControl) {
        KeyboardControl.destroy();
        simplekeyboard_layout_mapping_1.LayoutMappingDestroy();
    }
    // creating a keyboard instance returns a controller for existing keyboard instance
    return KeyboardControl = new simple_keyboard_1.default({
        onChange: onChange,
        disableCaretPositioning: true,
        sourceLayout: 'english',
        layout: LayoutsEyevocab[lang],
        modules: [simplekeyboard_layout_mapping_1.default]
    });
};
function onShift(upOrDown) {
    KeyboardControl.setOptions({
        layoutName: upOrDown === 'down' ? "shift" : "default"
    });
}
function onEnter(upOrDown) {
    console.log(upOrDown, 'enter');
    upOrDown === 'down' && InputReset();
}
function onKeyDown() {
}
function onKeyUp() {
}
function onChange(input) {
    KeyboardControl.setInput('');
    // if we are using Alt then we don't record any change
    if (InputControl.isAltPressed()) {
        return;
    }
    InputString = InputString + (InputLetter = input);
    mithril_1.default.redraw();
}
// the Input
var InputString = '';
var InputLetter = '';
exports.InputStringGet = function () { return InputString; };
exports.InputLetterGet = function () { return InputLetter; };
exports.InputUnicodeGet = function () { return InputLetter.length && ('\\u' + InputLetter.charCodeAt(0).toString(16).padStart(4, '0')) || ''; };
var InputReset = function () {
    InputString = '';
    InputLetter = '';
    mithril_1.default.redraw();
};

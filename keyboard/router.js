"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Router = void 0;
var keyboard_1 = require("./keyboard");
// initiate the Lang var with a default value 
// which is whatever is the first language in our list
var Lang = keyboard_1.LangList[0];
/**
 * Manages route changes.
 */
exports.Router = function (lang) {
    // if we are changing languages
    if (Lang !== lang) {
        // reset the route var
        Lang = lang;
        // if the lang code is NOT in the list of Languages
        if (keyboard_1.LangList.indexOf(lang) < 0) {
            // Send them back to start
            window.location.href = "/keyboard/#!/" + keyboard_1.LangList[0];
            return;
        }
        // change the window location so the url is right.
        window.location.href = "/keyboard/#!/" + lang;
        // create a new keyboard with the routed language
        keyboard_1.KeyboardInit(lang);
    }
};
/**
 * When the form input changes, handle input, event and route the change.
 */
exports.Router.fromInput = function (e) {
    exports.Router(e.target.value);
    e.target.blur();
};
/**
 * When the window location hash changes, handle the url and route the change.
 */
exports.Router.fromUrl = function (url) {
    exports.Router(url && url.split('/').pop());
};

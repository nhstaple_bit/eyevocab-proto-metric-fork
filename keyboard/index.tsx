import m from "mithril"
import {LangList,InputStringGet,InputLetterGet,InputUnicodeGet} from "./keyboard";
import {Router} from "./router"

const App = {
    oncreate:function(){
        Router(m.route.param('lang'))
    },
    view:function(){
        
        return (<div>
            <h1>EyeVocab #5</h1>
            <select value={m.route.param('lang')} onchange={Router.fromInput}>
            {
                LangList.map(lang=>(
                    <option value={lang}>{lang}</option>
                ))
            }
            </select>
            <br/>
            
            <div id="keyboard" class="simple-keyboard"></div>
            <br/>
            <table>
                
                <tr>
                    <td>input letter: </td>
                    <td><input value={InputLetterGet()} /></td>
                </tr>
                <tr>
                    <td>unicode: </td>
                    <td><input value={InputUnicodeGet()} /></td>
                </tr>
                <tr>
                    <td>input string: </td>
                    <td><input value={InputStringGet()} /></td>
                </tr>
            </table>
           
        </div>)
    }
};

window.onhashchange = e=>Router.fromUrl(e.newURL);

m.route(document.getElementById("m-app"), "/english", {

  // auth
  "/:lang": App


})


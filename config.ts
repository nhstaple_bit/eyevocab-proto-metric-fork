import dotenv from "dotenv"
import {ConfigModel} from "./model/config-model"
new ConfigModel(dotenv.config().parsed as ConfigModel)
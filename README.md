# EyeVocab ABC #

A simple code base for working one dedicated components including Keyboard and Spacing/Notifications.

## Required Tools ##
* [Git](http://git-scm.com/)
* [Nodejs](http://nodejs.org/)
    * [Node Version Manager (nvm) for Windows](https://github.com/coreybutler/nvm-windows)

## Local Development ##
* `npm install -g typescript tslint rollup concurrently`
* `cd /path/to/eyevocab-proto`
* `npm install`
* `npm run build`
* `npm start`
* you can also run `npm run dev` to watch build and serve at once but make sure concurrently is installed.

## Keyboard Development ##
* In the project root folder: keyboard/simplekeyboard/layouts-eyevocab
* Each file corresponds to a language layout and can be edited as such.
* Please note: it is best to use Unicode notation rather than the letters themselves. 
* Example: \u0047 is the letter "G" in English
* Look at the Thai language layout for example: keyboard/simplekeyboard/layouts-eyevocab/thai.js
* Character code converters can be found on the internet. There's also one in the keyboard app for convenience. 



## Run Locally ##
* Windows: add `127.0.0.1 eyevocab.local` to `C:\Windows\System32\drivers\etc\hosts`
* Linux/Mac: add `127.0.0.1 eyevocab.local` to `/etc/hosts`

## Start up ## 

* npm start
* the app demo is at `localhost:9000/app` (or whatever <SERVER_PORT>)
* the keyboard demo is at `localhost:9000/keyboard`

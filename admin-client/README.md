# Admin Client Folder #

Very similar to the Client itself, only providing components which allow for deeper control of the app than we are ready to provide to end users yet - a super user control panel.

## Task: Refactor from Alpha ##

Look at the Alpha version app and refactor similar to the Client.

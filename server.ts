import "./config"
import express from "express"
import bodyParser from "body-parser"
import {
    FailApi,
    ResponseApi,
    AuthApi,
    Fail
} from "./app-api"

import { 
    CollectionApi 
} from './app-api'

const app = express()

app.use(bodyParser.json())

app.use(express.static('public'))

app.use('/',ResponseApi)

app.use('/',AuthApi)

app.use('/api/collection',CollectionApi)

app.use((req) => {
    throw new Fail(404,req.path+": Route missing!")
    
})

app.use((err,req,res)=>FailApi(err,res))


const port = process.env.SERVER_PORT  || 9000

app.listen(port, () => console.log(`Visit http://localhost:${port}`));

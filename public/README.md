# Public Folder #

Please note that all content in this folder will automatically be built and deployed to a public S3 bucket. This content will ultimately become the static content for the public EyeVocab site at www.eyevocab.com. Until then you can see it here at the [EyeVocab Demo](http://demo.eyevocab.com) site.

To build the public content do not edit files in the public folder. Instead edit files in the `*/md*` folder, then from the terminal run:

`npm build:html`


## Task: AWS Infrastructure ##

Currently our public S3 bucket is pretty bare bones. Someone needs to set-up AWS CloudFront with a certificate for http`s` serving. CloudFront can also be set up to serve our API at api.eyevocab.com such that we can build our authentication component into the public static website and thereby collect contact information for users interested in EyeVocab Beta launch.
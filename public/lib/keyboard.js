(function () {
	'use strict';

	var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function createCommonjsModule(fn, basedir, module) {
		return module = {
		  path: basedir,
		  exports: {},
		  require: function (path, base) {
	      return commonjsRequire(path, (base === undefined || base === null) ? module.path : base);
	    }
		}, fn(module, module.exports), module.exports;
	}

	function commonjsRequire () {
		throw new Error('Dynamic requires are not currently supported by @rollup/plugin-commonjs');
	}

	function Vnode(tag, key, attrs, children, text, dom) {
		return {tag: tag, key: key, attrs: attrs, children: children, text: text, dom: dom, domSize: undefined, state: undefined, events: undefined, instance: undefined}
	}
	Vnode.normalize = function(node) {
		if (Array.isArray(node)) return Vnode("[", undefined, undefined, Vnode.normalizeChildren(node), undefined, undefined)
		if (node == null || typeof node === "boolean") return null
		if (typeof node === "object") return node
		return Vnode("#", undefined, undefined, String(node), undefined, undefined)
	};
	Vnode.normalizeChildren = function(input) {
		var children = [];
		if (input.length) {
			var isKeyed = input[0] != null && input[0].key != null;
			// Note: this is a *very* perf-sensitive check.
			// Fun fact: merging the loop like this is somehow faster than splitting
			// it, noticeably so.
			for (var i = 1; i < input.length; i++) {
				if ((input[i] != null && input[i].key != null) !== isKeyed) {
					throw new TypeError("Vnodes must either always have keys or never have keys!")
				}
			}
			for (var i = 0; i < input.length; i++) {
				children[i] = Vnode.normalize(input[i]);
			}
		}
		return children
	};

	var vnode = Vnode;

	// Call via `hyperscriptVnode.apply(startOffset, arguments)`
	//
	// The reason I do it this way, forwarding the arguments and passing the start
	// offset in `this`, is so I don't have to create a temporary array in a
	// performance-critical path.
	//
	// In native ES6, I'd instead add a final `...args` parameter to the
	// `hyperscript` and `fragment` factories and define this as
	// `hyperscriptVnode(...args)`, since modern engines do optimize that away. But
	// ES5 (what Mithril requires thanks to IE support) doesn't give me that luxury,
	// and engines aren't nearly intelligent enough to do either of these:
	//
	// 1. Elide the allocation for `[].slice.call(arguments, 1)` when it's passed to
	//    another function only to be indexed.
	// 2. Elide an `arguments` allocation when it's passed to any function other
	//    than `Function.prototype.apply` or `Reflect.apply`.
	//
	// In ES6, it'd probably look closer to this (I'd need to profile it, though):
	// module.exports = function(attrs, ...children) {
	//     if (attrs == null || typeof attrs === "object" && attrs.tag == null && !Array.isArray(attrs)) {
	//         if (children.length === 1 && Array.isArray(children[0])) children = children[0]
	//     } else {
	//         children = children.length === 0 && Array.isArray(attrs) ? attrs : [attrs, ...children]
	//         attrs = undefined
	//     }
	//
	//     if (attrs == null) attrs = {}
	//     return Vnode("", attrs.key, attrs, children)
	// }
	var hyperscriptVnode = function() {
		var attrs = arguments[this], start = this + 1, children;

		if (attrs == null) {
			attrs = {};
		} else if (typeof attrs !== "object" || attrs.tag != null || Array.isArray(attrs)) {
			attrs = {};
			start = this;
		}

		if (arguments.length === start + 1) {
			children = arguments[start];
			if (!Array.isArray(children)) children = [children];
		} else {
			children = [];
			while (start < arguments.length) children.push(arguments[start++]);
		}

		return vnode("", attrs.key, attrs, children)
	};

	var selectorParser = /(?:(^|#|\.)([^#\.\[\]]+))|(\[(.+?)(?:\s*=\s*("|'|)((?:\\["'\]]|.)*?)\5)?\])/g;
	var selectorCache = {};
	var hasOwn = {}.hasOwnProperty;

	function isEmpty(object) {
		for (var key in object) if (hasOwn.call(object, key)) return false
		return true
	}

	function compileSelector(selector) {
		var match, tag = "div", classes = [], attrs = {};
		while (match = selectorParser.exec(selector)) {
			var type = match[1], value = match[2];
			if (type === "" && value !== "") tag = value;
			else if (type === "#") attrs.id = value;
			else if (type === ".") classes.push(value);
			else if (match[3][0] === "[") {
				var attrValue = match[6];
				if (attrValue) attrValue = attrValue.replace(/\\(["'])/g, "$1").replace(/\\\\/g, "\\");
				if (match[4] === "class") classes.push(attrValue);
				else attrs[match[4]] = attrValue === "" ? attrValue : attrValue || true;
			}
		}
		if (classes.length > 0) attrs.className = classes.join(" ");
		return selectorCache[selector] = {tag: tag, attrs: attrs}
	}

	function execSelector(state, vnode$1) {
		var attrs = vnode$1.attrs;
		var children = vnode.normalizeChildren(vnode$1.children);
		var hasClass = hasOwn.call(attrs, "class");
		var className = hasClass ? attrs.class : attrs.className;

		vnode$1.tag = state.tag;
		vnode$1.attrs = null;
		vnode$1.children = undefined;

		if (!isEmpty(state.attrs) && !isEmpty(attrs)) {
			var newAttrs = {};

			for (var key in attrs) {
				if (hasOwn.call(attrs, key)) newAttrs[key] = attrs[key];
			}

			attrs = newAttrs;
		}

		for (var key in state.attrs) {
			if (hasOwn.call(state.attrs, key) && key !== "className" && !hasOwn.call(attrs, key)){
				attrs[key] = state.attrs[key];
			}
		}
		if (className != null || state.attrs.className != null) attrs.className =
			className != null
				? state.attrs.className != null
					? String(state.attrs.className) + " " + String(className)
					: className
				: state.attrs.className != null
					? state.attrs.className
					: null;

		if (hasClass) attrs.class = null;

		for (var key in attrs) {
			if (hasOwn.call(attrs, key) && key !== "key") {
				vnode$1.attrs = attrs;
				break
			}
		}

		if (Array.isArray(children) && children.length === 1 && children[0] != null && children[0].tag === "#") {
			vnode$1.text = children[0].children;
		} else {
			vnode$1.children = children;
		}

		return vnode$1
	}

	function hyperscript(selector) {
		if (selector == null || typeof selector !== "string" && typeof selector !== "function" && typeof selector.view !== "function") {
			throw Error("The selector must be either a string or a component.");
		}

		var vnode$1 = hyperscriptVnode.apply(1, arguments);

		if (typeof selector === "string") {
			vnode$1.children = vnode.normalizeChildren(vnode$1.children);
			if (selector !== "[") return execSelector(selectorCache[selector] || compileSelector(selector), vnode$1)
		}

		vnode$1.tag = selector;
		return vnode$1
	}

	var hyperscript_1 = hyperscript;

	var trust = function(html) {
		if (html == null) html = "";
		return vnode("<", undefined, undefined, html, undefined, undefined)
	};

	var fragment = function() {
		var vnode$1 = hyperscriptVnode.apply(0, arguments);

		vnode$1.tag = "[";
		vnode$1.children = vnode.normalizeChildren(vnode$1.children);
		return vnode$1
	};

	hyperscript_1.trust = trust;
	hyperscript_1.fragment = fragment;

	var hyperscript_1$1 = hyperscript_1;

	/** @constructor */
	var PromisePolyfill = function(executor) {
		if (!(this instanceof PromisePolyfill)) throw new Error("Promise must be called with `new`")
		if (typeof executor !== "function") throw new TypeError("executor must be a function")

		var self = this, resolvers = [], rejectors = [], resolveCurrent = handler(resolvers, true), rejectCurrent = handler(rejectors, false);
		var instance = self._instance = {resolvers: resolvers, rejectors: rejectors};
		var callAsync = typeof setImmediate === "function" ? setImmediate : setTimeout;
		function handler(list, shouldAbsorb) {
			return function execute(value) {
				var then;
				try {
					if (shouldAbsorb && value != null && (typeof value === "object" || typeof value === "function") && typeof (then = value.then) === "function") {
						if (value === self) throw new TypeError("Promise can't be resolved w/ itself")
						executeOnce(then.bind(value));
					}
					else {
						callAsync(function() {
							if (!shouldAbsorb && list.length === 0) console.error("Possible unhandled promise rejection:", value);
							for (var i = 0; i < list.length; i++) list[i](value);
							resolvers.length = 0, rejectors.length = 0;
							instance.state = shouldAbsorb;
							instance.retry = function() {execute(value);};
						});
					}
				}
				catch (e) {
					rejectCurrent(e);
				}
			}
		}
		function executeOnce(then) {
			var runs = 0;
			function run(fn) {
				return function(value) {
					if (runs++ > 0) return
					fn(value);
				}
			}
			var onerror = run(rejectCurrent);
			try {then(run(resolveCurrent), onerror);} catch (e) {onerror(e);}
		}

		executeOnce(executor);
	};
	PromisePolyfill.prototype.then = function(onFulfilled, onRejection) {
		var self = this, instance = self._instance;
		function handle(callback, list, next, state) {
			list.push(function(value) {
				if (typeof callback !== "function") next(value);
				else try {resolveNext(callback(value));} catch (e) {if (rejectNext) rejectNext(e);}
			});
			if (typeof instance.retry === "function" && state === instance.state) instance.retry();
		}
		var resolveNext, rejectNext;
		var promise = new PromisePolyfill(function(resolve, reject) {resolveNext = resolve, rejectNext = reject;});
		handle(onFulfilled, instance.resolvers, resolveNext, true), handle(onRejection, instance.rejectors, rejectNext, false);
		return promise
	};
	PromisePolyfill.prototype.catch = function(onRejection) {
		return this.then(null, onRejection)
	};
	PromisePolyfill.prototype.finally = function(callback) {
		return this.then(
			function(value) {
				return PromisePolyfill.resolve(callback()).then(function() {
					return value
				})
			},
			function(reason) {
				return PromisePolyfill.resolve(callback()).then(function() {
					return PromisePolyfill.reject(reason);
				})
			}
		)
	};
	PromisePolyfill.resolve = function(value) {
		if (value instanceof PromisePolyfill) return value
		return new PromisePolyfill(function(resolve) {resolve(value);})
	};
	PromisePolyfill.reject = function(value) {
		return new PromisePolyfill(function(resolve, reject) {reject(value);})
	};
	PromisePolyfill.all = function(list) {
		return new PromisePolyfill(function(resolve, reject) {
			var total = list.length, count = 0, values = [];
			if (list.length === 0) resolve([]);
			else for (var i = 0; i < list.length; i++) {
				(function(i) {
					function consume(value) {
						count++;
						values[i] = value;
						if (count === total) resolve(values);
					}
					if (list[i] != null && (typeof list[i] === "object" || typeof list[i] === "function") && typeof list[i].then === "function") {
						list[i].then(consume, reject);
					}
					else consume(list[i]);
				})(i);
			}
		})
	};
	PromisePolyfill.race = function(list) {
		return new PromisePolyfill(function(resolve, reject) {
			for (var i = 0; i < list.length; i++) {
				list[i].then(resolve, reject);
			}
		})
	};

	var polyfill = PromisePolyfill;

	var promise = createCommonjsModule(function (module) {



	if (typeof window !== "undefined") {
		if (typeof window.Promise === "undefined") {
			window.Promise = polyfill;
		} else if (!window.Promise.prototype.finally) {
			window.Promise.prototype.finally = polyfill.prototype.finally;
		}
		module.exports = window.Promise;
	} else if (typeof commonjsGlobal !== "undefined") {
		if (typeof commonjsGlobal.Promise === "undefined") {
			commonjsGlobal.Promise = polyfill;
		} else if (!commonjsGlobal.Promise.prototype.finally) {
			commonjsGlobal.Promise.prototype.finally = polyfill.prototype.finally;
		}
		module.exports = commonjsGlobal.Promise;
	} else {
		module.exports = polyfill;
	}
	});

	var render = function($window) {
		var $doc = $window && $window.document;
		var currentRedraw;

		var nameSpace = {
			svg: "http://www.w3.org/2000/svg",
			math: "http://www.w3.org/1998/Math/MathML"
		};

		function getNameSpace(vnode) {
			return vnode.attrs && vnode.attrs.xmlns || nameSpace[vnode.tag]
		}

		//sanity check to discourage people from doing `vnode.state = ...`
		function checkState(vnode, original) {
			if (vnode.state !== original) throw new Error("`vnode.state` must not be modified")
		}

		//Note: the hook is passed as the `this` argument to allow proxying the
		//arguments without requiring a full array allocation to do so. It also
		//takes advantage of the fact the current `vnode` is the first argument in
		//all lifecycle methods.
		function callHook(vnode) {
			var original = vnode.state;
			try {
				return this.apply(original, arguments)
			} finally {
				checkState(vnode, original);
			}
		}

		// IE11 (at least) throws an UnspecifiedError when accessing document.activeElement when
		// inside an iframe. Catch and swallow this error, and heavy-handidly return null.
		function activeElement() {
			try {
				return $doc.activeElement
			} catch (e) {
				return null
			}
		}
		//create
		function createNodes(parent, vnodes, start, end, hooks, nextSibling, ns) {
			for (var i = start; i < end; i++) {
				var vnode = vnodes[i];
				if (vnode != null) {
					createNode(parent, vnode, hooks, ns, nextSibling);
				}
			}
		}
		function createNode(parent, vnode, hooks, ns, nextSibling) {
			var tag = vnode.tag;
			if (typeof tag === "string") {
				vnode.state = {};
				if (vnode.attrs != null) initLifecycle(vnode.attrs, vnode, hooks);
				switch (tag) {
					case "#": createText(parent, vnode, nextSibling); break
					case "<": createHTML(parent, vnode, ns, nextSibling); break
					case "[": createFragment(parent, vnode, hooks, ns, nextSibling); break
					default: createElement(parent, vnode, hooks, ns, nextSibling);
				}
			}
			else createComponent(parent, vnode, hooks, ns, nextSibling);
		}
		function createText(parent, vnode, nextSibling) {
			vnode.dom = $doc.createTextNode(vnode.children);
			insertNode(parent, vnode.dom, nextSibling);
		}
		var possibleParents = {caption: "table", thead: "table", tbody: "table", tfoot: "table", tr: "tbody", th: "tr", td: "tr", colgroup: "table", col: "colgroup"};
		function createHTML(parent, vnode, ns, nextSibling) {
			var match = vnode.children.match(/^\s*?<(\w+)/im) || [];
			// not using the proper parent makes the child element(s) vanish.
			//     var div = document.createElement("div")
			//     div.innerHTML = "<td>i</td><td>j</td>"
			//     console.log(div.innerHTML)
			// --> "ij", no <td> in sight.
			var temp = $doc.createElement(possibleParents[match[1]] || "div");
			if (ns === "http://www.w3.org/2000/svg") {
				temp.innerHTML = "<svg xmlns=\"http://www.w3.org/2000/svg\">" + vnode.children + "</svg>";
				temp = temp.firstChild;
			} else {
				temp.innerHTML = vnode.children;
			}
			vnode.dom = temp.firstChild;
			vnode.domSize = temp.childNodes.length;
			// Capture nodes to remove, so we don't confuse them.
			vnode.instance = [];
			var fragment = $doc.createDocumentFragment();
			var child;
			while (child = temp.firstChild) {
				vnode.instance.push(child);
				fragment.appendChild(child);
			}
			insertNode(parent, fragment, nextSibling);
		}
		function createFragment(parent, vnode, hooks, ns, nextSibling) {
			var fragment = $doc.createDocumentFragment();
			if (vnode.children != null) {
				var children = vnode.children;
				createNodes(fragment, children, 0, children.length, hooks, null, ns);
			}
			vnode.dom = fragment.firstChild;
			vnode.domSize = fragment.childNodes.length;
			insertNode(parent, fragment, nextSibling);
		}
		function createElement(parent, vnode$1, hooks, ns, nextSibling) {
			var tag = vnode$1.tag;
			var attrs = vnode$1.attrs;
			var is = attrs && attrs.is;

			ns = getNameSpace(vnode$1) || ns;

			var element = ns ?
				is ? $doc.createElementNS(ns, tag, {is: is}) : $doc.createElementNS(ns, tag) :
				is ? $doc.createElement(tag, {is: is}) : $doc.createElement(tag);
			vnode$1.dom = element;

			if (attrs != null) {
				setAttrs(vnode$1, attrs, ns);
			}

			insertNode(parent, element, nextSibling);

			if (!maybeSetContentEditable(vnode$1)) {
				if (vnode$1.text != null) {
					if (vnode$1.text !== "") element.textContent = vnode$1.text;
					else vnode$1.children = [vnode("#", undefined, undefined, vnode$1.text, undefined, undefined)];
				}
				if (vnode$1.children != null) {
					var children = vnode$1.children;
					createNodes(element, children, 0, children.length, hooks, null, ns);
					if (vnode$1.tag === "select" && attrs != null) setLateSelectAttrs(vnode$1, attrs);
				}
			}
		}
		function initComponent(vnode$1, hooks) {
			var sentinel;
			if (typeof vnode$1.tag.view === "function") {
				vnode$1.state = Object.create(vnode$1.tag);
				sentinel = vnode$1.state.view;
				if (sentinel.$$reentrantLock$$ != null) return
				sentinel.$$reentrantLock$$ = true;
			} else {
				vnode$1.state = void 0;
				sentinel = vnode$1.tag;
				if (sentinel.$$reentrantLock$$ != null) return
				sentinel.$$reentrantLock$$ = true;
				vnode$1.state = (vnode$1.tag.prototype != null && typeof vnode$1.tag.prototype.view === "function") ? new vnode$1.tag(vnode$1) : vnode$1.tag(vnode$1);
			}
			initLifecycle(vnode$1.state, vnode$1, hooks);
			if (vnode$1.attrs != null) initLifecycle(vnode$1.attrs, vnode$1, hooks);
			vnode$1.instance = vnode.normalize(callHook.call(vnode$1.state.view, vnode$1));
			if (vnode$1.instance === vnode$1) throw Error("A view cannot return the vnode it received as argument")
			sentinel.$$reentrantLock$$ = null;
		}
		function createComponent(parent, vnode, hooks, ns, nextSibling) {
			initComponent(vnode, hooks);
			if (vnode.instance != null) {
				createNode(parent, vnode.instance, hooks, ns, nextSibling);
				vnode.dom = vnode.instance.dom;
				vnode.domSize = vnode.dom != null ? vnode.instance.domSize : 0;
			}
			else {
				vnode.domSize = 0;
			}
		}

		//update
		/**
		 * @param {Element|Fragment} parent - the parent element
		 * @param {Vnode[] | null} old - the list of vnodes of the last `render()` call for
		 *                               this part of the tree
		 * @param {Vnode[] | null} vnodes - as above, but for the current `render()` call.
		 * @param {Function[]} hooks - an accumulator of post-render hooks (oncreate/onupdate)
		 * @param {Element | null} nextSibling - the next DOM node if we're dealing with a
		 *                                       fragment that is not the last item in its
		 *                                       parent
		 * @param {'svg' | 'math' | String | null} ns) - the current XML namespace, if any
		 * @returns void
		 */
		// This function diffs and patches lists of vnodes, both keyed and unkeyed.
		//
		// We will:
		//
		// 1. describe its general structure
		// 2. focus on the diff algorithm optimizations
		// 3. discuss DOM node operations.

		// ## Overview:
		//
		// The updateNodes() function:
		// - deals with trivial cases
		// - determines whether the lists are keyed or unkeyed based on the first non-null node
		//   of each list.
		// - diffs them and patches the DOM if needed (that's the brunt of the code)
		// - manages the leftovers: after diffing, are there:
		//   - old nodes left to remove?
		// 	 - new nodes to insert?
		// 	 deal with them!
		//
		// The lists are only iterated over once, with an exception for the nodes in `old` that
		// are visited in the fourth part of the diff and in the `removeNodes` loop.

		// ## Diffing
		//
		// Reading https://github.com/localvoid/ivi/blob/ddc09d06abaef45248e6133f7040d00d3c6be853/packages/ivi/src/vdom/implementation.ts#L617-L837
		// may be good for context on longest increasing subsequence-based logic for moving nodes.
		//
		// In order to diff keyed lists, one has to
		//
		// 1) match nodes in both lists, per key, and update them accordingly
		// 2) create the nodes present in the new list, but absent in the old one
		// 3) remove the nodes present in the old list, but absent in the new one
		// 4) figure out what nodes in 1) to move in order to minimize the DOM operations.
		//
		// To achieve 1) one can create a dictionary of keys => index (for the old list), then iterate
		// over the new list and for each new vnode, find the corresponding vnode in the old list using
		// the map.
		// 2) is achieved in the same step: if a new node has no corresponding entry in the map, it is new
		// and must be created.
		// For the removals, we actually remove the nodes that have been updated from the old list.
		// The nodes that remain in that list after 1) and 2) have been performed can be safely removed.
		// The fourth step is a bit more complex and relies on the longest increasing subsequence (LIS)
		// algorithm.
		//
		// the longest increasing subsequence is the list of nodes that can remain in place. Imagine going
		// from `1,2,3,4,5` to `4,5,1,2,3` where the numbers are not necessarily the keys, but the indices
		// corresponding to the keyed nodes in the old list (keyed nodes `e,d,c,b,a` => `b,a,e,d,c` would
		//  match the above lists, for example).
		//
		// In there are two increasing subsequences: `4,5` and `1,2,3`, the latter being the longest. We
		// can update those nodes without moving them, and only call `insertNode` on `4` and `5`.
		//
		// @localvoid adapted the algo to also support node deletions and insertions (the `lis` is actually
		// the longest increasing subsequence *of old nodes still present in the new list*).
		//
		// It is a general algorithm that is fireproof in all circumstances, but it requires the allocation
		// and the construction of a `key => oldIndex` map, and three arrays (one with `newIndex => oldIndex`,
		// the `LIS` and a temporary one to create the LIS).
		//
		// So we cheat where we can: if the tails of the lists are identical, they are guaranteed to be part of
		// the LIS and can be updated without moving them.
		//
		// If two nodes are swapped, they are guaranteed not to be part of the LIS, and must be moved (with
		// the exception of the last node if the list is fully reversed).
		//
		// ## Finding the next sibling.
		//
		// `updateNode()` and `createNode()` expect a nextSibling parameter to perform DOM operations.
		// When the list is being traversed top-down, at any index, the DOM nodes up to the previous
		// vnode reflect the content of the new list, whereas the rest of the DOM nodes reflect the old
		// list. The next sibling must be looked for in the old list using `getNextSibling(... oldStart + 1 ...)`.
		//
		// In the other scenarios (swaps, upwards traversal, map-based diff),
		// the new vnodes list is traversed upwards. The DOM nodes at the bottom of the list reflect the
		// bottom part of the new vnodes list, and we can use the `v.dom`  value of the previous node
		// as the next sibling (cached in the `nextSibling` variable).


		// ## DOM node moves
		//
		// In most scenarios `updateNode()` and `createNode()` perform the DOM operations. However,
		// this is not the case if the node moved (second and fourth part of the diff algo). We move
		// the old DOM nodes before updateNode runs because it enables us to use the cached `nextSibling`
		// variable rather than fetching it using `getNextSibling()`.
		//
		// The fourth part of the diff currently inserts nodes unconditionally, leading to issues
		// like #1791 and #1999. We need to be smarter about those situations where adjascent old
		// nodes remain together in the new list in a way that isn't covered by parts one and
		// three of the diff algo.

		function updateNodes(parent, old, vnodes, hooks, nextSibling, ns) {
			if (old === vnodes || old == null && vnodes == null) return
			else if (old == null || old.length === 0) createNodes(parent, vnodes, 0, vnodes.length, hooks, nextSibling, ns);
			else if (vnodes == null || vnodes.length === 0) removeNodes(parent, old, 0, old.length);
			else {
				var isOldKeyed = old[0] != null && old[0].key != null;
				var isKeyed = vnodes[0] != null && vnodes[0].key != null;
				var start = 0, oldStart = 0;
				if (!isOldKeyed) while (oldStart < old.length && old[oldStart] == null) oldStart++;
				if (!isKeyed) while (start < vnodes.length && vnodes[start] == null) start++;
				if (isKeyed === null && isOldKeyed == null) return // both lists are full of nulls
				if (isOldKeyed !== isKeyed) {
					removeNodes(parent, old, oldStart, old.length);
					createNodes(parent, vnodes, start, vnodes.length, hooks, nextSibling, ns);
				} else if (!isKeyed) {
					// Don't index past the end of either list (causes deopts).
					var commonLength = old.length < vnodes.length ? old.length : vnodes.length;
					// Rewind if necessary to the first non-null index on either side.
					// We could alternatively either explicitly create or remove nodes when `start !== oldStart`
					// but that would be optimizing for sparse lists which are more rare than dense ones.
					start = start < oldStart ? start : oldStart;
					for (; start < commonLength; start++) {
						o = old[start];
						v = vnodes[start];
						if (o === v || o == null && v == null) continue
						else if (o == null) createNode(parent, v, hooks, ns, getNextSibling(old, start + 1, nextSibling));
						else if (v == null) removeNode(parent, o);
						else updateNode(parent, o, v, hooks, getNextSibling(old, start + 1, nextSibling), ns);
					}
					if (old.length > commonLength) removeNodes(parent, old, start, old.length);
					if (vnodes.length > commonLength) createNodes(parent, vnodes, start, vnodes.length, hooks, nextSibling, ns);
				} else {
					// keyed diff
					var oldEnd = old.length - 1, end = vnodes.length - 1, map, o, v, oe, ve, topSibling;

					// bottom-up
					while (oldEnd >= oldStart && end >= start) {
						oe = old[oldEnd];
						ve = vnodes[end];
						if (oe.key !== ve.key) break
						if (oe !== ve) updateNode(parent, oe, ve, hooks, nextSibling, ns);
						if (ve.dom != null) nextSibling = ve.dom;
						oldEnd--, end--;
					}
					// top-down
					while (oldEnd >= oldStart && end >= start) {
						o = old[oldStart];
						v = vnodes[start];
						if (o.key !== v.key) break
						oldStart++, start++;
						if (o !== v) updateNode(parent, o, v, hooks, getNextSibling(old, oldStart, nextSibling), ns);
					}
					// swaps and list reversals
					while (oldEnd >= oldStart && end >= start) {
						if (start === end) break
						if (o.key !== ve.key || oe.key !== v.key) break
						topSibling = getNextSibling(old, oldStart, nextSibling);
						moveNodes(parent, oe, topSibling);
						if (oe !== v) updateNode(parent, oe, v, hooks, topSibling, ns);
						if (++start <= --end) moveNodes(parent, o, nextSibling);
						if (o !== ve) updateNode(parent, o, ve, hooks, nextSibling, ns);
						if (ve.dom != null) nextSibling = ve.dom;
						oldStart++; oldEnd--;
						oe = old[oldEnd];
						ve = vnodes[end];
						o = old[oldStart];
						v = vnodes[start];
					}
					// bottom up once again
					while (oldEnd >= oldStart && end >= start) {
						if (oe.key !== ve.key) break
						if (oe !== ve) updateNode(parent, oe, ve, hooks, nextSibling, ns);
						if (ve.dom != null) nextSibling = ve.dom;
						oldEnd--, end--;
						oe = old[oldEnd];
						ve = vnodes[end];
					}
					if (start > end) removeNodes(parent, old, oldStart, oldEnd + 1);
					else if (oldStart > oldEnd) createNodes(parent, vnodes, start, end + 1, hooks, nextSibling, ns);
					else {
						// inspired by ivi https://github.com/ivijs/ivi/ by Boris Kaul
						var originalNextSibling = nextSibling, vnodesLength = end - start + 1, oldIndices = new Array(vnodesLength), li=0, i=0, pos = 2147483647, matched = 0, map, lisIndices;
						for (i = 0; i < vnodesLength; i++) oldIndices[i] = -1;
						for (i = end; i >= start; i--) {
							if (map == null) map = getKeyMap(old, oldStart, oldEnd + 1);
							ve = vnodes[i];
							var oldIndex = map[ve.key];
							if (oldIndex != null) {
								pos = (oldIndex < pos) ? oldIndex : -1; // becomes -1 if nodes were re-ordered
								oldIndices[i-start] = oldIndex;
								oe = old[oldIndex];
								old[oldIndex] = null;
								if (oe !== ve) updateNode(parent, oe, ve, hooks, nextSibling, ns);
								if (ve.dom != null) nextSibling = ve.dom;
								matched++;
							}
						}
						nextSibling = originalNextSibling;
						if (matched !== oldEnd - oldStart + 1) removeNodes(parent, old, oldStart, oldEnd + 1);
						if (matched === 0) createNodes(parent, vnodes, start, end + 1, hooks, nextSibling, ns);
						else {
							if (pos === -1) {
								// the indices of the indices of the items that are part of the
								// longest increasing subsequence in the oldIndices list
								lisIndices = makeLisIndices(oldIndices);
								li = lisIndices.length - 1;
								for (i = end; i >= start; i--) {
									v = vnodes[i];
									if (oldIndices[i-start] === -1) createNode(parent, v, hooks, ns, nextSibling);
									else {
										if (lisIndices[li] === i - start) li--;
										else moveNodes(parent, v, nextSibling);
									}
									if (v.dom != null) nextSibling = vnodes[i].dom;
								}
							} else {
								for (i = end; i >= start; i--) {
									v = vnodes[i];
									if (oldIndices[i-start] === -1) createNode(parent, v, hooks, ns, nextSibling);
									if (v.dom != null) nextSibling = vnodes[i].dom;
								}
							}
						}
					}
				}
			}
		}
		function updateNode(parent, old, vnode, hooks, nextSibling, ns) {
			var oldTag = old.tag, tag = vnode.tag;
			if (oldTag === tag) {
				vnode.state = old.state;
				vnode.events = old.events;
				if (shouldNotUpdate(vnode, old)) return
				if (typeof oldTag === "string") {
					if (vnode.attrs != null) {
						updateLifecycle(vnode.attrs, vnode, hooks);
					}
					switch (oldTag) {
						case "#": updateText(old, vnode); break
						case "<": updateHTML(parent, old, vnode, ns, nextSibling); break
						case "[": updateFragment(parent, old, vnode, hooks, nextSibling, ns); break
						default: updateElement(old, vnode, hooks, ns);
					}
				}
				else updateComponent(parent, old, vnode, hooks, nextSibling, ns);
			}
			else {
				removeNode(parent, old);
				createNode(parent, vnode, hooks, ns, nextSibling);
			}
		}
		function updateText(old, vnode) {
			if (old.children.toString() !== vnode.children.toString()) {
				old.dom.nodeValue = vnode.children;
			}
			vnode.dom = old.dom;
		}
		function updateHTML(parent, old, vnode, ns, nextSibling) {
			if (old.children !== vnode.children) {
				removeHTML(parent, old);
				createHTML(parent, vnode, ns, nextSibling);
			}
			else {
				vnode.dom = old.dom;
				vnode.domSize = old.domSize;
				vnode.instance = old.instance;
			}
		}
		function updateFragment(parent, old, vnode, hooks, nextSibling, ns) {
			updateNodes(parent, old.children, vnode.children, hooks, nextSibling, ns);
			var domSize = 0, children = vnode.children;
			vnode.dom = null;
			if (children != null) {
				for (var i = 0; i < children.length; i++) {
					var child = children[i];
					if (child != null && child.dom != null) {
						if (vnode.dom == null) vnode.dom = child.dom;
						domSize += child.domSize || 1;
					}
				}
				if (domSize !== 1) vnode.domSize = domSize;
			}
		}
		function updateElement(old, vnode$1, hooks, ns) {
			var element = vnode$1.dom = old.dom;
			ns = getNameSpace(vnode$1) || ns;

			if (vnode$1.tag === "textarea") {
				if (vnode$1.attrs == null) vnode$1.attrs = {};
				if (vnode$1.text != null) {
					vnode$1.attrs.value = vnode$1.text; //FIXME handle multiple children
					vnode$1.text = undefined;
				}
			}
			updateAttrs(vnode$1, old.attrs, vnode$1.attrs, ns);
			if (!maybeSetContentEditable(vnode$1)) {
				if (old.text != null && vnode$1.text != null && vnode$1.text !== "") {
					if (old.text.toString() !== vnode$1.text.toString()) old.dom.firstChild.nodeValue = vnode$1.text;
				}
				else {
					if (old.text != null) old.children = [vnode("#", undefined, undefined, old.text, undefined, old.dom.firstChild)];
					if (vnode$1.text != null) vnode$1.children = [vnode("#", undefined, undefined, vnode$1.text, undefined, undefined)];
					updateNodes(element, old.children, vnode$1.children, hooks, null, ns);
				}
			}
		}
		function updateComponent(parent, old, vnode$1, hooks, nextSibling, ns) {
			vnode$1.instance = vnode.normalize(callHook.call(vnode$1.state.view, vnode$1));
			if (vnode$1.instance === vnode$1) throw Error("A view cannot return the vnode it received as argument")
			updateLifecycle(vnode$1.state, vnode$1, hooks);
			if (vnode$1.attrs != null) updateLifecycle(vnode$1.attrs, vnode$1, hooks);
			if (vnode$1.instance != null) {
				if (old.instance == null) createNode(parent, vnode$1.instance, hooks, ns, nextSibling);
				else updateNode(parent, old.instance, vnode$1.instance, hooks, nextSibling, ns);
				vnode$1.dom = vnode$1.instance.dom;
				vnode$1.domSize = vnode$1.instance.domSize;
			}
			else if (old.instance != null) {
				removeNode(parent, old.instance);
				vnode$1.dom = undefined;
				vnode$1.domSize = 0;
			}
			else {
				vnode$1.dom = old.dom;
				vnode$1.domSize = old.domSize;
			}
		}
		function getKeyMap(vnodes, start, end) {
			var map = Object.create(null);
			for (; start < end; start++) {
				var vnode = vnodes[start];
				if (vnode != null) {
					var key = vnode.key;
					if (key != null) map[key] = start;
				}
			}
			return map
		}
		// Lifted from ivi https://github.com/ivijs/ivi/
		// takes a list of unique numbers (-1 is special and can
		// occur multiple times) and returns an array with the indices
		// of the items that are part of the longest increasing
		// subsequece
		var lisTemp = [];
		function makeLisIndices(a) {
			var result = [0];
			var u = 0, v = 0, i = 0;
			var il = lisTemp.length = a.length;
			for (var i = 0; i < il; i++) lisTemp[i] = a[i];
			for (var i = 0; i < il; ++i) {
				if (a[i] === -1) continue
				var j = result[result.length - 1];
				if (a[j] < a[i]) {
					lisTemp[i] = j;
					result.push(i);
					continue
				}
				u = 0;
				v = result.length - 1;
				while (u < v) {
					// Fast integer average without overflow.
					// eslint-disable-next-line no-bitwise
					var c = (u >>> 1) + (v >>> 1) + (u & v & 1);
					if (a[result[c]] < a[i]) {
						u = c + 1;
					}
					else {
						v = c;
					}
				}
				if (a[i] < a[result[u]]) {
					if (u > 0) lisTemp[i] = result[u - 1];
					result[u] = i;
				}
			}
			u = result.length;
			v = result[u - 1];
			while (u-- > 0) {
				result[u] = v;
				v = lisTemp[v];
			}
			lisTemp.length = 0;
			return result
		}

		function getNextSibling(vnodes, i, nextSibling) {
			for (; i < vnodes.length; i++) {
				if (vnodes[i] != null && vnodes[i].dom != null) return vnodes[i].dom
			}
			return nextSibling
		}

		// This covers a really specific edge case:
		// - Parent node is keyed and contains child
		// - Child is removed, returns unresolved promise in `onbeforeremove`
		// - Parent node is moved in keyed diff
		// - Remaining children still need moved appropriately
		//
		// Ideally, I'd track removed nodes as well, but that introduces a lot more
		// complexity and I'm not exactly interested in doing that.
		function moveNodes(parent, vnode, nextSibling) {
			var frag = $doc.createDocumentFragment();
			moveChildToFrag(parent, frag, vnode);
			insertNode(parent, frag, nextSibling);
		}
		function moveChildToFrag(parent, frag, vnode) {
			// Dodge the recursion overhead in a few of the most common cases.
			while (vnode.dom != null && vnode.dom.parentNode === parent) {
				if (typeof vnode.tag !== "string") {
					vnode = vnode.instance;
					if (vnode != null) continue
				} else if (vnode.tag === "<") {
					for (var i = 0; i < vnode.instance.length; i++) {
						frag.appendChild(vnode.instance[i]);
					}
				} else if (vnode.tag !== "[") {
					// Don't recurse for text nodes *or* elements, just fragments
					frag.appendChild(vnode.dom);
				} else if (vnode.children.length === 1) {
					vnode = vnode.children[0];
					if (vnode != null) continue
				} else {
					for (var i = 0; i < vnode.children.length; i++) {
						var child = vnode.children[i];
						if (child != null) moveChildToFrag(parent, frag, child);
					}
				}
				break
			}
		}

		function insertNode(parent, dom, nextSibling) {
			if (nextSibling != null) parent.insertBefore(dom, nextSibling);
			else parent.appendChild(dom);
		}

		function maybeSetContentEditable(vnode) {
			if (vnode.attrs == null || (
				vnode.attrs.contenteditable == null && // attribute
				vnode.attrs.contentEditable == null // property
			)) return false
			var children = vnode.children;
			if (children != null && children.length === 1 && children[0].tag === "<") {
				var content = children[0].children;
				if (vnode.dom.innerHTML !== content) vnode.dom.innerHTML = content;
			}
			else if (vnode.text != null || children != null && children.length !== 0) throw new Error("Child node of a contenteditable must be trusted")
			return true
		}

		//remove
		function removeNodes(parent, vnodes, start, end) {
			for (var i = start; i < end; i++) {
				var vnode = vnodes[i];
				if (vnode != null) removeNode(parent, vnode);
			}
		}
		function removeNode(parent, vnode) {
			var mask = 0;
			var original = vnode.state;
			var stateResult, attrsResult;
			if (typeof vnode.tag !== "string" && typeof vnode.state.onbeforeremove === "function") {
				var result = callHook.call(vnode.state.onbeforeremove, vnode);
				if (result != null && typeof result.then === "function") {
					mask = 1;
					stateResult = result;
				}
			}
			if (vnode.attrs && typeof vnode.attrs.onbeforeremove === "function") {
				var result = callHook.call(vnode.attrs.onbeforeremove, vnode);
				if (result != null && typeof result.then === "function") {
					// eslint-disable-next-line no-bitwise
					mask |= 2;
					attrsResult = result;
				}
			}
			checkState(vnode, original);

			// If we can, try to fast-path it and avoid all the overhead of awaiting
			if (!mask) {
				onremove(vnode);
				removeChild(parent, vnode);
			} else {
				if (stateResult != null) {
					var next = function () {
						// eslint-disable-next-line no-bitwise
						if (mask & 1) { mask &= 2; if (!mask) reallyRemove(); }
					};
					stateResult.then(next, next);
				}
				if (attrsResult != null) {
					var next = function () {
						// eslint-disable-next-line no-bitwise
						if (mask & 2) { mask &= 1; if (!mask) reallyRemove(); }
					};
					attrsResult.then(next, next);
				}
			}

			function reallyRemove() {
				checkState(vnode, original);
				onremove(vnode);
				removeChild(parent, vnode);
			}
		}
		function removeHTML(parent, vnode) {
			for (var i = 0; i < vnode.instance.length; i++) {
				parent.removeChild(vnode.instance[i]);
			}
		}
		function removeChild(parent, vnode) {
			// Dodge the recursion overhead in a few of the most common cases.
			while (vnode.dom != null && vnode.dom.parentNode === parent) {
				if (typeof vnode.tag !== "string") {
					vnode = vnode.instance;
					if (vnode != null) continue
				} else if (vnode.tag === "<") {
					removeHTML(parent, vnode);
				} else {
					if (vnode.tag !== "[") {
						parent.removeChild(vnode.dom);
						if (!Array.isArray(vnode.children)) break
					}
					if (vnode.children.length === 1) {
						vnode = vnode.children[0];
						if (vnode != null) continue
					} else {
						for (var i = 0; i < vnode.children.length; i++) {
							var child = vnode.children[i];
							if (child != null) removeChild(parent, child);
						}
					}
				}
				break
			}
		}
		function onremove(vnode) {
			if (typeof vnode.tag !== "string" && typeof vnode.state.onremove === "function") callHook.call(vnode.state.onremove, vnode);
			if (vnode.attrs && typeof vnode.attrs.onremove === "function") callHook.call(vnode.attrs.onremove, vnode);
			if (typeof vnode.tag !== "string") {
				if (vnode.instance != null) onremove(vnode.instance);
			} else {
				var children = vnode.children;
				if (Array.isArray(children)) {
					for (var i = 0; i < children.length; i++) {
						var child = children[i];
						if (child != null) onremove(child);
					}
				}
			}
		}

		//attrs
		function setAttrs(vnode, attrs, ns) {
			for (var key in attrs) {
				setAttr(vnode, key, null, attrs[key], ns);
			}
		}
		function setAttr(vnode, key, old, value, ns) {
			if (key === "key" || key === "is" || value == null || isLifecycleMethod(key) || (old === value && !isFormAttribute(vnode, key)) && typeof value !== "object") return
			if (key[0] === "o" && key[1] === "n") return updateEvent(vnode, key, value)
			if (key.slice(0, 6) === "xlink:") vnode.dom.setAttributeNS("http://www.w3.org/1999/xlink", key.slice(6), value);
			else if (key === "style") updateStyle(vnode.dom, old, value);
			else if (hasPropertyKey(vnode, key, ns)) {
				if (key === "value") {
					// Only do the coercion if we're actually going to check the value.
					/* eslint-disable no-implicit-coercion */
					//setting input[value] to same value by typing on focused element moves cursor to end in Chrome
					if ((vnode.tag === "input" || vnode.tag === "textarea") && vnode.dom.value === "" + value && vnode.dom === activeElement()) return
					//setting select[value] to same value while having select open blinks select dropdown in Chrome
					if (vnode.tag === "select" && old !== null && vnode.dom.value === "" + value) return
					//setting option[value] to same value while having select open blinks select dropdown in Chrome
					if (vnode.tag === "option" && old !== null && vnode.dom.value === "" + value) return
					/* eslint-enable no-implicit-coercion */
				}
				// If you assign an input type that is not supported by IE 11 with an assignment expression, an error will occur.
				if (vnode.tag === "input" && key === "type") vnode.dom.setAttribute(key, value);
				else vnode.dom[key] = value;
			} else {
				if (typeof value === "boolean") {
					if (value) vnode.dom.setAttribute(key, "");
					else vnode.dom.removeAttribute(key);
				}
				else vnode.dom.setAttribute(key === "className" ? "class" : key, value);
			}
		}
		function removeAttr(vnode, key, old, ns) {
			if (key === "key" || key === "is" || old == null || isLifecycleMethod(key)) return
			if (key[0] === "o" && key[1] === "n" && !isLifecycleMethod(key)) updateEvent(vnode, key, undefined);
			else if (key === "style") updateStyle(vnode.dom, old, null);
			else if (
				hasPropertyKey(vnode, key, ns)
				&& key !== "className"
				&& !(key === "value" && (
					vnode.tag === "option"
					|| vnode.tag === "select" && vnode.dom.selectedIndex === -1 && vnode.dom === activeElement()
				))
				&& !(vnode.tag === "input" && key === "type")
			) {
				vnode.dom[key] = null;
			} else {
				var nsLastIndex = key.indexOf(":");
				if (nsLastIndex !== -1) key = key.slice(nsLastIndex + 1);
				if (old !== false) vnode.dom.removeAttribute(key === "className" ? "class" : key);
			}
		}
		function setLateSelectAttrs(vnode, attrs) {
			if ("value" in attrs) {
				if(attrs.value === null) {
					if (vnode.dom.selectedIndex !== -1) vnode.dom.value = null;
				} else {
					var normalized = "" + attrs.value; // eslint-disable-line no-implicit-coercion
					if (vnode.dom.value !== normalized || vnode.dom.selectedIndex === -1) {
						vnode.dom.value = normalized;
					}
				}
			}
			if ("selectedIndex" in attrs) setAttr(vnode, "selectedIndex", null, attrs.selectedIndex, undefined);
		}
		function updateAttrs(vnode, old, attrs, ns) {
			if (attrs != null) {
				for (var key in attrs) {
					setAttr(vnode, key, old && old[key], attrs[key], ns);
				}
			}
			var val;
			if (old != null) {
				for (var key in old) {
					if (((val = old[key]) != null) && (attrs == null || attrs[key] == null)) {
						removeAttr(vnode, key, val, ns);
					}
				}
			}
		}
		function isFormAttribute(vnode, attr) {
			return attr === "value" || attr === "checked" || attr === "selectedIndex" || attr === "selected" && vnode.dom === activeElement() || vnode.tag === "option" && vnode.dom.parentNode === $doc.activeElement
		}
		function isLifecycleMethod(attr) {
			return attr === "oninit" || attr === "oncreate" || attr === "onupdate" || attr === "onremove" || attr === "onbeforeremove" || attr === "onbeforeupdate"
		}
		function hasPropertyKey(vnode, key, ns) {
			// Filter out namespaced keys
			return ns === undefined && (
				// If it's a custom element, just keep it.
				vnode.tag.indexOf("-") > -1 || vnode.attrs != null && vnode.attrs.is ||
				// If it's a normal element, let's try to avoid a few browser bugs.
				key !== "href" && key !== "list" && key !== "form" && key !== "width" && key !== "height"// && key !== "type"
				// Defer the property check until *after* we check everything.
			) && key in vnode.dom
		}

		//style
		var uppercaseRegex = /[A-Z]/g;
		function toLowerCase(capital) { return "-" + capital.toLowerCase() }
		function normalizeKey(key) {
			return key[0] === "-" && key[1] === "-" ? key :
				key === "cssFloat" ? "float" :
					key.replace(uppercaseRegex, toLowerCase)
		}
		function updateStyle(element, old, style) {
			if (old === style) ; else if (style == null) {
				// New style is missing, just clear it.
				element.style.cssText = "";
			} else if (typeof style !== "object") {
				// New style is a string, let engine deal with patching.
				element.style.cssText = style;
			} else if (old == null || typeof old !== "object") {
				// `old` is missing or a string, `style` is an object.
				element.style.cssText = "";
				// Add new style properties
				for (var key in style) {
					var value = style[key];
					if (value != null) element.style.setProperty(normalizeKey(key), String(value));
				}
			} else {
				// Both old & new are (different) objects.
				// Update style properties that have changed
				for (var key in style) {
					var value = style[key];
					if (value != null && (value = String(value)) !== String(old[key])) {
						element.style.setProperty(normalizeKey(key), value);
					}
				}
				// Remove style properties that no longer exist
				for (var key in old) {
					if (old[key] != null && style[key] == null) {
						element.style.removeProperty(normalizeKey(key));
					}
				}
			}
		}

		// Here's an explanation of how this works:
		// 1. The event names are always (by design) prefixed by `on`.
		// 2. The EventListener interface accepts either a function or an object
		//    with a `handleEvent` method.
		// 3. The object does not inherit from `Object.prototype`, to avoid
		//    any potential interference with that (e.g. setters).
		// 4. The event name is remapped to the handler before calling it.
		// 5. In function-based event handlers, `ev.target === this`. We replicate
		//    that below.
		// 6. In function-based event handlers, `return false` prevents the default
		//    action and stops event propagation. We replicate that below.
		function EventDict() {
			// Save this, so the current redraw is correctly tracked.
			this._ = currentRedraw;
		}
		EventDict.prototype = Object.create(null);
		EventDict.prototype.handleEvent = function (ev) {
			var handler = this["on" + ev.type];
			var result;
			if (typeof handler === "function") result = handler.call(ev.currentTarget, ev);
			else if (typeof handler.handleEvent === "function") handler.handleEvent(ev);
			if (this._ && ev.redraw !== false) (0, this._)();
			if (result === false) {
				ev.preventDefault();
				ev.stopPropagation();
			}
		};

		//event
		function updateEvent(vnode, key, value) {
			if (vnode.events != null) {
				if (vnode.events[key] === value) return
				if (value != null && (typeof value === "function" || typeof value === "object")) {
					if (vnode.events[key] == null) vnode.dom.addEventListener(key.slice(2), vnode.events, false);
					vnode.events[key] = value;
				} else {
					if (vnode.events[key] != null) vnode.dom.removeEventListener(key.slice(2), vnode.events, false);
					vnode.events[key] = undefined;
				}
			} else if (value != null && (typeof value === "function" || typeof value === "object")) {
				vnode.events = new EventDict();
				vnode.dom.addEventListener(key.slice(2), vnode.events, false);
				vnode.events[key] = value;
			}
		}

		//lifecycle
		function initLifecycle(source, vnode, hooks) {
			if (typeof source.oninit === "function") callHook.call(source.oninit, vnode);
			if (typeof source.oncreate === "function") hooks.push(callHook.bind(source.oncreate, vnode));
		}
		function updateLifecycle(source, vnode, hooks) {
			if (typeof source.onupdate === "function") hooks.push(callHook.bind(source.onupdate, vnode));
		}
		function shouldNotUpdate(vnode, old) {
			do {
				if (vnode.attrs != null && typeof vnode.attrs.onbeforeupdate === "function") {
					var force = callHook.call(vnode.attrs.onbeforeupdate, vnode, old);
					if (force !== undefined && !force) break
				}
				if (typeof vnode.tag !== "string" && typeof vnode.state.onbeforeupdate === "function") {
					var force = callHook.call(vnode.state.onbeforeupdate, vnode, old);
					if (force !== undefined && !force) break
				}
				return false
			} while (false); // eslint-disable-line no-constant-condition
			vnode.dom = old.dom;
			vnode.domSize = old.domSize;
			vnode.instance = old.instance;
			// One would think having the actual latest attributes would be ideal,
			// but it doesn't let us properly diff based on our current internal
			// representation. We have to save not only the old DOM info, but also
			// the attributes used to create it, as we diff *that*, not against the
			// DOM directly (with a few exceptions in `setAttr`). And, of course, we
			// need to save the children and text as they are conceptually not
			// unlike special "attributes" internally.
			vnode.attrs = old.attrs;
			vnode.children = old.children;
			vnode.text = old.text;
			return true
		}

		return function(dom, vnodes, redraw) {
			if (!dom) throw new TypeError("Ensure the DOM element being passed to m.route/m.mount/m.render is not undefined.")
			var hooks = [];
			var active = activeElement();
			var namespace = dom.namespaceURI;

			// First time rendering into a node clears it out
			if (dom.vnodes == null) dom.textContent = "";

			vnodes = vnode.normalizeChildren(Array.isArray(vnodes) ? vnodes : [vnodes]);
			var prevRedraw = currentRedraw;
			try {
				currentRedraw = typeof redraw === "function" ? redraw : undefined;
				updateNodes(dom, dom.vnodes, vnodes, hooks, null, namespace === "http://www.w3.org/1999/xhtml" ? undefined : namespace);
			} finally {
				currentRedraw = prevRedraw;
			}
			dom.vnodes = vnodes;
			// `document.activeElement` can return null: https://html.spec.whatwg.org/multipage/interaction.html#dom-document-activeelement
			if (active != null && activeElement() !== active && typeof active.focus === "function") active.focus();
			for (var i = 0; i < hooks.length; i++) hooks[i]();
		}
	};

	var render$1 = render(window);

	var mountRedraw = function(render, schedule, console) {
		var subscriptions = [];
		var rendering = false;
		var pending = false;

		function sync() {
			if (rendering) throw new Error("Nested m.redraw.sync() call")
			rendering = true;
			for (var i = 0; i < subscriptions.length; i += 2) {
				try { render(subscriptions[i], vnode(subscriptions[i + 1]), redraw); }
				catch (e) { console.error(e); }
			}
			rendering = false;
		}

		function redraw() {
			if (!pending) {
				pending = true;
				schedule(function() {
					pending = false;
					sync();
				});
			}
		}

		redraw.sync = sync;

		function mount(root, component) {
			if (component != null && component.view == null && typeof component !== "function") {
				throw new TypeError("m.mount(element, component) expects a component, not a vnode")
			}

			var index = subscriptions.indexOf(root);
			if (index >= 0) {
				subscriptions.splice(index, 2);
				render(root, [], redraw);
			}

			if (component != null) {
				subscriptions.push(root, component);
				render(root, vnode(component), redraw);
			}
		}

		return {mount: mount, redraw: redraw}
	};

	var mountRedraw$1 = mountRedraw(render$1, requestAnimationFrame, console);

	var build = function(object) {
		if (Object.prototype.toString.call(object) !== "[object Object]") return ""

		var args = [];
		for (var key in object) {
			destructure(key, object[key]);
		}

		return args.join("&")

		function destructure(key, value) {
			if (Array.isArray(value)) {
				for (var i = 0; i < value.length; i++) {
					destructure(key + "[" + i + "]", value[i]);
				}
			}
			else if (Object.prototype.toString.call(value) === "[object Object]") {
				for (var i in value) {
					destructure(key + "[" + i + "]", value[i]);
				}
			}
			else args.push(encodeURIComponent(key) + (value != null && value !== "" ? "=" + encodeURIComponent(value) : ""));
		}
	};

	var assign = Object.assign || function(target, source) {
		if(source) Object.keys(source).forEach(function(key) { target[key] = source[key]; });
	};

	// Returns `path` from `template` + `params`
	var build$1 = function(template, params) {
		if ((/:([^\/\.-]+)(\.{3})?:/).test(template)) {
			throw new SyntaxError("Template parameter names *must* be separated")
		}
		if (params == null) return template
		var queryIndex = template.indexOf("?");
		var hashIndex = template.indexOf("#");
		var queryEnd = hashIndex < 0 ? template.length : hashIndex;
		var pathEnd = queryIndex < 0 ? queryEnd : queryIndex;
		var path = template.slice(0, pathEnd);
		var query = {};

		assign(query, params);

		var resolved = path.replace(/:([^\/\.-]+)(\.{3})?/g, function(m, key, variadic) {
			delete query[key];
			// If no such parameter exists, don't interpolate it.
			if (params[key] == null) return m
			// Escape normal parameters, but not variadic ones.
			return variadic ? params[key] : encodeURIComponent(String(params[key]))
		});

		// In case the template substitution adds new query/hash parameters.
		var newQueryIndex = resolved.indexOf("?");
		var newHashIndex = resolved.indexOf("#");
		var newQueryEnd = newHashIndex < 0 ? resolved.length : newHashIndex;
		var newPathEnd = newQueryIndex < 0 ? newQueryEnd : newQueryIndex;
		var result = resolved.slice(0, newPathEnd);

		if (queryIndex >= 0) result += template.slice(queryIndex, queryEnd);
		if (newQueryIndex >= 0) result += (queryIndex < 0 ? "?" : "&") + resolved.slice(newQueryIndex, newQueryEnd);
		var querystring = build(query);
		if (querystring) result += (queryIndex < 0 && newQueryIndex < 0 ? "?" : "&") + querystring;
		if (hashIndex >= 0) result += template.slice(hashIndex);
		if (newHashIndex >= 0) result += (hashIndex < 0 ? "" : "&") + resolved.slice(newHashIndex);
		return result
	};

	var request = function($window, Promise, oncompletion) {
		var callbackCount = 0;

		function PromiseProxy(executor) {
			return new Promise(executor)
		}

		// In case the global Promise is some userland library's where they rely on
		// `foo instanceof this.constructor`, `this.constructor.resolve(value)`, or
		// similar. Let's *not* break them.
		PromiseProxy.prototype = Promise.prototype;
		PromiseProxy.__proto__ = Promise; // eslint-disable-line no-proto

		function makeRequest(factory) {
			return function(url, args) {
				if (typeof url !== "string") { args = url; url = url.url; }
				else if (args == null) args = {};
				var promise = new Promise(function(resolve, reject) {
					factory(build$1(url, args.params), args, function (data) {
						if (typeof args.type === "function") {
							if (Array.isArray(data)) {
								for (var i = 0; i < data.length; i++) {
									data[i] = new args.type(data[i]);
								}
							}
							else data = new args.type(data);
						}
						resolve(data);
					}, reject);
				});
				if (args.background === true) return promise
				var count = 0;
				function complete() {
					if (--count === 0 && typeof oncompletion === "function") oncompletion();
				}

				return wrap(promise)

				function wrap(promise) {
					var then = promise.then;
					// Set the constructor, so engines know to not await or resolve
					// this as a native promise. At the time of writing, this is
					// only necessary for V8, but their behavior is the correct
					// behavior per spec. See this spec issue for more details:
					// https://github.com/tc39/ecma262/issues/1577. Also, see the
					// corresponding comment in `request/tests/test-request.js` for
					// a bit more background on the issue at hand.
					promise.constructor = PromiseProxy;
					promise.then = function() {
						count++;
						var next = then.apply(promise, arguments);
						next.then(complete, function(e) {
							complete();
							if (count === 0) throw e
						});
						return wrap(next)
					};
					return promise
				}
			}
		}

		function hasHeader(args, name) {
			for (var key in args.headers) {
				if ({}.hasOwnProperty.call(args.headers, key) && name.test(key)) return true
			}
			return false
		}

		return {
			request: makeRequest(function(url, args, resolve, reject) {
				var method = args.method != null ? args.method.toUpperCase() : "GET";
				var body = args.body;
				var assumeJSON = (args.serialize == null || args.serialize === JSON.serialize) && !(body instanceof $window.FormData);
				var responseType = args.responseType || (typeof args.extract === "function" ? "" : "json");

				var xhr = new $window.XMLHttpRequest(), aborted = false;
				var original = xhr, replacedAbort;
				var abort = xhr.abort;

				xhr.abort = function() {
					aborted = true;
					abort.call(this);
				};

				xhr.open(method, url, args.async !== false, typeof args.user === "string" ? args.user : undefined, typeof args.password === "string" ? args.password : undefined);

				if (assumeJSON && body != null && !hasHeader(args, /^content-type$/i)) {
					xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
				}
				if (typeof args.deserialize !== "function" && !hasHeader(args, /^accept$/i)) {
					xhr.setRequestHeader("Accept", "application/json, text/*");
				}
				if (args.withCredentials) xhr.withCredentials = args.withCredentials;
				if (args.timeout) xhr.timeout = args.timeout;
				xhr.responseType = responseType;

				for (var key in args.headers) {
					if ({}.hasOwnProperty.call(args.headers, key)) {
						xhr.setRequestHeader(key, args.headers[key]);
					}
				}

				xhr.onreadystatechange = function(ev) {
					// Don't throw errors on xhr.abort().
					if (aborted) return

					if (ev.target.readyState === 4) {
						try {
							var success = (ev.target.status >= 200 && ev.target.status < 300) || ev.target.status === 304 || (/^file:\/\//i).test(url);
							// When the response type isn't "" or "text",
							// `xhr.responseText` is the wrong thing to use.
							// Browsers do the right thing and throw here, and we
							// should honor that and do the right thing by
							// preferring `xhr.response` where possible/practical.
							var response = ev.target.response, message;

							if (responseType === "json") {
								// For IE and Edge, which don't implement
								// `responseType: "json"`.
								if (!ev.target.responseType && typeof args.extract !== "function") response = JSON.parse(ev.target.responseText);
							} else if (!responseType || responseType === "text") {
								// Only use this default if it's text. If a parsed
								// document is needed on old IE and friends (all
								// unsupported), the user should use a custom
								// `config` instead. They're already using this at
								// their own risk.
								if (response == null) response = ev.target.responseText;
							}

							if (typeof args.extract === "function") {
								response = args.extract(ev.target, args);
								success = true;
							} else if (typeof args.deserialize === "function") {
								response = args.deserialize(response);
							}
							if (success) resolve(response);
							else {
								try { message = ev.target.responseText; }
								catch (e) { message = response; }
								var error = new Error(message);
								error.code = ev.target.status;
								error.response = response;
								reject(error);
							}
						}
						catch (e) {
							reject(e);
						}
					}
				};

				if (typeof args.config === "function") {
					xhr = args.config(xhr, args, url) || xhr;

					// Propagate the `abort` to any replacement XHR as well.
					if (xhr !== original) {
						replacedAbort = xhr.abort;
						xhr.abort = function() {
							aborted = true;
							replacedAbort.call(this);
						};
					}
				}

				if (body == null) xhr.send();
				else if (typeof args.serialize === "function") xhr.send(args.serialize(body));
				else if (body instanceof $window.FormData) xhr.send(body);
				else xhr.send(JSON.stringify(body));
			}),
			jsonp: makeRequest(function(url, args, resolve, reject) {
				var callbackName = args.callbackName || "_mithril_" + Math.round(Math.random() * 1e16) + "_" + callbackCount++;
				var script = $window.document.createElement("script");
				$window[callbackName] = function(data) {
					delete $window[callbackName];
					script.parentNode.removeChild(script);
					resolve(data);
				};
				script.onerror = function() {
					delete $window[callbackName];
					script.parentNode.removeChild(script);
					reject(new Error("JSONP request failed"));
				};
				script.src = url + (url.indexOf("?") < 0 ? "?" : "&") +
					encodeURIComponent(args.callbackKey || "callback") + "=" +
					encodeURIComponent(callbackName);
				$window.document.documentElement.appendChild(script);
			}),
		}
	};

	var request$1 = request(window, promise, mountRedraw$1.redraw);

	var parse = function(string) {
		if (string === "" || string == null) return {}
		if (string.charAt(0) === "?") string = string.slice(1);

		var entries = string.split("&"), counters = {}, data = {};
		for (var i = 0; i < entries.length; i++) {
			var entry = entries[i].split("=");
			var key = decodeURIComponent(entry[0]);
			var value = entry.length === 2 ? decodeURIComponent(entry[1]) : "";

			if (value === "true") value = true;
			else if (value === "false") value = false;

			var levels = key.split(/\]\[?|\[/);
			var cursor = data;
			if (key.indexOf("[") > -1) levels.pop();
			for (var j = 0; j < levels.length; j++) {
				var level = levels[j], nextLevel = levels[j + 1];
				var isNumber = nextLevel == "" || !isNaN(parseInt(nextLevel, 10));
				if (level === "") {
					var key = levels.slice(0, j).join();
					if (counters[key] == null) {
						counters[key] = Array.isArray(cursor) ? cursor.length : 0;
					}
					level = counters[key]++;
				}
				// Disallow direct prototype pollution
				else if (level === "__proto__") break
				if (j === levels.length - 1) cursor[level] = value;
				else {
					// Read own properties exclusively to disallow indirect
					// prototype pollution
					var desc = Object.getOwnPropertyDescriptor(cursor, level);
					if (desc != null) desc = desc.value;
					if (desc == null) cursor[level] = desc = isNumber ? [] : {};
					cursor = desc;
				}
			}
		}
		return data
	};

	// Returns `{path, params}` from `url`
	var parse$1 = function(url) {
		var queryIndex = url.indexOf("?");
		var hashIndex = url.indexOf("#");
		var queryEnd = hashIndex < 0 ? url.length : hashIndex;
		var pathEnd = queryIndex < 0 ? queryEnd : queryIndex;
		var path = url.slice(0, pathEnd).replace(/\/{2,}/g, "/");

		if (!path) path = "/";
		else {
			if (path[0] !== "/") path = "/" + path;
			if (path.length > 1 && path[path.length - 1] === "/") path = path.slice(0, -1);
		}
		return {
			path: path,
			params: queryIndex < 0
				? {}
				: parse(url.slice(queryIndex + 1, queryEnd)),
		}
	};

	// Compiles a template into a function that takes a resolved path (without query
	// strings) and returns an object containing the template parameters with their
	// parsed values. This expects the input of the compiled template to be the
	// output of `parsePathname`. Note that it does *not* remove query parameters
	// specified in the template.
	var compileTemplate = function(template) {
		var templateData = parse$1(template);
		var templateKeys = Object.keys(templateData.params);
		var keys = [];
		var regexp = new RegExp("^" + templateData.path.replace(
			// I escape literal text so people can use things like `:file.:ext` or
			// `:lang-:locale` in routes. This is all merged into one pass so I
			// don't also accidentally escape `-` and make it harder to detect it to
			// ban it from template parameters.
			/:([^\/.-]+)(\.{3}|\.(?!\.)|-)?|[\\^$*+.()|\[\]{}]/g,
			function(m, key, extra) {
				if (key == null) return "\\" + m
				keys.push({k: key, r: extra === "..."});
				if (extra === "...") return "(.*)"
				if (extra === ".") return "([^/]+)\\."
				return "([^/]+)" + (extra || "")
			}
		) + "$");
		return function(data) {
			// First, check the params. Usually, there isn't any, and it's just
			// checking a static set.
			for (var i = 0; i < templateKeys.length; i++) {
				if (templateData.params[templateKeys[i]] !== data.params[templateKeys[i]]) return false
			}
			// If no interpolations exist, let's skip all the ceremony
			if (!keys.length) return regexp.test(data.path)
			var values = regexp.exec(data.path);
			if (values == null) return false
			for (var i = 0; i < keys.length; i++) {
				data.params[keys[i].k] = keys[i].r ? values[i + 1] : decodeURIComponent(values[i + 1]);
			}
			return true
		}
	};

	var sentinel = {};

	var router = function($window, mountRedraw) {
		var fireAsync;

		function setPath(path, data, options) {
			path = build$1(path, data);
			if (fireAsync != null) {
				fireAsync();
				var state = options ? options.state : null;
				var title = options ? options.title : null;
				if (options && options.replace) $window.history.replaceState(state, title, route.prefix + path);
				else $window.history.pushState(state, title, route.prefix + path);
			}
			else {
				$window.location.href = route.prefix + path;
			}
		}

		var currentResolver = sentinel, component, attrs, currentPath, lastUpdate;

		var SKIP = route.SKIP = {};

		function route(root, defaultRoute, routes) {
			if (root == null) throw new Error("Ensure the DOM element that was passed to `m.route` is not undefined")
			// 0 = start
			// 1 = init
			// 2 = ready
			var state = 0;

			var compiled = Object.keys(routes).map(function(route) {
				if (route[0] !== "/") throw new SyntaxError("Routes must start with a `/`")
				if ((/:([^\/\.-]+)(\.{3})?:/).test(route)) {
					throw new SyntaxError("Route parameter names must be separated with either `/`, `.`, or `-`")
				}
				return {
					route: route,
					component: routes[route],
					check: compileTemplate(route),
				}
			});
			var callAsync = typeof setImmediate === "function" ? setImmediate : setTimeout;
			var p = promise.resolve();
			var scheduled = false;
			var onremove;

			fireAsync = null;

			if (defaultRoute != null) {
				var defaultData = parse$1(defaultRoute);

				if (!compiled.some(function (i) { return i.check(defaultData) })) {
					throw new ReferenceError("Default route doesn't match any known routes")
				}
			}

			function resolveRoute() {
				scheduled = false;
				// Consider the pathname holistically. The prefix might even be invalid,
				// but that's not our problem.
				var prefix = $window.location.hash;
				if (route.prefix[0] !== "#") {
					prefix = $window.location.search + prefix;
					if (route.prefix[0] !== "?") {
						prefix = $window.location.pathname + prefix;
						if (prefix[0] !== "/") prefix = "/" + prefix;
					}
				}
				// This seemingly useless `.concat()` speeds up the tests quite a bit,
				// since the representation is consistently a relatively poorly
				// optimized cons string.
				var path = prefix.concat()
					.replace(/(?:%[a-f89][a-f0-9])+/gim, decodeURIComponent)
					.slice(route.prefix.length);
				var data = parse$1(path);

				assign(data.params, $window.history.state);

				function fail() {
					if (path === defaultRoute) throw new Error("Could not resolve default route " + defaultRoute)
					setPath(defaultRoute, null, {replace: true});
				}

				loop(0);
				function loop(i) {
					// 0 = init
					// 1 = scheduled
					// 2 = done
					for (; i < compiled.length; i++) {
						if (compiled[i].check(data)) {
							var payload = compiled[i].component;
							var matchedRoute = compiled[i].route;
							var localComp = payload;
							var update = lastUpdate = function(comp) {
								if (update !== lastUpdate) return
								if (comp === SKIP) return loop(i + 1)
								component = comp != null && (typeof comp.view === "function" || typeof comp === "function")? comp : "div";
								attrs = data.params, currentPath = path, lastUpdate = null;
								currentResolver = payload.render ? payload : null;
								if (state === 2) mountRedraw.redraw();
								else {
									state = 2;
									mountRedraw.redraw.sync();
								}
							};
							// There's no understating how much I *wish* I could
							// use `async`/`await` here...
							if (payload.view || typeof payload === "function") {
								payload = {};
								update(localComp);
							}
							else if (payload.onmatch) {
								p.then(function () {
									return payload.onmatch(data.params, path, matchedRoute)
								}).then(update, fail);
							}
							else update("div");
							return
						}
					}
					fail();
				}
			}

			// Set it unconditionally so `m.route.set` and `m.route.Link` both work,
			// even if neither `pushState` nor `hashchange` are supported. It's
			// cleared if `hashchange` is used, since that makes it automatically
			// async.
			fireAsync = function() {
				if (!scheduled) {
					scheduled = true;
					callAsync(resolveRoute);
				}
			};

			if (typeof $window.history.pushState === "function") {
				onremove = function() {
					$window.removeEventListener("popstate", fireAsync, false);
				};
				$window.addEventListener("popstate", fireAsync, false);
			} else if (route.prefix[0] === "#") {
				fireAsync = null;
				onremove = function() {
					$window.removeEventListener("hashchange", resolveRoute, false);
				};
				$window.addEventListener("hashchange", resolveRoute, false);
			}

			return mountRedraw.mount(root, {
				onbeforeupdate: function() {
					state = state ? 2 : 1;
					return !(!state || sentinel === currentResolver)
				},
				oncreate: resolveRoute,
				onremove: onremove,
				view: function() {
					if (!state || sentinel === currentResolver) return
					// Wrap in a fragment to preserve existing key semantics
					var vnode$1 = [vnode(component, attrs.key, attrs)];
					if (currentResolver) vnode$1 = currentResolver.render(vnode$1[0]);
					return vnode$1
				},
			})
		}
		route.set = function(path, data, options) {
			if (lastUpdate != null) {
				options = options || {};
				options.replace = true;
			}
			lastUpdate = null;
			setPath(path, data, options);
		};
		route.get = function() {return currentPath};
		route.prefix = "#!";
		route.Link = {
			view: function(vnode) {
				var options = vnode.attrs.options;
				// Remove these so they don't get overwritten
				var attrs = {}, onclick, href;
				assign(attrs, vnode.attrs);
				// The first two are internal, but the rest are magic attributes
				// that need censored to not screw up rendering.
				attrs.selector = attrs.options = attrs.key = attrs.oninit =
				attrs.oncreate = attrs.onbeforeupdate = attrs.onupdate =
				attrs.onbeforeremove = attrs.onremove = null;

				// Do this now so we can get the most current `href` and `disabled`.
				// Those attributes may also be specified in the selector, and we
				// should honor that.
				var child = hyperscript_1(vnode.attrs.selector || "a", attrs, vnode.children);

				// Let's provide a *right* way to disable a route link, rather than
				// letting people screw up accessibility on accident.
				//
				// The attribute is coerced so users don't get surprised over
				// `disabled: 0` resulting in a button that's somehow routable
				// despite being visibly disabled.
				if (child.attrs.disabled = Boolean(child.attrs.disabled)) {
					child.attrs.href = null;
					child.attrs["aria-disabled"] = "true";
					// If you *really* do want to do this on a disabled link, use
					// an `oncreate` hook to add it.
					child.attrs.onclick = null;
				} else {
					onclick = child.attrs.onclick;
					href = child.attrs.href;
					child.attrs.href = route.prefix + href;
					child.attrs.onclick = function(e) {
						var result;
						if (typeof onclick === "function") {
							result = onclick.call(e.currentTarget, e);
						} else if (onclick == null || typeof onclick !== "object") ; else if (typeof onclick.handleEvent === "function") {
							onclick.handleEvent(e);
						}

						// Adapted from React Router's implementation:
						// https://github.com/ReactTraining/react-router/blob/520a0acd48ae1b066eb0b07d6d4d1790a1d02482/packages/react-router-dom/modules/Link.js
						//
						// Try to be flexible and intuitive in how we handle links.
						// Fun fact: links aren't as obvious to get right as you
						// would expect. There's a lot more valid ways to click a
						// link than this, and one might want to not simply click a
						// link, but right click or command-click it to copy the
						// link target, etc. Nope, this isn't just for blind people.
						if (
							// Skip if `onclick` prevented default
							result !== false && !e.defaultPrevented &&
							// Ignore everything but left clicks
							(e.button === 0 || e.which === 0 || e.which === 1) &&
							// Let the browser handle `target=_blank`, etc.
							(!e.currentTarget.target || e.currentTarget.target === "_self") &&
							// No modifier keys
							!e.ctrlKey && !e.metaKey && !e.shiftKey && !e.altKey
						) {
							e.preventDefault();
							e.redraw = false;
							route.set(href, null, options);
						}
					};
				}
				return child
			},
		};
		route.param = function(key) {
			return attrs && key != null ? attrs[key] : attrs
		};

		return route
	};

	var route = router(window, mountRedraw$1);

	var m = function m() { return hyperscript_1$1.apply(this, arguments) };
	m.m = hyperscript_1$1;
	m.trust = hyperscript_1$1.trust;
	m.fragment = hyperscript_1$1.fragment;
	m.mount = mountRedraw$1.mount;
	m.route = route;
	m.render = render$1;
	m.redraw = mountRedraw$1.redraw;
	m.request = request$1.request;
	m.jsonp = request$1.jsonp;
	m.parseQueryString = parse;
	m.buildQueryString = build;
	m.parsePathname = parse$1;
	m.buildPathname = build$1;
	m.vnode = vnode;
	m.PromisePolyfill = polyfill;

	var mithril = m;

	var input = createCommonjsModule(function (module, exports) {
	// handler module for physical keyboard events
	Object.defineProperty(exports, "__esModule", { value: true });
	function default_1(_a) {
	    var onKeyDown = _a.onKeyDown, onKeyUp = _a.onKeyUp, onShift = _a.onShift, onAlt = _a.onAlt, onEnter = _a.onEnter;
	    var AltState = 'down', ShiftState = 'down', isAltPressed = false, isShiftPressed = false, isInit = false;
	    init();
	    return {
	        init: init,
	        destroy: destroy,
	        isAltPressed: function () { return isAltPressed; },
	        isShiftPressed: function () { return isShiftPressed; }
	    };
	    function init() {
	        if (isInit)
	            return;
	        isInit = true;
	        window.addEventListener('keyup', eventOnKeyUp, false);
	        window.addEventListener('keydown', eventOnKeyDown, false);
	    }
	    function destroy() {
	        isInit = false;
	        window.removeEventListener('keydown', eventOnKeyDown, false);
	        window.removeEventListener('keyup', eventOnKeyUp, false);
	    }
	    function eventOnKeyUp(e) {
	        if (onKeyUp) {
	            onKeyUp(keycodeMap(e, 'up'));
	        }
	    }
	    function eventOnKeyDown(e) {
	        if (onKeyDown) {
	            onKeyDown(keycodeMap(e, 'down'));
	        }
	    }
	    function keycodeMap(e, upOrDown) {
	        var IsAlt, IsShift;
	        switch (e.keyCode) {
	            case 16:
	                isShiftPressed = (IsShift = ShiftState = upOrDown) === 'down';
	                onShift && onShift(upOrDown);
	                break;
	            case 18:
	                isAltPressed = (IsAlt = AltState = upOrDown) === 'down';
	                onAlt && onAlt(upOrDown);
	                break;
	            case 13:
	                onEnter && onEnter(upOrDown);
	                break;
	        }
	        return Object.assign(e, {
	            IsShift: IsShift,
	            ShiftState: ShiftState,
	            IsAlt: IsAlt,
	            AltState: AltState
	        });
	    }
	}
	exports.default = default_1;
	});

	var build$2 = createCommonjsModule(function (module, exports) {
	/*!
	 * 
	 *   simple-keyboard v2.29.72
	 *   https://github.com/hodgef/simple-keyboard
	 * 
	 *   Copyright (c) Francisco Hodge (https://github.com/hodgef)
	 * 
	 *   This source code is licensed under the MIT license found in the
	 *   LICENSE file in the root directory of this source tree.
	 *   
	 */
	!function(t,e){module.exports=e();}(window,(function(){return function(t){var e={};function __webpack_require__(n){if(e[n])return e[n].exports;var o=e[n]={i:n,l:!1,exports:{}};return t[n].call(o.exports,o,o.exports,__webpack_require__),o.l=!0,o.exports}return __webpack_require__.m=t,__webpack_require__.c=e,__webpack_require__.d=function(t,e,n){__webpack_require__.o(t,e)||Object.defineProperty(t,e,{enumerable:!0,get:n});},__webpack_require__.r=function(t){"undefined"!==typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0});},__webpack_require__.t=function(t,e){if(1&e&&(t=__webpack_require__(t)),8&e)return t;if(4&e&&"object"===typeof t&&t&&t.__esModule)return t;var n=Object.create(null);if(__webpack_require__.r(n),Object.defineProperty(n,"default",{enumerable:!0,value:t}),2&e&&"string"!=typeof t)for(var o in t)__webpack_require__.d(n,o,function(e){return t[e]}.bind(null,o));return n},__webpack_require__.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return __webpack_require__.d(e,"a",e),e},__webpack_require__.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},__webpack_require__.p="",__webpack_require__(__webpack_require__.s=0)}([function(t,e,n){t.exports=n(2);},function(t,e,n){},function(t,e,n){n.r(e);n(1);function _createForOfIteratorHelper(t,e){var n;if("undefined"===typeof Symbol||null==t[Symbol.iterator]){if(Array.isArray(t)||(n=function(t,e){if(!t)return;if("string"===typeof t)return _arrayLikeToArray(t,e);var n=Object.prototype.toString.call(t).slice(8,-1);"Object"===n&&t.constructor&&(n=t.constructor.name);if("Map"===n||"Set"===n)return Array.from(t);if("Arguments"===n||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n))return _arrayLikeToArray(t,e)}(t))||e&&t&&"number"===typeof t.length){n&&(t=n);var o=0,F=function(){};return {s:F,n:function(){return o>=t.length?{done:!0}:{done:!1,value:t[o++]}},e:function(t){throw t},f:F}}throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}var i,s=!0,a=!1;return {s:function(){n=t[Symbol.iterator]();},n:function(){var t=n.next();return s=t.done,t},e:function(t){a=!0,i=t;},f:function(){try{s||null==n.return||n.return();}finally{if(a)throw i}}}}function _arrayLikeToArray(t,e){(null==e||e>t.length)&&(e=t.length);for(var n=0,o=new Array(e);n<e;n++)o[n]=t[n];return o}function _typeof(t){return (_typeof="function"===typeof Symbol&&"symbol"===typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"===typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t})(t)}function _defineProperties(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o);}}var o=function(){function Utilities(t){var e=t.getOptions,n=t.getCaretPosition,o=t.dispatch;!function(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}(this,Utilities),this.getOptions=e,this.getCaretPosition=n,this.dispatch=o,Utilities.bindMethods(Utilities,this);}var t,e,n;return t=Utilities,n=[{key:"bindMethods",value:function(t,e){var n,o=_createForOfIteratorHelper(Object.getOwnPropertyNames(t.prototype));try{for(o.s();!(n=o.n()).done;){var i=n.value;"constructor"===i||"bindMethods"===i||(e[i]=e[i].bind(e));}}catch(s){o.e(s);}finally{o.f();}}}],(e=[{key:"getButtonClass",value:function(t){var e=t.includes("{")&&t.includes("}")&&"{//}"!==t?"functionBtn":"standardBtn",n=t.replace("{","").replace("}",""),o="";return "standardBtn"!==e&&(o=" hg-button-".concat(n)),"hg-".concat(e).concat(o)}},{key:"getDefaultDiplay",value:function(){return {"{bksp}":"backspace","{backspace}":"backspace","{enter}":"< enter","{shift}":"shift","{shiftleft}":"shift","{shiftright}":"shift","{alt}":"alt","{s}":"shift","{tab}":"tab","{lock}":"caps","{capslock}":"caps","{accept}":"Submit","{space}":" ","{//}":" ","{esc}":"esc","{escape}":"esc","{f1}":"f1","{f2}":"f2","{f3}":"f3","{f4}":"f4","{f5}":"f5","{f6}":"f6","{f7}":"f7","{f8}":"f8","{f9}":"f9","{f10}":"f10","{f11}":"f11","{f12}":"f12","{numpaddivide}":"/","{numlock}":"lock","{arrowup}":"\u2191","{arrowleft}":"\u2190","{arrowdown}":"\u2193","{arrowright}":"\u2192","{prtscr}":"print","{scrolllock}":"scroll","{pause}":"pause","{insert}":"ins","{home}":"home","{pageup}":"up","{delete}":"del","{end}":"end","{pagedown}":"down","{numpadmultiply}":"*","{numpadsubtract}":"-","{numpadadd}":"+","{numpadenter}":"enter","{period}":".","{numpaddecimal}":".","{numpad0}":"0","{numpad1}":"1","{numpad2}":"2","{numpad3}":"3","{numpad4}":"4","{numpad5}":"5","{numpad6}":"6","{numpad7}":"7","{numpad8}":"8","{numpad9}":"9"}}},{key:"getButtonDisplayName",value:function(t,e,n){return (e=n?Object.assign({},this.getDefaultDiplay(),e):e||this.getDefaultDiplay())[t]||t}},{key:"getUpdatedInput",value:function(t,e,n,o){var i=this.getOptions(),s=e;return ("{bksp}"===t||"{backspace}"===t)&&s.length>0?s=this.removeAt(s,n,o):"{space}"===t?s=this.addStringAt(s," ",n,o):"{tab}"!==t||"boolean"===typeof i.tabCharOnTab&&!1===i.tabCharOnTab?"{enter}"!==t&&"{numpadenter}"!==t||!i.newLineOnEnter?t.includes("numpad")&&Number.isInteger(Number(t[t.length-2]))?s=this.addStringAt(s,t[t.length-2],n,o):"{numpaddivide}"===t?s=this.addStringAt(s,"/",n,o):"{numpadmultiply}"===t?s=this.addStringAt(s,"*",n,o):"{numpadsubtract}"===t?s=this.addStringAt(s,"-",n,o):"{numpadadd}"===t?s=this.addStringAt(s,"+",n,o):"{numpaddecimal}"===t?s=this.addStringAt(s,".",n,o):"{"===t||"}"===t?s=this.addStringAt(s,t,n,o):t.includes("{")||t.includes("}")||(s=this.addStringAt(s,t,n,o)):s=this.addStringAt(s,"\n",n,o):s=this.addStringAt(s,"\t",n,o),s}},{key:"updateCaretPos",value:function(t,e){var n=this.updateCaretPosAction(t,e);this.dispatch((function(t){t.caretPosition=n;}));}},{key:"updateCaretPosAction",value:function(t,e){var n=this.getOptions(),o=this.getCaretPosition();return e?o>0&&(o-=t):o+=t,n.debug&&console.log("Caret at:",o,"(".concat(this.keyboardDOMClass,")")),o}},{key:"addStringAt",value:function(t,e,n,o){var i;return n||0===n?(i=[t.slice(0,n),e,t.slice(n)].join(""),this.isMaxLengthReached()||o&&this.updateCaretPos(e.length)):i=t+e,i}},{key:"removeAt",value:function(t,e,n){var o;if(0===this.getCaretPosition())return t;var i=/([\uD800-\uDBFF][\uDC00-\uDFFF])/g;return e&&e>=0?t.substring(e-2,e).match(i)?(o=t.substr(0,e-2)+t.substr(e),n&&this.updateCaretPos(2,!0)):(o=t.substr(0,e-1)+t.substr(e),n&&this.updateCaretPos(1,!0)):t.slice(-2).match(i)?(o=t.slice(0,-2),n&&this.updateCaretPos(2,!0)):(o=t.slice(0,-1),n&&this.updateCaretPos(1,!0)),o}},{key:"handleMaxLength",value:function(t,e){var n=this.getOptions(),o=n.maxLength,i=t[n.inputName],s=e.length-1>=o;if(e.length<=i.length)return !1;if(Number.isInteger(o))return n.debug&&console.log("maxLength (num) reached:",s),s?(this.maxLengthReached=!0,!0):(this.maxLengthReached=!1,!1);if("object"===_typeof(o)){var a=i.length===o[n.inputName];return n.debug&&console.log("maxLength (obj) reached:",a),a?(this.maxLengthReached=!0,!0):(this.maxLengthReached=!1,!1)}}},{key:"isMaxLengthReached",value:function(){return Boolean(this.maxLengthReached)}},{key:"isTouchDevice",value:function(){return "ontouchstart"in window||navigator.maxTouchPoints}},{key:"pointerEventsSupported",value:function(){return window.PointerEvent}},{key:"camelCase",value:function(t){return !!t&&t.toLowerCase().trim().split(/[.\-_\s]/g).reduce((function(t,e){return e.length?t+e[0].toUpperCase()+e.slice(1):t}))}}])&&_defineProperties(t.prototype,e),n&&_defineProperties(t,n),Utilities}();function PhysicalKeyboard_defineProperties(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o);}}var i=function(){function PhysicalKeyboard(t){var e=t.dispatch,n=t.getOptions;!function(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}(this,PhysicalKeyboard),this.dispatch=e,this.getOptions=n,o.bindMethods(PhysicalKeyboard,this);}var t,e;return t=PhysicalKeyboard,(e=[{key:"handleHighlightKeyDown",value:function(t){var e=this.getOptions(),n=this.getSimpleKeyboardLayoutKey(t);this.dispatch((function(t){var o=t.getButtonElement(n)||t.getButtonElement("{".concat(n,"}"));o&&(o.style.backgroundColor=e.physicalKeyboardHighlightBgColor||"#9ab4d0",o.style.color=e.physicalKeyboardHighlightTextColor||"white");}));}},{key:"handleHighlightKeyUp",value:function(t){var e=this.getSimpleKeyboardLayoutKey(t);this.dispatch((function(t){var n=t.getButtonElement(e)||t.getButtonElement("{".concat(e,"}"));n&&n.removeAttribute&&n.removeAttribute("style");}));}},{key:"getSimpleKeyboardLayoutKey",value:function(t){var e;return ((e=t.code.includes("Numpad")||t.code.includes("Shift")||t.code.includes("Space")||t.code.includes("Backspace")||t.code.includes("Control")||t.code.includes("Alt")||t.code.includes("Meta")?t.code:t.key)!==e.toUpperCase()||"F"===t.code[0]&&Number.isInteger(Number(t.code[1]))&&t.code.length<=3)&&(e=e.toLowerCase()),e}}])&&PhysicalKeyboard_defineProperties(t.prototype,e),PhysicalKeyboard}();function _toConsumableArray(t){return function(t){if(Array.isArray(t))return Keyboard_arrayLikeToArray(t)}(t)||function(t){if("undefined"!==typeof Symbol&&Symbol.iterator in Object(t))return Array.from(t)}(t)||function(t,e){if(!t)return;if("string"===typeof t)return Keyboard_arrayLikeToArray(t,e);var n=Object.prototype.toString.call(t).slice(8,-1);"Object"===n&&t.constructor&&(n=t.constructor.name);if("Map"===n||"Set"===n)return Array.from(t);if("Arguments"===n||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n))return Keyboard_arrayLikeToArray(t,e)}(t)||function(){throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")}()}function Keyboard_arrayLikeToArray(t,e){(null==e||e>t.length)&&(e=t.length);for(var n=0,o=new Array(e);n<e;n++)o[n]=t[n];return o}function Keyboard_classCallCheck(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}function Keyboard_defineProperties(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o);}}function _defineProperty(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}var s=function(){function SimpleKeyboard(){var t=this;Keyboard_classCallCheck(this,SimpleKeyboard),_defineProperty(this,"handleParams",(function(t){var e,n,o;if("string"===typeof t[0])e=t[0].split(".").join(""),n=document.querySelector(".".concat(e)),o=t[1];else if(t[0]instanceof HTMLDivElement){if(!t[0].className)throw console.warn("Any DOM element passed as parameter must have a class."),new Error("KEYBOARD_DOM_CLASS_ERROR");e=t[0].className.split(" ")[0],n=t[0],o=t[1];}else e="simple-keyboard",n=document.querySelector(".".concat(e)),o=t[0];return {keyboardDOMClass:e,keyboardDOM:n,options:o}})),_defineProperty(this,"getOptions",(function(){return t.options})),_defineProperty(this,"getCaretPosition",(function(){return t.caretPosition})),_defineProperty(this,"registerModule",(function(e,n){t.modules[e]||(t.modules[e]={}),n(t.modules[e]);})),_defineProperty(this,"getKeyboardClassString",(function(){for(var e=arguments.length,n=new Array(e),o=0;o<e;o++)n[o]=arguments[o];var i=[t.keyboardDOMClass].concat(n).filter((function(t){return !!t}));return i.join(" ")}));for(var e=arguments.length,n=new Array(e),s=0;s<e;s++)n[s]=arguments[s];var a=this.handleParams(n),r=a.keyboardDOMClass,u=a.keyboardDOM,c=a.options,l=void 0===c?{}:c;if(this.utilities=new o({getOptions:this.getOptions,getCaretPosition:this.getCaretPosition,dispatch:this.dispatch}),this.caretPosition=null,this.keyboardDOM=u,this.options=l,this.options.layoutName=this.options.layoutName||"default",this.options.theme=this.options.theme||"hg-theme-default",this.options.inputName=this.options.inputName||"default",this.options.preventMouseDownDefault=this.options.preventMouseDownDefault||!1,this.keyboardPluginClasses="",o.bindMethods(SimpleKeyboard,this),this.input={},this.input[this.options.inputName]="",this.keyboardDOMClass=r,this.buttonElements={},window.SimpleKeyboardInstances||(window.SimpleKeyboardInstances={}),this.currentInstanceName=this.utilities.camelCase(this.keyboardDOMClass),window.SimpleKeyboardInstances[this.currentInstanceName]=this,this.allKeyboardInstances=window.SimpleKeyboardInstances,this.keyboardInstanceNames=Object.keys(window.SimpleKeyboardInstances),this.isFirstKeyboardInstance=this.keyboardInstanceNames[0]===this.currentInstanceName,this.physicalKeyboard=new i({dispatch:this.dispatch,getOptions:this.getOptions}),!this.keyboardDOM)throw console.warn('".'.concat(r,'" was not found in the DOM.')),new Error("KEYBOARD_DOM_ERROR");this.render(),this.modules={},this.loadModules();}var t,e;return t=SimpleKeyboard,(e=[{key:"handleButtonClicked",value:function(t){var e=this.options.debug;if("{//}"===t)return !1;"function"===typeof this.options.onKeyPress&&this.options.onKeyPress(t),this.input[this.options.inputName]||(this.input[this.options.inputName]="");var n=this.utilities.getUpdatedInput(t,this.input[this.options.inputName],this.caretPosition);if(this.input[this.options.inputName]!==n&&(!this.options.inputPattern||this.options.inputPattern&&this.inputPatternIsValid(n))){if(this.options.maxLength&&this.utilities.handleMaxLength(this.input,n))return !1;this.input[this.options.inputName]=this.utilities.getUpdatedInput(t,this.input[this.options.inputName],this.caretPosition,!0),e&&console.log("Input changed:",this.input),this.options.syncInstanceInputs&&this.syncInstanceInputs(),"function"===typeof this.options.onChange&&this.options.onChange(this.input[this.options.inputName]),"function"===typeof this.options.onChangeAll&&this.options.onChangeAll(this.input);}e&&console.log("Key pressed:",t);}},{key:"handleButtonMouseDown",value:function(t,e){var n=this;this.options.preventMouseDownDefault&&e.preventDefault(),this.options.stopMouseDownPropagation&&e.stopPropagation(),e&&e.target.classList.add(this.activeButtonClass),this.isMouseHold=!0,this.holdInteractionTimeout&&clearTimeout(this.holdInteractionTimeout),this.holdTimeout&&clearTimeout(this.holdTimeout),this.options.disableButtonHold||(this.holdTimeout=setTimeout((function(){(n.isMouseHold&&(!t.includes("{")&&!t.includes("}")||"{delete}"===t||"{backspace}"===t||"{bksp}"===t||"{space}"===t||"{tab}"===t)||"{arrowright}"===t||"{arrowleft}"===t||"{arrowup}"===t||"{arrowdown}"===t)&&(n.options.debug&&console.log("Button held:",t),n.handleButtonHold(t,e)),clearTimeout(n.holdTimeout);}),500));}},{key:"handleButtonMouseUp",value:function(t){var e=this;this.recurseButtons((function(t){t.classList.remove(e.activeButtonClass);})),this.isMouseHold=!1,this.holdInteractionTimeout&&clearTimeout(this.holdInteractionTimeout),t&&"function"===typeof this.options.onKeyReleased&&this.options.onKeyReleased(t);}},{key:"handleKeyboardContainerMouseDown",value:function(t){this.options.preventMouseDownDefault&&t.preventDefault();}},{key:"handleButtonHold",value:function(t){var e=this;this.holdInteractionTimeout&&clearTimeout(this.holdInteractionTimeout),this.holdInteractionTimeout=setTimeout((function(){e.isMouseHold?(e.handleButtonClicked(t),e.handleButtonHold(t)):clearTimeout(e.holdInteractionTimeout);}),100);}},{key:"syncInstanceInputs",value:function(){var t=this;this.dispatch((function(e){e.replaceInput(t.input),e.caretPosition=t.caretPosition;}));}},{key:"clearInput",value:function(t){t=t||this.options.inputName,this.input[t]="",this.caretPosition=0,this.options.syncInstanceInputs&&this.syncInstanceInputs();}},{key:"getInput",value:function(t){return t=t||this.options.inputName,this.options.syncInstanceInputs&&this.syncInstanceInputs(),this.input[t]}},{key:"setInput",value:function(t,e){e=e||this.options.inputName,this.input[e]=t,this.options.syncInstanceInputs&&this.syncInstanceInputs();}},{key:"replaceInput",value:function(t){this.input=t;}},{key:"setOptions",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},e=this.changedOptions(t);this.options=Object.assign(this.options,t),e.length&&(this.options.debug&&console.log("changedOptions",e),this.onSetOptions(t),this.render());}},{key:"changedOptions",value:function(t){var e=this;return Object.keys(t).filter((function(n){return JSON.stringify(t[n])!==JSON.stringify(e.options[n])}))}},{key:"onSetOptions",value:function(t){t.inputName&&(this.options.debug&&console.log("inputName changed. caretPosition reset."),this.caretPosition=null);}},{key:"clear",value:function(){this.keyboardDOM.innerHTML="",this.keyboardDOM.className=this.keyboardDOMClass,this.buttonElements={};}},{key:"dispatch",value:function(t){if(!window.SimpleKeyboardInstances)throw console.warn("SimpleKeyboardInstances is not defined. Dispatch cannot be called."),new Error("INSTANCES_VAR_ERROR");return Object.keys(window.SimpleKeyboardInstances).forEach((function(e){t(window.SimpleKeyboardInstances[e],e);}))}},{key:"addButtonTheme",value:function(t,e){var n=this;if(!e||!t)return !1;t.split(" ").forEach((function(o){e.split(" ").forEach((function(e){n.options.buttonTheme||(n.options.buttonTheme=[]);var i=!1;n.options.buttonTheme.map((function(t){if(t.class.split(" ").includes(e)){i=!0;var n=t.buttons.split(" ");n.includes(o)||(i=!0,n.push(o),t.buttons=n.join(" "));}return t})),i||n.options.buttonTheme.push({class:e,buttons:t});}));})),this.render();}},{key:"removeButtonTheme",value:function(t,e){var n=this;if(!t&&!e)return this.options.buttonTheme=[],this.render(),!1;t&&Array.isArray(this.options.buttonTheme)&&this.options.buttonTheme.length&&(t.split(" ").forEach((function(t){n.options.buttonTheme.map((function(o,i){if(e&&e.includes(o.class)||!e){var s=o.buttons.split(" ").filter((function(e){return e!==t}));s.length?o.buttons=s.join(" "):(n.options.buttonTheme.splice(i,1),o=null);}return o}));})),this.render());}},{key:"getButtonElement",value:function(t){var e,n=this.buttonElements[t];return n&&(e=n.length>1?n:n[0]),e}},{key:"inputPatternIsValid",value:function(t){var e,n=this.options.inputPattern;if((e=n instanceof RegExp?n:n[this.options.inputName])&&t){var o=e.test(t);return this.options.debug&&console.log('inputPattern ("'.concat(e,'"): ').concat(o?"passed":"did not pass!")),o}return !0}},{key:"setEventListeners",value:function(){!this.isFirstKeyboardInstance&&this.allKeyboardInstances||(this.options.debug&&console.log("Caret handling started (".concat(this.keyboardDOMClass,")")),document.addEventListener("keyup",this.handleKeyUp),document.addEventListener("keydown",this.handleKeyDown),document.addEventListener("mouseup",this.handleMouseUp),document.addEventListener("touchend",this.handleTouchEnd));}},{key:"handleKeyUp",value:function(t){this.caretEventHandler(t),this.options.physicalKeyboardHighlight&&this.physicalKeyboard.handleHighlightKeyUp(t);}},{key:"handleKeyDown",value:function(t){this.options.physicalKeyboardHighlight&&this.physicalKeyboard.handleHighlightKeyDown(t);}},{key:"handleMouseUp",value:function(t){this.caretEventHandler(t);}},{key:"handleTouchEnd",value:function(t){this.caretEventHandler(t);}},{key:"caretEventHandler",value:function(t){var e;t.target.tagName&&(e=t.target.tagName.toLowerCase()),this.dispatch((function(n){n.isMouseHold&&(n.isMouseHold=!1),"textarea"!==e&&"input"!==e||n.options.disableCaretPositioning?n.options.disableCaretPositioning&&(n.caretPosition=null):(n.caretPosition=t.target.selectionStart,n.options.debug&&console.log("Caret at: ",t.target.selectionStart,t.target.tagName.toLowerCase(),"(".concat(n.keyboardDOMClass,")")));}));}},{key:"recurseButtons",value:function(t){var e=this;if(!t)return !1;Object.keys(this.buttonElements).forEach((function(n){return e.buttonElements[n].forEach(t)}));}},{key:"destroy",value:function(){this.options.debug&&console.log("Destroying simple-keyboard instance: ".concat(this.currentInstanceName)),document.removeEventListener("keyup",this.handleKeyUp),document.removeEventListener("keydown",this.handleKeyDown),document.removeEventListener("mouseup",this.handleMouseUp),document.removeEventListener("touchend",this.handleTouchEnd),document.onpointerup=null,document.ontouchend=null,document.ontouchcancel=null,document.onmouseup=null;var deleteButton=function(t){t.onpointerdown=null,t.onpointerup=null,t.onpointercancel=null,t.ontouchstart=null,t.ontouchend=null,t.ontouchcancel=null,t.onclick=null,t.onmousedown=null,t.onmouseup=null,t.remove(),t=null;};this.recurseButtons(deleteButton),this.recurseButtons=null,deleteButton=null,this.keyboardDOM.onpointerdown=null,this.keyboardDOM.ontouchstart=null,this.keyboardDOM.onmousedown=null,this.clear(),window.SimpleKeyboardInstances[this.currentInstanceName]=null,delete window.SimpleKeyboardInstances[this.currentInstanceName],this.initialized=!1;}},{key:"getButtonThemeClasses",value:function(t){var e=this.options.buttonTheme,n=[];return Array.isArray(e)&&e.forEach((function(e){if(e.class&&"string"===typeof e.class&&e.buttons&&"string"===typeof e.buttons){var o=e.class.split(" ");e.buttons.split(" ").includes(t)&&(n=[].concat(_toConsumableArray(n),_toConsumableArray(o)));}else console.warn('Incorrect "buttonTheme". Please check the documentation.',e);})),n}},{key:"setDOMButtonAttributes",value:function(t,e){var n=this.options.buttonAttributes;Array.isArray(n)&&n.forEach((function(n){n.attribute&&"string"===typeof n.attribute&&n.value&&"string"===typeof n.value&&n.buttons&&"string"===typeof n.buttons?n.buttons.split(" ").includes(t)&&e(n.attribute,n.value):console.warn('Incorrect "buttonAttributes". Please check the documentation.',n);}));}},{key:"onTouchDeviceDetected",value:function(){this.processAutoTouchEvents(),this.disableContextualWindow();}},{key:"disableContextualWindow",value:function(){window.oncontextmenu=function(t){if(t.target.classList.contains("hg-button"))return t.preventDefault(),t.stopPropagation(),!1};}},{key:"processAutoTouchEvents",value:function(){this.options.autoUseTouchEvents&&(this.options.useTouchEvents=!0,this.options.debug&&console.log("autoUseTouchEvents: Touch device detected, useTouchEvents enabled."));}},{key:"onInit",value:function(){this.options.debug&&console.log("".concat(this.keyboardDOMClass," Initialized")),this.setEventListeners(),"function"===typeof this.options.onInit&&this.options.onInit();}},{key:"beforeFirstRender",value:function(){this.utilities.isTouchDevice()&&this.onTouchDeviceDetected(),"function"===typeof this.options.beforeFirstRender&&this.options.beforeFirstRender(),this.isFirstKeyboardInstance&&this.utilities.pointerEventsSupported()&&!this.options.useTouchEvents&&!this.options.useMouseEvents&&this.options.debug&&console.log("Using PointerEvents as it is supported by this browser"),this.options.useTouchEvents&&this.options.debug&&console.log("useTouchEvents has been enabled. Only touch events will be used.");}},{key:"beforeRender",value:function(){"function"===typeof this.options.beforeRender&&this.options.beforeRender();}},{key:"onRender",value:function(){"function"===typeof this.options.onRender&&this.options.onRender();}},{key:"onModulesLoaded",value:function(){"function"===typeof this.options.onModulesLoaded&&this.options.onModulesLoaded();}},{key:"loadModules",value:function(){var t=this;Array.isArray(this.options.modules)&&(this.options.modules.forEach((function(e){var n=new e;if(n.constructor.name&&"Function"!==n.constructor.name){var o="module-".concat(t.utilities.camelCase(n.constructor.name));t.keyboardPluginClasses=t.keyboardPluginClasses+" ".concat(o);}n.init(t);})),this.keyboardPluginClasses=this.keyboardPluginClasses+" modules-loaded",this.render(),this.onModulesLoaded());}},{key:"getModuleProp",value:function(t,e){return !!this.modules[t]&&this.modules[t][e]}},{key:"getModulesList",value:function(){return Object.keys(this.modules)}},{key:"parseRowDOMContainers",value:function(t,e,n,o){var i=this,s=Array.from(t.children),a=0;return s.length&&n.forEach((function(n,r){var u=o[r];if(!u||!(u>n))return !1;var c=n-a,l=u-a,h=document.createElement("div");h.className+="hg-button-container";var d="".concat(i.options.layoutName,"-r").concat(e,"c").concat(r);h.setAttribute("data-skUID",d);var p=s.splice(c,l-c+1);a=l-c,p.forEach((function(t){return h.appendChild(t)})),s.splice(c,0,h),t.innerHTML="",s.forEach((function(e){return t.appendChild(e)})),i.options.debug&&console.log("rowDOMContainer",p,c,l,a+1);})),t}},{key:"render",value:function(){var t=this;this.clear(),this.initialized||this.beforeFirstRender(),this.beforeRender();var e="hg-layout-".concat(this.options.layoutName),n=this.options.layout||{default:["` 1 2 3 4 5 6 7 8 9 0 - = {bksp}","{tab} q w e r t y u i o p [ ] \\","{lock} a s d f g h j k l ; ' {enter}","{shift} z x c v b n m , . / {shift}",".com @ {space}"],shift:["~ ! @ # $ % ^ & * ( ) _ + {bksp}","{tab} Q W E R T Y U I O P { } |",'{lock} A S D F G H J K L : " {enter}',"{shift} Z X C V B N M < > ? {shift}",".com @ {space}"]},o=this.options.useTouchEvents||!1,i=o?"hg-touch-events":"",s=this.options.useMouseEvents||!1,a=this.options.disableRowButtonContainers;this.keyboardDOM.className=this.getKeyboardClassString(this.options.theme,e,this.keyboardPluginClasses,i),n[this.options.layoutName].forEach((function(e,n){var i=e.split(" "),r=document.createElement("div");r.className+="hg-row";var u=[],c=[];i.forEach((function(e,i){var l,h=!a&&"string"===typeof e&&e.length>1&&0===e.indexOf("["),d=!a&&"string"===typeof e&&e.length>1&&e.indexOf("]")===e.length-1;h&&(u.push(i),e=e.replace(/\[/g,"")),d&&(c.push(i),e=e.replace(/\]/g,""));var p=t.utilities.getButtonClass(e),f=t.utilities.getButtonDisplayName(e,t.options.display,t.options.mergeDisplay),y=t.options.useButtonTag?"button":"div",b=document.createElement(y);b.className+="hg-button ".concat(p),(l=b.classList).add.apply(l,_toConsumableArray(t.getButtonThemeClasses(e))),t.setDOMButtonAttributes(e,(function(t,e){b.setAttribute(t,e);})),t.activeButtonClass="hg-activeButton",!t.utilities.pointerEventsSupported()||o||s?o?(b.ontouchstart=function(n){t.handleButtonClicked(e),t.handleButtonMouseDown(e,n);},b.ontouchend=function(){t.handleButtonMouseUp(e);},b.ontouchcancel=function(){t.handleButtonMouseUp(e);}):(b.onclick=function(){t.isMouseHold=!1,t.handleButtonClicked(e);},b.onmousedown=function(n){t.handleButtonMouseDown(e,n);},b.onmouseup=function(){t.handleButtonMouseUp(e);}):(b.onpointerdown=function(n){t.handleButtonClicked(e),t.handleButtonMouseDown(e,n);},b.onpointerup=function(){t.handleButtonMouseUp(e);},b.onpointercancel=function(){t.handleButtonMouseUp(e);}),b.setAttribute("data-skBtn",e);var m="".concat(t.options.layoutName,"-r").concat(n,"b").concat(i);b.setAttribute("data-skBtnUID",m);var g=document.createElement("span");g.innerHTML=f,b.appendChild(g),t.buttonElements[e]||(t.buttonElements[e]=[]),t.buttonElements[e].push(b),r.appendChild(b);})),r=t.parseRowDOMContainers(r,n,u,c),t.keyboardDOM.appendChild(r);})),this.onRender(),this.initialized||(this.initialized=!0,!this.utilities.pointerEventsSupported()||o||s?o?(document.ontouchend=function(){return t.handleButtonMouseUp()},document.ontouchcancel=function(){return t.handleButtonMouseUp()},this.keyboardDOM.ontouchstart=function(e){return t.handleKeyboardContainerMouseDown(e)}):o||(document.onmouseup=function(){return t.handleButtonMouseUp()},this.keyboardDOM.onmousedown=function(e){return t.handleKeyboardContainerMouseDown(e)}):(document.onpointerup=function(){return t.handleButtonMouseUp()},this.keyboardDOM.onpointerdown=function(e){return t.handleKeyboardContainerMouseDown(e)}),this.onInit());}}])&&Keyboard_defineProperties(t.prototype,e),SimpleKeyboard}();e.default=s;}])}));

	});

	var build$3 = createCommonjsModule(function (module, exports) {
	/*!
	 * 
	 *   simple-keyboard-layouts v1.13.51
	 *   https://github.com/hodgef/simple-keyboard-layouts
	 * 
	 *   Copyright (c) Francisco Hodge (https://github.com/hodgef)
	 * 
	 *   This source code is licensed under the MIT license found in the
	 *   LICENSE file in the root directory of this source tree.
	 *   
	 */
	!function(t,e){module.exports=e();}(window,(function(){return function(t){var e={};function s(f){if(e[f])return e[f].exports;var i=e[f]={i:f,l:!1,exports:{}};return t[f].call(i.exports,i,i.exports,s),i.l=!0,i.exports}return s.m=t,s.c=e,s.d=function(t,e,f){s.o(t,e)||Object.defineProperty(t,e,{enumerable:!0,get:f});},s.r=function(t){"undefined"!==typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(t,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(t,"__esModule",{value:!0});},s.t=function(t,e){if(1&e&&(t=s(t)),8&e)return t;if(4&e&&"object"===typeof t&&t&&t.__esModule)return t;var f=Object.create(null);if(s.r(f),Object.defineProperty(f,"default",{enumerable:!0,value:t}),2&e&&"string"!=typeof t)for(var i in t)s.d(f,i,function(e){return t[e]}.bind(null,i));return f},s.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return s.d(e,"a",e),e},s.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},s.p="",s(s.s=0)}([function(t,e,s){t.exports=s(1);},function(t,e,s){s.r(e);var f={default:["\u0630 1 2 3 4 5 6 7 8 9 0 - = {bksp}","{tab} \u0636 \u0635 \u062b \u0642 \u0641 \u063a \u0639 \u0647 \u062e \u062d \u062c \u062f \\","{lock} \u0634 \u0633 \u064a \u0628 \u0644 \u0627 \u062a \u0646 \u0645 \u0643 \u0637 {enter}","{shift} \u0626 \u0621 \u0624 \u0631 \u0644\u0627 \u0649 \u0629 \u0648 \u0632 \u0638 {shift}",".com @ {space}"],shift:["\u0651 ! @ # $ % ^ & * ) ( _ + {bksp}","{tab} \u064e \u064b \u064f \u064c \u0644\u0625 \u0625 \u2018 \xf7 \xd7 \u061b < > |",'{lock} \u0650 \u064d ] [ \u0644\u0623 \u0623 \u0640 \u060c / : " {enter}',"{shift} ~ \u0652 } { \u0644\u0622 \u0622 \u2019 , . \u061f {shift}",".com @ {space}"]},i={default:["\u0965 \u09e7 \u09e8 \u09e9 \u09ea \u09eb \u09ec \u09ed \u09ee \u09ef \u09e6 - \u09c3 {bksp}","{tab} \u09cc \u09c8 \u09be \u09c0 \u09c2 \u09ac \u09b9 \u0997 \u09a6 \u099c \u09a1 \u09bc","\u09cb \u09c7 \u09cd \u09bf \u09c1 \u09aa \u09f0 \u0995 \u09a4 \u099a \u099f {enter}","{shift} \u0982 \u09ae \u09a8 \u09f1 \u09b2 \u09b8 , . \u09df {shift}",".com @ {space}"],shift:["! @ ( ) \u0983 \u098b {bksp}","{tab} \u0994 \u0990 \u0986 \u0998 \u098a \u09ad \u0999 \u0998 \u09a7 \u099d \u09a2 \u099e","\u0993 \u098f \u0985 \u0987 \u0989 \u09ab  \u0996 \u09a5 \u099b \u099b \u09a0 {enter}","{shift} \u0981 \u09a3 \u09b6 \u09b7 \u0964 \u09af {shift}",".com @ {space}"]},c={default:["\u1050 \u1041 \u1042 \u1043 \u1044 \u1045 \u1046 \u1047 \u1048 \u1049 \u1040 - = {bksp}","{tab} \u1006 \u1010 \u1014 \u1019 \u1021 \u1015 \u1000 \u1004 \u101e \u1005 \u101f \u1029 \u104f","{lock} \u1031 \u103a \u102d \u1039 \u102b \u1037 \u103b \u102f \u1030 \u1038 ' {enter}","{shift} \u1016 \u1011 \u1001 \u101c \u1018 \u100a \u102c , . / {shift}",".com @ {space}"],shift:["\u100e \u100d \u1052 \u100b \u1053 \u1054 \u1055 \u101b * ( ) _ + {bksp}","{tab} \u1008 \u101d \u1023 \u104e \u1024 \u104c \u1025 \u104d \u103f \u100f \u1027 \u102a \u1051",'{lock} \u1017 \u103d \u102e \u1064 \u103c \u1036 \u1032 \u1012 \u1013 \u1002 " {enter}',"{shift} \u1007 \u100c \u1003 \u1020 \u101a \u1009 \u1026 \u104a \u104b ? {shift}",".com @ {space}"]},a={default:["\u20ac \u3105 \u3109 \u02c7 \u02cb \u3113 \u02ca \u02d9 \u311a \u311e \u3122 \u3126 = {bksp}","{tab} \u3106 \u310a \u310d \u3110 \u3114 \u3117 \u3127 \u311b \u311f \u3123 [ ] \\","{lock} \u3107 \u310b \u310e \u3111 \u3115 \u3118 \u3128 \u311c \u3120 \u3124 ' {enter}","{shift} \u3108 \u310c \u310f \u3112 \u3116 \u3119 \u3129 \u311d \u3121 \u3125",".com @ {space}"],shift:["~ ! @ # $ % ^ & * ) ( _ + {bksp}","{tab} q w e r t y u i o p { } |",'{lock} a s d f g h j k l : " {enter}',"{shift} z x c v b n m , < > ? {shift}",".com @ {space}"]},o={default:["; + \u011b \u0161 \u010d \u0159 \u017e \xfd \xe1 \xed \xe9 \xb4 {bksp}","{tab} q w e r t y u i o p \xfa ) \xa8","{lock} a s d f g h j k l \u016f \xa7 {enter}","{shift} \\ z x c v b n m , . - {shift}",".com @ {space}"],shift:["\xb0 1 2 3 4 5 6 7 8 9 0 % \u02c7 {bksp}","{tab} Q W E R T Y U I O P / ( '",'{lock} A S D F G H J K L " ! {enter}',"{shift} | Z X C V B N M ? : _ {shift}",".com @ {space}"]},n={default:["` 1 2 3 4 5 6 7 8 9 0 - = {bksp}","{tab} q w e r t y u i o p [ ] \\","{lock} a s d f g h j k l ; ' {enter}","{shift} z x c v b n m , . / {shift}",".com @ {space}"],shift:["~ ! @ # $ % ^ & * ( ) _ + {bksp}","{tab} Q W E R T Y U I O P { } |",'{lock} A S D F G H J K L : " {enter}',"{shift} Z X C V B N M < > ? {shift}",".com @ {space}"]},h={default:["` \u06f1 \u06f2 \u06f3 \u06f4 \u06f5 \u06f6 \u06f7 \u06f8 \u06f9 \u06f0 - = {bksp}","{tab} \u0636 \u0635 \u062b \u0642 \u0641 \u063a \u0639 \u0647 \u062e \u062d \u062c \u0686 \\","{lock} \u0634 \u0633 \u06cc \u0628 \u0644 \u0627 \u062a \u0646 \u0645 \u06a9 \u06af {enter}","{shift} \u0638 \u0637 \u0632 \u0631 \u0630 \u062f \u067e \u0648 . / {shift}",".com @ {space}"],shift:["\xf7 ! \u066c \u066b \ufdfc \u066a \xd7 \u06f7 * ) ( \u0640 + {bksp}","{tab} \u0652 \u064c \u064d \u064b \u064f \u0650 \u064e \u0651 ] [ } {","{lock} \u0624 \u0626 \u064a \u0625 \u0623 \u0622 \u0629 \xbb \xab : \u061b {enter}","{shift} \u0643 \u0653 \u0698 \u0670 \u200c \u0654 \u0621 < > \u061f {shift}",".com @ {space}"]},r={default:["\xb2 & \xe9 \" ' ( - \xe8 _ \xe7 \xe0 ) = {bksp}","{tab} a z e r t y u i o p ^ $","{lock} q s d f g h j k l m \xf9 * {enter}","{shift} < w x c v b n , ; : ! {shift}",".com @ {space}"],shift:["{//} 1 2 3 4 5 6 7 8 9 0 \xb0 + {bksp}","{tab} A Z E R T Y U I O P \xa8 \xa3","{lock} Q S D F G H J K L M % \xb5 {enter}","{shift} > W X C V B N ? . / \xa7 {shift}",".com @ {space}"]},p={default:["\u201e 1 2 3 4 5 6 7 8 9 0 - = {bksp}","{tab} \u10e5 \u10ec \u10d4 \u10e0 \u10e2 \u10e7 \u10e3 \u10d8 \u10dd \u10de [ ] \\","{lock} \u10d0 \u10e1 \u10d3 \u10e4 \u10d2 \u10f0 \u10ef \u10d9 \u10da ; ' {enter}","{shift} \u10d6 \u10ee \u10ea \u10d5 \u10d1 \u10dc \u10db , . / {shift}",".com @ {space}"],shift:["\u201c ! @ # $ % ^ & * ( ) _ + {bksp}","{tab} \u10e5 \u10ed \u10d4 \u10e6 \u10d7 \u10e7 \u10e3 \u10d8 \u10dd \u10de { } | ~",'{lock} \u10d0 \u10e8 \u10d3 \u10e4 \u10d2 \u10f0 \u10df \u10d9 \u20be : " {enter}',"{shift} \u10eb \u10ee \u10e9 \u10d5 \u10d1 \u10dc \u10db < > ? {shift}",".com @ {space}"]},b={default:["^ 1 2 3 4 5 6 7 8 9 0 \xdf \xb4 {bksp}","{tab} q w e r t z u i o p \xfc +","{lock} a s d f g h j k l \xf6 \xe4 # {enter}","{shift} < y x c v b n m , . - {shift}",".com @ {space}"],shift:['\xb0 ! " \xa7 $ % & / ( ) = ? ` {bksp}',"{tab} Q W E R T Z U I O P \xdc *","{lock} A S D F G H J K L \xd6 \xc4 ' {enter}","{shift} > Y X C V B N M ; : _ {shift}",".com @ {space}"]},l={default:["` 1 2 3 4 5 6 7 8 9 0 - = {bksp}","{tab} ; \u03c2 \u03b5 \u03c1 \u03c4 \u03c5 \u03b8 \u03b9 \u03bf \u03c0 [ ] \\","{lock} \u03b1 \u03c3 \u03b4 \u03c6 \u03b3 \u03b7 \u03be \u03ba \u03bb \u0384 ' {enter}","{shift} < \u03b6 \u03c7 \u03c8 \u03c9 \u03b2 \u03bd \u03bc , . / {shift}",".com @ {space}"],shift:["~ ! @ # $ % ^ & * ( ) _ + {bksp}","{tab} : \u0385 \u0395 \u03a1 \u03a4 \u03a5 \u0398 \u0399 \u039f \u03a0 { } |",'{lock} \u0391 \u03a3 \u0394 \u03a6 \u0393 \u0397 \u039e \u039a \u039b \xa8 " {enter}',"{shift} > \u0396 \u03a7 \u03a8 \u03a9 \u0392 \u039d \u039c < > ? {shift}",".com @ {space}"]},k={default:["` 1 2 3 4 5 6 7 8 9 0 - = {bksp}","{tab} / ' \u05e7 \u05e8 \u05d0 \u05d8 \u05d5 \u05df \u05dd \u05e4 ] [ \\","{lock} \u05e9 \u05d3 \u05d2 \u05db \u05e2 \u05d9 \u05d7 \u05dc \u05da \u05e3 , {enter}","{shift} \u05d6 \u05e1 \u05d1 \u05d4 \u05e0 \u05de \u05e6 \u05ea \u05e5 . {shift}",".com @ {space}"],shift:["~ ! @ # $ % ^ & * ( ) _ + {bksp}","{tab} Q W E R T Y U I O P { } |",'{lock} A S D F G H J K L : " {enter}',"{shift} Z X C V B N M < > ? {shift}",".com @ {space}"]},u={default:["` \u090d \u0945 \u094d\u0930 \u0930\u094d \u091c\u094d\u091e \u0924\u094d\u0930 \u0915\u094d\u0937 \u0936\u094d\u0930 \u096f \u0966 - \u0943 {bksp}","{tab} \u094c \u0948 \u093e \u0940 \u0942 \u092c \u0939 \u0917 \u0926 \u091c \u0921 \u093c \u0949 \\","{lock} \u094b \u0947 \u094d \u093f \u0941 \u092a \u0930 \u0915 \u0924 \u091a \u091f {enter}","{shift} \u0902 \u092e \u0928 \u0935 \u0932 \u0938 , . \u092f {shift}",".com @ {space}"],shift:["~ \u0967 \u0968 \u0969 \u096a \u096b \u096c \u096d \u096e \u096f \u0966 \u0903 \u090b {bksp}","{tab} \u0914 \u0910 \u0906 \u0908 \u090a \u092d \u0919 \u0918 \u0927 \u091d \u0922 \u091e \u0911","{lock} \u0913 \u090f \u0905 \u0907 \u0909 \u092b \u0931 \u0916 \u0925 \u091b \u0920 {enter}",'{shift} "" \u0901 \u0923 \u0928 \u0935 \u0933 \u0936 \u0937 \u0964 \u095f {shift}',".com @ {space}"]},m={default:["\\ 1 2 3 4 5 6 7 8 9 0 ' \xec {bksp}","{tab} q w e r t y u i o p \xe8 +","{lock} a s d f g h j k l \xf2 \xe0 \xf9 {enter}","{shift} < z x c v b n m , . - {shift}",".com @ {space}"],shift:['| ! " \xa3 $ % & / ( ) = ? ^ {bksp}',"{tab} Q W E R T Y U I O P \xe9 *","{lock} A S D F G H J K L \xe7 \xb0 \xa7 {enter}","{shift} > Z X C V B N M ; : _ {shift}",".com @ {space}"]},d={default:["\u308d \u306c \u3075 \u3042 \u3046 \u3048 \u304a \u3084 \u3086 \u3088 \u308f \u307b \u3078 {bksp}","{tab} \u305f \u3066 \u3044 \u3059 \u304b \u3093 \u306a \u306b \u3089 \u305b \u309b \u309c \u3080","{lock} \u3061 \u3068 \u3057 \u306f \u304d \u304f \u307e \u306e \u308a \u308c \u3051 {enter}","{shift} \u3064 \u3055 \u305d \u3072 \u3053 \u307f \u3082 \u306d \u308b \u3081 {shift}",".com @ {space}"],shift:["\u308d \u306c \u3075 \u3041 \u3045 \u3047 \u3049 \u3083 \u3085 \u3087 \u3092 \u30fc \u3078 {bksp}","{tab} \u305f \u3066 \u3043 \u3059 \u304b \u3093 \u306a \u306b \u3089 \u305b \u300c \u300d \u3080","{lock} \u3061 \u3068 \u3057 \u306f \u304d \u304f \u307e \u306e \u308a \u308c \u3051 {enter}","{shift} \u3063 \u3055 \u305d \u3072 \u3053 \u307f \u3082 \u3001 \u3002 \u30fb {shift}",".com @ {space}"]},y={default:["\u0cca 1 2 3 4 5 6 7 8 9 0 - \u0cc3 {bksp}","{tab} \u0ccc \u0cc8 \u0cbe \u0cc0 \u0cc2 \u0cac \u0cb9 \u0c97 \u0ca6 \u0c9c \u0ca1","\u0ccb \u0cc7 \u0ccd \u0cbf \u0cc1 \u0caa \u0cb0 \u0c95 \u0ca4 \u0c9a \u0c9f {enter}","{shift} \u0cc6 \u0c82 \u0cae \u0ca8 \u0cb5 \u0cb2 \u0cb8 , . / {shift}",".com @ {space}"],shift:["\u0c92 \u0ccd\u0cb0 \u0cb0\u0ccd \u0c9c\u0ccd\u0c9e \u0ca4\u0ccd\u0cb0 \u0c95\u0ccd\u0cb7 \u0cb6\u0ccd\u0cb0 ( ) \u0c83 \u0c8b {bksp}","{tab} \u0c94 \u0c90 \u0c86 \u0c88 \u0c8a \u0cad \u0c99 \u0c98 \u0ca7 \u0c9d \u0ca2 \u0c9e","\u0c93 \u0c8f \u0c85 \u0c87 \u0c89 \u0cab \u0cb1 \u0c96 \u0ca5 \u0c9b \u0ca0 {enter}","{shift} \u0c8e \u0ca3 \u0cb3 \u0cb6 \u0cb7 | / {shift}",".com @ {space}"]},_={default:["` 1 2 3 4 5 6 7 8 9 0 - = {bksp}","{tab} \u1107 \u110c \u1103 \u1100 \u1109 \u116d \u1167 \u1163 \u1162 \u1166 [ ] \u20a9","{lock} \u1106 \u1102 \u110b \u1105 \u1112 \u1169 \u1165 \u1161 \u1175 ; ' {enter}","{shift} \u110f \u1110 \u110e \u1111 \u1172 \u116e \u1173 , . / {shift}",".com @ {space}"],shift:["~ ! @ # $ % ^ & * ( ) _ + {bksp}","{tab} \u1108 \u110d \u1104 \u1101 \u110a \u116d \u1167 \u1163 \u1164 \u1168 { } |",'{lock} \u1106 \u1102 \u110b \u1105 \u1112 \u1169 \u1165 \u1161 \u1175 : " {enter}',"{shift} \u110f \u1110 \u110e \u1111 \u1172 \u116e \u1173 < > ? {shift}",".com @ {space}"]},g={default:["\u02db 1 2 3 4 5 6 7 8 9 0 + ' {bksp}","{tab} q w e r t z u i o p \u017c \u015b","{lock} a s d f g h j k l \u0142 \u0105 \xf3 {enter}","{shift} < y x c v b n m , . - {shift}",".com @ {space}"],shift:['\xb7 ! " # \xa4 % & / ( ) = ? * {bksp}',"{tab} Q W E R T Z U I O P \u0144 \u0107","{lock} A S D F G H J K L \u0141 \u0119 \u017a {enter}","{shift} > Y X C V B N M ; : _ {shift}",".com @ {space}"]},j={default:["\u0451 1 2 3 4 5 6 7 8 9 0 - = {bksp}","{tab} \u0439 \u0446 \u0443 \u043a \u0435 \u043d \u0433 \u0448 \u0449 \u0437 \u0445 \u044a \\","{lock} \u0444 \u044b \u0432 \u0430 \u043f \u0440 \u043e \u043b \u0434 \u0436 \u044d {enter}","{shift} \\ \u044f \u0447 \u0441 \u043c \u0438 \u0442 \u044c \u0431 \u044e / {shift}",".com @ {space}"],shift:['\u0401 ! " \u2116 ; % : ? * ( ) _ + {bksp}',"{tab} \u0419 \u0426 \u0423 \u041a \u0415 \u041d \u0413 \u0428 \u0429 \u0417 \u0425 \u042a /","{lock} \u0424 \u042b \u0412 \u0410 \u041f \u0420 \u041e \u041b \u0414 \u0416 \u042d {enter}","{shift} / \u042f \u0427 \u0421 \u041c \u0418 \u0422 \u042c \u0411 \u042e / {shift}",".com @ {space}"]},v={default:["` \u0661 \u0662 \u0663 \u0664 \u0665 \u0666 \u0667 \u0668 \u0669 \u0660 - = {bksp}","{tab} \u0642 \ufeed \u0639 \u0631 \u062a \u0680 \u0621 \u064a \ufba6 \u067e [ ]","{lock} \u0627 \u0633 \u062f \u0641 \u06af \u06be \u062c \u06a9 \u0644 \u061b \u060c {enter}","{shift} \u0632 \u0634 \u0686 \u0637 \u0628 \u0646 \u0645 \u0687 , . / {shift}",".com @ {space}"],shift:["~ ! @ # $ \u066a ^ & * ( ) _ + {bksp}","{tab} \ufe70 \u068c \u06aa \u0699 \u067d \ufe7a \ufefb \ufe8b \u06a6 | { }","{lock} \u067b \u0635 \u068a \u060d \u063a \u062d \u0636 \u062e \u06d4 \u0703 \u05f4 {enter}","{shift} \u0630 \u067f \u062b \u0638 \u067a \u066b \u0640 < > \u061f {shift}",".com @ {space}"]},x={default:["| 1 2 3 4 5 6 7 8 9 0 ' \xbf {bksp}","{tab} q w e r t y u i o p \u0301 +","{lock} a s d f g h j k l \xf1 { } {enter}","{shift} < z x c v b n m , . - {shift}",".com @ {space}"],shift:['\xb0 ! " # $ % & / ( ) = ? \xa1 {bksp}',"{tab} Q W E R T Y U I O P \u0308 *","{lock} A S D F G H J K L \xd1 [ ] {enter}","{shift} > Z X C V B N M ; : _ {shift}",".com @ {space}"]},w={default:["\xa7 1 2 3 4 5 6 7 8 9 0 + \xb4 {bksp}","{tab} q w e r t y u i o p \xe5 \xa8","{lock} a s d f g h j k l \xf6 \xe4 ' {enter}","{shift} < z x c v b n m , . - {shift}",".com @ {space}"],shift:['\xb0 ! " # $ % & / ( ) = ? ` {bksp}',"{tab} Q W E R T Y U I O P \xc5 ^","{lock} A S D F G H J K L \xd6 \xc4 * {enter}","{shift} > Z X C V B N M ; : _ {shift}",".com @ {space}"]},O={default:["_ \u0e45 / - \u0e20 \u0e16 \u0e38 \u0e36 \u0e04 \u0e05 \u0e08 \u0e02 \u0e0a {bksp}","{tab} \u0e46 \u0e44 \u0e33 \u0e1e \u0e30 \u0e31 \u0e35 \u0e23 \u0e19 \u0e22 \u0e1a \u0e25 \u0e03 ","{lock} \u0e1f \u0e2b \u0e01 \u0e14 \u0e40 \u0e49 \u0e48 \u0e32 \u0e2a \u0e27 \u0e07 {enter}","{shift} \u0e1c \u0e1b \u0e41 \u0e2d \u0e34 \u0e37 \u0e17 \u0e21 \u0e43 \u0e1d {shift}",".com @ {space}"],shift:["% + \u0e51 \u0e52 \u0e53 \u0e54 \u0e39 \u0e3f \u0e55 \u0e56 \u0e57 \u0e58 \u0e59 {bksp}",'{tab} \u0e50 " \u0e0e \u0e11 \u0e18 \u0e4d \u0e4a \u0e13 \u0e2f \u0e0d \u0e10 , \u0e05',"{lock} \u0e24 \u0e06 \u0e0f \u0e42 \u0e0c \u0e47 \u0e4b \u0e29 \u0e28 \u0e0b . {enter}","{shift} ( ) \u0e09 \u0e2e \u0e3a \u0e4c ? \u0e12 \u0e2c \u0e26 {shift}",".com @ {space}"]},S={default:['" 1 2 3 4 5 6 7 8 9 0 * - # {bksp}',"{tab} q w e r t y u \u0131 o p \u011f \xfc [ ]","{lock} a s d f g h j k l \u015f i , {enter}","{shift} < z x c v b n m \xf6 \xe7 . | $ \u20ac {shift}",".com @ {space}"],shift:["\xe9 ! ' ^ + % & / ( ) = ? _ ~ {bksp}","{tab} Q W E R T Y U I O P \u011e \xdc { }","{lock} A S D F G H J K L \u015e \u0130 ; {enter}","{shift} > Z X C V B N M \xd6 \xc7 : \\ ` \xb4 {shift}",".com @ {space}"]},P={default:["' 1 2 3 4 5 6 7 8 9 0 - = {bksp}","{tab} \u0439 \u0446 \u0443 \u043a \u0435 \u043d \u0433 \u0448 \u0449 \u0437 \u0445 \u0457 \\","{lock} \u0444 \u0456 \u0432 \u0430 \u043f \u0440 \u043e \u043b \u0434 \u0436 \u0454 {enter}","{shift} \\ \u044f \u0447 \u0441 \u043c \u0438 \u0442 \u044c \u0431 \u044e / {shift}",".com @ {space}"],shift:['\u20b4 ! " \u2116 ; % : ? * ( ) _ + {bksp}',"{tab} \u0419 \u0426 \u0423 \u041a \u0415 \u041d \u0413 \u0428 \u0429 \u0417 \u0425 \u0407 /","{lock} \u0424 \u0406 \u0412 \u0410 \u041f \u0420 \u041e \u041b \u0414 \u0416 \u0404 {enter}","{shift} / \u042f \u0427 \u0421 \u041c \u0418 \u0422 \u042c \u0411 \u042e / {shift}",".com @ {space}"]},$={default:["` \u0661 \u0662 \u0663 \u0664 \u0665 \u0666 \u0667 \u0668 \u0669 \u0660 - = {bksp}","{tab} \u0642 \ufeed \u0639 \u0631 \u062a \u06d2 \u0621 \u0649 \ufba6 \u067e [ ]","{lock} \u0627 \u0633 \u062f \u0641 \u06af \u06be \u062c \u06a9 \u0644 \u061b \u060c {enter}","{shift} \u0632 \u0634 \u0686 \u0637 \u0628 \u0646 \u0645 \u06e4 , . / {shift}",".com @ {space}"],shift:["~ ! @ # $ \u066a ^ & * ( ) _ + {bksp}","{tab} \ufe70 \ufe77 \ufe79 \u0691 \u0679 \ufe7a \ufefb \ufe8b \u0629 | { }","{lock} \u0622 \u0635 \u0688 \u060d \u063a \u062d \u0636 \u062e \u06d4 \u0703 \u05f4 {enter}","{shift} \u0630 \u0698 \u062b \u0638 \u06ba \u066b \u0640 < > \u061f {shift}",".com @ {space}"]};function M(t,e,s){return e in t?Object.defineProperty(t,e,{value:s,enumerable:!0,configurable:!0,writable:!0}):t[e]=s,t}var K=function t(){var e=this;!function(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}(this,t),M(this,"layouts",{arabic:f,assamese:i,burmese:c,chinese:a,czech:o,english:n,farsi:h,french:r,georgian:p,german:b,greek:l,hebrew:k,hindi:u,italian:m,japanese:d,kannada:y,korean:_,polish:g,russian:j,sindhi:v,spanish:x,swedish:w,thai:O,turkish:S,ukrainian:P,urdu:$}),M(this,"get",(function(t){return t?e.layouts[t]:e.layouts}));};e.default=K;}])}));

	});

	var simplekeyboard_layout_mapping = createCommonjsModule(function (module, exports) {
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.SubKeysMap = exports.SuperKeysMap = exports.SourceLayoutMod = exports.LanguageLayoutMap = exports.LayoutMappingDestroy = void 0;
	var simple_keyboard_layouts_1 = __importDefault(build$3);
	var LayoutMapping = /** @class */ (function () {
	    function LayoutMapping() {
	        this.layouts = new simple_keyboard_layouts_1.default();
	    }
	    LayoutMapping.prototype.init = function (keyboard) {
	        var _this = this;
	        /**
	         * Registering module
	         */
	        keyboard.registerModule("layoutMapping", function (module) {
	            var _a = keyboard.options, sourceLayout = _a.sourceLayout, layout = _a.layout;
	            var sourceLayoutObj = typeof sourceLayout === "object"
	                ? sourceLayout
	                : _this.layouts.get(sourceLayout);
	            var layoutObj = typeof layout === "object"
	                ? layout
	                : _this.layouts.get(layout);
	            /**
	             * Highlight button
	             */
	            module.highlightButton = function (event) {
	                var physicalKeyboardKeyName = module.sourceLayoutKeyMaps(keyboard.physicalKeyboard.getSimpleKeyboardLayoutKey(event));
	                var sourceLayoutIndexes = module.getLayoutKeyIndex(physicalKeyboardKeyName, sourceLayoutObj);
	                if (sourceLayoutIndexes) {
	                    var rIndex = sourceLayoutIndexes.rIndex, bIndex = sourceLayoutIndexes.bIndex;
	                    var layoutKeyName = module.findLayoutKeyByIndex(rIndex, bIndex, layoutObj);
	                    var buttonElement = module.getButtonInLayout(layoutKeyName);
	                    if (!buttonElement) {
	                        console.log("Could not find button in layout", layoutKeyName);
	                        return false;
	                    }
	                    if (Array.isArray(buttonElement)) {
	                        buttonElement.forEach(function (item) {
	                            item.style.background = "#9ab4d0";
	                            item.style.color = "white";
	                        });
	                        /**
	                         * Trigger press
	                         */
	                        buttonElement[0].onpointerdown();
	                        buttonElement[0].onpointerup();
	                    }
	                    else {
	                        buttonElement.style.background = "#9ab4d0";
	                        buttonElement.style.color = "white";
	                        /**
	                         * Trigger press
	                         */
	                        buttonElement.onpointerdown();
	                        buttonElement.onpointerup();
	                    }
	                }
	                else {
	                    console.error("Key", physicalKeyboardKeyName, "not found in source layout");
	                }
	            };
	            /**
	             * Unhighlight button
	             */
	            module.unhighlightButton = function (event) {
	                var physicalKeyboardKeyName = module.sourceLayoutKeyMaps(keyboard.physicalKeyboard.getSimpleKeyboardLayoutKey(event));
	                var sourceLayoutIndexes = module.getLayoutKeyIndex(physicalKeyboardKeyName, sourceLayoutObj);
	                if (sourceLayoutIndexes) {
	                    var rIndex = sourceLayoutIndexes.rIndex, bIndex = sourceLayoutIndexes.bIndex;
	                    var layoutKeyName = module.findLayoutKeyByIndex(rIndex, bIndex, layoutObj);
	                    var buttonElement = module.getButtonInLayout(layoutKeyName);
	                    if (!buttonElement) {
	                        console.log("Could not find button in layout", layoutKeyName);
	                        return false;
	                    }
	                    if (Array.isArray(buttonElement)) {
	                        buttonElement.forEach(function (item) {
	                            item.removeAttribute("style");
	                        });
	                    }
	                    else {
	                        buttonElement.removeAttribute("style");
	                    }
	                }
	            };
	            /**
	             * Get button in layout
	             */
	            module.getButtonInLayout = function (layoutKeyName) {
	                var buttonElement = keyboard.getButtonElement(layoutKeyName) ||
	                    keyboard.getButtonElement("{" + layoutKeyName + "}");
	                return buttonElement;
	            };
	            /**
	             * Get layout key's index
	             */
	            module.getLayoutKeyIndex = function (layoutKey, layout) {
	                try {
	                    var layoutName = keyboard.options.layoutName;
	                    layout[layoutName].forEach(function (row, rIndex) {
	                        var rowButtons = row.split(" ");
	                        rowButtons.forEach(function (button, bIndex) {
	                            if (button === layoutKey) {
	                                throw {
	                                    rIndex: rIndex,
	                                    bIndex: bIndex
	                                };
	                            }
	                        });
	                    });
	                    return false;
	                }
	                catch (res) {
	                    return res;
	                }
	            };
	            /**
	             * Find layout key by index
	             */
	            module.findLayoutKeyByIndex = function (rIndex, bIndex, layout) {
	                var layoutName = keyboard.options.layoutName;
	                var row = layout[layoutName][rIndex];
	                if (row) {
	                    var rowButtons = row.split(" ");
	                    return rowButtons[bIndex];
	                }
	            };
	            /**
	             * Define key listeners
	             */
	            module.initListeners = function () {
	                /**
	                 * Handle keyboard press
	                 */
	                HighlightKeyDownFn = HighlightKeyDown(module);
	                document.addEventListener("keydown", HighlightKeyDownFn);
	                HighlightKeyUpFn = HighlightKeyUp(module);
	                document.addEventListener("keyup", HighlightKeyUpFn);
	            };
	            /**
	             * Custom layout overrides
	             */
	            module.sourceLayoutKeyMaps = function (keyName) {
	                var retval;
	                switch (keyName) {
	                    case "backspace":
	                        retval = "{bksp}";
	                        break;
	                    case "shiftleft":
	                        retval = "{shift}";
	                        break;
	                    case "shiftright":
	                        retval = "{shift}";
	                        break;
	                    case "space":
	                        retval = "{space}";
	                        break;
	                    case "enter":
	                        retval = "{enter}";
	                        break;
	                    default:
	                        retval = keyName;
	                        break;
	                }
	                return retval;
	            };
	            /**
	             * Start module
	             */
	            module.start = function () {
	                module.initListeners();
	                keyboard.setOptions({
	                    layout: layoutObj
	                });
	            };
	            module.start();
	        });
	    };
	    return LayoutMapping;
	}());
	function LayoutMappingDestroy() {
	    HighlightKeyDownFn && document.removeEventListener("keydown", HighlightKeyDownFn);
	    HighlightKeyUpFn && document.removeEventListener("keyup", HighlightKeyUpFn);
	}
	exports.LayoutMappingDestroy = LayoutMappingDestroy;
	var HighlightKeyDownFn;
	var HighlightKeyUpFn;
	var HighlightKeyDown = function (that) { return function (e) {
	    that.highlightButton(e);
	}; };
	var HighlightKeyUp = function (that) { return function (e) {
	    that.unhighlightButton(e);
	}; };
	exports.default = LayoutMapping;
	exports.LanguageLayoutMap = {
	    'language_arabic_2d_edition': 'arabic',
	    'language_latin_boyd_pharr': 'english',
	    'language_latin': 'english',
	    'language_latin_jrjs': 'english',
	    'language_arabic': 'arabic',
	    'language_latin_lnm_2': 'english',
	    'language_arabic_through_ak_i_3d_edition': 'arabic',
	    'language_greek': 'greek',
	    'language_arabic_copy': 'arabic',
	    'language_latin_ecce': 'english',
	    'language_arabic_typing_module': 'arabic',
	    'language_russian_from_latin': 'russian',
	    'language_arabictext': 'arabic',
	    'language_latin_tarq_fall??': 'english',
	    'language_german': 'german',
	    'language_latin_henle_1': 'english',
	    'language_sounds_feb_12': 'english',
	    'language_spanish_archive_march_13_2014': 'spanish',
	    'language_spanish_current_1-2-3': 'spanish',
	    'language_latin_unzipped': 'english',
	    'language_latin_mueller_caesar': 'english',
	    'language_spanish_nexos_original_mainly': 'spanish',
	    'language_spanish_xxx': 'spanish',
	    'language_latin_wheelock': 'english',
	    'language_latin_tarquinius_rising': 'english',
	    'language_spanish': 'spanish',
	    'language_punjabi': 'punjabi'
	};
	exports.SourceLayoutMod = {
	    default: [
	        "` 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
	        "{tab} q w e r t y u i o p [ ] \\",
	        "{lock} a s d f g h j k l ; ' {enter}",
	        "{shift} z x c v b n m , . / {shift}",
	        ".com @ {space}"
	    ],
	    shift: [
	        "` 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
	        "{tab} q w e r t y u i o p [ ] \\",
	        "{lock} a s d f g h j k l ; ' {enter}",
	        "{shift} z x c v b n m , . / {shift}",
	        ".com @ {space}"
	    ]
	};
	// this is the set of keys per language that must
	// be pressed in combination
	exports.SuperKeysMap = {
	    spanish: [
	        // first is the supering key,
	        // second is the letter key
	        // third is the target letter
	        //['`','n','ñ'],
	        ["¨", 'u', 'ü'],
	        ["´", 'e', 'é'],
	        ["´", 'i', 'í'],
	        ["´", 'o', 'ó'],
	        ["´", 'u', 'ú'],
	        ["´", 'a', 'á']
	    ],
	    arabic: []
	};
	// SubKeys are letters that can be modified
	// by pushing other keys
	exports.SubKeysMap = {
	    punjabi: [
	        // the pattern goes that letter in first position
	        // can be turned into any letter in last position
	        // by pressing any keys in between in any order
	        // "a" can be modified by "b" to produce "e"
	        // ['a','b','e'],
	        // "a" can be modified by "c" to produce "f"
	        // ['a','c','f'],
	        // "a" can be modified by "c" of "b" to produce "g"
	        // note that the modified key
	        // ['a','c','b','g'],
	        // however if the target is "z"
	        // then you can git "a" then "b" then "g"
	        // to produce "z"
	        // by adding this:
	        ['b', 'g', 'z'],
	        // this works in languages like Punjabi
	        // where  key can be modified more than once
	        // by pressing the letter, then the modifiers
	        // so if there are two ways to produce "z"
	        // as in the order of the modifier doesn't matter
	        // then to reach a single modifier
	        // you can do this:
	        ['a', 'g', 'd'],
	        // and then reach the same double modifier "z"
	        // like this:
	        ['g', 'b', 'z']
	        // and the result is that you can
	        // produce "z" in two ways:
	        // a + b + g = z
	        // a + g + f = z
	        // and as well each of the combinations
	        // can be used to describe a single modified letter "a"
	        // a + b = c
	        // a + g = d
	        // to handle multi modifier keys
	        // in which the letter is pressed followed by
	    ]
	};
	});

	const arabic = {
	  default: [
	    "\u0630 \u0661 \u0662 \u0663 \u0664 \u0665 \u0666 \u0667 \u0668 \u0669 \u0660 \u002d \u003d {bksp}",
	    "{tab}  \u0636 \u0635 \u062b \u0642 \u0641 \u063a \u0639 \u0647 \u062e \u062d \u062c \u062f  \\",
	    "{lock}  \u0634 \u0633 \u064a \u0628 \u0644 \u0627 \u062a \u0646 \u0645 \u0643 \u0637  {enter}",
	    "{shift} \u0626 \u0621 \u0624 \u0631 \u0644\u0627 \u0649 \u0629 \u0648 \u0632 \u0638  {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "\u0021 \u0040 \u0023 \u0024 \u0025 \u005e \u0026 \u002a \u0029 \u0028 \u005f \u002b {bksp}",
	    "{tab}  \u064e \u064b \u064f \u064c \ufef9 \u0625 \u2019 \u00f7 \u00d7 \u061b \u003e \u003c \u007c",
	    '{lock}  \u0650 \u064d \u005d \u005b \u0644\u0623 \u0623 \u0640 \u060c \u002f \u003a \u0022  {enter}',
	    "{shift}  \u007e \u0652 \u007b \u007d \u0644\u0622 \u0622 \u2018 \u002c \u002e \u061f  {shift}",
	    ".com @ {space}"
	  ]
	};

	//export const arabic = {
	//  default: [
	//    "\u0630 ١ ٢ ٣ ٤ ٥ ٦ ٧ ٨ ٩ ٠ - = {bksp}",
	//    "{tab} ض ص ث ق ف غ ع ه خ ح ج د \\",
	//    "{lock} ش س ي ب ل ا ت ن م ك ط {enter}",
	//    "{shift} ئ ء ؤ ر لا ى ة و ز ظ {shift}",
	//    ".com @ {space}"
	//  ],
	//  shift: [
	//    "ّ ! @ # $ % ^ & * ) ( _ + {bksp}",
	//    "{tab} َ ً ُ ٌ ﻹ إ ’ ÷ × ؛ > < |",
	//   '{lock} ِ ٍ ] [ لأ أ ـ ، / : " {enter}',
	//    "{shift} ~ ْ { } لآ آ ‘ , . ؟ {shift}",
	//    ".com @ {space}"
	//  ]
	//};


	// Robert's Shift keys
	//
	// \u0634 \u0633 \u064A \u0628 \u0644 \u0627 \u062A \u0646 \u0645 \u0643 \u061B
	// \u0638 \u0637 \u0630 \u062F \u0632 \u0631 \u0648 \u060C \u002E

	const burmese = {
	  default: [
	    "\u1050 \u1041 \u1042 \u1043 \u1044 \u1045 \u1046 \u1047 \u1048 \u1049 \u1040 \u002D \u003D {bksp}",
	    "{tab} \u1006 \u1010 \u1014 \u1019 \u1021 \u1015 \u1000 \u1004 \u101E \u1005 \u101F \u1029 \u104F",
	    "{lock} \u1031 \u103A \u102D \u1039 \u102B \u1037 \u103B \u102F \u1030 \u1038 \u0027 {enter}",
	    "{shift} \u1016 \u1011 \u1001 \u101C \u1018 \u100A \u102C \u002C \u002E \u002F {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "\u100E \u100D \u1052 \u100B \u1053 \u1054 \u1055 \u101B \u002A \u0028 \u0029 \u005F \u002B {bksp}",
	    "{tab} \u1008 \u101D \u1023 \u104E \u1024 \u104C \u1025 \u104D \u103F \u100F \u1027 \u102A \u1051",
	    "{lock} \u1017 \u103D \u102E \u1064 \u103C \u1036 \u1032 \u1012 \u1013 \u1002 \u0022 {enter}",
	    "{shift} \u1007 \u100C \u1003 \u1020 \u101A \u1009 \u1026 \u104A \u104B \u003F {shift}",
	    ".com @ {space}"
	  ]
	};

	const chinese = {
	  default: [
	    "\u20AC \u3105 \u3109 \u02C7 \u02CB \u3113 \u02CA \u02D9 \u311A \u311E \u3122 \u3126 = {bksp}",
	    "{tab} \u3106 \u310A \u310D \u3110 \u3114 \u3117 \u3127 \u311B \u311F \u3123 [ ] \\",
	    "{lock} \u3107 \u310B \u310E \u3111 \u3115 \u3118 \u3128 \u311C \u3120 \u3124 ' {enter}",
	    "{shift} \u3108 \u310C \u310F \u3112 \u3116 \u3119 \u3129 \u311D \u3121 \u3125",
	    ".com @ {space}"
	  ],
	  shift: [
	    "~ ! @ # $ % ^ & * ) ( _ + {bksp}",
	    "{tab} q w e r t y u i o p { } |",
	    '{lock} a s d f g h j k l : " {enter}',
	    "{shift} z x c v b n m , < > ? {shift}",
	    ".com @ {space}"
	  ]
	};

	const czech = {
	  default: [
	    "; + \u011B \u0161 \u010D \u0159 \u017E \u00FD \u00E1 \u00ED \u00E9 \u00B4 {bksp}",
	    "{tab} q w e r t y u i o p \u00FA ) \u00A8",
	    "{lock} a s d f g h j k l \u016F \u00A7 {enter}",
	    "{shift} \\ z x c v b n m , . - {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "\u00b0 1 2 3 4 5 6 7 8 9 0 % \u02c7 {bksp}",
	    "{tab} Q W E R T Y U I O P / ( '",
	    '{lock} A S D F G H J K L " ! {enter}',
	    "{shift} | Z X C V B N M ? : _ {shift}",
	    ".com @ {space}"
	  ]
	};

	const english = {
	  default: [
	    "` 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
	    "{tab} \u0071 w e r t y u i o p [ ] \\",
	    "{lock} \u0061 s d f g h j k l ; ' {enter}",
	    "{shift} z x c v b n m , . / {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "~ ! @ # $ % ^ & * ( ) _ + {bksp}",
	    "{tab} Q W E R T Y U I O P { } |",
	    '{lock} A S D F G H J K L : " {enter}',
	    "{shift} Z X C V B N M < > ? {shift}",
	    ".com @ {space}"
	  ]
	};

	const french = {
	  default: [
	    "\u00B2 & \u00E9 \" ' ( - \u00E8 _ \u00E7 \u00E0 ) = {bksp}",
	    "{tab} a z e r t y u i o p ^ $",
	    "{lock} q s d f g h j k l m \u00F9 * {enter}",
	    "{shift} < w x c v b n , ; : ! {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "{//} 1 2 3 4 5 6 7 8 9 0 \u00B0 + {bksp}",
	    "{tab} A Z E R T Y U I O P \u00A8 \u00A3",
	    "{lock} Q S D F G H J K L M % \u00B5 {enter}",
	    "{shift} > W X C V B N ? . / \u00A7 {shift}",
	    ".com @ {space}"
	  ]
	};

	const georgian = {
	  default: [
	    "„ 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
	    "{tab} ქ წ ე რ ტ ყ უ ი ო პ [ ] \\",
	    "{lock} ა ს დ ფ გ ჰ ჯ კ ლ ; ' {enter}",
	    "{shift} ზ ხ ც ვ ბ ნ მ , . / {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "“ ! @ # $ % ^ & * ( ) _ + {bksp}",
	    "{tab} ქ ჭ ე ღ თ ყ უ ი ო პ { } | ~",
	    '{lock} ა შ დ ფ გ ჰ ჟ კ ₾ : " {enter}',
	    "{shift} ძ ხ ჩ ვ ბ ნ მ < > ? {shift}",
	    ".com @ {space}"
	  ]
	};

	const german = {
	  default: [
	    "\u005E \u0031 \u0032 \u0033 \u0034 \u0035 \u0036 \u0037 \u0038 \u0039 \u0030 \u00DF \u00B4 {bksp}",
	    "{tab} \u0071 \u0077 \u0065 \u0072 \u0074 \u007A \u0075 \u0069 \u006F \u0070 \u00FC \u002B",
	    "{lock} \u0061 \u0073 \u0064 \u0066 \u0067 \u0068 \u006A \u006B \u006C \u00F6 \u00E4 # {enter}",
	    "{shift} \u003C \u0079 \u0078 \u0063 \u0076 \u0062 \u006E \u006D \u002C \u002E \u002D {shift}",
	    ".com \u0040 {space}"
	  ],
	  shift: [
	    '\u00B0 \u0021 \u0022 \u00A7 \u20AC \u0025 \u0026 \u002F \u0028 \u0029 \u003D \u003F \u0060 {bksp}',
	    "{tab} \u0051 \u0057 \u0045 \u0052 \u0054 \u005A \u0055 \u0049 \u004F \u0050 \u00DC \u002A",
	    "{lock} \u0041 \u0053 \u0044 \u0046 \u0047 \u0048 \u004A \u004B \u004C \u00D6 \u00C4 \u0027 {enter}",
	    "{shift} \u003E \u0059 \u0058 \u0043 \u0056 \u0042 \u004E \u004D \u003B \u003A \u005F {shift}",
	    ".com \u0040 {space}"
	  ]
	};


	//export const german = {
	//  default: [
	//    "^ 1 2 3 4 5 6 7 8 9 0 \u00DF \u00B4 {bksp}",
	//    "{tab} q w e r t z u i o p \u00FC +",
	//    "{lock} a s d f g h j k l \u00F6 \u00E4 # {enter}",
	//    "{shift} < y x c v b n m , . - {shift}",
	//    ".com @ {space}"
	//  ],
	//  shift: [
	//    '\u00B0 ! " \u00A7 $ % & / ( ) = ? ` {bksp}',
	//    "{tab} Q W E R T Z U I O P \u00DC *",
	//    "{lock} A S D F G H J K L \u00D6 \u00C4 ' {enter}",
	//    "{shift} > Y X C V B N M ; : _ {shift}",
	//    ".com @ {space}"
	//  ]
	//};

	const hindi = {
	  default: [
	    "` \u090D \u0945 \u094D\u0930 \u0930\u094D \u091C\u094D\u091E \u0924\u094D\u0930 \u0915\u094D\u0937 \u0936\u094D\u0930 \u096F \u0966 - \u0943 {bksp}",
	    "{tab} \u094C \u0948 \u093E \u0940 \u0942 \u092C \u0939 \u0917 \u0926 \u091C \u0921 \u093C \u0949 \\",
	    "{lock} \u094B \u0947 \u094D \u093F \u0941 \u092A \u0930 \u0915 \u0924 \u091A \u091F {enter}",
	    "{shift} \u0902 \u092E \u0928 \u0935 \u0932 \u0938 , . \u092F {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "~ \u0967 \u0968 \u0969 \u096A \u096B \u096C \u096D \u096E \u096F \u0966 \u0903 \u090B {bksp}",
	    "{tab} \u0914 \u0910 \u0906 \u0908 \u090A \u092D \u0919 \u0918 \u0927 \u091D \u0922 \u091E \u0911",
	    "{lock} \u0913 \u090F \u0905 \u0907 \u0909 \u092B \u0931 \u0916 \u0925 \u091B \u0920 {enter}",
	    '{shift} "" \u0901 \u0923 \u0928 \u0935 \u0933 \u0936 \u0937 \u0964 \u095F {shift}',
	    ".com @ {space}"
	  ]
	};

	//for some keys in Punjabi we have to have 3 options, I'm not sure how to write that into the keyboard. 
	//these are all Punjabi symbols on the keyboard we need, missing are numbers and some diacritics such as : or ;

	const punjabi = {
	  default: [
	    "\u0a67 \u0a68 \u0a69 \u0a6a \u0a6b \u0a6c \u0a6d \u0a6e \u0a6f \u0a66 \u002d \u003d {bksp}",
	    "{tab} \u0a73 \u0a05 \u0a47 \u0a30 \u0a24 \u0a2f \u0a41 \u0a3f \u0a4b \u0a2a \u0a71 \u0a70 \u005c",
	    "{lock} \u0a3e \u0a38 \u0a26 \u0a4d \u0a17 \u0a39 \u0a1c \u0a15 \u0a32 \u003b \u0027 {enter}",
	    "{shift} \u0a19 \u0a5c \u0a1a \u0a35 \u0a2c \u0a28 \u0a2e \u002c \u0964 \u002f {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "\u0a75 \u0021 \u0040 \u0023 \u0024 \u0025 \u005e \u0026 \u002a \u0028 \u0029 \u005f \u002b {bksp}",
	    "{tab} \u0a72 \u0a06 \u0a48 \u0a25 \u0a42 \u0a40 \u0a4c \u0a2b \u005b \u005d \u007c" ,
	    "{lock} \u0a3e \u0a36 \u0a27 \u0a3c \u0a18 \u0a03 \u0a1d \u0a16 \u0a33 \u003a \u0022 {enter}",
	     "{shift}  \u0a5c\u0a4d\u0a39 \u0a1b \u0a74 \u0a2d \u0a23 \u0a02 \u002e \u0965 \u003f {shift}",
	    ".com @ {space}"
	//  ],
	//  option: [
	//    "\u0060 \u0031 \u0032 \u0033 \u0034 \u0035 \u0036 \u0037 \u0038 \u0039 \u0030 \u002d \u003d {bksp}",
	//    "{tab} \u0a0f \u0a1f \u0a09 \u0a07 \u0a13 \u0a5e \u005b \u005d \u005c" ,
	//    "{lock} \u0a05 \u0a21 \u0a5a \u0a5b \u0a59 \u003b \u0027 {enter}",
	//     "{shift} \u0a1e \u0a01 \u003c \u003e {shift}",
	//    ".com @ {space}"
	  ]
	};


	//export const punjabi = {
	//  default: [
	//    "` \u090D \u0945 \u094D\u0930 \u0930\u094D \u091C\u094D\u091E \u0924\u094D\u0930 \u0915\u094D\u0937 \u0936\u094D\u0930 \u096F \u0966 - \u0943 {bksp}",
	//    "{tab} \u094C \u0948 \u093E \u0940 \u0942 \u092C \u0939 \u0917 \u0926 \u091C \u0921 \u093C \u0949 \\",
	//    "{lock} \u094B \u0947 \u094D \u093F \u0941 \u092A \u0930 \u0915 \u0924 \u091A \u091F {enter}",
	//    "{shift} \u0902 \u092E \u0928 \u0935 \u0932 \u0938 , . \u092F {shift}",
	//    ".com @ {space}"
	//  ],
	//  shift: [
	//    "~ \u0967 \u0968 \u0969 \u096A \u096B \u096C \u096D \u096E \u096F \u0966 \u0903 \u090B {bksp}",
	//    "{tab} \u0914 \u0910 \u0906 \u0908 \u090A \u092D \u0919 \u0918 \u0927 \u091D \u0922 \u091E \u0911",
	//    "{lock} \u0913 \u090F \u0905 \u0907 \u0909 \u092B \u0931 \u0916 \u0925 \u091B \u0920 {enter}",
	//    '{shift} "" \u0901 \u0923 \u0928 \u0935 \u0933 \u0936 \u0937 \u0964 \u095F {shift}',
	//    ".com @ {space}"
	//  ]
	// };

	const italian = {
	  default: [
	    "\\ 1 2 3 4 5 6 7 8 9 0 ' \u00EC {bksp}",
	    "{tab} q w e r t y u i o p \u00E8 +",
	    "{lock} a s d f g h j k l \u00F2 \u00E0 \u00F9 {enter}",
	    "{shift} < z x c v b n m , . - {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    '| ! " \u00A3 $ % & / ( ) = ? ^ {bksp}',
	    "{tab} Q W E R T Y U I O P \u00E9 *",
	    "{lock} A S D F G H J K L \u00E7 \u00B0 \u00A7 {enter}",
	    "{shift} > Z X C V B N M ; : _ {shift}",
	    ".com @ {space}"
	  ]
	};

	const japanese = {
	  default: [
	    "\u308d \u306c \u3075 \u3042 \u3046 \u3048 \u304a \u3084 \u3086 \u3088 \u308f \u307b \u3078 {bksp}",
	    "{tab} \u305f \u3066 \u3044 \u3059 \u304b \u3093 \u306a \u306b \u3089 \u305b \u309b \u309c \u3080",
	    "{lock} \u3061 \u3068 \u3057 \u306f \u304D \u304f \u307e \u306e \u308a \u308c \u3051 {enter}",
	    "{shift} \u3064 \u3055 \u305d \u3072 \u3053 \u307f \u3082 \u306d \u308b \u3081 {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "\u308d \u306c \u3075 \u3041 \u3045 \u3047 \u3049 \u3083 \u3085 \u3087 \u3092 \u30fc \u3078 {bksp}",
	    "{tab} \u305f \u3066 \u3043 \u3059 \u304b \u3093 \u306a \u306b \u3089 \u305b \u300c \u300d \u3080",
	    "{lock} \u3061 \u3068 \u3057 \u306f \u304D \u304f \u307e \u306e \u308a \u308c \u3051 {enter}",
	    "{shift} \u3063 \u3055 \u305d \u3072 \u3053 \u307f \u3082 \u3001 \u3002 \u30fb {shift}",
	    ".com @ {space}"
	  ]
	};

	const korean = {
	  default: [
	    "` 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
	    "{tab} \u1107 \u110c \u1103 \u1100 \u1109 \u116d \u1167 \u1163 \u1162 \u1166 [ ] \u20a9",
	    "{lock} \u1106 \u1102 \u110b \u1105 \u1112 \u1169 \u1165 \u1161 \u1175 ; ' {enter}",
	    "{shift} \u110f \u1110 \u110e \u1111 \u1172 \u116e \u1173 , . / {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "~ ! @ # $ % ^ & * ( ) _ + {bksp}",
	    "{tab} \u1108 \u110d \u1104 \u1101 \u110a \u116d \u1167 \u1163 \u1164 \u1168 { } |",
	    '{lock} \u1106 \u1102 \u110b \u1105 \u1112 \u1169 \u1165 \u1161 \u1175 : " {enter}',
	    "{shift} \u110f \u1110 \u110e \u1111 \u1172 \u116e \u1173 < > ? {shift}",
	    ".com @ {space}"
	  ]
	};

	const russian = {
	  default: [
	    "\u0451 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
	    "{tab} \u0439 \u0446 \u0443 \u043a \u0435 \u043d \u0433 \u0448 \u0449 \u0437 \u0445 \u044a \\",
	    "{lock} \u0444 \u044b \u0432 \u0430 \u043f \u0440 \u043e \u043b \u0434 \u0436 \u044d {enter}",
	    "{shift} \\ \u044f \u0447 \u0441 \u043c \u0438 \u0442 \u044c \u0431 \u044e / {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    '\u0401 ! " \u2116 ; % : ? * ( ) _ + {bksp}',
	    "{tab} \u0419 \u0426 \u0423 \u041a \u0415 \u041d \u0413 \u0428 \u0429 \u0417 \u0425 \u042a /",
	    "{lock} \u0424 \u042b \u0412 \u0410 \u041f \u0420 \u041e \u041b \u0414 \u0416 \u042d {enter}",
	    "{shift} / \u042f \u0427 \u0421 \u041c \u0418 \u0422 \u042c \u0411 \u042e / {shift}",
	    ".com @ {space}"
	  ]
	};

	const sindhi = {
	  default: [
	    "` \u0661 \u0662 \u0663 \u0664 \u0665 \u0666 \u0667 \u0668 \u0669 \u0660 - = {bksp}",
	    "{tab} \u0642 \uFEED \u0639 \u0631 \u062A \u0680 \u0621 \u064A \uFBA6 \u067E [ ]",
	    "{lock} \u0627 \u0633 \u062F \u0641 \u06AF \u06BE \u062C \u06A9 \u0644 \u061B \u060C {enter}",
	    "{shift} \u0632 \u0634 \u0686 \u0637 \u0628 \u0646 \u0645 \u0687 , . / {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "~ ! @ # $ \u066A ^ & * ( ) _ + {bksp}",
	    "{tab} \uFE70 \u068C \u06AA \u0699 \u067D \uFE7A \uFEFB \uFE8B \u06A6 | { }",
	    "{lock} \u067B \u0635 \u068A \u060D \u063A \u062D \u0636 \u062E \u06D4 \u0703 \u05f4 {enter}",
	    "{shift} \u0630 \u067F \u062B \u0638 \u067A \u066b \u0640 < > \u061F {shift}",
	    ".com @ {space}"
	  ]
	};

	/*
	 * Spanish layouts by Paco Alcantara (https://github.com/pacoalcantara)
	 * Based on: http://ascii-table.com/keyboard.php/171 and http://ascii-table.com/keyboard.php/071-2
	 */

	const spanish = {
	  default: [
	    // "\u007c 1 2 3 4 5 6 7 8 9 0 ' ¡ {bksp}",
	    // "{tab} q w e r t y u i o p \u0301 +",
	    // "{lock} a s d f g h j k l \u00f1 \u007b \u007d {enter}",
	    // "{shift} < z x c v b n m , . - {shift}",
	    // ".com @ {space}"


	    // < 1 2 3 4 5 6 7 8 9 0 ‘ ¡ ¿
	    "< 1 2 3 4 5 6 7 8 9 0 ‘ ¡ ¿ {bksp}",
	    // q w e r t y u i o p `+ ç
	    "{tab} q w e r t y u i o p ` + ç",
	    // a s d f g h j k l ñ ´
	    "{lock} a s d f g h j k l ñ ´ {enter}",
	    // z x c v b n m , . -
	    "{shift} z x c v b n m , . - {shift}",
	    ".com @ {space} @ .com"
	  ],
	  shift: [
	    // > ! ” · $ % & / ( ) = ? ¿
	    '> ! ” · $ % & / ( ) = ? ¿ {bksp}',
	    // Q W E R T Y U I O P ^* Ç
	    "{tab} Q W E R T Y U I O P ^ * Ç",
	    //  A S D F G H J K L Ñ ¨
	    "{lock} A S D F G H J K L Ñ ¨ {enter}",
	    // Z X C V B N M ; : _
	    "{shift} Z X C V B N M ; : _ {shift}",
	    ".com @ {space}"
	  ]
	 };


	// const spanish = {
	//   default: [
	//     "\u007c 1 2 3 4 5 6 7 8 9 0 ' \u00bf {bksp}",
	//     "{tab} q w e r t y u i o p \u0301 +",
	//     "{lock} a s d f g h j k l \u00f1 \u007b \u007d {enter}",
	//     "{shift} < z x c v b n m , . - {shift}",
	//     ".com @ {space}"
	//   ],
	//   shift: [
	//     '\u00b0 ! " # $ % & / ( ) = ? \u00a1 {bksp}',
	//     "{tab} Q W E R T Y U I O P \u0308 *",
	//     "{lock} A S D F G H J K L \u00d1 \u005b \u005d {enter}",
	//     "{shift} > Z X C V B N M ; : _ {shift}",
	//     ".com @ {space}"
	//   ]
	// };

	const thai = {
	  default: [
	    "\u005F \u0E45 \u002F \u002D \u0E20 \u0E16 \u0E38 \u0E36 \u0E04 \u0E05 \u0E08 \u0E02 \u0E0A {bksp}",
	    "{tab} \u0E46 \u0E44 \u0E33 \u0E1E \u0E30 \u0E31 \u0E35 \u0E23 \u0E19 \u0E22 \u0E1A \u0E25 \u0E03 ",
	    "{lock} \u0E1F \u0E2B \u0E01 \u0E14 \u0E40 \u0E49 \u0E48 \u0E32 \u0E2A \u0E27 \u0E07 {enter}",
	    "{shift} \u0E1C \u0E1B \u0E41 \u0E2D \u0E34 \u0E37 \u0E17 \u0E21 \u0E43 \u0E1D {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "% + \u0E51 \u0E52 \u0E53 \u0E54 \u0E39 \u0E3F \u0E55 \u0E56 \u0E57 \u0E58 \u0E59 {bksp}",
	    "{tab} \u0E50 \u0022 \u0E0E \u0E11 \u0E18 \u0E4D \u0E4A \u0E13 \u0E2F \u0E0D \u0E10 \u002C \u0E05",
	    "{lock} \u0E24 \u0E06 \u0E0F \u0E42 \u0E0C \u0E47 \u0E4B \u0E29 \u0E28 \u0E0B \u002E {enter}",
	    "{shift} ( ) \u0E09 \u0E2E \u0E3A \u0E4C \u003F \u0E12 \u0E2C \u0E26 {shift}",
	    ".com @ {space}"
	  ]
	};

	const turkish = {
	  default: [
	    '" 1 2 3 4 5 6 7 8 9 0 * - # {bksp}',
	    "{tab} q w e r t y u ı o p ğ ü [ ]",
	    "{lock} a s d f g h j k l ş i , {enter}",
	    "{shift} < z x c v b n m ö ç . | $ € {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "é ! ' ^ + % & / ( ) = ? _ ~ {bksp}",
	    "{tab} Q W E R T Y U I O P Ğ Ü { }",
	    "{lock} A S D F G H J K L Ş İ ; {enter}",
	    "{shift} > Z X C V B N M Ö Ç : \\ ` ´ {shift}",
	    ".com @ {space}"
	  ]
	};

	// import arabic from './arabic'
	// import burmese from './burmese'
	// import chinese from './chinese'
	// import czech from './czech'
	// import english from './english'
	// import french from './french'
	// import georgian from './georgian'
	// import german from './german'
	// import hindi from './hindi'
	// import punjabi from './punjabi'
	// import italian from './italian'
	// import japanese from './japanese'
	// import korean from './korean'
	// import russian from './russian'
	// import sindhi from './sindhi'
	// import spanish from './spanish'
	// import thai from './thai'
	// import turkish from './turkish'
	// import urdu from './urdu'

	// export default {
	//   arabic,
	//   burmese,
	//   chinese,
	//   czech,
	//   english,
	//   french,
	//   georgian,
	//   german,
	//   hindi,
	//   punjabi,
	//   italian,
	//   japanese,
	//   korean,
	//   russian,
	//   sindhi,
	//   spanish,
	//   thai,
	//   turkish,
	//   urdu
	// }

	var layoutsEyevocab = /*#__PURE__*/Object.freeze({
		__proto__: null,
		arabic: arabic,
		burmese: burmese,
		chinese: chinese,
		czech: czech,
		english: english,
		french: french,
		georgian: georgian,
		german: german,
		hindi: hindi,
		punjabi: punjabi,
		italian: italian,
		japanese: japanese,
		korean: korean,
		russian: russian,
		sindhi: sindhi,
		spanish: spanish,
		thai: thai,
		turkish: turkish
	});

	var keyboard = createCommonjsModule(function (module, exports) {
	var __createBinding = (commonjsGlobal && commonjsGlobal.__createBinding) || (Object.create ? (function(o, m, k, k2) {
	    if (k2 === undefined) k2 = k;
	    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
	}) : (function(o, m, k, k2) {
	    if (k2 === undefined) k2 = k;
	    o[k2] = m[k];
	}));
	var __setModuleDefault = (commonjsGlobal && commonjsGlobal.__setModuleDefault) || (Object.create ? (function(o, v) {
	    Object.defineProperty(o, "default", { enumerable: true, value: v });
	}) : function(o, v) {
	    o["default"] = v;
	});
	var __importStar = (commonjsGlobal && commonjsGlobal.__importStar) || function (mod) {
	    if (mod && mod.__esModule) return mod;
	    var result = {};
	    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
	    __setModuleDefault(result, mod);
	    return result;
	};
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.InputUnicodeGet = exports.InputLetterGet = exports.InputStringGet = exports.KeyboardInit = exports.LangList = void 0;
	var mithril_1 = __importDefault(mithril);
	var input_1 = __importDefault(input);
	var simple_keyboard_1 = __importDefault(build$2);
	var simplekeyboard_layout_mapping_1 = __importStar(simplekeyboard_layout_mapping);
	var LayoutsEyevocab = __importStar(layoutsEyevocab);
	// list our language codes
	exports.LangList = Object.keys(LayoutsEyevocab);
	// initialize our general input listener for super keys
	var InputControl = input_1.default({
	    onKeyDown: onKeyDown,
	    onKeyUp: onKeyUp,
	    onShift: onShift,
	    onEnter: onEnter
	});
	// reference our Keyboard instance
	var KeyboardControl;
	/**
	 * Function initializes keyboard per language
	 */
	exports.KeyboardInit = function (lang) {
	    if (lang === void 0) { lang = 'english'; }
	    InputReset();
	    if (KeyboardControl) {
	        KeyboardControl.destroy();
	        simplekeyboard_layout_mapping_1.LayoutMappingDestroy();
	    }
	    // creating a keyboard instance returns a controller for existing keyboard instance
	    return KeyboardControl = new simple_keyboard_1.default({
	        onChange: onChange,
	        disableCaretPositioning: true,
	        sourceLayout: 'english',
	        layout: LayoutsEyevocab[lang],
	        modules: [simplekeyboard_layout_mapping_1.default]
	    });
	};
	function onShift(upOrDown) {
	    KeyboardControl.setOptions({
	        layoutName: upOrDown === 'down' ? "shift" : "default"
	    });
	}
	function onEnter(upOrDown) {
	    console.log(upOrDown, 'enter');
	    upOrDown === 'down' && InputReset();
	}
	function onKeyDown() {
	}
	function onKeyUp() {
	}
	function onChange(input) {
	    KeyboardControl.setInput('');
	    // if we are using Alt then we don't record any change
	    if (InputControl.isAltPressed()) {
	        return;
	    }
	    InputString = InputString + (InputLetter = input);
	    mithril_1.default.redraw();
	}
	// the Input
	var InputString = '';
	var InputLetter = '';
	exports.InputStringGet = function () { return InputString; };
	exports.InputLetterGet = function () { return InputLetter; };
	exports.InputUnicodeGet = function () { return InputLetter.length && ('\\u' + InputLetter.charCodeAt(0).toString(16).padStart(4, '0')) || ''; };
	var InputReset = function () {
	    InputString = '';
	    InputLetter = '';
	    mithril_1.default.redraw();
	};
	});

	var router$1 = createCommonjsModule(function (module, exports) {
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.Router = void 0;

	// initiate the Lang var with a default value 
	// which is whatever is the first language in our list
	var Lang = keyboard.LangList[0];
	/**
	 * Manages route changes.
	 */
	exports.Router = function (lang) {
	    // if we are changing languages
	    if (Lang !== lang) {
	        // reset the route var
	        Lang = lang;
	        // if the lang code is NOT in the list of Languages
	        if (keyboard.LangList.indexOf(lang) < 0) {
	            // Send them back to start
	            window.location.href = "/keyboard/#!/" + keyboard.LangList[0];
	            return;
	        }
	        // change the window location so the url is right.
	        window.location.href = "/keyboard/#!/" + lang;
	        // create a new keyboard with the routed language
	        keyboard.KeyboardInit(lang);
	    }
	};
	/**
	 * When the form input changes, handle input, event and route the change.
	 */
	exports.Router.fromInput = function (e) {
	    exports.Router(e.target.value);
	    e.target.blur();
	};
	/**
	 * When the window location hash changes, handle the url and route the change.
	 */
	exports.Router.fromUrl = function (url) {
	    exports.Router(url && url.split('/').pop());
	};
	});

	var keyboard$1 = createCommonjsModule(function (module, exports) {
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	var mithril_1 = __importDefault(mithril);


	var App = {
	    oncreate: function () {
	        router$1.Router(mithril_1.default.route.param('lang'));
	    },
	    view: function () {
	        return (mithril_1.default("div", null,
	            mithril_1.default("h1", null, "EyeVocab #5"),
	            mithril_1.default("select", { value: mithril_1.default.route.param('lang'), onchange: router$1.Router.fromInput }, keyboard.LangList.map(function (lang) { return (mithril_1.default("option", { value: lang }, lang)); })),
	            mithril_1.default("br", null),
	            mithril_1.default("div", { id: "keyboard", class: "simple-keyboard" }),
	            mithril_1.default("br", null),
	            mithril_1.default("table", null,
	                mithril_1.default("tr", null,
	                    mithril_1.default("td", null, "input letter: "),
	                    mithril_1.default("td", null,
	                        mithril_1.default("input", { value: keyboard.InputLetterGet() }))),
	                mithril_1.default("tr", null,
	                    mithril_1.default("td", null, "unicode: "),
	                    mithril_1.default("td", null,
	                        mithril_1.default("input", { value: keyboard.InputUnicodeGet() }))),
	                mithril_1.default("tr", null,
	                    mithril_1.default("td", null, "input string: "),
	                    mithril_1.default("td", null,
	                        mithril_1.default("input", { value: keyboard.InputStringGet() }))))));
	    }
	};
	window.onhashchange = function (e) { return router$1.Router.fromUrl(e.newURL); };
	mithril_1.default.route(document.getElementById("m-app"), "/english", {
	    // auth
	    "/:lang": App
	});
	});

	return keyboard$1;

}());
//# sourceMappingURL=keyboard.js.map

(function () {
	'use strict';

	var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function createCommonjsModule(fn, basedir, module) {
		return module = {
		  path: basedir,
		  exports: {},
		  require: function (path, base) {
	      return commonjsRequire(path, (base === undefined || base === null) ? module.path : base);
	    }
		}, fn(module, module.exports), module.exports;
	}

	function getCjsExportFromNamespace (n) {
		return n && n['default'] || n;
	}

	function commonjsRequire () {
		throw new Error('Dynamic requires are not currently supported by @rollup/plugin-commonjs');
	}

	function Vnode(tag, key, attrs, children, text, dom) {
		return {tag: tag, key: key, attrs: attrs, children: children, text: text, dom: dom, domSize: undefined, state: undefined, events: undefined, instance: undefined}
	}
	Vnode.normalize = function(node) {
		if (Array.isArray(node)) return Vnode("[", undefined, undefined, Vnode.normalizeChildren(node), undefined, undefined)
		if (node == null || typeof node === "boolean") return null
		if (typeof node === "object") return node
		return Vnode("#", undefined, undefined, String(node), undefined, undefined)
	};
	Vnode.normalizeChildren = function(input) {
		var children = [];
		if (input.length) {
			var isKeyed = input[0] != null && input[0].key != null;
			// Note: this is a *very* perf-sensitive check.
			// Fun fact: merging the loop like this is somehow faster than splitting
			// it, noticeably so.
			for (var i = 1; i < input.length; i++) {
				if ((input[i] != null && input[i].key != null) !== isKeyed) {
					throw new TypeError("Vnodes must either always have keys or never have keys!")
				}
			}
			for (var i = 0; i < input.length; i++) {
				children[i] = Vnode.normalize(input[i]);
			}
		}
		return children
	};

	var vnode = Vnode;

	// Call via `hyperscriptVnode.apply(startOffset, arguments)`
	//
	// The reason I do it this way, forwarding the arguments and passing the start
	// offset in `this`, is so I don't have to create a temporary array in a
	// performance-critical path.
	//
	// In native ES6, I'd instead add a final `...args` parameter to the
	// `hyperscript` and `fragment` factories and define this as
	// `hyperscriptVnode(...args)`, since modern engines do optimize that away. But
	// ES5 (what Mithril requires thanks to IE support) doesn't give me that luxury,
	// and engines aren't nearly intelligent enough to do either of these:
	//
	// 1. Elide the allocation for `[].slice.call(arguments, 1)` when it's passed to
	//    another function only to be indexed.
	// 2. Elide an `arguments` allocation when it's passed to any function other
	//    than `Function.prototype.apply` or `Reflect.apply`.
	//
	// In ES6, it'd probably look closer to this (I'd need to profile it, though):
	// module.exports = function(attrs, ...children) {
	//     if (attrs == null || typeof attrs === "object" && attrs.tag == null && !Array.isArray(attrs)) {
	//         if (children.length === 1 && Array.isArray(children[0])) children = children[0]
	//     } else {
	//         children = children.length === 0 && Array.isArray(attrs) ? attrs : [attrs, ...children]
	//         attrs = undefined
	//     }
	//
	//     if (attrs == null) attrs = {}
	//     return Vnode("", attrs.key, attrs, children)
	// }
	var hyperscriptVnode = function() {
		var attrs = arguments[this], start = this + 1, children;

		if (attrs == null) {
			attrs = {};
		} else if (typeof attrs !== "object" || attrs.tag != null || Array.isArray(attrs)) {
			attrs = {};
			start = this;
		}

		if (arguments.length === start + 1) {
			children = arguments[start];
			if (!Array.isArray(children)) children = [children];
		} else {
			children = [];
			while (start < arguments.length) children.push(arguments[start++]);
		}

		return vnode("", attrs.key, attrs, children)
	};

	var selectorParser = /(?:(^|#|\.)([^#\.\[\]]+))|(\[(.+?)(?:\s*=\s*("|'|)((?:\\["'\]]|.)*?)\5)?\])/g;
	var selectorCache = {};
	var hasOwn = {}.hasOwnProperty;

	function isEmpty(object) {
		for (var key in object) if (hasOwn.call(object, key)) return false
		return true
	}

	function compileSelector(selector) {
		var match, tag = "div", classes = [], attrs = {};
		while (match = selectorParser.exec(selector)) {
			var type = match[1], value = match[2];
			if (type === "" && value !== "") tag = value;
			else if (type === "#") attrs.id = value;
			else if (type === ".") classes.push(value);
			else if (match[3][0] === "[") {
				var attrValue = match[6];
				if (attrValue) attrValue = attrValue.replace(/\\(["'])/g, "$1").replace(/\\\\/g, "\\");
				if (match[4] === "class") classes.push(attrValue);
				else attrs[match[4]] = attrValue === "" ? attrValue : attrValue || true;
			}
		}
		if (classes.length > 0) attrs.className = classes.join(" ");
		return selectorCache[selector] = {tag: tag, attrs: attrs}
	}

	function execSelector(state, vnode$1) {
		var attrs = vnode$1.attrs;
		var children = vnode.normalizeChildren(vnode$1.children);
		var hasClass = hasOwn.call(attrs, "class");
		var className = hasClass ? attrs.class : attrs.className;

		vnode$1.tag = state.tag;
		vnode$1.attrs = null;
		vnode$1.children = undefined;

		if (!isEmpty(state.attrs) && !isEmpty(attrs)) {
			var newAttrs = {};

			for (var key in attrs) {
				if (hasOwn.call(attrs, key)) newAttrs[key] = attrs[key];
			}

			attrs = newAttrs;
		}

		for (var key in state.attrs) {
			if (hasOwn.call(state.attrs, key) && key !== "className" && !hasOwn.call(attrs, key)){
				attrs[key] = state.attrs[key];
			}
		}
		if (className != null || state.attrs.className != null) attrs.className =
			className != null
				? state.attrs.className != null
					? String(state.attrs.className) + " " + String(className)
					: className
				: state.attrs.className != null
					? state.attrs.className
					: null;

		if (hasClass) attrs.class = null;

		for (var key in attrs) {
			if (hasOwn.call(attrs, key) && key !== "key") {
				vnode$1.attrs = attrs;
				break
			}
		}

		if (Array.isArray(children) && children.length === 1 && children[0] != null && children[0].tag === "#") {
			vnode$1.text = children[0].children;
		} else {
			vnode$1.children = children;
		}

		return vnode$1
	}

	function hyperscript(selector) {
		if (selector == null || typeof selector !== "string" && typeof selector !== "function" && typeof selector.view !== "function") {
			throw Error("The selector must be either a string or a component.");
		}

		var vnode$1 = hyperscriptVnode.apply(1, arguments);

		if (typeof selector === "string") {
			vnode$1.children = vnode.normalizeChildren(vnode$1.children);
			if (selector !== "[") return execSelector(selectorCache[selector] || compileSelector(selector), vnode$1)
		}

		vnode$1.tag = selector;
		return vnode$1
	}

	var hyperscript_1 = hyperscript;

	var trust = function(html) {
		if (html == null) html = "";
		return vnode("<", undefined, undefined, html, undefined, undefined)
	};

	var fragment = function() {
		var vnode$1 = hyperscriptVnode.apply(0, arguments);

		vnode$1.tag = "[";
		vnode$1.children = vnode.normalizeChildren(vnode$1.children);
		return vnode$1
	};

	hyperscript_1.trust = trust;
	hyperscript_1.fragment = fragment;

	var hyperscript_1$1 = hyperscript_1;

	/** @constructor */
	var PromisePolyfill = function(executor) {
		if (!(this instanceof PromisePolyfill)) throw new Error("Promise must be called with `new`")
		if (typeof executor !== "function") throw new TypeError("executor must be a function")

		var self = this, resolvers = [], rejectors = [], resolveCurrent = handler(resolvers, true), rejectCurrent = handler(rejectors, false);
		var instance = self._instance = {resolvers: resolvers, rejectors: rejectors};
		var callAsync = typeof setImmediate === "function" ? setImmediate : setTimeout;
		function handler(list, shouldAbsorb) {
			return function execute(value) {
				var then;
				try {
					if (shouldAbsorb && value != null && (typeof value === "object" || typeof value === "function") && typeof (then = value.then) === "function") {
						if (value === self) throw new TypeError("Promise can't be resolved w/ itself")
						executeOnce(then.bind(value));
					}
					else {
						callAsync(function() {
							if (!shouldAbsorb && list.length === 0) console.error("Possible unhandled promise rejection:", value);
							for (var i = 0; i < list.length; i++) list[i](value);
							resolvers.length = 0, rejectors.length = 0;
							instance.state = shouldAbsorb;
							instance.retry = function() {execute(value);};
						});
					}
				}
				catch (e) {
					rejectCurrent(e);
				}
			}
		}
		function executeOnce(then) {
			var runs = 0;
			function run(fn) {
				return function(value) {
					if (runs++ > 0) return
					fn(value);
				}
			}
			var onerror = run(rejectCurrent);
			try {then(run(resolveCurrent), onerror);} catch (e) {onerror(e);}
		}

		executeOnce(executor);
	};
	PromisePolyfill.prototype.then = function(onFulfilled, onRejection) {
		var self = this, instance = self._instance;
		function handle(callback, list, next, state) {
			list.push(function(value) {
				if (typeof callback !== "function") next(value);
				else try {resolveNext(callback(value));} catch (e) {if (rejectNext) rejectNext(e);}
			});
			if (typeof instance.retry === "function" && state === instance.state) instance.retry();
		}
		var resolveNext, rejectNext;
		var promise = new PromisePolyfill(function(resolve, reject) {resolveNext = resolve, rejectNext = reject;});
		handle(onFulfilled, instance.resolvers, resolveNext, true), handle(onRejection, instance.rejectors, rejectNext, false);
		return promise
	};
	PromisePolyfill.prototype.catch = function(onRejection) {
		return this.then(null, onRejection)
	};
	PromisePolyfill.prototype.finally = function(callback) {
		return this.then(
			function(value) {
				return PromisePolyfill.resolve(callback()).then(function() {
					return value
				})
			},
			function(reason) {
				return PromisePolyfill.resolve(callback()).then(function() {
					return PromisePolyfill.reject(reason);
				})
			}
		)
	};
	PromisePolyfill.resolve = function(value) {
		if (value instanceof PromisePolyfill) return value
		return new PromisePolyfill(function(resolve) {resolve(value);})
	};
	PromisePolyfill.reject = function(value) {
		return new PromisePolyfill(function(resolve, reject) {reject(value);})
	};
	PromisePolyfill.all = function(list) {
		return new PromisePolyfill(function(resolve, reject) {
			var total = list.length, count = 0, values = [];
			if (list.length === 0) resolve([]);
			else for (var i = 0; i < list.length; i++) {
				(function(i) {
					function consume(value) {
						count++;
						values[i] = value;
						if (count === total) resolve(values);
					}
					if (list[i] != null && (typeof list[i] === "object" || typeof list[i] === "function") && typeof list[i].then === "function") {
						list[i].then(consume, reject);
					}
					else consume(list[i]);
				})(i);
			}
		})
	};
	PromisePolyfill.race = function(list) {
		return new PromisePolyfill(function(resolve, reject) {
			for (var i = 0; i < list.length; i++) {
				list[i].then(resolve, reject);
			}
		})
	};

	var polyfill = PromisePolyfill;

	var promise = createCommonjsModule(function (module) {



	if (typeof window !== "undefined") {
		if (typeof window.Promise === "undefined") {
			window.Promise = polyfill;
		} else if (!window.Promise.prototype.finally) {
			window.Promise.prototype.finally = polyfill.prototype.finally;
		}
		module.exports = window.Promise;
	} else if (typeof commonjsGlobal !== "undefined") {
		if (typeof commonjsGlobal.Promise === "undefined") {
			commonjsGlobal.Promise = polyfill;
		} else if (!commonjsGlobal.Promise.prototype.finally) {
			commonjsGlobal.Promise.prototype.finally = polyfill.prototype.finally;
		}
		module.exports = commonjsGlobal.Promise;
	} else {
		module.exports = polyfill;
	}
	});

	var render = function($window) {
		var $doc = $window && $window.document;
		var currentRedraw;

		var nameSpace = {
			svg: "http://www.w3.org/2000/svg",
			math: "http://www.w3.org/1998/Math/MathML"
		};

		function getNameSpace(vnode) {
			return vnode.attrs && vnode.attrs.xmlns || nameSpace[vnode.tag]
		}

		//sanity check to discourage people from doing `vnode.state = ...`
		function checkState(vnode, original) {
			if (vnode.state !== original) throw new Error("`vnode.state` must not be modified")
		}

		//Note: the hook is passed as the `this` argument to allow proxying the
		//arguments without requiring a full array allocation to do so. It also
		//takes advantage of the fact the current `vnode` is the first argument in
		//all lifecycle methods.
		function callHook(vnode) {
			var original = vnode.state;
			try {
				return this.apply(original, arguments)
			} finally {
				checkState(vnode, original);
			}
		}

		// IE11 (at least) throws an UnspecifiedError when accessing document.activeElement when
		// inside an iframe. Catch and swallow this error, and heavy-handidly return null.
		function activeElement() {
			try {
				return $doc.activeElement
			} catch (e) {
				return null
			}
		}
		//create
		function createNodes(parent, vnodes, start, end, hooks, nextSibling, ns) {
			for (var i = start; i < end; i++) {
				var vnode = vnodes[i];
				if (vnode != null) {
					createNode(parent, vnode, hooks, ns, nextSibling);
				}
			}
		}
		function createNode(parent, vnode, hooks, ns, nextSibling) {
			var tag = vnode.tag;
			if (typeof tag === "string") {
				vnode.state = {};
				if (vnode.attrs != null) initLifecycle(vnode.attrs, vnode, hooks);
				switch (tag) {
					case "#": createText(parent, vnode, nextSibling); break
					case "<": createHTML(parent, vnode, ns, nextSibling); break
					case "[": createFragment(parent, vnode, hooks, ns, nextSibling); break
					default: createElement(parent, vnode, hooks, ns, nextSibling);
				}
			}
			else createComponent(parent, vnode, hooks, ns, nextSibling);
		}
		function createText(parent, vnode, nextSibling) {
			vnode.dom = $doc.createTextNode(vnode.children);
			insertNode(parent, vnode.dom, nextSibling);
		}
		var possibleParents = {caption: "table", thead: "table", tbody: "table", tfoot: "table", tr: "tbody", th: "tr", td: "tr", colgroup: "table", col: "colgroup"};
		function createHTML(parent, vnode, ns, nextSibling) {
			var match = vnode.children.match(/^\s*?<(\w+)/im) || [];
			// not using the proper parent makes the child element(s) vanish.
			//     var div = document.createElement("div")
			//     div.innerHTML = "<td>i</td><td>j</td>"
			//     console.log(div.innerHTML)
			// --> "ij", no <td> in sight.
			var temp = $doc.createElement(possibleParents[match[1]] || "div");
			if (ns === "http://www.w3.org/2000/svg") {
				temp.innerHTML = "<svg xmlns=\"http://www.w3.org/2000/svg\">" + vnode.children + "</svg>";
				temp = temp.firstChild;
			} else {
				temp.innerHTML = vnode.children;
			}
			vnode.dom = temp.firstChild;
			vnode.domSize = temp.childNodes.length;
			// Capture nodes to remove, so we don't confuse them.
			vnode.instance = [];
			var fragment = $doc.createDocumentFragment();
			var child;
			while (child = temp.firstChild) {
				vnode.instance.push(child);
				fragment.appendChild(child);
			}
			insertNode(parent, fragment, nextSibling);
		}
		function createFragment(parent, vnode, hooks, ns, nextSibling) {
			var fragment = $doc.createDocumentFragment();
			if (vnode.children != null) {
				var children = vnode.children;
				createNodes(fragment, children, 0, children.length, hooks, null, ns);
			}
			vnode.dom = fragment.firstChild;
			vnode.domSize = fragment.childNodes.length;
			insertNode(parent, fragment, nextSibling);
		}
		function createElement(parent, vnode$1, hooks, ns, nextSibling) {
			var tag = vnode$1.tag;
			var attrs = vnode$1.attrs;
			var is = attrs && attrs.is;

			ns = getNameSpace(vnode$1) || ns;

			var element = ns ?
				is ? $doc.createElementNS(ns, tag, {is: is}) : $doc.createElementNS(ns, tag) :
				is ? $doc.createElement(tag, {is: is}) : $doc.createElement(tag);
			vnode$1.dom = element;

			if (attrs != null) {
				setAttrs(vnode$1, attrs, ns);
			}

			insertNode(parent, element, nextSibling);

			if (!maybeSetContentEditable(vnode$1)) {
				if (vnode$1.text != null) {
					if (vnode$1.text !== "") element.textContent = vnode$1.text;
					else vnode$1.children = [vnode("#", undefined, undefined, vnode$1.text, undefined, undefined)];
				}
				if (vnode$1.children != null) {
					var children = vnode$1.children;
					createNodes(element, children, 0, children.length, hooks, null, ns);
					if (vnode$1.tag === "select" && attrs != null) setLateSelectAttrs(vnode$1, attrs);
				}
			}
		}
		function initComponent(vnode$1, hooks) {
			var sentinel;
			if (typeof vnode$1.tag.view === "function") {
				vnode$1.state = Object.create(vnode$1.tag);
				sentinel = vnode$1.state.view;
				if (sentinel.$$reentrantLock$$ != null) return
				sentinel.$$reentrantLock$$ = true;
			} else {
				vnode$1.state = void 0;
				sentinel = vnode$1.tag;
				if (sentinel.$$reentrantLock$$ != null) return
				sentinel.$$reentrantLock$$ = true;
				vnode$1.state = (vnode$1.tag.prototype != null && typeof vnode$1.tag.prototype.view === "function") ? new vnode$1.tag(vnode$1) : vnode$1.tag(vnode$1);
			}
			initLifecycle(vnode$1.state, vnode$1, hooks);
			if (vnode$1.attrs != null) initLifecycle(vnode$1.attrs, vnode$1, hooks);
			vnode$1.instance = vnode.normalize(callHook.call(vnode$1.state.view, vnode$1));
			if (vnode$1.instance === vnode$1) throw Error("A view cannot return the vnode it received as argument")
			sentinel.$$reentrantLock$$ = null;
		}
		function createComponent(parent, vnode, hooks, ns, nextSibling) {
			initComponent(vnode, hooks);
			if (vnode.instance != null) {
				createNode(parent, vnode.instance, hooks, ns, nextSibling);
				vnode.dom = vnode.instance.dom;
				vnode.domSize = vnode.dom != null ? vnode.instance.domSize : 0;
			}
			else {
				vnode.domSize = 0;
			}
		}

		//update
		/**
		 * @param {Element|Fragment} parent - the parent element
		 * @param {Vnode[] | null} old - the list of vnodes of the last `render()` call for
		 *                               this part of the tree
		 * @param {Vnode[] | null} vnodes - as above, but for the current `render()` call.
		 * @param {Function[]} hooks - an accumulator of post-render hooks (oncreate/onupdate)
		 * @param {Element | null} nextSibling - the next DOM node if we're dealing with a
		 *                                       fragment that is not the last item in its
		 *                                       parent
		 * @param {'svg' | 'math' | String | null} ns) - the current XML namespace, if any
		 * @returns void
		 */
		// This function diffs and patches lists of vnodes, both keyed and unkeyed.
		//
		// We will:
		//
		// 1. describe its general structure
		// 2. focus on the diff algorithm optimizations
		// 3. discuss DOM node operations.

		// ## Overview:
		//
		// The updateNodes() function:
		// - deals with trivial cases
		// - determines whether the lists are keyed or unkeyed based on the first non-null node
		//   of each list.
		// - diffs them and patches the DOM if needed (that's the brunt of the code)
		// - manages the leftovers: after diffing, are there:
		//   - old nodes left to remove?
		// 	 - new nodes to insert?
		// 	 deal with them!
		//
		// The lists are only iterated over once, with an exception for the nodes in `old` that
		// are visited in the fourth part of the diff and in the `removeNodes` loop.

		// ## Diffing
		//
		// Reading https://github.com/localvoid/ivi/blob/ddc09d06abaef45248e6133f7040d00d3c6be853/packages/ivi/src/vdom/implementation.ts#L617-L837
		// may be good for context on longest increasing subsequence-based logic for moving nodes.
		//
		// In order to diff keyed lists, one has to
		//
		// 1) match nodes in both lists, per key, and update them accordingly
		// 2) create the nodes present in the new list, but absent in the old one
		// 3) remove the nodes present in the old list, but absent in the new one
		// 4) figure out what nodes in 1) to move in order to minimize the DOM operations.
		//
		// To achieve 1) one can create a dictionary of keys => index (for the old list), then iterate
		// over the new list and for each new vnode, find the corresponding vnode in the old list using
		// the map.
		// 2) is achieved in the same step: if a new node has no corresponding entry in the map, it is new
		// and must be created.
		// For the removals, we actually remove the nodes that have been updated from the old list.
		// The nodes that remain in that list after 1) and 2) have been performed can be safely removed.
		// The fourth step is a bit more complex and relies on the longest increasing subsequence (LIS)
		// algorithm.
		//
		// the longest increasing subsequence is the list of nodes that can remain in place. Imagine going
		// from `1,2,3,4,5` to `4,5,1,2,3` where the numbers are not necessarily the keys, but the indices
		// corresponding to the keyed nodes in the old list (keyed nodes `e,d,c,b,a` => `b,a,e,d,c` would
		//  match the above lists, for example).
		//
		// In there are two increasing subsequences: `4,5` and `1,2,3`, the latter being the longest. We
		// can update those nodes without moving them, and only call `insertNode` on `4` and `5`.
		//
		// @localvoid adapted the algo to also support node deletions and insertions (the `lis` is actually
		// the longest increasing subsequence *of old nodes still present in the new list*).
		//
		// It is a general algorithm that is fireproof in all circumstances, but it requires the allocation
		// and the construction of a `key => oldIndex` map, and three arrays (one with `newIndex => oldIndex`,
		// the `LIS` and a temporary one to create the LIS).
		//
		// So we cheat where we can: if the tails of the lists are identical, they are guaranteed to be part of
		// the LIS and can be updated without moving them.
		//
		// If two nodes are swapped, they are guaranteed not to be part of the LIS, and must be moved (with
		// the exception of the last node if the list is fully reversed).
		//
		// ## Finding the next sibling.
		//
		// `updateNode()` and `createNode()` expect a nextSibling parameter to perform DOM operations.
		// When the list is being traversed top-down, at any index, the DOM nodes up to the previous
		// vnode reflect the content of the new list, whereas the rest of the DOM nodes reflect the old
		// list. The next sibling must be looked for in the old list using `getNextSibling(... oldStart + 1 ...)`.
		//
		// In the other scenarios (swaps, upwards traversal, map-based diff),
		// the new vnodes list is traversed upwards. The DOM nodes at the bottom of the list reflect the
		// bottom part of the new vnodes list, and we can use the `v.dom`  value of the previous node
		// as the next sibling (cached in the `nextSibling` variable).


		// ## DOM node moves
		//
		// In most scenarios `updateNode()` and `createNode()` perform the DOM operations. However,
		// this is not the case if the node moved (second and fourth part of the diff algo). We move
		// the old DOM nodes before updateNode runs because it enables us to use the cached `nextSibling`
		// variable rather than fetching it using `getNextSibling()`.
		//
		// The fourth part of the diff currently inserts nodes unconditionally, leading to issues
		// like #1791 and #1999. We need to be smarter about those situations where adjascent old
		// nodes remain together in the new list in a way that isn't covered by parts one and
		// three of the diff algo.

		function updateNodes(parent, old, vnodes, hooks, nextSibling, ns) {
			if (old === vnodes || old == null && vnodes == null) return
			else if (old == null || old.length === 0) createNodes(parent, vnodes, 0, vnodes.length, hooks, nextSibling, ns);
			else if (vnodes == null || vnodes.length === 0) removeNodes(parent, old, 0, old.length);
			else {
				var isOldKeyed = old[0] != null && old[0].key != null;
				var isKeyed = vnodes[0] != null && vnodes[0].key != null;
				var start = 0, oldStart = 0;
				if (!isOldKeyed) while (oldStart < old.length && old[oldStart] == null) oldStart++;
				if (!isKeyed) while (start < vnodes.length && vnodes[start] == null) start++;
				if (isKeyed === null && isOldKeyed == null) return // both lists are full of nulls
				if (isOldKeyed !== isKeyed) {
					removeNodes(parent, old, oldStart, old.length);
					createNodes(parent, vnodes, start, vnodes.length, hooks, nextSibling, ns);
				} else if (!isKeyed) {
					// Don't index past the end of either list (causes deopts).
					var commonLength = old.length < vnodes.length ? old.length : vnodes.length;
					// Rewind if necessary to the first non-null index on either side.
					// We could alternatively either explicitly create or remove nodes when `start !== oldStart`
					// but that would be optimizing for sparse lists which are more rare than dense ones.
					start = start < oldStart ? start : oldStart;
					for (; start < commonLength; start++) {
						o = old[start];
						v = vnodes[start];
						if (o === v || o == null && v == null) continue
						else if (o == null) createNode(parent, v, hooks, ns, getNextSibling(old, start + 1, nextSibling));
						else if (v == null) removeNode(parent, o);
						else updateNode(parent, o, v, hooks, getNextSibling(old, start + 1, nextSibling), ns);
					}
					if (old.length > commonLength) removeNodes(parent, old, start, old.length);
					if (vnodes.length > commonLength) createNodes(parent, vnodes, start, vnodes.length, hooks, nextSibling, ns);
				} else {
					// keyed diff
					var oldEnd = old.length - 1, end = vnodes.length - 1, map, o, v, oe, ve, topSibling;

					// bottom-up
					while (oldEnd >= oldStart && end >= start) {
						oe = old[oldEnd];
						ve = vnodes[end];
						if (oe.key !== ve.key) break
						if (oe !== ve) updateNode(parent, oe, ve, hooks, nextSibling, ns);
						if (ve.dom != null) nextSibling = ve.dom;
						oldEnd--, end--;
					}
					// top-down
					while (oldEnd >= oldStart && end >= start) {
						o = old[oldStart];
						v = vnodes[start];
						if (o.key !== v.key) break
						oldStart++, start++;
						if (o !== v) updateNode(parent, o, v, hooks, getNextSibling(old, oldStart, nextSibling), ns);
					}
					// swaps and list reversals
					while (oldEnd >= oldStart && end >= start) {
						if (start === end) break
						if (o.key !== ve.key || oe.key !== v.key) break
						topSibling = getNextSibling(old, oldStart, nextSibling);
						moveNodes(parent, oe, topSibling);
						if (oe !== v) updateNode(parent, oe, v, hooks, topSibling, ns);
						if (++start <= --end) moveNodes(parent, o, nextSibling);
						if (o !== ve) updateNode(parent, o, ve, hooks, nextSibling, ns);
						if (ve.dom != null) nextSibling = ve.dom;
						oldStart++; oldEnd--;
						oe = old[oldEnd];
						ve = vnodes[end];
						o = old[oldStart];
						v = vnodes[start];
					}
					// bottom up once again
					while (oldEnd >= oldStart && end >= start) {
						if (oe.key !== ve.key) break
						if (oe !== ve) updateNode(parent, oe, ve, hooks, nextSibling, ns);
						if (ve.dom != null) nextSibling = ve.dom;
						oldEnd--, end--;
						oe = old[oldEnd];
						ve = vnodes[end];
					}
					if (start > end) removeNodes(parent, old, oldStart, oldEnd + 1);
					else if (oldStart > oldEnd) createNodes(parent, vnodes, start, end + 1, hooks, nextSibling, ns);
					else {
						// inspired by ivi https://github.com/ivijs/ivi/ by Boris Kaul
						var originalNextSibling = nextSibling, vnodesLength = end - start + 1, oldIndices = new Array(vnodesLength), li=0, i=0, pos = 2147483647, matched = 0, map, lisIndices;
						for (i = 0; i < vnodesLength; i++) oldIndices[i] = -1;
						for (i = end; i >= start; i--) {
							if (map == null) map = getKeyMap(old, oldStart, oldEnd + 1);
							ve = vnodes[i];
							var oldIndex = map[ve.key];
							if (oldIndex != null) {
								pos = (oldIndex < pos) ? oldIndex : -1; // becomes -1 if nodes were re-ordered
								oldIndices[i-start] = oldIndex;
								oe = old[oldIndex];
								old[oldIndex] = null;
								if (oe !== ve) updateNode(parent, oe, ve, hooks, nextSibling, ns);
								if (ve.dom != null) nextSibling = ve.dom;
								matched++;
							}
						}
						nextSibling = originalNextSibling;
						if (matched !== oldEnd - oldStart + 1) removeNodes(parent, old, oldStart, oldEnd + 1);
						if (matched === 0) createNodes(parent, vnodes, start, end + 1, hooks, nextSibling, ns);
						else {
							if (pos === -1) {
								// the indices of the indices of the items that are part of the
								// longest increasing subsequence in the oldIndices list
								lisIndices = makeLisIndices(oldIndices);
								li = lisIndices.length - 1;
								for (i = end; i >= start; i--) {
									v = vnodes[i];
									if (oldIndices[i-start] === -1) createNode(parent, v, hooks, ns, nextSibling);
									else {
										if (lisIndices[li] === i - start) li--;
										else moveNodes(parent, v, nextSibling);
									}
									if (v.dom != null) nextSibling = vnodes[i].dom;
								}
							} else {
								for (i = end; i >= start; i--) {
									v = vnodes[i];
									if (oldIndices[i-start] === -1) createNode(parent, v, hooks, ns, nextSibling);
									if (v.dom != null) nextSibling = vnodes[i].dom;
								}
							}
						}
					}
				}
			}
		}
		function updateNode(parent, old, vnode, hooks, nextSibling, ns) {
			var oldTag = old.tag, tag = vnode.tag;
			if (oldTag === tag) {
				vnode.state = old.state;
				vnode.events = old.events;
				if (shouldNotUpdate(vnode, old)) return
				if (typeof oldTag === "string") {
					if (vnode.attrs != null) {
						updateLifecycle(vnode.attrs, vnode, hooks);
					}
					switch (oldTag) {
						case "#": updateText(old, vnode); break
						case "<": updateHTML(parent, old, vnode, ns, nextSibling); break
						case "[": updateFragment(parent, old, vnode, hooks, nextSibling, ns); break
						default: updateElement(old, vnode, hooks, ns);
					}
				}
				else updateComponent(parent, old, vnode, hooks, nextSibling, ns);
			}
			else {
				removeNode(parent, old);
				createNode(parent, vnode, hooks, ns, nextSibling);
			}
		}
		function updateText(old, vnode) {
			if (old.children.toString() !== vnode.children.toString()) {
				old.dom.nodeValue = vnode.children;
			}
			vnode.dom = old.dom;
		}
		function updateHTML(parent, old, vnode, ns, nextSibling) {
			if (old.children !== vnode.children) {
				removeHTML(parent, old);
				createHTML(parent, vnode, ns, nextSibling);
			}
			else {
				vnode.dom = old.dom;
				vnode.domSize = old.domSize;
				vnode.instance = old.instance;
			}
		}
		function updateFragment(parent, old, vnode, hooks, nextSibling, ns) {
			updateNodes(parent, old.children, vnode.children, hooks, nextSibling, ns);
			var domSize = 0, children = vnode.children;
			vnode.dom = null;
			if (children != null) {
				for (var i = 0; i < children.length; i++) {
					var child = children[i];
					if (child != null && child.dom != null) {
						if (vnode.dom == null) vnode.dom = child.dom;
						domSize += child.domSize || 1;
					}
				}
				if (domSize !== 1) vnode.domSize = domSize;
			}
		}
		function updateElement(old, vnode$1, hooks, ns) {
			var element = vnode$1.dom = old.dom;
			ns = getNameSpace(vnode$1) || ns;

			if (vnode$1.tag === "textarea") {
				if (vnode$1.attrs == null) vnode$1.attrs = {};
				if (vnode$1.text != null) {
					vnode$1.attrs.value = vnode$1.text; //FIXME handle multiple children
					vnode$1.text = undefined;
				}
			}
			updateAttrs(vnode$1, old.attrs, vnode$1.attrs, ns);
			if (!maybeSetContentEditable(vnode$1)) {
				if (old.text != null && vnode$1.text != null && vnode$1.text !== "") {
					if (old.text.toString() !== vnode$1.text.toString()) old.dom.firstChild.nodeValue = vnode$1.text;
				}
				else {
					if (old.text != null) old.children = [vnode("#", undefined, undefined, old.text, undefined, old.dom.firstChild)];
					if (vnode$1.text != null) vnode$1.children = [vnode("#", undefined, undefined, vnode$1.text, undefined, undefined)];
					updateNodes(element, old.children, vnode$1.children, hooks, null, ns);
				}
			}
		}
		function updateComponent(parent, old, vnode$1, hooks, nextSibling, ns) {
			vnode$1.instance = vnode.normalize(callHook.call(vnode$1.state.view, vnode$1));
			if (vnode$1.instance === vnode$1) throw Error("A view cannot return the vnode it received as argument")
			updateLifecycle(vnode$1.state, vnode$1, hooks);
			if (vnode$1.attrs != null) updateLifecycle(vnode$1.attrs, vnode$1, hooks);
			if (vnode$1.instance != null) {
				if (old.instance == null) createNode(parent, vnode$1.instance, hooks, ns, nextSibling);
				else updateNode(parent, old.instance, vnode$1.instance, hooks, nextSibling, ns);
				vnode$1.dom = vnode$1.instance.dom;
				vnode$1.domSize = vnode$1.instance.domSize;
			}
			else if (old.instance != null) {
				removeNode(parent, old.instance);
				vnode$1.dom = undefined;
				vnode$1.domSize = 0;
			}
			else {
				vnode$1.dom = old.dom;
				vnode$1.domSize = old.domSize;
			}
		}
		function getKeyMap(vnodes, start, end) {
			var map = Object.create(null);
			for (; start < end; start++) {
				var vnode = vnodes[start];
				if (vnode != null) {
					var key = vnode.key;
					if (key != null) map[key] = start;
				}
			}
			return map
		}
		// Lifted from ivi https://github.com/ivijs/ivi/
		// takes a list of unique numbers (-1 is special and can
		// occur multiple times) and returns an array with the indices
		// of the items that are part of the longest increasing
		// subsequece
		var lisTemp = [];
		function makeLisIndices(a) {
			var result = [0];
			var u = 0, v = 0, i = 0;
			var il = lisTemp.length = a.length;
			for (var i = 0; i < il; i++) lisTemp[i] = a[i];
			for (var i = 0; i < il; ++i) {
				if (a[i] === -1) continue
				var j = result[result.length - 1];
				if (a[j] < a[i]) {
					lisTemp[i] = j;
					result.push(i);
					continue
				}
				u = 0;
				v = result.length - 1;
				while (u < v) {
					// Fast integer average without overflow.
					// eslint-disable-next-line no-bitwise
					var c = (u >>> 1) + (v >>> 1) + (u & v & 1);
					if (a[result[c]] < a[i]) {
						u = c + 1;
					}
					else {
						v = c;
					}
				}
				if (a[i] < a[result[u]]) {
					if (u > 0) lisTemp[i] = result[u - 1];
					result[u] = i;
				}
			}
			u = result.length;
			v = result[u - 1];
			while (u-- > 0) {
				result[u] = v;
				v = lisTemp[v];
			}
			lisTemp.length = 0;
			return result
		}

		function getNextSibling(vnodes, i, nextSibling) {
			for (; i < vnodes.length; i++) {
				if (vnodes[i] != null && vnodes[i].dom != null) return vnodes[i].dom
			}
			return nextSibling
		}

		// This covers a really specific edge case:
		// - Parent node is keyed and contains child
		// - Child is removed, returns unresolved promise in `onbeforeremove`
		// - Parent node is moved in keyed diff
		// - Remaining children still need moved appropriately
		//
		// Ideally, I'd track removed nodes as well, but that introduces a lot more
		// complexity and I'm not exactly interested in doing that.
		function moveNodes(parent, vnode, nextSibling) {
			var frag = $doc.createDocumentFragment();
			moveChildToFrag(parent, frag, vnode);
			insertNode(parent, frag, nextSibling);
		}
		function moveChildToFrag(parent, frag, vnode) {
			// Dodge the recursion overhead in a few of the most common cases.
			while (vnode.dom != null && vnode.dom.parentNode === parent) {
				if (typeof vnode.tag !== "string") {
					vnode = vnode.instance;
					if (vnode != null) continue
				} else if (vnode.tag === "<") {
					for (var i = 0; i < vnode.instance.length; i++) {
						frag.appendChild(vnode.instance[i]);
					}
				} else if (vnode.tag !== "[") {
					// Don't recurse for text nodes *or* elements, just fragments
					frag.appendChild(vnode.dom);
				} else if (vnode.children.length === 1) {
					vnode = vnode.children[0];
					if (vnode != null) continue
				} else {
					for (var i = 0; i < vnode.children.length; i++) {
						var child = vnode.children[i];
						if (child != null) moveChildToFrag(parent, frag, child);
					}
				}
				break
			}
		}

		function insertNode(parent, dom, nextSibling) {
			if (nextSibling != null) parent.insertBefore(dom, nextSibling);
			else parent.appendChild(dom);
		}

		function maybeSetContentEditable(vnode) {
			if (vnode.attrs == null || (
				vnode.attrs.contenteditable == null && // attribute
				vnode.attrs.contentEditable == null // property
			)) return false
			var children = vnode.children;
			if (children != null && children.length === 1 && children[0].tag === "<") {
				var content = children[0].children;
				if (vnode.dom.innerHTML !== content) vnode.dom.innerHTML = content;
			}
			else if (vnode.text != null || children != null && children.length !== 0) throw new Error("Child node of a contenteditable must be trusted")
			return true
		}

		//remove
		function removeNodes(parent, vnodes, start, end) {
			for (var i = start; i < end; i++) {
				var vnode = vnodes[i];
				if (vnode != null) removeNode(parent, vnode);
			}
		}
		function removeNode(parent, vnode) {
			var mask = 0;
			var original = vnode.state;
			var stateResult, attrsResult;
			if (typeof vnode.tag !== "string" && typeof vnode.state.onbeforeremove === "function") {
				var result = callHook.call(vnode.state.onbeforeremove, vnode);
				if (result != null && typeof result.then === "function") {
					mask = 1;
					stateResult = result;
				}
			}
			if (vnode.attrs && typeof vnode.attrs.onbeforeremove === "function") {
				var result = callHook.call(vnode.attrs.onbeforeremove, vnode);
				if (result != null && typeof result.then === "function") {
					// eslint-disable-next-line no-bitwise
					mask |= 2;
					attrsResult = result;
				}
			}
			checkState(vnode, original);

			// If we can, try to fast-path it and avoid all the overhead of awaiting
			if (!mask) {
				onremove(vnode);
				removeChild(parent, vnode);
			} else {
				if (stateResult != null) {
					var next = function () {
						// eslint-disable-next-line no-bitwise
						if (mask & 1) { mask &= 2; if (!mask) reallyRemove(); }
					};
					stateResult.then(next, next);
				}
				if (attrsResult != null) {
					var next = function () {
						// eslint-disable-next-line no-bitwise
						if (mask & 2) { mask &= 1; if (!mask) reallyRemove(); }
					};
					attrsResult.then(next, next);
				}
			}

			function reallyRemove() {
				checkState(vnode, original);
				onremove(vnode);
				removeChild(parent, vnode);
			}
		}
		function removeHTML(parent, vnode) {
			for (var i = 0; i < vnode.instance.length; i++) {
				parent.removeChild(vnode.instance[i]);
			}
		}
		function removeChild(parent, vnode) {
			// Dodge the recursion overhead in a few of the most common cases.
			while (vnode.dom != null && vnode.dom.parentNode === parent) {
				if (typeof vnode.tag !== "string") {
					vnode = vnode.instance;
					if (vnode != null) continue
				} else if (vnode.tag === "<") {
					removeHTML(parent, vnode);
				} else {
					if (vnode.tag !== "[") {
						parent.removeChild(vnode.dom);
						if (!Array.isArray(vnode.children)) break
					}
					if (vnode.children.length === 1) {
						vnode = vnode.children[0];
						if (vnode != null) continue
					} else {
						for (var i = 0; i < vnode.children.length; i++) {
							var child = vnode.children[i];
							if (child != null) removeChild(parent, child);
						}
					}
				}
				break
			}
		}
		function onremove(vnode) {
			if (typeof vnode.tag !== "string" && typeof vnode.state.onremove === "function") callHook.call(vnode.state.onremove, vnode);
			if (vnode.attrs && typeof vnode.attrs.onremove === "function") callHook.call(vnode.attrs.onremove, vnode);
			if (typeof vnode.tag !== "string") {
				if (vnode.instance != null) onremove(vnode.instance);
			} else {
				var children = vnode.children;
				if (Array.isArray(children)) {
					for (var i = 0; i < children.length; i++) {
						var child = children[i];
						if (child != null) onremove(child);
					}
				}
			}
		}

		//attrs
		function setAttrs(vnode, attrs, ns) {
			for (var key in attrs) {
				setAttr(vnode, key, null, attrs[key], ns);
			}
		}
		function setAttr(vnode, key, old, value, ns) {
			if (key === "key" || key === "is" || value == null || isLifecycleMethod(key) || (old === value && !isFormAttribute(vnode, key)) && typeof value !== "object") return
			if (key[0] === "o" && key[1] === "n") return updateEvent(vnode, key, value)
			if (key.slice(0, 6) === "xlink:") vnode.dom.setAttributeNS("http://www.w3.org/1999/xlink", key.slice(6), value);
			else if (key === "style") updateStyle(vnode.dom, old, value);
			else if (hasPropertyKey(vnode, key, ns)) {
				if (key === "value") {
					// Only do the coercion if we're actually going to check the value.
					/* eslint-disable no-implicit-coercion */
					//setting input[value] to same value by typing on focused element moves cursor to end in Chrome
					if ((vnode.tag === "input" || vnode.tag === "textarea") && vnode.dom.value === "" + value && vnode.dom === activeElement()) return
					//setting select[value] to same value while having select open blinks select dropdown in Chrome
					if (vnode.tag === "select" && old !== null && vnode.dom.value === "" + value) return
					//setting option[value] to same value while having select open blinks select dropdown in Chrome
					if (vnode.tag === "option" && old !== null && vnode.dom.value === "" + value) return
					/* eslint-enable no-implicit-coercion */
				}
				// If you assign an input type that is not supported by IE 11 with an assignment expression, an error will occur.
				if (vnode.tag === "input" && key === "type") vnode.dom.setAttribute(key, value);
				else vnode.dom[key] = value;
			} else {
				if (typeof value === "boolean") {
					if (value) vnode.dom.setAttribute(key, "");
					else vnode.dom.removeAttribute(key);
				}
				else vnode.dom.setAttribute(key === "className" ? "class" : key, value);
			}
		}
		function removeAttr(vnode, key, old, ns) {
			if (key === "key" || key === "is" || old == null || isLifecycleMethod(key)) return
			if (key[0] === "o" && key[1] === "n" && !isLifecycleMethod(key)) updateEvent(vnode, key, undefined);
			else if (key === "style") updateStyle(vnode.dom, old, null);
			else if (
				hasPropertyKey(vnode, key, ns)
				&& key !== "className"
				&& !(key === "value" && (
					vnode.tag === "option"
					|| vnode.tag === "select" && vnode.dom.selectedIndex === -1 && vnode.dom === activeElement()
				))
				&& !(vnode.tag === "input" && key === "type")
			) {
				vnode.dom[key] = null;
			} else {
				var nsLastIndex = key.indexOf(":");
				if (nsLastIndex !== -1) key = key.slice(nsLastIndex + 1);
				if (old !== false) vnode.dom.removeAttribute(key === "className" ? "class" : key);
			}
		}
		function setLateSelectAttrs(vnode, attrs) {
			if ("value" in attrs) {
				if(attrs.value === null) {
					if (vnode.dom.selectedIndex !== -1) vnode.dom.value = null;
				} else {
					var normalized = "" + attrs.value; // eslint-disable-line no-implicit-coercion
					if (vnode.dom.value !== normalized || vnode.dom.selectedIndex === -1) {
						vnode.dom.value = normalized;
					}
				}
			}
			if ("selectedIndex" in attrs) setAttr(vnode, "selectedIndex", null, attrs.selectedIndex, undefined);
		}
		function updateAttrs(vnode, old, attrs, ns) {
			if (attrs != null) {
				for (var key in attrs) {
					setAttr(vnode, key, old && old[key], attrs[key], ns);
				}
			}
			var val;
			if (old != null) {
				for (var key in old) {
					if (((val = old[key]) != null) && (attrs == null || attrs[key] == null)) {
						removeAttr(vnode, key, val, ns);
					}
				}
			}
		}
		function isFormAttribute(vnode, attr) {
			return attr === "value" || attr === "checked" || attr === "selectedIndex" || attr === "selected" && vnode.dom === activeElement() || vnode.tag === "option" && vnode.dom.parentNode === $doc.activeElement
		}
		function isLifecycleMethod(attr) {
			return attr === "oninit" || attr === "oncreate" || attr === "onupdate" || attr === "onremove" || attr === "onbeforeremove" || attr === "onbeforeupdate"
		}
		function hasPropertyKey(vnode, key, ns) {
			// Filter out namespaced keys
			return ns === undefined && (
				// If it's a custom element, just keep it.
				vnode.tag.indexOf("-") > -1 || vnode.attrs != null && vnode.attrs.is ||
				// If it's a normal element, let's try to avoid a few browser bugs.
				key !== "href" && key !== "list" && key !== "form" && key !== "width" && key !== "height"// && key !== "type"
				// Defer the property check until *after* we check everything.
			) && key in vnode.dom
		}

		//style
		var uppercaseRegex = /[A-Z]/g;
		function toLowerCase(capital) { return "-" + capital.toLowerCase() }
		function normalizeKey(key) {
			return key[0] === "-" && key[1] === "-" ? key :
				key === "cssFloat" ? "float" :
					key.replace(uppercaseRegex, toLowerCase)
		}
		function updateStyle(element, old, style) {
			if (old === style) ; else if (style == null) {
				// New style is missing, just clear it.
				element.style.cssText = "";
			} else if (typeof style !== "object") {
				// New style is a string, let engine deal with patching.
				element.style.cssText = style;
			} else if (old == null || typeof old !== "object") {
				// `old` is missing or a string, `style` is an object.
				element.style.cssText = "";
				// Add new style properties
				for (var key in style) {
					var value = style[key];
					if (value != null) element.style.setProperty(normalizeKey(key), String(value));
				}
			} else {
				// Both old & new are (different) objects.
				// Update style properties that have changed
				for (var key in style) {
					var value = style[key];
					if (value != null && (value = String(value)) !== String(old[key])) {
						element.style.setProperty(normalizeKey(key), value);
					}
				}
				// Remove style properties that no longer exist
				for (var key in old) {
					if (old[key] != null && style[key] == null) {
						element.style.removeProperty(normalizeKey(key));
					}
				}
			}
		}

		// Here's an explanation of how this works:
		// 1. The event names are always (by design) prefixed by `on`.
		// 2. The EventListener interface accepts either a function or an object
		//    with a `handleEvent` method.
		// 3. The object does not inherit from `Object.prototype`, to avoid
		//    any potential interference with that (e.g. setters).
		// 4. The event name is remapped to the handler before calling it.
		// 5. In function-based event handlers, `ev.target === this`. We replicate
		//    that below.
		// 6. In function-based event handlers, `return false` prevents the default
		//    action and stops event propagation. We replicate that below.
		function EventDict() {
			// Save this, so the current redraw is correctly tracked.
			this._ = currentRedraw;
		}
		EventDict.prototype = Object.create(null);
		EventDict.prototype.handleEvent = function (ev) {
			var handler = this["on" + ev.type];
			var result;
			if (typeof handler === "function") result = handler.call(ev.currentTarget, ev);
			else if (typeof handler.handleEvent === "function") handler.handleEvent(ev);
			if (this._ && ev.redraw !== false) (0, this._)();
			if (result === false) {
				ev.preventDefault();
				ev.stopPropagation();
			}
		};

		//event
		function updateEvent(vnode, key, value) {
			if (vnode.events != null) {
				if (vnode.events[key] === value) return
				if (value != null && (typeof value === "function" || typeof value === "object")) {
					if (vnode.events[key] == null) vnode.dom.addEventListener(key.slice(2), vnode.events, false);
					vnode.events[key] = value;
				} else {
					if (vnode.events[key] != null) vnode.dom.removeEventListener(key.slice(2), vnode.events, false);
					vnode.events[key] = undefined;
				}
			} else if (value != null && (typeof value === "function" || typeof value === "object")) {
				vnode.events = new EventDict();
				vnode.dom.addEventListener(key.slice(2), vnode.events, false);
				vnode.events[key] = value;
			}
		}

		//lifecycle
		function initLifecycle(source, vnode, hooks) {
			if (typeof source.oninit === "function") callHook.call(source.oninit, vnode);
			if (typeof source.oncreate === "function") hooks.push(callHook.bind(source.oncreate, vnode));
		}
		function updateLifecycle(source, vnode, hooks) {
			if (typeof source.onupdate === "function") hooks.push(callHook.bind(source.onupdate, vnode));
		}
		function shouldNotUpdate(vnode, old) {
			do {
				if (vnode.attrs != null && typeof vnode.attrs.onbeforeupdate === "function") {
					var force = callHook.call(vnode.attrs.onbeforeupdate, vnode, old);
					if (force !== undefined && !force) break
				}
				if (typeof vnode.tag !== "string" && typeof vnode.state.onbeforeupdate === "function") {
					var force = callHook.call(vnode.state.onbeforeupdate, vnode, old);
					if (force !== undefined && !force) break
				}
				return false
			} while (false); // eslint-disable-line no-constant-condition
			vnode.dom = old.dom;
			vnode.domSize = old.domSize;
			vnode.instance = old.instance;
			// One would think having the actual latest attributes would be ideal,
			// but it doesn't let us properly diff based on our current internal
			// representation. We have to save not only the old DOM info, but also
			// the attributes used to create it, as we diff *that*, not against the
			// DOM directly (with a few exceptions in `setAttr`). And, of course, we
			// need to save the children and text as they are conceptually not
			// unlike special "attributes" internally.
			vnode.attrs = old.attrs;
			vnode.children = old.children;
			vnode.text = old.text;
			return true
		}

		return function(dom, vnodes, redraw) {
			if (!dom) throw new TypeError("Ensure the DOM element being passed to m.route/m.mount/m.render is not undefined.")
			var hooks = [];
			var active = activeElement();
			var namespace = dom.namespaceURI;

			// First time rendering into a node clears it out
			if (dom.vnodes == null) dom.textContent = "";

			vnodes = vnode.normalizeChildren(Array.isArray(vnodes) ? vnodes : [vnodes]);
			var prevRedraw = currentRedraw;
			try {
				currentRedraw = typeof redraw === "function" ? redraw : undefined;
				updateNodes(dom, dom.vnodes, vnodes, hooks, null, namespace === "http://www.w3.org/1999/xhtml" ? undefined : namespace);
			} finally {
				currentRedraw = prevRedraw;
			}
			dom.vnodes = vnodes;
			// `document.activeElement` can return null: https://html.spec.whatwg.org/multipage/interaction.html#dom-document-activeelement
			if (active != null && activeElement() !== active && typeof active.focus === "function") active.focus();
			for (var i = 0; i < hooks.length; i++) hooks[i]();
		}
	};

	var render$1 = render(window);

	var mountRedraw = function(render, schedule, console) {
		var subscriptions = [];
		var rendering = false;
		var pending = false;

		function sync() {
			if (rendering) throw new Error("Nested m.redraw.sync() call")
			rendering = true;
			for (var i = 0; i < subscriptions.length; i += 2) {
				try { render(subscriptions[i], vnode(subscriptions[i + 1]), redraw); }
				catch (e) { console.error(e); }
			}
			rendering = false;
		}

		function redraw() {
			if (!pending) {
				pending = true;
				schedule(function() {
					pending = false;
					sync();
				});
			}
		}

		redraw.sync = sync;

		function mount(root, component) {
			if (component != null && component.view == null && typeof component !== "function") {
				throw new TypeError("m.mount(element, component) expects a component, not a vnode")
			}

			var index = subscriptions.indexOf(root);
			if (index >= 0) {
				subscriptions.splice(index, 2);
				render(root, [], redraw);
			}

			if (component != null) {
				subscriptions.push(root, component);
				render(root, vnode(component), redraw);
			}
		}

		return {mount: mount, redraw: redraw}
	};

	var mountRedraw$1 = mountRedraw(render$1, requestAnimationFrame, console);

	var build = function(object) {
		if (Object.prototype.toString.call(object) !== "[object Object]") return ""

		var args = [];
		for (var key in object) {
			destructure(key, object[key]);
		}

		return args.join("&")

		function destructure(key, value) {
			if (Array.isArray(value)) {
				for (var i = 0; i < value.length; i++) {
					destructure(key + "[" + i + "]", value[i]);
				}
			}
			else if (Object.prototype.toString.call(value) === "[object Object]") {
				for (var i in value) {
					destructure(key + "[" + i + "]", value[i]);
				}
			}
			else args.push(encodeURIComponent(key) + (value != null && value !== "" ? "=" + encodeURIComponent(value) : ""));
		}
	};

	var assign = Object.assign || function(target, source) {
		if(source) Object.keys(source).forEach(function(key) { target[key] = source[key]; });
	};

	// Returns `path` from `template` + `params`
	var build$1 = function(template, params) {
		if ((/:([^\/\.-]+)(\.{3})?:/).test(template)) {
			throw new SyntaxError("Template parameter names *must* be separated")
		}
		if (params == null) return template
		var queryIndex = template.indexOf("?");
		var hashIndex = template.indexOf("#");
		var queryEnd = hashIndex < 0 ? template.length : hashIndex;
		var pathEnd = queryIndex < 0 ? queryEnd : queryIndex;
		var path = template.slice(0, pathEnd);
		var query = {};

		assign(query, params);

		var resolved = path.replace(/:([^\/\.-]+)(\.{3})?/g, function(m, key, variadic) {
			delete query[key];
			// If no such parameter exists, don't interpolate it.
			if (params[key] == null) return m
			// Escape normal parameters, but not variadic ones.
			return variadic ? params[key] : encodeURIComponent(String(params[key]))
		});

		// In case the template substitution adds new query/hash parameters.
		var newQueryIndex = resolved.indexOf("?");
		var newHashIndex = resolved.indexOf("#");
		var newQueryEnd = newHashIndex < 0 ? resolved.length : newHashIndex;
		var newPathEnd = newQueryIndex < 0 ? newQueryEnd : newQueryIndex;
		var result = resolved.slice(0, newPathEnd);

		if (queryIndex >= 0) result += template.slice(queryIndex, queryEnd);
		if (newQueryIndex >= 0) result += (queryIndex < 0 ? "?" : "&") + resolved.slice(newQueryIndex, newQueryEnd);
		var querystring = build(query);
		if (querystring) result += (queryIndex < 0 && newQueryIndex < 0 ? "?" : "&") + querystring;
		if (hashIndex >= 0) result += template.slice(hashIndex);
		if (newHashIndex >= 0) result += (hashIndex < 0 ? "" : "&") + resolved.slice(newHashIndex);
		return result
	};

	var request = function($window, Promise, oncompletion) {
		var callbackCount = 0;

		function PromiseProxy(executor) {
			return new Promise(executor)
		}

		// In case the global Promise is some userland library's where they rely on
		// `foo instanceof this.constructor`, `this.constructor.resolve(value)`, or
		// similar. Let's *not* break them.
		PromiseProxy.prototype = Promise.prototype;
		PromiseProxy.__proto__ = Promise; // eslint-disable-line no-proto

		function makeRequest(factory) {
			return function(url, args) {
				if (typeof url !== "string") { args = url; url = url.url; }
				else if (args == null) args = {};
				var promise = new Promise(function(resolve, reject) {
					factory(build$1(url, args.params), args, function (data) {
						if (typeof args.type === "function") {
							if (Array.isArray(data)) {
								for (var i = 0; i < data.length; i++) {
									data[i] = new args.type(data[i]);
								}
							}
							else data = new args.type(data);
						}
						resolve(data);
					}, reject);
				});
				if (args.background === true) return promise
				var count = 0;
				function complete() {
					if (--count === 0 && typeof oncompletion === "function") oncompletion();
				}

				return wrap(promise)

				function wrap(promise) {
					var then = promise.then;
					// Set the constructor, so engines know to not await or resolve
					// this as a native promise. At the time of writing, this is
					// only necessary for V8, but their behavior is the correct
					// behavior per spec. See this spec issue for more details:
					// https://github.com/tc39/ecma262/issues/1577. Also, see the
					// corresponding comment in `request/tests/test-request.js` for
					// a bit more background on the issue at hand.
					promise.constructor = PromiseProxy;
					promise.then = function() {
						count++;
						var next = then.apply(promise, arguments);
						next.then(complete, function(e) {
							complete();
							if (count === 0) throw e
						});
						return wrap(next)
					};
					return promise
				}
			}
		}

		function hasHeader(args, name) {
			for (var key in args.headers) {
				if ({}.hasOwnProperty.call(args.headers, key) && name.test(key)) return true
			}
			return false
		}

		return {
			request: makeRequest(function(url, args, resolve, reject) {
				var method = args.method != null ? args.method.toUpperCase() : "GET";
				var body = args.body;
				var assumeJSON = (args.serialize == null || args.serialize === JSON.serialize) && !(body instanceof $window.FormData);
				var responseType = args.responseType || (typeof args.extract === "function" ? "" : "json");

				var xhr = new $window.XMLHttpRequest(), aborted = false;
				var original = xhr, replacedAbort;
				var abort = xhr.abort;

				xhr.abort = function() {
					aborted = true;
					abort.call(this);
				};

				xhr.open(method, url, args.async !== false, typeof args.user === "string" ? args.user : undefined, typeof args.password === "string" ? args.password : undefined);

				if (assumeJSON && body != null && !hasHeader(args, /^content-type$/i)) {
					xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
				}
				if (typeof args.deserialize !== "function" && !hasHeader(args, /^accept$/i)) {
					xhr.setRequestHeader("Accept", "application/json, text/*");
				}
				if (args.withCredentials) xhr.withCredentials = args.withCredentials;
				if (args.timeout) xhr.timeout = args.timeout;
				xhr.responseType = responseType;

				for (var key in args.headers) {
					if ({}.hasOwnProperty.call(args.headers, key)) {
						xhr.setRequestHeader(key, args.headers[key]);
					}
				}

				xhr.onreadystatechange = function(ev) {
					// Don't throw errors on xhr.abort().
					if (aborted) return

					if (ev.target.readyState === 4) {
						try {
							var success = (ev.target.status >= 200 && ev.target.status < 300) || ev.target.status === 304 || (/^file:\/\//i).test(url);
							// When the response type isn't "" or "text",
							// `xhr.responseText` is the wrong thing to use.
							// Browsers do the right thing and throw here, and we
							// should honor that and do the right thing by
							// preferring `xhr.response` where possible/practical.
							var response = ev.target.response, message;

							if (responseType === "json") {
								// For IE and Edge, which don't implement
								// `responseType: "json"`.
								if (!ev.target.responseType && typeof args.extract !== "function") response = JSON.parse(ev.target.responseText);
							} else if (!responseType || responseType === "text") {
								// Only use this default if it's text. If a parsed
								// document is needed on old IE and friends (all
								// unsupported), the user should use a custom
								// `config` instead. They're already using this at
								// their own risk.
								if (response == null) response = ev.target.responseText;
							}

							if (typeof args.extract === "function") {
								response = args.extract(ev.target, args);
								success = true;
							} else if (typeof args.deserialize === "function") {
								response = args.deserialize(response);
							}
							if (success) resolve(response);
							else {
								try { message = ev.target.responseText; }
								catch (e) { message = response; }
								var error = new Error(message);
								error.code = ev.target.status;
								error.response = response;
								reject(error);
							}
						}
						catch (e) {
							reject(e);
						}
					}
				};

				if (typeof args.config === "function") {
					xhr = args.config(xhr, args, url) || xhr;

					// Propagate the `abort` to any replacement XHR as well.
					if (xhr !== original) {
						replacedAbort = xhr.abort;
						xhr.abort = function() {
							aborted = true;
							replacedAbort.call(this);
						};
					}
				}

				if (body == null) xhr.send();
				else if (typeof args.serialize === "function") xhr.send(args.serialize(body));
				else if (body instanceof $window.FormData) xhr.send(body);
				else xhr.send(JSON.stringify(body));
			}),
			jsonp: makeRequest(function(url, args, resolve, reject) {
				var callbackName = args.callbackName || "_mithril_" + Math.round(Math.random() * 1e16) + "_" + callbackCount++;
				var script = $window.document.createElement("script");
				$window[callbackName] = function(data) {
					delete $window[callbackName];
					script.parentNode.removeChild(script);
					resolve(data);
				};
				script.onerror = function() {
					delete $window[callbackName];
					script.parentNode.removeChild(script);
					reject(new Error("JSONP request failed"));
				};
				script.src = url + (url.indexOf("?") < 0 ? "?" : "&") +
					encodeURIComponent(args.callbackKey || "callback") + "=" +
					encodeURIComponent(callbackName);
				$window.document.documentElement.appendChild(script);
			}),
		}
	};

	var request$1 = request(window, promise, mountRedraw$1.redraw);

	var parse = function(string) {
		if (string === "" || string == null) return {}
		if (string.charAt(0) === "?") string = string.slice(1);

		var entries = string.split("&"), counters = {}, data = {};
		for (var i = 0; i < entries.length; i++) {
			var entry = entries[i].split("=");
			var key = decodeURIComponent(entry[0]);
			var value = entry.length === 2 ? decodeURIComponent(entry[1]) : "";

			if (value === "true") value = true;
			else if (value === "false") value = false;

			var levels = key.split(/\]\[?|\[/);
			var cursor = data;
			if (key.indexOf("[") > -1) levels.pop();
			for (var j = 0; j < levels.length; j++) {
				var level = levels[j], nextLevel = levels[j + 1];
				var isNumber = nextLevel == "" || !isNaN(parseInt(nextLevel, 10));
				if (level === "") {
					var key = levels.slice(0, j).join();
					if (counters[key] == null) {
						counters[key] = Array.isArray(cursor) ? cursor.length : 0;
					}
					level = counters[key]++;
				}
				// Disallow direct prototype pollution
				else if (level === "__proto__") break
				if (j === levels.length - 1) cursor[level] = value;
				else {
					// Read own properties exclusively to disallow indirect
					// prototype pollution
					var desc = Object.getOwnPropertyDescriptor(cursor, level);
					if (desc != null) desc = desc.value;
					if (desc == null) cursor[level] = desc = isNumber ? [] : {};
					cursor = desc;
				}
			}
		}
		return data
	};

	// Returns `{path, params}` from `url`
	var parse$1 = function(url) {
		var queryIndex = url.indexOf("?");
		var hashIndex = url.indexOf("#");
		var queryEnd = hashIndex < 0 ? url.length : hashIndex;
		var pathEnd = queryIndex < 0 ? queryEnd : queryIndex;
		var path = url.slice(0, pathEnd).replace(/\/{2,}/g, "/");

		if (!path) path = "/";
		else {
			if (path[0] !== "/") path = "/" + path;
			if (path.length > 1 && path[path.length - 1] === "/") path = path.slice(0, -1);
		}
		return {
			path: path,
			params: queryIndex < 0
				? {}
				: parse(url.slice(queryIndex + 1, queryEnd)),
		}
	};

	// Compiles a template into a function that takes a resolved path (without query
	// strings) and returns an object containing the template parameters with their
	// parsed values. This expects the input of the compiled template to be the
	// output of `parsePathname`. Note that it does *not* remove query parameters
	// specified in the template.
	var compileTemplate = function(template) {
		var templateData = parse$1(template);
		var templateKeys = Object.keys(templateData.params);
		var keys = [];
		var regexp = new RegExp("^" + templateData.path.replace(
			// I escape literal text so people can use things like `:file.:ext` or
			// `:lang-:locale` in routes. This is all merged into one pass so I
			// don't also accidentally escape `-` and make it harder to detect it to
			// ban it from template parameters.
			/:([^\/.-]+)(\.{3}|\.(?!\.)|-)?|[\\^$*+.()|\[\]{}]/g,
			function(m, key, extra) {
				if (key == null) return "\\" + m
				keys.push({k: key, r: extra === "..."});
				if (extra === "...") return "(.*)"
				if (extra === ".") return "([^/]+)\\."
				return "([^/]+)" + (extra || "")
			}
		) + "$");
		return function(data) {
			// First, check the params. Usually, there isn't any, and it's just
			// checking a static set.
			for (var i = 0; i < templateKeys.length; i++) {
				if (templateData.params[templateKeys[i]] !== data.params[templateKeys[i]]) return false
			}
			// If no interpolations exist, let's skip all the ceremony
			if (!keys.length) return regexp.test(data.path)
			var values = regexp.exec(data.path);
			if (values == null) return false
			for (var i = 0; i < keys.length; i++) {
				data.params[keys[i].k] = keys[i].r ? values[i + 1] : decodeURIComponent(values[i + 1]);
			}
			return true
		}
	};

	var sentinel = {};

	var router = function($window, mountRedraw) {
		var fireAsync;

		function setPath(path, data, options) {
			path = build$1(path, data);
			if (fireAsync != null) {
				fireAsync();
				var state = options ? options.state : null;
				var title = options ? options.title : null;
				if (options && options.replace) $window.history.replaceState(state, title, route.prefix + path);
				else $window.history.pushState(state, title, route.prefix + path);
			}
			else {
				$window.location.href = route.prefix + path;
			}
		}

		var currentResolver = sentinel, component, attrs, currentPath, lastUpdate;

		var SKIP = route.SKIP = {};

		function route(root, defaultRoute, routes) {
			if (root == null) throw new Error("Ensure the DOM element that was passed to `m.route` is not undefined")
			// 0 = start
			// 1 = init
			// 2 = ready
			var state = 0;

			var compiled = Object.keys(routes).map(function(route) {
				if (route[0] !== "/") throw new SyntaxError("Routes must start with a `/`")
				if ((/:([^\/\.-]+)(\.{3})?:/).test(route)) {
					throw new SyntaxError("Route parameter names must be separated with either `/`, `.`, or `-`")
				}
				return {
					route: route,
					component: routes[route],
					check: compileTemplate(route),
				}
			});
			var callAsync = typeof setImmediate === "function" ? setImmediate : setTimeout;
			var p = promise.resolve();
			var scheduled = false;
			var onremove;

			fireAsync = null;

			if (defaultRoute != null) {
				var defaultData = parse$1(defaultRoute);

				if (!compiled.some(function (i) { return i.check(defaultData) })) {
					throw new ReferenceError("Default route doesn't match any known routes")
				}
			}

			function resolveRoute() {
				scheduled = false;
				// Consider the pathname holistically. The prefix might even be invalid,
				// but that's not our problem.
				var prefix = $window.location.hash;
				if (route.prefix[0] !== "#") {
					prefix = $window.location.search + prefix;
					if (route.prefix[0] !== "?") {
						prefix = $window.location.pathname + prefix;
						if (prefix[0] !== "/") prefix = "/" + prefix;
					}
				}
				// This seemingly useless `.concat()` speeds up the tests quite a bit,
				// since the representation is consistently a relatively poorly
				// optimized cons string.
				var path = prefix.concat()
					.replace(/(?:%[a-f89][a-f0-9])+/gim, decodeURIComponent)
					.slice(route.prefix.length);
				var data = parse$1(path);

				assign(data.params, $window.history.state);

				function fail() {
					if (path === defaultRoute) throw new Error("Could not resolve default route " + defaultRoute)
					setPath(defaultRoute, null, {replace: true});
				}

				loop(0);
				function loop(i) {
					// 0 = init
					// 1 = scheduled
					// 2 = done
					for (; i < compiled.length; i++) {
						if (compiled[i].check(data)) {
							var payload = compiled[i].component;
							var matchedRoute = compiled[i].route;
							var localComp = payload;
							var update = lastUpdate = function(comp) {
								if (update !== lastUpdate) return
								if (comp === SKIP) return loop(i + 1)
								component = comp != null && (typeof comp.view === "function" || typeof comp === "function")? comp : "div";
								attrs = data.params, currentPath = path, lastUpdate = null;
								currentResolver = payload.render ? payload : null;
								if (state === 2) mountRedraw.redraw();
								else {
									state = 2;
									mountRedraw.redraw.sync();
								}
							};
							// There's no understating how much I *wish* I could
							// use `async`/`await` here...
							if (payload.view || typeof payload === "function") {
								payload = {};
								update(localComp);
							}
							else if (payload.onmatch) {
								p.then(function () {
									return payload.onmatch(data.params, path, matchedRoute)
								}).then(update, fail);
							}
							else update("div");
							return
						}
					}
					fail();
				}
			}

			// Set it unconditionally so `m.route.set` and `m.route.Link` both work,
			// even if neither `pushState` nor `hashchange` are supported. It's
			// cleared if `hashchange` is used, since that makes it automatically
			// async.
			fireAsync = function() {
				if (!scheduled) {
					scheduled = true;
					callAsync(resolveRoute);
				}
			};

			if (typeof $window.history.pushState === "function") {
				onremove = function() {
					$window.removeEventListener("popstate", fireAsync, false);
				};
				$window.addEventListener("popstate", fireAsync, false);
			} else if (route.prefix[0] === "#") {
				fireAsync = null;
				onremove = function() {
					$window.removeEventListener("hashchange", resolveRoute, false);
				};
				$window.addEventListener("hashchange", resolveRoute, false);
			}

			return mountRedraw.mount(root, {
				onbeforeupdate: function() {
					state = state ? 2 : 1;
					return !(!state || sentinel === currentResolver)
				},
				oncreate: resolveRoute,
				onremove: onremove,
				view: function() {
					if (!state || sentinel === currentResolver) return
					// Wrap in a fragment to preserve existing key semantics
					var vnode$1 = [vnode(component, attrs.key, attrs)];
					if (currentResolver) vnode$1 = currentResolver.render(vnode$1[0]);
					return vnode$1
				},
			})
		}
		route.set = function(path, data, options) {
			if (lastUpdate != null) {
				options = options || {};
				options.replace = true;
			}
			lastUpdate = null;
			setPath(path, data, options);
		};
		route.get = function() {return currentPath};
		route.prefix = "#!";
		route.Link = {
			view: function(vnode) {
				var options = vnode.attrs.options;
				// Remove these so they don't get overwritten
				var attrs = {}, onclick, href;
				assign(attrs, vnode.attrs);
				// The first two are internal, but the rest are magic attributes
				// that need censored to not screw up rendering.
				attrs.selector = attrs.options = attrs.key = attrs.oninit =
				attrs.oncreate = attrs.onbeforeupdate = attrs.onupdate =
				attrs.onbeforeremove = attrs.onremove = null;

				// Do this now so we can get the most current `href` and `disabled`.
				// Those attributes may also be specified in the selector, and we
				// should honor that.
				var child = hyperscript_1(vnode.attrs.selector || "a", attrs, vnode.children);

				// Let's provide a *right* way to disable a route link, rather than
				// letting people screw up accessibility on accident.
				//
				// The attribute is coerced so users don't get surprised over
				// `disabled: 0` resulting in a button that's somehow routable
				// despite being visibly disabled.
				if (child.attrs.disabled = Boolean(child.attrs.disabled)) {
					child.attrs.href = null;
					child.attrs["aria-disabled"] = "true";
					// If you *really* do want to do this on a disabled link, use
					// an `oncreate` hook to add it.
					child.attrs.onclick = null;
				} else {
					onclick = child.attrs.onclick;
					href = child.attrs.href;
					child.attrs.href = route.prefix + href;
					child.attrs.onclick = function(e) {
						var result;
						if (typeof onclick === "function") {
							result = onclick.call(e.currentTarget, e);
						} else if (onclick == null || typeof onclick !== "object") ; else if (typeof onclick.handleEvent === "function") {
							onclick.handleEvent(e);
						}

						// Adapted from React Router's implementation:
						// https://github.com/ReactTraining/react-router/blob/520a0acd48ae1b066eb0b07d6d4d1790a1d02482/packages/react-router-dom/modules/Link.js
						//
						// Try to be flexible and intuitive in how we handle links.
						// Fun fact: links aren't as obvious to get right as you
						// would expect. There's a lot more valid ways to click a
						// link than this, and one might want to not simply click a
						// link, but right click or command-click it to copy the
						// link target, etc. Nope, this isn't just for blind people.
						if (
							// Skip if `onclick` prevented default
							result !== false && !e.defaultPrevented &&
							// Ignore everything but left clicks
							(e.button === 0 || e.which === 0 || e.which === 1) &&
							// Let the browser handle `target=_blank`, etc.
							(!e.currentTarget.target || e.currentTarget.target === "_self") &&
							// No modifier keys
							!e.ctrlKey && !e.metaKey && !e.shiftKey && !e.altKey
						) {
							e.preventDefault();
							e.redraw = false;
							route.set(href, null, options);
						}
					};
				}
				return child
			},
		};
		route.param = function(key) {
			return attrs && key != null ? attrs[key] : attrs
		};

		return route
	};

	var route = router(window, mountRedraw$1);

	var m = function m() { return hyperscript_1$1.apply(this, arguments) };
	m.m = hyperscript_1$1;
	m.trust = hyperscript_1$1.trust;
	m.fragment = hyperscript_1$1.fragment;
	m.mount = mountRedraw$1.mount;
	m.route = route;
	m.render = render$1;
	m.redraw = mountRedraw$1.redraw;
	m.request = request$1.request;
	m.jsonp = request$1.jsonp;
	m.parseQueryString = parse;
	m.buildQueryString = build;
	m.parsePathname = parse$1;
	m.buildPathname = build$1;
	m.vnode = vnode;
	m.PromisePolyfill = polyfill;

	var mithril = m;

	const arabic = {
	  default: [
	    "\u0630 \u0661 \u0662 \u0663 \u0664 \u0665 \u0666 \u0667 \u0668 \u0669 \u0660 \u002d \u003d {bksp}",
	    "{tab}  \u0636 \u0635 \u062b \u0642 \u0641 \u063a \u0639 \u0647 \u062e \u062d \u062c \u062f  \\",
	    "{lock}  \u0634 \u0633 \u064a \u0628 \u0644 \u0627 \u062a \u0646 \u0645 \u0643 \u0637  {enter}",
	    "{shift} \u0626 \u0621 \u0624 \u0631 \u0644\u0627 \u0649 \u0629 \u0648 \u0632 \u0638  {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "\u0021 \u0040 \u0023 \u0024 \u0025 \u005e \u0026 \u002a \u0029 \u0028 \u005f \u002b {bksp}",
	    "{tab}  \u064e \u064b \u064f \u064c \ufef9 \u0625 \u2019 \u00f7 \u00d7 \u061b \u003e \u003c \u007c",
	    '{lock}  \u0650 \u064d \u005d \u005b \u0644\u0623 \u0623 \u0640 \u060c \u002f \u003a \u0022  {enter}',
	    "{shift}  \u007e \u0652 \u007b \u007d \u0644\u0622 \u0622 \u2018 \u002c \u002e \u061f  {shift}",
	    ".com @ {space}"
	  ]
	};

	//export const arabic = {
	//  default: [
	//    "\u0630 ١ ٢ ٣ ٤ ٥ ٦ ٧ ٨ ٩ ٠ - = {bksp}",
	//    "{tab} ض ص ث ق ف غ ع ه خ ح ج د \\",
	//    "{lock} ش س ي ب ل ا ت ن م ك ط {enter}",
	//    "{shift} ئ ء ؤ ر لا ى ة و ز ظ {shift}",
	//    ".com @ {space}"
	//  ],
	//  shift: [
	//    "ّ ! @ # $ % ^ & * ) ( _ + {bksp}",
	//    "{tab} َ ً ُ ٌ ﻹ إ ’ ÷ × ؛ > < |",
	//   '{lock} ِ ٍ ] [ لأ أ ـ ، / : " {enter}',
	//    "{shift} ~ ْ { } لآ آ ‘ , . ؟ {shift}",
	//    ".com @ {space}"
	//  ]
	//};


	// Robert's Shift keys
	//
	// \u0634 \u0633 \u064A \u0628 \u0644 \u0627 \u062A \u0646 \u0645 \u0643 \u061B
	// \u0638 \u0637 \u0630 \u062F \u0632 \u0631 \u0648 \u060C \u002E

	const burmese = {
	  default: [
	    "\u1050 \u1041 \u1042 \u1043 \u1044 \u1045 \u1046 \u1047 \u1048 \u1049 \u1040 \u002D \u003D {bksp}",
	    "{tab} \u1006 \u1010 \u1014 \u1019 \u1021 \u1015 \u1000 \u1004 \u101E \u1005 \u101F \u1029 \u104F",
	    "{lock} \u1031 \u103A \u102D \u1039 \u102B \u1037 \u103B \u102F \u1030 \u1038 \u0027 {enter}",
	    "{shift} \u1016 \u1011 \u1001 \u101C \u1018 \u100A \u102C \u002C \u002E \u002F {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "\u100E \u100D \u1052 \u100B \u1053 \u1054 \u1055 \u101B \u002A \u0028 \u0029 \u005F \u002B {bksp}",
	    "{tab} \u1008 \u101D \u1023 \u104E \u1024 \u104C \u1025 \u104D \u103F \u100F \u1027 \u102A \u1051",
	    "{lock} \u1017 \u103D \u102E \u1064 \u103C \u1036 \u1032 \u1012 \u1013 \u1002 \u0022 {enter}",
	    "{shift} \u1007 \u100C \u1003 \u1020 \u101A \u1009 \u1026 \u104A \u104B \u003F {shift}",
	    ".com @ {space}"
	  ]
	};

	const chinese = {
	  default: [
	    "\u20AC \u3105 \u3109 \u02C7 \u02CB \u3113 \u02CA \u02D9 \u311A \u311E \u3122 \u3126 = {bksp}",
	    "{tab} \u3106 \u310A \u310D \u3110 \u3114 \u3117 \u3127 \u311B \u311F \u3123 [ ] \\",
	    "{lock} \u3107 \u310B \u310E \u3111 \u3115 \u3118 \u3128 \u311C \u3120 \u3124 ' {enter}",
	    "{shift} \u3108 \u310C \u310F \u3112 \u3116 \u3119 \u3129 \u311D \u3121 \u3125",
	    ".com @ {space}"
	  ],
	  shift: [
	    "~ ! @ # $ % ^ & * ) ( _ + {bksp}",
	    "{tab} q w e r t y u i o p { } |",
	    '{lock} a s d f g h j k l : " {enter}',
	    "{shift} z x c v b n m , < > ? {shift}",
	    ".com @ {space}"
	  ]
	};

	const czech = {
	  default: [
	    "; + \u011B \u0161 \u010D \u0159 \u017E \u00FD \u00E1 \u00ED \u00E9 \u00B4 {bksp}",
	    "{tab} q w e r t y u i o p \u00FA ) \u00A8",
	    "{lock} a s d f g h j k l \u016F \u00A7 {enter}",
	    "{shift} \\ z x c v b n m , . - {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "\u00b0 1 2 3 4 5 6 7 8 9 0 % \u02c7 {bksp}",
	    "{tab} Q W E R T Y U I O P / ( '",
	    '{lock} A S D F G H J K L " ! {enter}',
	    "{shift} | Z X C V B N M ? : _ {shift}",
	    ".com @ {space}"
	  ]
	};

	const english = {
	  default: [
	    "` 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
	    "{tab} \u0071 w e r t y u i o p [ ] \\",
	    "{lock} \u0061 s d f g h j k l ; ' {enter}",
	    "{shift} z x c v b n m , . / {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "~ ! @ # $ % ^ & * ( ) _ + {bksp}",
	    "{tab} Q W E R T Y U I O P { } |",
	    '{lock} A S D F G H J K L : " {enter}',
	    "{shift} Z X C V B N M < > ? {shift}",
	    ".com @ {space}"
	  ]
	};

	const french = {
	  default: [
	    "\u00B2 & \u00E9 \" ' ( - \u00E8 _ \u00E7 \u00E0 ) = {bksp}",
	    "{tab} a z e r t y u i o p ^ $",
	    "{lock} q s d f g h j k l m \u00F9 * {enter}",
	    "{shift} < w x c v b n , ; : ! {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "{//} 1 2 3 4 5 6 7 8 9 0 \u00B0 + {bksp}",
	    "{tab} A Z E R T Y U I O P \u00A8 \u00A3",
	    "{lock} Q S D F G H J K L M % \u00B5 {enter}",
	    "{shift} > W X C V B N ? . / \u00A7 {shift}",
	    ".com @ {space}"
	  ]
	};

	const georgian = {
	  default: [
	    "„ 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
	    "{tab} ქ წ ე რ ტ ყ უ ი ო პ [ ] \\",
	    "{lock} ა ს დ ფ გ ჰ ჯ კ ლ ; ' {enter}",
	    "{shift} ზ ხ ც ვ ბ ნ მ , . / {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "“ ! @ # $ % ^ & * ( ) _ + {bksp}",
	    "{tab} ქ ჭ ე ღ თ ყ უ ი ო პ { } | ~",
	    '{lock} ა შ დ ფ გ ჰ ჟ კ ₾ : " {enter}',
	    "{shift} ძ ხ ჩ ვ ბ ნ მ < > ? {shift}",
	    ".com @ {space}"
	  ]
	};

	const german = {
	  default: [
	    "\u005E \u0031 \u0032 \u0033 \u0034 \u0035 \u0036 \u0037 \u0038 \u0039 \u0030 \u00DF \u00B4 {bksp}",
	    "{tab} \u0071 \u0077 \u0065 \u0072 \u0074 \u007A \u0075 \u0069 \u006F \u0070 \u00FC \u002B",
	    "{lock} \u0061 \u0073 \u0064 \u0066 \u0067 \u0068 \u006A \u006B \u006C \u00F6 \u00E4 # {enter}",
	    "{shift} \u003C \u0079 \u0078 \u0063 \u0076 \u0062 \u006E \u006D \u002C \u002E \u002D {shift}",
	    ".com \u0040 {space}"
	  ],
	  shift: [
	    '\u00B0 \u0021 \u0022 \u00A7 \u20AC \u0025 \u0026 \u002F \u0028 \u0029 \u003D \u003F \u0060 {bksp}',
	    "{tab} \u0051 \u0057 \u0045 \u0052 \u0054 \u005A \u0055 \u0049 \u004F \u0050 \u00DC \u002A",
	    "{lock} \u0041 \u0053 \u0044 \u0046 \u0047 \u0048 \u004A \u004B \u004C \u00D6 \u00C4 \u0027 {enter}",
	    "{shift} \u003E \u0059 \u0058 \u0043 \u0056 \u0042 \u004E \u004D \u003B \u003A \u005F {shift}",
	    ".com \u0040 {space}"
	  ]
	};


	//export const german = {
	//  default: [
	//    "^ 1 2 3 4 5 6 7 8 9 0 \u00DF \u00B4 {bksp}",
	//    "{tab} q w e r t z u i o p \u00FC +",
	//    "{lock} a s d f g h j k l \u00F6 \u00E4 # {enter}",
	//    "{shift} < y x c v b n m , . - {shift}",
	//    ".com @ {space}"
	//  ],
	//  shift: [
	//    '\u00B0 ! " \u00A7 $ % & / ( ) = ? ` {bksp}',
	//    "{tab} Q W E R T Z U I O P \u00DC *",
	//    "{lock} A S D F G H J K L \u00D6 \u00C4 ' {enter}",
	//    "{shift} > Y X C V B N M ; : _ {shift}",
	//    ".com @ {space}"
	//  ]
	//};

	const hindi = {
	  default: [
	    "` \u090D \u0945 \u094D\u0930 \u0930\u094D \u091C\u094D\u091E \u0924\u094D\u0930 \u0915\u094D\u0937 \u0936\u094D\u0930 \u096F \u0966 - \u0943 {bksp}",
	    "{tab} \u094C \u0948 \u093E \u0940 \u0942 \u092C \u0939 \u0917 \u0926 \u091C \u0921 \u093C \u0949 \\",
	    "{lock} \u094B \u0947 \u094D \u093F \u0941 \u092A \u0930 \u0915 \u0924 \u091A \u091F {enter}",
	    "{shift} \u0902 \u092E \u0928 \u0935 \u0932 \u0938 , . \u092F {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "~ \u0967 \u0968 \u0969 \u096A \u096B \u096C \u096D \u096E \u096F \u0966 \u0903 \u090B {bksp}",
	    "{tab} \u0914 \u0910 \u0906 \u0908 \u090A \u092D \u0919 \u0918 \u0927 \u091D \u0922 \u091E \u0911",
	    "{lock} \u0913 \u090F \u0905 \u0907 \u0909 \u092B \u0931 \u0916 \u0925 \u091B \u0920 {enter}",
	    '{shift} "" \u0901 \u0923 \u0928 \u0935 \u0933 \u0936 \u0937 \u0964 \u095F {shift}',
	    ".com @ {space}"
	  ]
	};

	//for some keys in Punjabi we have to have 3 options, I'm not sure how to write that into the keyboard. 
	//these are all Punjabi symbols on the keyboard we need, missing are numbers and some diacritics such as : or ;

	const punjabi = {
	  default: [
	    "\u0a67 \u0a68 \u0a69 \u0a6a \u0a6b \u0a6c \u0a6d \u0a6e \u0a6f \u0a66 \u002d \u003d {bksp}",
	    "{tab} \u0a73 \u0a05 \u0a47 \u0a30 \u0a24 \u0a2f \u0a41 \u0a3f \u0a4b \u0a2a \u0a71 \u0a70 \u005c",
	    "{lock} \u0a3e \u0a38 \u0a26 \u0a4d \u0a17 \u0a39 \u0a1c \u0a15 \u0a32 \u003b \u0027 {enter}",
	    "{shift} \u0a19 \u0a5c \u0a1a \u0a35 \u0a2c \u0a28 \u0a2e \u002c \u0964 \u002f {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "\u0a75 \u0021 \u0040 \u0023 \u0024 \u0025 \u005e \u0026 \u002a \u0028 \u0029 \u005f \u002b {bksp}",
	    "{tab} \u0a72 \u0a06 \u0a48 \u0a25 \u0a42 \u0a40 \u0a4c \u0a2b \u005b \u005d \u007c" ,
	    "{lock} \u0a3e \u0a36 \u0a27 \u0a3c \u0a18 \u0a03 \u0a1d \u0a16 \u0a33 \u003a \u0022 {enter}",
	     "{shift}  \u0a5c\u0a4d\u0a39 \u0a1b \u0a74 \u0a2d \u0a23 \u0a02 \u002e \u0965 \u003f {shift}",
	    ".com @ {space}"
	//  ],
	//  option: [
	//    "\u0060 \u0031 \u0032 \u0033 \u0034 \u0035 \u0036 \u0037 \u0038 \u0039 \u0030 \u002d \u003d {bksp}",
	//    "{tab} \u0a0f \u0a1f \u0a09 \u0a07 \u0a13 \u0a5e \u005b \u005d \u005c" ,
	//    "{lock} \u0a05 \u0a21 \u0a5a \u0a5b \u0a59 \u003b \u0027 {enter}",
	//     "{shift} \u0a1e \u0a01 \u003c \u003e {shift}",
	//    ".com @ {space}"
	  ]
	};


	//export const punjabi = {
	//  default: [
	//    "` \u090D \u0945 \u094D\u0930 \u0930\u094D \u091C\u094D\u091E \u0924\u094D\u0930 \u0915\u094D\u0937 \u0936\u094D\u0930 \u096F \u0966 - \u0943 {bksp}",
	//    "{tab} \u094C \u0948 \u093E \u0940 \u0942 \u092C \u0939 \u0917 \u0926 \u091C \u0921 \u093C \u0949 \\",
	//    "{lock} \u094B \u0947 \u094D \u093F \u0941 \u092A \u0930 \u0915 \u0924 \u091A \u091F {enter}",
	//    "{shift} \u0902 \u092E \u0928 \u0935 \u0932 \u0938 , . \u092F {shift}",
	//    ".com @ {space}"
	//  ],
	//  shift: [
	//    "~ \u0967 \u0968 \u0969 \u096A \u096B \u096C \u096D \u096E \u096F \u0966 \u0903 \u090B {bksp}",
	//    "{tab} \u0914 \u0910 \u0906 \u0908 \u090A \u092D \u0919 \u0918 \u0927 \u091D \u0922 \u091E \u0911",
	//    "{lock} \u0913 \u090F \u0905 \u0907 \u0909 \u092B \u0931 \u0916 \u0925 \u091B \u0920 {enter}",
	//    '{shift} "" \u0901 \u0923 \u0928 \u0935 \u0933 \u0936 \u0937 \u0964 \u095F {shift}',
	//    ".com @ {space}"
	//  ]
	// };

	const italian = {
	  default: [
	    "\\ 1 2 3 4 5 6 7 8 9 0 ' \u00EC {bksp}",
	    "{tab} q w e r t y u i o p \u00E8 +",
	    "{lock} a s d f g h j k l \u00F2 \u00E0 \u00F9 {enter}",
	    "{shift} < z x c v b n m , . - {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    '| ! " \u00A3 $ % & / ( ) = ? ^ {bksp}',
	    "{tab} Q W E R T Y U I O P \u00E9 *",
	    "{lock} A S D F G H J K L \u00E7 \u00B0 \u00A7 {enter}",
	    "{shift} > Z X C V B N M ; : _ {shift}",
	    ".com @ {space}"
	  ]
	};

	const japanese = {
	  default: [
	    "\u308d \u306c \u3075 \u3042 \u3046 \u3048 \u304a \u3084 \u3086 \u3088 \u308f \u307b \u3078 {bksp}",
	    "{tab} \u305f \u3066 \u3044 \u3059 \u304b \u3093 \u306a \u306b \u3089 \u305b \u309b \u309c \u3080",
	    "{lock} \u3061 \u3068 \u3057 \u306f \u304D \u304f \u307e \u306e \u308a \u308c \u3051 {enter}",
	    "{shift} \u3064 \u3055 \u305d \u3072 \u3053 \u307f \u3082 \u306d \u308b \u3081 {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "\u308d \u306c \u3075 \u3041 \u3045 \u3047 \u3049 \u3083 \u3085 \u3087 \u3092 \u30fc \u3078 {bksp}",
	    "{tab} \u305f \u3066 \u3043 \u3059 \u304b \u3093 \u306a \u306b \u3089 \u305b \u300c \u300d \u3080",
	    "{lock} \u3061 \u3068 \u3057 \u306f \u304D \u304f \u307e \u306e \u308a \u308c \u3051 {enter}",
	    "{shift} \u3063 \u3055 \u305d \u3072 \u3053 \u307f \u3082 \u3001 \u3002 \u30fb {shift}",
	    ".com @ {space}"
	  ]
	};

	const korean = {
	  default: [
	    "` 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
	    "{tab} \u1107 \u110c \u1103 \u1100 \u1109 \u116d \u1167 \u1163 \u1162 \u1166 [ ] \u20a9",
	    "{lock} \u1106 \u1102 \u110b \u1105 \u1112 \u1169 \u1165 \u1161 \u1175 ; ' {enter}",
	    "{shift} \u110f \u1110 \u110e \u1111 \u1172 \u116e \u1173 , . / {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "~ ! @ # $ % ^ & * ( ) _ + {bksp}",
	    "{tab} \u1108 \u110d \u1104 \u1101 \u110a \u116d \u1167 \u1163 \u1164 \u1168 { } |",
	    '{lock} \u1106 \u1102 \u110b \u1105 \u1112 \u1169 \u1165 \u1161 \u1175 : " {enter}',
	    "{shift} \u110f \u1110 \u110e \u1111 \u1172 \u116e \u1173 < > ? {shift}",
	    ".com @ {space}"
	  ]
	};

	const russian = {
	  default: [
	    "\u0451 1 2 3 4 5 6 7 8 9 0 - = {bksp}",
	    "{tab} \u0439 \u0446 \u0443 \u043a \u0435 \u043d \u0433 \u0448 \u0449 \u0437 \u0445 \u044a \\",
	    "{lock} \u0444 \u044b \u0432 \u0430 \u043f \u0440 \u043e \u043b \u0434 \u0436 \u044d {enter}",
	    "{shift} \\ \u044f \u0447 \u0441 \u043c \u0438 \u0442 \u044c \u0431 \u044e / {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    '\u0401 ! " \u2116 ; % : ? * ( ) _ + {bksp}',
	    "{tab} \u0419 \u0426 \u0423 \u041a \u0415 \u041d \u0413 \u0428 \u0429 \u0417 \u0425 \u042a /",
	    "{lock} \u0424 \u042b \u0412 \u0410 \u041f \u0420 \u041e \u041b \u0414 \u0416 \u042d {enter}",
	    "{shift} / \u042f \u0427 \u0421 \u041c \u0418 \u0422 \u042c \u0411 \u042e / {shift}",
	    ".com @ {space}"
	  ]
	};

	const sindhi = {
	  default: [
	    "` \u0661 \u0662 \u0663 \u0664 \u0665 \u0666 \u0667 \u0668 \u0669 \u0660 - = {bksp}",
	    "{tab} \u0642 \uFEED \u0639 \u0631 \u062A \u0680 \u0621 \u064A \uFBA6 \u067E [ ]",
	    "{lock} \u0627 \u0633 \u062F \u0641 \u06AF \u06BE \u062C \u06A9 \u0644 \u061B \u060C {enter}",
	    "{shift} \u0632 \u0634 \u0686 \u0637 \u0628 \u0646 \u0645 \u0687 , . / {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "~ ! @ # $ \u066A ^ & * ( ) _ + {bksp}",
	    "{tab} \uFE70 \u068C \u06AA \u0699 \u067D \uFE7A \uFEFB \uFE8B \u06A6 | { }",
	    "{lock} \u067B \u0635 \u068A \u060D \u063A \u062D \u0636 \u062E \u06D4 \u0703 \u05f4 {enter}",
	    "{shift} \u0630 \u067F \u062B \u0638 \u067A \u066b \u0640 < > \u061F {shift}",
	    ".com @ {space}"
	  ]
	};

	/*
	 * Spanish layouts by Paco Alcantara (https://github.com/pacoalcantara)
	 * Based on: http://ascii-table.com/keyboard.php/171 and http://ascii-table.com/keyboard.php/071-2
	 */

	const spanish = {
	  default: [
	    // "\u007c 1 2 3 4 5 6 7 8 9 0 ' ¡ {bksp}",
	    // "{tab} q w e r t y u i o p \u0301 +",
	    // "{lock} a s d f g h j k l \u00f1 \u007b \u007d {enter}",
	    // "{shift} < z x c v b n m , . - {shift}",
	    // ".com @ {space}"


	    // < 1 2 3 4 5 6 7 8 9 0 ‘ ¡ ¿
	    "< 1 2 3 4 5 6 7 8 9 0 ‘ ¡ ¿ {bksp}",
	    // q w e r t y u i o p `+ ç
	    "{tab} q w e r t y u i o p ` + ç",
	    // a s d f g h j k l ñ ´
	    "{lock} a s d f g h j k l ñ ´ {enter}",
	    // z x c v b n m , . -
	    "{shift} z x c v b n m , . - {shift}",
	    ".com @ {space} @ .com"
	  ],
	  shift: [
	    // > ! ” · $ % & / ( ) = ? ¿
	    '> ! ” · $ % & / ( ) = ? ¿ {bksp}',
	    // Q W E R T Y U I O P ^* Ç
	    "{tab} Q W E R T Y U I O P ^ * Ç",
	    //  A S D F G H J K L Ñ ¨
	    "{lock} A S D F G H J K L Ñ ¨ {enter}",
	    // Z X C V B N M ; : _
	    "{shift} Z X C V B N M ; : _ {shift}",
	    ".com @ {space}"
	  ]
	 };


	// const spanish = {
	//   default: [
	//     "\u007c 1 2 3 4 5 6 7 8 9 0 ' \u00bf {bksp}",
	//     "{tab} q w e r t y u i o p \u0301 +",
	//     "{lock} a s d f g h j k l \u00f1 \u007b \u007d {enter}",
	//     "{shift} < z x c v b n m , . - {shift}",
	//     ".com @ {space}"
	//   ],
	//   shift: [
	//     '\u00b0 ! " # $ % & / ( ) = ? \u00a1 {bksp}',
	//     "{tab} Q W E R T Y U I O P \u0308 *",
	//     "{lock} A S D F G H J K L \u00d1 \u005b \u005d {enter}",
	//     "{shift} > Z X C V B N M ; : _ {shift}",
	//     ".com @ {space}"
	//   ]
	// };

	const thai = {
	  default: [
	    "\u005F \u0E45 \u002F \u002D \u0E20 \u0E16 \u0E38 \u0E36 \u0E04 \u0E05 \u0E08 \u0E02 \u0E0A {bksp}",
	    "{tab} \u0E46 \u0E44 \u0E33 \u0E1E \u0E30 \u0E31 \u0E35 \u0E23 \u0E19 \u0E22 \u0E1A \u0E25 \u0E03 ",
	    "{lock} \u0E1F \u0E2B \u0E01 \u0E14 \u0E40 \u0E49 \u0E48 \u0E32 \u0E2A \u0E27 \u0E07 {enter}",
	    "{shift} \u0E1C \u0E1B \u0E41 \u0E2D \u0E34 \u0E37 \u0E17 \u0E21 \u0E43 \u0E1D {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "% + \u0E51 \u0E52 \u0E53 \u0E54 \u0E39 \u0E3F \u0E55 \u0E56 \u0E57 \u0E58 \u0E59 {bksp}",
	    "{tab} \u0E50 \u0022 \u0E0E \u0E11 \u0E18 \u0E4D \u0E4A \u0E13 \u0E2F \u0E0D \u0E10 \u002C \u0E05",
	    "{lock} \u0E24 \u0E06 \u0E0F \u0E42 \u0E0C \u0E47 \u0E4B \u0E29 \u0E28 \u0E0B \u002E {enter}",
	    "{shift} ( ) \u0E09 \u0E2E \u0E3A \u0E4C \u003F \u0E12 \u0E2C \u0E26 {shift}",
	    ".com @ {space}"
	  ]
	};

	const turkish = {
	  default: [
	    '" 1 2 3 4 5 6 7 8 9 0 * - # {bksp}',
	    "{tab} q w e r t y u ı o p ğ ü [ ]",
	    "{lock} a s d f g h j k l ş i , {enter}",
	    "{shift} < z x c v b n m ö ç . | $ € {shift}",
	    ".com @ {space}"
	  ],
	  shift: [
	    "é ! ' ^ + % & / ( ) = ? _ ~ {bksp}",
	    "{tab} Q W E R T Y U I O P Ğ Ü { }",
	    "{lock} A S D F G H J K L Ş İ ; {enter}",
	    "{shift} > Z X C V B N M Ö Ç : \\ ` ´ {shift}",
	    ".com @ {space}"
	  ]
	};

	// import arabic from './arabic'
	// import burmese from './burmese'
	// import chinese from './chinese'
	// import czech from './czech'
	// import english from './english'
	// import french from './french'
	// import georgian from './georgian'
	// import german from './german'
	// import hindi from './hindi'
	// import punjabi from './punjabi'
	// import italian from './italian'
	// import japanese from './japanese'
	// import korean from './korean'
	// import russian from './russian'
	// import sindhi from './sindhi'
	// import spanish from './spanish'
	// import thai from './thai'
	// import turkish from './turkish'
	// import urdu from './urdu'

	// export default {
	//   arabic,
	//   burmese,
	//   chinese,
	//   czech,
	//   english,
	//   french,
	//   georgian,
	//   german,
	//   hindi,
	//   punjabi,
	//   italian,
	//   japanese,
	//   korean,
	//   russian,
	//   sindhi,
	//   spanish,
	//   thai,
	//   turkish,
	//   urdu
	// }

	var layoutsEyevocab = /*#__PURE__*/Object.freeze({
		__proto__: null,
		arabic: arabic,
		burmese: burmese,
		chinese: chinese,
		czech: czech,
		english: english,
		french: french,
		georgian: georgian,
		german: german,
		hindi: hindi,
		punjabi: punjabi,
		italian: italian,
		japanese: japanese,
		korean: korean,
		russian: russian,
		sindhi: sindhi,
		spanish: spanish,
		thai: thai,
		turkish: turkish
	});

	var require$$0 = getCjsExportFromNamespace(layoutsEyevocab);

	var keyboard = createCommonjsModule(function (module, exports) {
	var __createBinding = (commonjsGlobal && commonjsGlobal.__createBinding) || (Object.create ? (function(o, m, k, k2) {
	    if (k2 === undefined) k2 = k;
	    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
	}) : (function(o, m, k, k2) {
	    if (k2 === undefined) k2 = k;
	    o[k2] = m[k];
	}));
	var __setModuleDefault = (commonjsGlobal && commonjsGlobal.__setModuleDefault) || (Object.create ? (function(o, v) {
	    Object.defineProperty(o, "default", { enumerable: true, value: v });
	}) : function(o, v) {
	    o["default"] = v;
	});
	var __importStar = (commonjsGlobal && commonjsGlobal.__importStar) || function (mod) {
	    if (mod && mod.__esModule) return mod;
	    var result = {};
	    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
	    __setModuleDefault(result, mod);
	    return result;
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.LangList = void 0;
	var LayoutsEyevocab = __importStar(require$$0);
	// list our language codes
	exports.LangList = Object.keys(LayoutsEyevocab);
	});

	var dash = createCommonjsModule(function (module, exports) {
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.Dash = void 0;
	var mithril_1 = __importDefault(mithril);

	exports.Dash = {
	    view: function (_a) {
	        var state = _a.attrs;
	        return (mithril_1.default("div", { class: "component-dash" },
	            mithril_1.default("h2", null, "Dash!"),
	            mithril_1.default("ul", null,
	                mithril_1.default("li", null, "Languages List"),
	                mithril_1.default("ul", null, keyboard.LangList.map(function (lang) {
	                    return (mithril_1.default("li", null,
	                        mithril_1.default("a", { href: "/app/#!/" + lang }, lang)));
	                })),
	                mithril_1.default("li", null, "Collections"),
	                mithril_1.default("ul", null,
	                    mithril_1.default("li", null, "Create New Collection -- Collection Create"),
	                    mithril_1.default("li", null, "Search Collections"),
	                    mithril_1.default("li", null, "Collection List Result"),
	                    mithril_1.default("ul", null,
	                        mithril_1.default("li", null, "Launch Practice/Test Session -- Deck"),
	                        mithril_1.default("li", null, "Edit -- Collection Edit"))),
	                mithril_1.default("li", null, "Collection Create"),
	                mithril_1.default("ul", null,
	                    mithril_1.default("li", null, "Name"),
	                    mithril_1.default("li", null, "Description")),
	                mithril_1.default("li", null, "Collection Edit"),
	                mithril_1.default("ul", null,
	                    mithril_1.default("li", null, "Name"),
	                    mithril_1.default("li", null, "Description"),
	                    mithril_1.default("li", null, "Session Spacing"),
	                    mithril_1.default("li", null, "Word list add/delete/order")),
	                mithril_1.default("li", null, "Collection Share"),
	                mithril_1.default("ul", null,
	                    mithril_1.default("li", null, "Invite: Invite existing users or by email/phone"),
	                    mithril_1.default("li", null, "Access: Edit access controls")))));
	    }
	};
	});

	var language = createCommonjsModule(function (module, exports) {
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.Language = void 0;
	var mithril_1 = __importDefault(mithril);
	exports.Language = {
	    view: function (_a) {
	        var state = _a.attrs;
	        console.log(state.collectionList);
	        return (mithril_1.default("div", { class: "dash-language" },
	            mithril_1.default("ul", null,
	                mithril_1.default("li", null,
	                    mithril_1.default("a", { href: "/app/#!/" + state.languageOne.id + "/_new" },
	                        "Create ",
	                        state.languageOne.id,
	                        " Collection"))),
	            "Collection List",
	            mithril_1.default("ul", null, state.collectionList.map(function (one) {
	                return mithril_1.default("li", null,
	                    mithril_1.default("a", { href: "/app/#!/" + state.languageOne.id + "/" + one.id },
	                        "Edit ",
	                        one.name));
	            }))));
	    }
	};
	});

	var stream = createCommonjsModule(function (module) {
	(function() {
	/* eslint-enable */
	Stream.SKIP = {};
	Stream.lift = lift;
	Stream.scan = scan;
	Stream.merge = merge;
	Stream.combine = combine;
	Stream.scanMerge = scanMerge;
	Stream["fantasy-land/of"] = Stream;

	var warnedHalt = false;
	Object.defineProperty(Stream, "HALT", {
		get: function() {
			warnedHalt || console.log("HALT is deprecated and has been renamed to SKIP");
			warnedHalt = true;
			return Stream.SKIP
		}
	});

	function Stream(value) {
		var dependentStreams = [];
		var dependentFns = [];

		function stream(v) {
			if (arguments.length && v !== Stream.SKIP) {
				value = v;
				if (open(stream)) {
					stream._changing();
					stream._state = "active";
					dependentStreams.forEach(function(s, i) { s(dependentFns[i](value)); });
				}
			}

			return value
		}

		stream.constructor = Stream;
		stream._state = arguments.length && value !== Stream.SKIP ? "active" : "pending";
		stream._parents = [];

		stream._changing = function() {
			if (open(stream)) stream._state = "changing";
			dependentStreams.forEach(function(s) {
				s._changing();
			});
		};

		stream._map = function(fn, ignoreInitial) {
			var target = ignoreInitial ? Stream() : Stream(fn(value));
			target._parents.push(stream);
			dependentStreams.push(target);
			dependentFns.push(fn);
			return target
		};

		stream.map = function(fn) {
			return stream._map(fn, stream._state !== "active")
		};

		var end;
		function createEnd() {
			end = Stream();
			end.map(function(value) {
				if (value === true) {
					stream._parents.forEach(function (p) {p._unregisterChild(stream);});
					stream._state = "ended";
					stream._parents.length = dependentStreams.length = dependentFns.length = 0;
				}
				return value
			});
			return end
		}

		stream.toJSON = function() { return value != null && typeof value.toJSON === "function" ? value.toJSON() : value };

		stream["fantasy-land/map"] = stream.map;
		stream["fantasy-land/ap"] = function(x) { return combine(function(s1, s2) { return s1()(s2()) }, [x, stream]) };

		stream._unregisterChild = function(child) {
			var childIndex = dependentStreams.indexOf(child);
			if (childIndex !== -1) {
				dependentStreams.splice(childIndex, 1);
				dependentFns.splice(childIndex, 1);
			}
		};

		Object.defineProperty(stream, "end", {
			get: function() { return end || createEnd() }
		});

		return stream
	}

	function combine(fn, streams) {
		var ready = streams.every(function(s) {
			if (s.constructor !== Stream)
				throw new Error("Ensure that each item passed to stream.combine/stream.merge/lift is a stream")
			return s._state === "active"
		});
		var stream = ready
			? Stream(fn.apply(null, streams.concat([streams])))
			: Stream();

		var changed = [];

		var mappers = streams.map(function(s) {
			return s._map(function(value) {
				changed.push(s);
				if (ready || streams.every(function(s) { return s._state !== "pending" })) {
					ready = true;
					stream(fn.apply(null, streams.concat([changed])));
					changed = [];
				}
				return value
			}, true)
		});

		var endStream = stream.end.map(function(value) {
			if (value === true) {
				mappers.forEach(function(mapper) { mapper.end(true); });
				endStream.end(true);
			}
			return undefined
		});

		return stream
	}

	function merge(streams) {
		return combine(function() { return streams.map(function(s) { return s() }) }, streams)
	}

	function scan(fn, acc, origin) {
		var stream = origin.map(function(v) {
			var next = fn(acc, v);
			if (next !== Stream.SKIP) acc = next;
			return next
		});
		stream(acc);
		return stream
	}

	function scanMerge(tuples, seed) {
		var streams = tuples.map(function(tuple) { return tuple[0] });

		var stream = combine(function() {
			var changed = arguments[arguments.length - 1];
			streams.forEach(function(stream, i) {
				if (changed.indexOf(stream) > -1)
					seed = tuples[i][1](seed, stream());
			});

			return seed
		}, streams);

		stream(seed);

		return stream
	}

	function lift() {
		var fn = arguments[0];
		var streams = Array.prototype.slice.call(arguments, 1);
		return merge(streams).map(function(streams) {
			return fn.apply(undefined, streams)
		})
	}

	function open(s) {
		return s._state === "pending" || s._state === "active" || s._state === "changing"
	}

	module["exports"] = Stream;

	}());
	});

	var stream$1 = stream;

	const e=Object.assign||((e,t)=>(t&&Object.keys(t).forEach(o=>e[o]=t[o]),e)),t=(e,r,s)=>{const c=typeof s;if(s&&"object"===c)if(Array.isArray(s))for(const o of s)r=t(e,r,o);else for(const c of Object.keys(s)){const f=s[c];"function"==typeof f?r[c]=f(r[c],o):void 0===f?e&&!isNaN(c)?r.splice(c,1):delete r[c]:null===f||"object"!=typeof f||Array.isArray(f)?r[c]=f:"object"==typeof r[c]?r[c]=f===r[c]?f:o(r[c],f):r[c]=t(!1,{},f);}else "function"===c&&(r=s(r,o));return r},o=(o,...r)=>{const s=Array.isArray(o);return t(s,s?o.slice():e({},o),r)};

	var model = createCommonjsModule(function (module, exports) {
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.CollectionClient = void 0;
	var CollectionClient = /** @class */ (function () {
	    function CollectionClient(collection) {
	        this.setRemindList = [];
	        Object.assign(this, collection);
	    }
	    return CollectionClient;
	}());
	exports.CollectionClient = CollectionClient;
	});

	var clientState = createCommonjsModule(function (module, exports) {
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.ClientState = void 0;

	exports.ClientState = {
	    component: "Dash",
	    display: "Dash",
	    dashDisplay: "Create",
	    Deck: {
	        collectionOne: new model.CollectionClient({ id: '_new' })
	    },
	    Dash: {
	        languageOne: {
	            id: ''
	        },
	        collectionList: [],
	        collectionOne: new model.CollectionClient({ id: '_new' }),
	        setCreateOptions: [
	            {
	                id: "manual",
	                name: "Manual Set",
	                description: "Uses the sets in the order created. Automatically chooses the next not-yet-practiced set. Units are still randomized per set."
	            },
	            {
	                id: "random",
	                name: "Random Set",
	                description: "Creates a set at random from the entire collection. Will not repeat words."
	            },
	            {
	                id: "smart_incrementing",
	                name: "Smart Set Even Distribution",
	                description: "Automatically generates sets of the most challenging words in a language."
	            },
	            {
	                id: "smart_random",
	                name: "Smart Set Random Distribution",
	                description: "Automatically generates sets of words at random that are evenly challenging."
	            }
	        ],
	        sessionNextOptions: [
	            {
	                id: "incremental",
	                name: "Next Set",
	                description: "Advances the set in the order of the collection item list by automatically choosing the next not-yet-practiced set. Units are still randomized per set."
	            },
	            {
	                id: "smart_collection",
	                name: "Smart Collection Set",
	                description: "Will attempt to choose a set of language items (words) that are most challenging to the user based on evaluation of the user's past collection sessions. Only adds words that have already been practiced"
	            },
	            {
	                id: "smart_language_descending",
	                name: "Smart Set Hardest",
	                description: "Will generate a set of most challenging language items (words) from a collection based on evaluation of a entire language's metrics. Evaluates per user for least practiced language items."
	            },
	            { id: "smart_language_ascending",
	                name: "Smart Set Easiest",
	                description: "Will generate a set of least challenging language items (words) from a collection based on evaluation of a entire language's metrics. Evaluates per user for least practiced language items."
	            }
	        ],
	        sessionIntervalOptions: [
	            {
	                id: "after_first",
	                name: "After First",
	                description: "Reminder is sent at an interval relative to the timestamp of the first practice session. This makes for a more uniform collection reminder schedule."
	            },
	            {
	                id: "after_last",
	                name: "After Last (recommended)",
	                description: "Reminder is sent at an interval relative to the timestamp of the last practice session. This makes for a more forgiving reminder schedule."
	            }
	        ],
	        sessionModeOptions: [
	            {
	                id: "test",
	                name: "Test",
	                description: "Test mode is hard."
	            },
	            {
	                id: "practice",
	                name: "Practice",
	                description: "Practice Mode is very fogiving with hints and etc."
	            },
	            {
	                id: "review",
	                name: "Review",
	                description: "A simple look-over session."
	            }
	        ],
	        sessionSetOptions: [
	            {
	                id: "smart",
	                name: "Smart Set",
	                description: "Creates a set at random from the entire collection. Will not repeat words."
	            },
	            {
	                id: "random",
	                name: "Random Set",
	                description: "Creates a set at random from the entire collection. Will not repeat words."
	            },
	            {
	                id: "manual",
	                name: "Manual Set",
	                description: "Uses the sets in the order created. Automatically chooses the next not-yet-practiced set. Units are still randomized per set."
	            }
	        ],
	    }
	};
	});

	var state = createCommonjsModule(function (module, exports) {
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.StateSet = exports.StateGet = void 0;
	var mithril_1 = __importDefault(mithril);
	var stream_1 = __importDefault(stream$1);
	var mergerino_1 = __importDefault(o);

	// for convenience in the actions
	var Set = stream_1.default();
	/**
	 * The StateGet function is exported as default
	 */
	exports.StateGet = stream_1.default.scan(mergerino_1.default, clientState.ClientState, Set);
	exports.StateSet = function (state) {
	    Set(state);
	    mithril_1.default.redraw();
	};
	});

	var service = createCommonjsModule(function (module, exports) {
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.Service = void 0;
	// manages requests, data, and state

	// this is probably better off refactored into small modular functions
	var Service = /** @class */ (function () {
	    function Service() {
	    }
	    // posts collection state changes to the server
	    Service.CollectionPost = function () { };
	    // validates changes to a collection settings
	    Service.CollectionValid = function () { };
	    // validates changes to a collection session settings
	    Service.CollectionSessionValid = function () { };
	    // add a reminder
	    Service.CollectionSessionStateAdd = function () {
	        state.StateGet().Dash.collectionOne.setRemindList.unshift({});
	    };
	    Service.CollectionSessionStateSet = function (i, sessionState) {
	    };
	    Service.CollectionSessionStateRemove = function (i) {
	    };
	    Service.CollectionCreate = function () {
	        var _a = state.StateGet().Dash, languageOne = _a.languageOne, collectionOne = _a.collectionOne, collectionList = _a.collectionList;
	        if (collectionOne.id === '_new') {
	            collectionOne.id = '_' + Math.floor((Math.random() * 10000));
	            collectionOne.idLanguage = languageOne.id;
	        }
	        collectionList.unshift(collectionOne);
	        state.StateSet({ Dash: { collectionOne: collectionOne, collectionList: collectionList } });
	        return collectionOne;
	    };
	    return Service;
	}());
	exports.Service = Service;
	});

	var action = createCommonjsModule(function (module, exports) {
	var __awaiter = (commonjsGlobal && commonjsGlobal.__awaiter) || function (thisArg, _arguments, P, generator) {
	    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
	    return new (P || (P = Promise))(function (resolve, reject) {
	        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
	        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
	        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
	        step((generator = generator.apply(thisArg, _arguments || [])).next());
	    });
	};
	var __generator = (commonjsGlobal && commonjsGlobal.__generator) || function (thisArg, body) {
	    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
	    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
	    function verb(n) { return function (v) { return step([n, v]); }; }
	    function step(op) {
	        if (f) throw new TypeError("Generator is already executing.");
	        while (_) try {
	            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
	            if (y = 0, t) op = [op[0] & 2, t.value];
	            switch (op[0]) {
	                case 0: case 1: t = op; break;
	                case 4: _.label++; return { value: op[1], done: false };
	                case 5: _.label++; y = op[1]; op = [0]; continue;
	                case 7: op = _.ops.pop(); _.trys.pop(); continue;
	                default:
	                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
	                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
	                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
	                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
	                    if (t[2]) _.ops.pop();
	                    _.trys.pop(); continue;
	            }
	            op = body.call(thisArg, _);
	        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
	        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
	    }
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.CollectionSessionStateRemove = exports.CollectionSessionStateAdd = exports.CollectionSessionPost = exports.CollectionPost = exports.CollectionCreateAction = exports.CollectionSessionDisplay = exports.CollectionEditDisplay = void 0;
	// provides actions to components


	exports.CollectionEditDisplay = function (e) {
	    e.preventDefault();
	    state.StateSet({ display: 'CollectionEdit' });
	};
	exports.CollectionSessionDisplay = function (e) {
	    e.preventDefault();
	    state.StateSet({ display: 'CollectionEditSession' });
	};
	exports.CollectionCreateAction = function (e) {
	    e.preventDefault();
	    var collection = service.Service.CollectionCreate();
	    window.location.href = "/app/#!/" + collection.idLanguage + "/" + collection.id;
	};
	exports.CollectionPost = function (e) { return __awaiter(void 0, void 0, void 0, function () {
	    return __generator(this, function (_a) {
	        e.preventDefault();
	        service.Service.CollectionValid();
	        service.Service.CollectionPost();
	        return [2 /*return*/];
	    });
	}); };
	exports.CollectionSessionPost = function (e) { return __awaiter(void 0, void 0, void 0, function () {
	    return __generator(this, function (_a) {
	        e.preventDefault();
	        service.Service.CollectionSessionValid();
	        service.Service.CollectionPost();
	        return [2 /*return*/];
	    });
	}); };
	// export const CollectionSessionStateSet = (e)=>{
	//     e.preventDefault()
	//     Service.CollectionSessionStateAdd()
	// }
	exports.CollectionSessionStateAdd = function (e) {
	    e.preventDefault();
	    service.Service.CollectionSessionStateAdd();
	};
	exports.CollectionSessionStateRemove = function (i) { return function (e) { return __awaiter(void 0, void 0, void 0, function () {
	    return __generator(this, function (_a) {
	        e.preventDefault();
	        service.Service.CollectionSessionStateRemove(i);
	        return [2 /*return*/];
	    });
	}); }; };
	});

	var collection = createCommonjsModule(function (module, exports) {
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.Collection = void 0;
	var mithril_1 = __importDefault(mithril);

	exports.Collection = {
	    view: function (_a) {
	        var state = _a.attrs;
	        return (mithril_1.default("div", { class: "dash-collection" },
	            mithril_1.default("h2", null,
	                "Collection Name ",
	                state.collectionOne.name),
	            mithril_1.default("ul", null,
	                mithril_1.default("li", null, "Name  & Description"),
	                mithril_1.default("li", null,
	                    mithril_1.default("a", { href: "", onclick: action.CollectionEditDisplay }, "Name  & Description")),
	                mithril_1.default("li", null,
	                    mithril_1.default("a", { href: "", onclick: action.CollectionSessionDisplay }, "Spacing  & Notifications")),
	                mithril_1.default("li", null, "Language Item edit List"),
	                mithril_1.default("li", null, "Invite & Access"))));
	    }
	};
	});

	var collectionEdit = createCommonjsModule(function (module, exports) {
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.CollectionEdit = void 0;
	var mithril_1 = __importDefault(mithril);
	exports.CollectionEdit = {
	    view: function (_a) {
	        var state = _a.attrs;
	        return (mithril_1.default("div", { class: "collection-edit" },
	            mithril_1.default("h2", null,
	                "Edit Collection:",
	                state.collectionOne.name),
	            mithril_1.default("ul", null,
	                mithril_1.default("li", null, "Name  & Description"),
	                mithril_1.default("li", null, "Spacing  & Notifications (display, but no edit"),
	                mithril_1.default("li", null, "Language Item edit List"),
	                mithril_1.default("li", null, "Invite & Access"))));
	    }
	};
	});

	var collectionSession = createCommonjsModule(function (module, exports) {
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.CollectionEditSession = void 0;
	var mithril_1 = __importDefault(mithril);

	exports.CollectionEditSession = {
	    view: function (_a) {
	        var state = _a.attrs;
	        var _b = state, collectionOne = _b.collectionOne, sessionSetOptions = _b.sessionSetOptions, sessionModeOptions = _b.sessionModeOptions, sessionNextOptions = _b.sessionNextOptions, sessionIntervalOptions = _b.sessionIntervalOptions;
	        return (mithril_1.default("form", { onsubmit: action.CollectionSessionPost, class: "collection-spacing" },
	            mithril_1.default("h2", null, "Spacing & Notifications"),
	            mithril_1.default("fieldset", null,
	                mithril_1.default("legend", null, "Sessions and Reminders: Determine how sessions and reminders behave."),
	                collectionOne.setRemindList.map(function (one, i) { return (mithril_1.default("div", null,
	                    mithril_1.default("h2", null,
	                        "Reminder #",
	                        i + 1,
	                        " ",
	                        mithril_1.default("button", { onclick: action.CollectionSessionStateRemove(i) }, "Remove Reminder")),
	                    mithril_1.default("div", { class: "form-item" },
	                        mithril_1.default("label", null, "Session Type"),
	                        mithril_1.default("select", { value: one.sessionType, oninput: function (e) { return one.sessionType = e.target.value; } }, sessionSetOptions.map(function (a) { return (mithril_1.default("option", { value: a.id }, a.name)); })),
	                        mithril_1.default("ol", null,
	                            mithril_1.default("strong", null, "How do sessions behave by default? (Different than set behavior above: a session is a user engaging with a set.)"),
	                            sessionSetOptions.map(function (a) { return (mithril_1.default("li", null,
	                                mithril_1.default("strong", null, a.name),
	                                " ",
	                                mithril_1.default("span", null, a.description))); }))),
	                    mithril_1.default("div", { class: "form-item" },
	                        mithril_1.default("label", null, "Reminder Mode"),
	                        mithril_1.default("select", { value: one.type, oninput: function (e) { return one.type = e.target.value; } }, sessionModeOptions.map(function (a, j) { return (mithril_1.default("option", { value: a.id },
	                            j + 1,
	                            " ",
	                            ' ',
	                            " ",
	                            a.name)); })),
	                        mithril_1.default("ol", null,
	                            mithril_1.default("strong", null, "Remind Mode allows for test or practice reminder."),
	                            sessionModeOptions.map(function (a) { return (mithril_1.default("li", null,
	                                mithril_1.default("strong", null, a.name),
	                                " ",
	                                mithril_1.default("span", null, a.description))); }))),
	                    mithril_1.default("div", { class: "form-item" },
	                        mithril_1.default("label", null, "Reminder Type"),
	                        mithril_1.default("select", { value: one.type, oninput: function (e) { return one.type = e.target.value; } }, sessionNextOptions.map(function (a, j) { return (mithril_1.default("option", { value: a.id },
	                            j + 1,
	                            " ",
	                            ' ',
	                            " ",
	                            a.name)); })),
	                        mithril_1.default("ol", null,
	                            mithril_1.default("strong", null, "Remind Type allows for reminders to behave differently per collection."),
	                            sessionNextOptions.map(function (a) { return (mithril_1.default("li", null,
	                                mithril_1.default("strong", null, a.name),
	                                " ",
	                                mithril_1.default("span", null, a.description))); }))),
	                    mithril_1.default("div", { class: "form-item" },
	                        mithril_1.default("label", null, "Practice Delay"),
	                        mithril_1.default("input", { value: one.intervalHour, oninput: function (e) { return one.intervalHour = e.target.value; } }),
	                        mithril_1.default("ul", null,
	                            mithril_1.default("strong", null,
	                                "Remind to practice this many hour(s) after ",
	                                state.collectionOne.setRemindList[i].type === 'after_last' ? 'last' : 'first',
	                                " session."),
	                            sessionIntervalOptions.map(function (one) { return (mithril_1.default("li", null,
	                                mithril_1.default("strong", null, one.name),
	                                " ",
	                                mithril_1.default("span", null, one.description))); }))),
	                    mithril_1.default("div", { class: "form-item" },
	                        mithril_1.default("label", null, "Test Delay"),
	                        mithril_1.default("input", { value: one.testIntervalHour, oninput: function (e) { return one.testIntervalHour = e.target.value; } }),
	                        mithril_1.default("ul", null,
	                            mithril_1.default("li", null,
	                                "Remind to test this many hour(s) after ",
	                                state.collectionOne.setRemindList[i].type === 'after_last' ? 'last' : 'first',
	                                " session."))),
	                    mithril_1.default("div", { class: "form-item" },
	                        mithril_1.default("label", null, "Repeat Count"),
	                        mithril_1.default("input", { value: one.repeatCount, oninput: function (e) { return one.repeatCount = e.target.value; } }),
	                        mithril_1.default("ul", null,
	                            mithril_1.default("li", null, "How many practices before the reminders stop. Zero for infinite."))),
	                    mithril_1.default("div", { class: "form-item" },
	                        mithril_1.default("label", null, "% Terminal Threshold"),
	                        mithril_1.default("input", { value: one.terminalPercent, oninput: function (e) { return one.terminalPercent = e.target.value; } }),
	                        mithril_1.default("ul", null,
	                            mithril_1.default("li", null, "What score (percent) for all words in a set above which reminders cease."))),
	                    mithril_1.default("hr", null))); }),
	                mithril_1.default("div", { class: "form-item" },
	                    mithril_1.default("button", { onclick: action.CollectionSessionStateAdd }, "Add Reminder"),
	                    mithril_1.default("ul", null,
	                        mithril_1.default("li", null,
	                            mithril_1.default("strong", null, "Add Reminder"),
	                            " ",
	                            mithril_1.default("span", null, "Add another reminder.")))),
	                mithril_1.default("hr", null),
	                mithril_1.default("div", { class: "form-item" },
	                    mithril_1.default("input", { type: "submit", value: "Save Set Options" }),
	                    mithril_1.default("ul", null,
	                        mithril_1.default("li", null,
	                            mithril_1.default("strong", null, "Set Reminders"),
	                            " ",
	                            mithril_1.default("span", null, "Only saves set reminders.")))))));
	    }
	};
	});

	var collectionCreate = createCommonjsModule(function (module, exports) {
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.CollectionCreate = void 0;
	var mithril_1 = __importDefault(mithril);

	exports.CollectionCreate = {
	    view: function (_a) {
	        var state = _a.attrs;
	        return (mithril_1.default("form", { onsubmit: action.CollectionCreateAction, class: "collection-create" },
	            mithril_1.default("h2", null,
	                "Create ",
	                state.languageOne.id,
	                " Collection"),
	            mithril_1.default("ul", null,
	                mithril_1.default("li", null,
	                    "Name  ",
	                    mithril_1.default("input", { onkeyup: function (e) { return state.collectionOne.name = e.target.value; }, value: state.collectionOne.name })),
	                mithril_1.default("li", null,
	                    "Description  ",
	                    mithril_1.default("input", { onkeyup: function (e) { return state.collectionOne.description = e.target.value; }, value: state.collectionOne.description })),
	                mithril_1.default("li", null,
	                    mithril_1.default("button", null, "Create")),
	                mithril_1.default("li", null, "Spacing  & Notifications (create once, cannot edit?)"))));
	    }
	};
	});

	var dash$1 = createCommonjsModule(function (module, exports) {
	var __createBinding = (commonjsGlobal && commonjsGlobal.__createBinding) || (Object.create ? (function(o, m, k, k2) {
	    if (k2 === undefined) k2 = k;
	    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
	}) : (function(o, m, k, k2) {
	    if (k2 === undefined) k2 = k;
	    o[k2] = m[k];
	}));
	var __exportStar = (commonjsGlobal && commonjsGlobal.__exportStar) || function(m, exports) {
	    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	__exportStar(dash, exports);
	__exportStar(language, exports);
	__exportStar(collection, exports);
	__exportStar(collectionEdit, exports);
	__exportStar(collectionSession, exports);
	__exportStar(collectionCreate, exports);
	});

	var deck = createCommonjsModule(function (module, exports) {
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.Deck = void 0;
	var mithril_1 = __importDefault(mithril);
	exports.Deck = {
	    view: function (_a) {
	        var state = _a.attrs;
	        return (mithril_1.default("div", { class: "component-deck" },
	            mithril_1.default("h2", null, "Deck!"),
	            mithril_1.default("ul", null,
	                mithril_1.default("li", null, "Nav/Position"),
	                mithril_1.default("li", null, "Card"),
	                mithril_1.default("ul", null,
	                    mithril_1.default("li", null, "image"),
	                    mithril_1.default("li", null, "audio"),
	                    mithril_1.default("li", null, "text")),
	                mithril_1.default("li", null, "Meta Card"),
	                mithril_1.default("ul", null,
	                    mithril_1.default("li", null, "image name and description"),
	                    mithril_1.default("li", null, "image credit/source"),
	                    mithril_1.default("li", null, "etc meta-data")),
	                mithril_1.default("li", null, "input/prompt"),
	                mithril_1.default("li", null, "keyboard"),
	                mithril_1.default("li", null, "control menu"))));
	    }
	};
	});

	var deck$1 = createCommonjsModule(function (module, exports) {
	var __createBinding = (commonjsGlobal && commonjsGlobal.__createBinding) || (Object.create ? (function(o, m, k, k2) {
	    if (k2 === undefined) k2 = k;
	    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
	}) : (function(o, m, k, k2) {
	    if (k2 === undefined) k2 = k;
	    o[k2] = m[k];
	}));
	var __exportStar = (commonjsGlobal && commonjsGlobal.__exportStar) || function(m, exports) {
	    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	__exportStar(deck, exports);
	});

	var app = createCommonjsModule(function (module, exports) {
	var __createBinding = (commonjsGlobal && commonjsGlobal.__createBinding) || (Object.create ? (function(o, m, k, k2) {
	    if (k2 === undefined) k2 = k;
	    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
	}) : (function(o, m, k, k2) {
	    if (k2 === undefined) k2 = k;
	    o[k2] = m[k];
	}));
	var __setModuleDefault = (commonjsGlobal && commonjsGlobal.__setModuleDefault) || (Object.create ? (function(o, v) {
	    Object.defineProperty(o, "default", { enumerable: true, value: v });
	}) : function(o, v) {
	    o["default"] = v;
	});
	var __importStar = (commonjsGlobal && commonjsGlobal.__importStar) || function (mod) {
	    if (mod && mod.__esModule) return mod;
	    var result = {};
	    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
	    __setModuleDefault(result, mod);
	    return result;
	};
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.App = void 0;
	var mithril_1 = __importDefault(mithril);
	var Dash = __importStar(dash$1);
	var Deck = __importStar(deck$1);
	exports.App = {
	    view: function (_a) {
	        var state = _a.attrs;
	        return (mithril_1.default("div", null,
	            mithril_1.default("h1", null, "EyeVocab App"),
	            state.component === "Dash" && mithril_1.default(Dash[state.display], state.Dash),
	            state.component === "Deck" && mithril_1.default(Deck[state.display], state.Deck)));
	    }
	};
	});

	var router$1 = createCommonjsModule(function (module, exports) {
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.Router = exports.Routes = void 0;
	var mithril_1 = __importDefault(mithril);


	// an empty component for handling route changes
	var RouterComponent = {
	    oncreate: function () { return exports.Router(mithril_1.default.route.param()); },
	    // don't think we need to check the route each view render
	    //view:()=>Router(m.route.param())
	    view: function () { }
	};
	// mithril handles these route paths
	exports.Routes = {
	    "/": RouterComponent,
	    "/:lang": RouterComponent,
	    "/:lang/:idCollection": RouterComponent,
	    "/:lang/:idCollection/:idSession": RouterComponent
	};
	// initiate the Lang var with a default value 
	// which is whatever is the first language in our list
	var Lang, IdCollection;
	/**
	 * Manages route changes.
	 */
	exports.Router = function (_a) {
	    var lang = _a.lang, idCollection = _a.idCollection, idSession = _a.idSession;
	    // if we are changing languages
	    if (Lang !== lang) {
	        // reset the route var
	        Lang = lang;
	        if (!lang) {
	            state.StateSet({ display: 'Dash' });
	            return;
	        }
	        else 
	        // if the lang code is NOT in the list of Languages
	        if (!keyboard.LangList.includes(lang)) {
	            // Send them back to start
	            window.location.href = "/app/#!/";
	            return;
	        }
	        // change the window location so the url is right.
	        window.location.href = "/app/#!/" + lang;
	        // get list of collections from the api
	        // set app state
	        state.StateSet({ display: 'Language', Dash: {
	                languageOne: {
	                    id: lang
	                }
	            } });
	    }
	    // if we are changing collections
	    if (idCollection === '_new') {
	        state.StateSet({ display: 'CollectionCreate' });
	    }
	    else if (idCollection !== IdCollection) {
	        IdCollection = idCollection;
	        console.log(idCollection);
	        var collectionOne = state.StateGet().Dash.collectionList.find(function (a) { return a.id === idCollection; });
	        state.StateSet({ display: 'Collection', Dash: { collectionOne: collectionOne } });
	        // get one collection from api
	    }
	};
	/**
	 * When the form input changes, handle input, event and route the change.
	 */
	exports.Router.fromInput = function (e) {
	    exports.Router(e.target.value);
	    e.target.blur();
	};
	/**
	 * When the window location hash changes, handle the url and route the change.
	 */
	exports.Router.fromUrl = function (url) {
	    console.log(url);
	    var path = url.split('#!/').pop().split('/');
	    exports.Router({
	        lang: path[0],
	        idCollection: path[1],
	        idSession: path[2]
	    });
	};
	});

	var client = createCommonjsModule(function (module, exports) {
	var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
	    return (mod && mod.__esModule) ? mod : { "default": mod };
	};
	Object.defineProperty(exports, "__esModule", { value: true });
	var mithril_1 = __importDefault(mithril);



	// when the url hash chagnes we fire the router's fromUrl static method
	window.onhashchange = function (e) { return router$1.Router.fromUrl(e.newURL); };
	// set our routes
	mithril_1.default.route(document.getElementById("m-route"), "/", router$1.Routes);
	// mount our app
	mithril_1.default.mount(document.getElementById("m-app"), {
	    view: function () { return mithril_1.default(app.App, state.StateGet()); }
	});
	});

	return client;

}());
//# sourceMappingURL=script.js.map

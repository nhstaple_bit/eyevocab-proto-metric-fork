FROM node:12.18.2-alpine

ENV TZ=America/Los_Angeles

RUN apk add -U tzdata \
  && cp /usr/share/zoneinfo/$TZ /etc/localtime

# Try to utilize docker caching by putting least changing objects first
COPY node_modules /app/node_modules
COPY package.json /app/package.json
COPY public /app/public
COPY build-api /app/build-api

WORKDIR /app

EXPOSE 3000

CMD ["npm", "start"]
